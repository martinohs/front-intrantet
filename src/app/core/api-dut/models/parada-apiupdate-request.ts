/* tslint:disable */
export interface ParadaAPIUpdateRequest {
  activa?: boolean;
  aprobada?: boolean;
  cityTown?: string;
  county?: string;
  displayName?: string;
  latitud?: number;
  localidad?: number;
  longitud?: number;
  nombre?: string;
  state?: string;
  stateDistrict?: string;
}
