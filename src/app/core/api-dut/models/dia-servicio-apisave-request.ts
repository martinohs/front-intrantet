/* tslint:disable */
export interface DiaServicioAPISaveRequest {
  activo?: boolean;
  categoriaServicioPlantaBaja?: number;
  categoriaServicioPrimerPiso?: number;
  dia?: number;
}
