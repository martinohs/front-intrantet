// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	entorno: 'LOCALE',
	//api_dut_url: 'https://dev-api.cnrt.gob.ar/dut',
	// api_dut_url: 'http://localhost:8000',
	//api_seop_url: 'https://dev-api.cnrt.gob.ar/seop/doc',
	//georef_api_url: 'https://dev-api.cnrt.gob.ar/georef/api/v1',
	log_level: 5,
	googleAnalyticsKey: false,
	redirect_uri: 'http://localhost:4200',
	current_idp: 'hydra',
	//authConfig: {
		//issuer: 'https://dev-api.cnrt.gob.ar/oidc/',
		//clientId: 'local',
		//customUserinfoEndpoint: 'https://dev-api.cnrt.gob.ar/sia/api/v1/usuarios/actual',
	//},
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
