/* tslint:disable */
import { DetalleModalidadTraficoAPI } from './detalle-modalidad-trafico-api';
import { EstadoTramiteAPI2 } from './estado-tramite-api2';
import { ServicioAPISinCMT } from './servicio-apisin-cmt';
import { TramiteAPI } from './tramite-api';
export interface ServicioModalidadTraficoAPI2 {
  createdAt?: any;
  createdBy?: string;
  detalle?: Array<DetalleModalidadTraficoAPI>;
  estadoTramite?: EstadoTramiteAPI2;
  id?: number;
  servicio?: ServicioAPISinCMT;
  tramite?: TramiteAPI;
  vigenciaDesde?: any;
  vigenciaHasta?: any;
}
