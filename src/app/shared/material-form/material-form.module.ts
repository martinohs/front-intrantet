import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
// material
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule, MAT_RADIO_DEFAULT_OPTIONS } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import {MatListModule} from '@angular/material/list';


// config
import { AppDateAdapter, APP_DATE_FORMATS } from './formats';

@NgModule({
	declarations: [],
	imports: [
		MatAutocompleteModule,
		MatCheckboxModule,
		MatDatepickerModule,
		MatFormFieldModule,
		MatInputModule,
		MatRadioModule,
		MatSelectModule,
		MatListModule,
	],
	exports: [
		MatAutocompleteModule,
		MatCheckboxModule,
		MatDatepickerModule,
		MatFormFieldModule,
		MatInputModule,
		MatRadioModule,
		MatSelectModule,
		MatListModule,
	],
	providers: [],
})
export class MaterialFormModule {
	constructor(@Optional() @SkipSelf() parentModule: MaterialFormModule) {
		if (parentModule) {
			throw new Error('MaterialFormModule ya fue importado');
		}
	}
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: MaterialFormModule,
			providers: [
				{
					provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
					useValue: { appearance: 'outline', hideRequiredMarker: false },
				},
				{ provide: MAT_DATE_LOCALE, useValue: 'es-Ar' },
				{ provide: DateAdapter, useClass: AppDateAdapter },
				{ provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS },
				{
					provide: MAT_RADIO_DEFAULT_OPTIONS,
					useValue: { color: 'accent' },
				},
			],
		};
	}
}
