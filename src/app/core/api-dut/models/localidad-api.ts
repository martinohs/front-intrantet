/* tslint:disable */
import { ProvinciaAPI } from './provincia-api';
export interface LocalidadAPI {
  activo?: boolean;
  codigoINDEC?: string;
  createdAt?: any;
  departamento?: number;
  descripcion?: string;
  fuente?: string;
  id?: number;
  lat?: number;
  lon?: number;
  parada?: any;
  provincia?: ProvinciaAPI;
  updatedAt?: any;
}
