/* tslint:disable */
import { ModalidadTraficoAPI } from './modalidad-trafico-api';
export interface RecorridoFrecuenciaAPI {
  frecuencia?: string;
  id?: number;
  modalidadTrafico?: ModalidadTraficoAPI;
  periodo?: string;
  recorridoId?: number;
  temporada?: string;
  vigenciaDesde?: any;
  vigenciaHasta?: any;
}
