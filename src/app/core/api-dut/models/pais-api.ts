/* tslint:disable */
export interface PaisAPI {
  abrev?: string;
  activo?: boolean;
  codigo?: string;
  descripcion?: string;
  id?: number;
}
