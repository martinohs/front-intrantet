import { CNRTAuthConfig } from './cnrt.auth.config';
import { environment } from '@env/environment';

export const authConfig: Array<CNRTAuthConfig> = [
	// CNRT SIA: https://api.cnrt.gob.ar/oidc/.well-known/openid-configuration
	{
		id: 'hydra',
		name: 'cnrt-hydra',
		config: {
			issuer: environment.authConfig.issuer,
			responseType: 'code',
			redirectUri: environment.redirect_uri,
			logoutUrl: environment.authConfig.issuer + 'oauth2/auth/sessions/login/revoke',
			clientId: environment.authConfig.clientId,
			scope: 'openid offline',
			strictDiscoveryDocumentValidation: false,
			disableAtHashCheck: true,
			customUserinfoEndpoint: environment.authConfig.customUserinfoEndpoint,
		},
	},
];
