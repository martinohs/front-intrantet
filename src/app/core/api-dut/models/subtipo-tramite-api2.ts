/* tslint:disable */
import { TipoTramiteAPI } from './tipo-tramite-api';
export interface SubtipoTramiteAPI2 {
  abrev?: string;
  activo?: boolean;
  descripcion?: string;
  id?: number;
  tipoTramite?: TipoTramiteAPI;
}
