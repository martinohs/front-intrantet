import { Component, OnInit } from '@angular/core';
import { CoreFacade } from '@core/facades/core.facade';
import { AuthService } from '@auth/services/auth.service';


@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  constructor(private coreFacade: CoreFacade,
    private authService: AuthService,
    // private postFacade : PostFacade
    ) {}

	ngOnInit(): void {}

	get user$() {
		return this.coreFacade.user$;
	}

	logout() {
	this.authService.logout();
	}

	goToLogin() {
		this.authService.login();
   }
}
