/* tslint:disable */
import { TipoDocumentoSeopAPI } from './tipo-documento-seop-api';
export interface EmpresaAPI {
  cuitEmpresaAnterior?: string;
  duracion?: number;
  email?: string;
  esPersonaFisica?: boolean;
  estadoEmpresa?: number;
  fechaInscripcionCNRT?: any;
  fechaInscripcionRegistroNacionalOl?: any;
  fechaVencimientoRegistroNacionalOl?: any;
  grupoEmpresario?: string;
  id?: number;
  lineasUrbanas?: Array<string>;
  matriculaComerciante?: boolean;
  nombreFantasia?: string;
  nroDocumento?: string;
  observacion?: string;
  paisOrigen?: number;
  paut?: string;
  razonSocial?: string;
  referenciaMercosur?: string;
  textoHash?: string;
  tipoDocumento?: TipoDocumentoSeopAPI;
  tipoSociedad?: number;
  vigenciaDesde?: any;
  vigenciaHasta?: any;
}
