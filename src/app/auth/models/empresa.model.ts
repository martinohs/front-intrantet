export class Empresa {
	id: number;
	idEmpresa: number;
	razonSocial: string;
	cuit: string;

	constructor(data) {
		if (data) {
			this.id = data.id;
			this.idEmpresa = data.idEmpresa;
			this.razonSocial = data.razonSocial;
			this.cuit = data.cuit;
		}
	}
}
