import { NgModule } from '@angular/core';
import { ModuloEjemploRoutingModule } from './modulo-ejemplo-routing.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BackendInterceptor } from '@auth/services/backend.interceptor';
import { SharedModule } from '@shared/shared.module';

//material
import { MaterialTableModule } from '@shared/material-table/material-table.module';
import { MatChipsModule } from '@angular/material/chips';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule, MAT_TOOLTIP_DEFAULT_OPTIONS } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { MatStepperModule } from '@angular/material/stepper';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTreeModule } from '@angular/material/tree';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSortModule } from '@angular/material/sort';

//components
import { ModuloEjemploComponent, EmpresaComponent, EmpresaDetalleComponent } from './index';

import { MaterialFormModule } from '@shared/material-form/material-form.module';
import { VariosModule } from '@shared/varios/varios.module';

@NgModule({
	declarations: [ModuloEjemploComponent, EmpresaComponent, EmpresaDetalleComponent],
	imports: [
		ModuloEjemploRoutingModule,
		SharedModule,
		VariosModule,
		MaterialTableModule.forRoot(),
		MatCardModule,
		MatButtonModule,
		MatTooltipModule,
		MatStepperModule,
		MatIconModule,
		MatChipsModule,
		MatDialogModule,
		MatExpansionModule,
		MatListModule,
		MaterialFormModule.forRoot(),
		MatSortModule,
	],
	entryComponents: [],
	exports: [],
	providers: [
		{ provide: MAT_TOOLTIP_DEFAULT_OPTIONS, useValue: { position: 'above' } },
		{ provide: HTTP_INTERCEPTORS, useClass: BackendInterceptor, multi: true },
	],
})
export class ModuloEjemploModule {}
