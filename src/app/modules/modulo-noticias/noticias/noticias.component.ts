import { Component, NgModule, OnInit } from '@angular/core';
import { CoreFacade } from '@core/facades/core.facade';
import { Post } from '@core/models/post.interface';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.scss']
})
export class  NoticiasComponent implements OnInit {

  lisPost: Post[];



  constructor(
    private coreFacade:CoreFacade,
    private rutaActiva: ActivatedRoute
    ) { }

  pageNumber:number;

  ngOnInit(): void {
   this.getPosteos();

   this.pageNumber = this.rutaActiva.snapshot.params.page;
  }

  getPosteos() {
    this.coreFacade.obtenerTodasLasNoticias().subscribe((noticias) => this.lisPost = noticias);
    return this.lisPost;
     }

  limitarCaracteres(unTexto: String){
    if (unTexto.length > 450 ){
      unTexto = unTexto.substr(0,450)+" ...";
    }
      return unTexto
    }

  eliminarFormato(texto){
    var tmp = document.createElement('div');
      tmp.innerHTML = texto;

    return tmp.textContent || tmp.innerText;
    }

  }
