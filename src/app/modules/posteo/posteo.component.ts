import { Component, Input, OnInit } from '@angular/core';
import { CoreFacade } from '@core/facades/core.facade';
import { Post } from '@core/models/post.interface';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-posteo',
  templateUrl: './posteo.component.html',
  styleUrls: ['./posteo.component.scss'],
})
export class PosteoComponent implements OnInit {

  // tESTPOST = {
  //   id: null,
  //   title: "nocontent",
  //   isImportant: null,
  //   likes: null,
  //   content: null,
  //   image: null,
  //   creator: null,

  // } as  Post;

unPost: Post;
d;

  constructor(
    private coreFacade:CoreFacade,
    private rutaActiva: ActivatedRoute,

  ) {
    // Obtener el post usando Resolve
    // this.unPost = this.rutaActiva.snapshot.data.post;
  }


  // ME DEVUELVE LA ID DE LA RUTA LA CUAL SERVIRA PARA OBTENER EL POST
  id = this.rutaActiva.snapshot.params.id;

  ngOnInit(): void {

     this.getPost();

  }

  //DE ESTA FORMA UTILIZO SUBSCRIPCION A LA FUNCION TEST2 (TEST2 HACE LO MISMO QUE obtenerPost pero devuelve un observable) Y CARGO MI POST
  getPost(): void{
    this.coreFacade.obtenerPost(this.id).subscribe((data) => {this.unPost = data});
  }





}
