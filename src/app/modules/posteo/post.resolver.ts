import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router'
import { CoreFacade } from '@core/facades/core.facade';
import { Post } from '@core/models/post.interface';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class PostResolver implements Resolve<Observable<Post>>{

  constructor (
    private coreFacade: CoreFacade
  ){}

    resolve(route: ActivatedRouteSnapshot){
      // return this.coreFacade.test2(route.paramMap.get('id'));
      return this.coreFacade.obtenerPost(2).pipe(
        // delay (2000)
      );
    }



}
