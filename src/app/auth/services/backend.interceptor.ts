import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { CoreFacade } from '@core/facades/core.facade';
import { finalize, delay } from 'rxjs/operators';

@Injectable()
export class BackendInterceptor implements HttpInterceptor {
	constructor(protected _authService: AuthService, private coreFacade: CoreFacade) {}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		this.coreFacade.cargando = true;
		// Cargamos el header authorization con el token si es una llamada a API SEOP y el token es vigente
		if (this._authService.isAuthenticated()) {
			req = req.clone({
				headers: req.headers.set('Authorization', 'Bearer ' + this._authService.getIdToken()),
			});
		}

		return next.handle(req).pipe(finalize(() => (delay(200), (this.coreFacade.cargando = false))));
	}
}
