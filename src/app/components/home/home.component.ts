import { Component, OnInit } from '@angular/core';
import { CoreFacade } from '@core/facades/core.facade';
import { AuthService } from '@auth/services/auth.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
	constructor(private coreFacade: CoreFacade, private authService: AuthService) {}

	ngOnInit(): void {}

	get user$() {
		return this.coreFacade.user$;
	}
}
