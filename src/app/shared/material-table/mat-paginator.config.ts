import { MatPaginatorIntl } from '@angular/material/paginator';
export class MatPaginatorIntlCNRT extends MatPaginatorIntl {
	itemsPerPageLabel = 'Ver por Página';
	nextPageLabel = 'Página Siguiente';
	previousPageLabel = 'Página Anterior';
	firstPageLabel = 'Primera Página';
	lastPageLabel = 'Última Página';
	getRangeLabel = function (page, pageSize, length) {
		if (length === 0 || pageSize === 0) {
			return '0 de ' + length;
		}
		length = Math.max(length, 0);
		const startIndex = page * pageSize;

		const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;

		return (
			'Mostrando: ' +
			(startIndex + 1) +
			' - ' +
			endIndex +
			' de ' +
			length +
			' resultados.' +
			'\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0' +
			'Página  - ' +
			(page + 1) +
			' de ' +
			Math.ceil(length / pageSize)
		);
	};
}
