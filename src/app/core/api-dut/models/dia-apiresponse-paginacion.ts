/* tslint:disable */
import { ResponseMetadata } from './response-metadata';
import { DiaAPI } from './dia-api';
export interface DiaAPIResponsePaginacion {
  actions?: string;
  data?: {metadata?: ResponseMetadata, results?: Array<DiaAPI>};
  result?: string;
  status?: number;
  userMessage?: string;
}
