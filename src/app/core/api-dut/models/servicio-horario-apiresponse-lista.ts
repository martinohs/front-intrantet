/* tslint:disable */
import { ServicioHorarioAPI } from './servicio-horario-api';
export interface ServicioHorarioAPIResponseLista {
  actions?: string;
  data?: Array<ServicioHorarioAPI>;
  result?: string;
  status?: number;
  userMessage?: string;
}
