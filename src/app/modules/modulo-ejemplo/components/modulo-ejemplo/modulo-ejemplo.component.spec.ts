import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuloEjemploComponent } from './modulo-ejemplo.component';

describe('ModuloEjemploComponent', () => {
  let component: ModuloEjemploComponent;
  let fixture: ComponentFixture<ModuloEjemploComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuloEjemploComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuloEjemploComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
