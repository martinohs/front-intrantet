/* tslint:disable */
import { RecorridoFrecuenciaAPI } from './recorrido-frecuencia-api';
export interface RecorridoAPI {
  aplicaGasoil?: boolean;
  cantidadConductores?: number;
  cuadroTarifarioPath?: string;
  destino?: string;
  frecuencias?: Array<RecorridoFrecuenciaAPI>;
  id?: number;
  itinerarioIda?: string;
  itinerarioVuelta?: string;
  ncl?: string;
  nroAmparoJudicial?: string;
  observacion?: string;
  origen?: string;
  recorrido?: string;
  recorridoGis?: number;
  sitioDestino?: string;
  sitioOrigen?: string;
  transito?: string;
  vigenciaDesde?: any;
  vigenciaHasta?: any;
  vinculacionCaminera?: string;
}
