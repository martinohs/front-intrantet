/* tslint:disable */
import { Injectable } from '@angular/core';

/**
 * Global configuration for ApiDut services
 */
@Injectable({
  providedIn: 'root',
})
export class ApiDutConfiguration {
  rootUrl: string = 'https://dev-api.cnrt.gob.ar/dut';
}

export interface ApiDutConfigurationInterface {
  rootUrl?: string;
}
