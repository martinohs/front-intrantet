/* tslint:disable */
import { ResponseMetadata } from './response-metadata';
import { DiaServicioAPI } from './dia-servicio-api';
export interface DiaServicioAPIResponsePaginacion {
  actions?: string;
  data?: {metadata?: ResponseMetadata, results?: Array<DiaServicioAPI>};
  result?: string;
  status?: number;
  userMessage?: string;
}
