/* tslint:disable */
import { DiaServicioAPI } from './dia-servicio-api';
import { ServicioHorarioAPI } from './servicio-horario-api';
import { OperadorAPI } from './operador-api';
import { RecorridoAPI } from './recorrido-api';
import { RecorridoSentidoAPI } from './recorrido-sentido-api';
import { TramiteAPI } from './tramite-api';
export interface ServicioAPISinCMT {
  aprobado?: boolean;
  dias?: Array<DiaServicioAPI>;
  horaLlegada?: any;
  horaSalida?: any;
  horarios?: Array<ServicioHorarioAPI>;
  id?: number;
  operador?: OperadorAPI;
  recorrido?: RecorridoAPI;
  recorridoSentido?: RecorridoSentidoAPI;
  servicioReemplazado?: number;
  tramite?: TramiteAPI;
  velocidadPromedioTotal?: number;
  vigenciaDesde?: any;
  vigenciaHasta?: any;
}
