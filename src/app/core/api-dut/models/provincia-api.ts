/* tslint:disable */
import { PaisAPI } from './pais-api';
export interface ProvinciaAPI {
  activo?: boolean;
  codigo?: string;
  codigoINDEC?: string;
  descripcion?: string;
  id?: number;
  pais?: PaisAPI;
}
