/* tslint:disable */
export interface ModalidadTraficoAPI {
  abrev?: string;
  activo?: boolean;
  descripcion?: string;
  id?: number;
}
