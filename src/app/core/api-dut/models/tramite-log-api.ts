/* tslint:disable */
import { EstadoTramiteAPI2 } from './estado-tramite-api2';
export interface TramiteLogAPI {
  estadoTramite?: EstadoTramiteAPI2;
  fechaMovimiento?: any;
  id?: number;
  observaciones?: string;
  tramiteId?: number;
  usuario?: string;
}
