/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiDutConfiguration as __Configuration } from '../api-dut-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { DiaServicioAPIResponsePaginacion } from '../models/dia-servicio-apiresponse-paginacion';
import { DiaServicioAPIResponseData } from '../models/dia-servicio-apiresponse-data';
@Injectable({
  providedIn: 'root',
})
class DiaServicioService extends __BaseService {
  static readonly getApiV2DiasServiciosPath = '/api/v2/diasServicios';
  static readonly findDiaServicioPath = '/api/v2/diasServicios/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Obtiene una lista de Días de Servicio paginadas
   * @param params The `DiaServicioService.GetApiV2DiasServiciosParams` containing the following parameters:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `servicio-vigenciaHasta[ne]`:
   *
   * - `servicio-vigenciaHasta[lt]`:
   *
   * - `servicio-vigenciaHasta[le]`:
   *
   * - `servicio-vigenciaHasta[gt]`:
   *
   * - `servicio-vigenciaHasta[ge]`:
   *
   * - `servicio-vigenciaHasta`:
   *
   * - `servicio-vigenciaDesde[ne]`:
   *
   * - `servicio-vigenciaDesde[lt]`:
   *
   * - `servicio-vigenciaDesde[le]`:
   *
   * - `servicio-vigenciaDesde[gt]`:
   *
   * - `servicio-vigenciaDesde[ge]`:
   *
   * - `servicio-vigenciaDesde`:
   *
   * - `servicio-velocidadPromedioTotal[ne]`:
   *
   * - `servicio-velocidadPromedioTotal[lt]`:
   *
   * - `servicio-velocidadPromedioTotal[le]`:
   *
   * - `servicio-velocidadPromedioTotal[gt]`:
   *
   * - `servicio-velocidadPromedioTotal[ge]`:
   *
   * - `servicio-velocidadPromedioTotal`:
   *
   * - `servicio-tramite-usuario[ne]`:
   *
   * - `servicio-tramite-usuario[like]`:
   *
   * - `servicio-tramite-usuario`:
   *
   * - `servicio-tramite-tipoTramite-id[ne]`:
   *
   * - `servicio-tramite-tipoTramite-id[lt]`:
   *
   * - `servicio-tramite-tipoTramite-id[le]`:
   *
   * - `servicio-tramite-tipoTramite-id[gt]`:
   *
   * - `servicio-tramite-tipoTramite-id[ge]`:
   *
   * - `servicio-tramite-tipoTramite-id`:
   *
   * - `servicio-tramite-tipoTramite-descripcion[ne]`:
   *
   * - `servicio-tramite-tipoTramite-descripcion[like]`:
   *
   * - `servicio-tramite-tipoTramite-descripcion`:
   *
   * - `servicio-tramite-tipoTramite-activo`:
   *
   * - `servicio-tramite-subtipoTramite-id[ne]`:
   *
   * - `servicio-tramite-subtipoTramite-id[lt]`:
   *
   * - `servicio-tramite-subtipoTramite-id[le]`:
   *
   * - `servicio-tramite-subtipoTramite-id[gt]`:
   *
   * - `servicio-tramite-subtipoTramite-id[ge]`:
   *
   * - `servicio-tramite-subtipoTramite-id`:
   *
   * - `servicio-tramite-subtipoTramite-descripcion[ne]`:
   *
   * - `servicio-tramite-subtipoTramite-descripcion[like]`:
   *
   * - `servicio-tramite-subtipoTramite-descripcion`:
   *
   * - `servicio-tramite-subtipoTramite-activo`:
   *
   * - `servicio-tramite-nroBoletaPago[ne]`:
   *
   * - `servicio-tramite-nroBoletaPago[lt]`:
   *
   * - `servicio-tramite-nroBoletaPago[le]`:
   *
   * - `servicio-tramite-nroBoletaPago[gt]`:
   *
   * - `servicio-tramite-nroBoletaPago[ge]`:
   *
   * - `servicio-tramite-nroBoletaPago`:
   *
   * - `servicio-tramite-id[ne]`:
   *
   * - `servicio-tramite-id[lt]`:
   *
   * - `servicio-tramite-id[le]`:
   *
   * - `servicio-tramite-id[gt]`:
   *
   * - `servicio-tramite-id[ge]`:
   *
   * - `servicio-tramite-id`:
   *
   * - `servicio-tramite-estadoTramite-id[ne]`:
   *
   * - `servicio-tramite-estadoTramite-id[lt]`:
   *
   * - `servicio-tramite-estadoTramite-id[le]`:
   *
   * - `servicio-tramite-estadoTramite-id[gt]`:
   *
   * - `servicio-tramite-estadoTramite-id[ge]`:
   *
   * - `servicio-tramite-estadoTramite-id`:
   *
   * - `servicio-tramite-estadoTramite-descripcion[ne]`:
   *
   * - `servicio-tramite-estadoTramite-descripcion[like]`:
   *
   * - `servicio-tramite-estadoTramite-descripcion`:
   *
   * - `servicio-tramite-estadoTramite-activo`:
   *
   * - `servicio-tramite-estadoTramite-abrev[ne]`:
   *
   * - `servicio-tramite-estadoTramite-abrev[like]`:
   *
   * - `servicio-tramite-estadoTramite-abrev`:
   *
   * - `servicio-tramite-empresa-vigenciaHasta[ne]`:
   *
   * - `servicio-tramite-empresa-vigenciaHasta[lt]`:
   *
   * - `servicio-tramite-empresa-vigenciaHasta[le]`:
   *
   * - `servicio-tramite-empresa-vigenciaHasta[gt]`:
   *
   * - `servicio-tramite-empresa-vigenciaHasta[ge]`:
   *
   * - `servicio-tramite-empresa-vigenciaHasta`:
   *
   * - `servicio-tramite-empresa-vigenciaDesde[ne]`:
   *
   * - `servicio-tramite-empresa-vigenciaDesde[lt]`:
   *
   * - `servicio-tramite-empresa-vigenciaDesde[le]`:
   *
   * - `servicio-tramite-empresa-vigenciaDesde[gt]`:
   *
   * - `servicio-tramite-empresa-vigenciaDesde[ge]`:
   *
   * - `servicio-tramite-empresa-vigenciaDesde`:
   *
   * - `servicio-tramite-empresa-tipoSociedad[ne]`:
   *
   * - `servicio-tramite-empresa-tipoSociedad[lt]`:
   *
   * - `servicio-tramite-empresa-tipoSociedad[le]`:
   *
   * - `servicio-tramite-empresa-tipoSociedad[gt]`:
   *
   * - `servicio-tramite-empresa-tipoSociedad[ge]`:
   *
   * - `servicio-tramite-empresa-tipoSociedad`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-id[ne]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-id[lt]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-id[le]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-id[gt]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-id[ge]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-id`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-descripcion[ne]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-descripcion[like]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-descripcion`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-abrev[ne]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-abrev[like]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-abrev`:
   *
   * - `servicio-tramite-empresa-textoHash`:
   *
   * - `servicio-tramite-empresa-razonSocial[ne]`:
   *
   * - `servicio-tramite-empresa-razonSocial[like]`:
   *
   * - `servicio-tramite-empresa-razonSocial`:
   *
   * - `servicio-tramite-empresa-paut[ne]`:
   *
   * - `servicio-tramite-empresa-paut[like]`:
   *
   * - `servicio-tramite-empresa-paut`:
   *
   * - `servicio-tramite-empresa-observacion[ne]`:
   *
   * - `servicio-tramite-empresa-observacion[like]`:
   *
   * - `servicio-tramite-empresa-observacion`:
   *
   * - `servicio-tramite-empresa-nroDocumento[ne]`:
   *
   * - `servicio-tramite-empresa-nroDocumento[like]`:
   *
   * - `servicio-tramite-empresa-nroDocumento`:
   *
   * - `servicio-tramite-empresa-nombreFantasia[ne]`:
   *
   * - `servicio-tramite-empresa-nombreFantasia[like]`:
   *
   * - `servicio-tramite-empresa-nombreFantasia`:
   *
   * - `servicio-tramite-empresa-id[ne]`:
   *
   * - `servicio-tramite-empresa-id[lt]`:
   *
   * - `servicio-tramite-empresa-id[le]`:
   *
   * - `servicio-tramite-empresa-id[gt]`:
   *
   * - `servicio-tramite-empresa-id[ge]`:
   *
   * - `servicio-tramite-empresa-id`:
   *
   * - `servicio-tramite-empresa-estadoEmpresa[ne]`:
   *
   * - `servicio-tramite-empresa-estadoEmpresa[lt]`:
   *
   * - `servicio-tramite-empresa-estadoEmpresa[le]`:
   *
   * - `servicio-tramite-empresa-estadoEmpresa[gt]`:
   *
   * - `servicio-tramite-empresa-estadoEmpresa[ge]`:
   *
   * - `servicio-tramite-empresa-estadoEmpresa`:
   *
   * - `servicio-tramite-empresa-esPersonaFisica`:
   *
   * - `servicio-tramite-empresa-email[ne]`:
   *
   * - `servicio-tramite-empresa-email[like]`:
   *
   * - `servicio-tramite-empresa-email`:
   *
   * - `servicio-tramite-empresa-cuitEmpresaAnterior[ne]`:
   *
   * - `servicio-tramite-empresa-cuitEmpresaAnterior[like]`:
   *
   * - `servicio-tramite-empresa-cuitEmpresaAnterior`:
   *
   * - `servicio-recorridoSentido-vinculacionCaminera[ne]`:
   *
   * - `servicio-recorridoSentido-vinculacionCaminera[like]`:
   *
   * - `servicio-recorridoSentido-vinculacionCaminera`:
   *
   * - `servicio-recorridoSentido-vigenciaHasta[ne]`:
   *
   * - `servicio-recorridoSentido-vigenciaHasta[lt]`:
   *
   * - `servicio-recorridoSentido-vigenciaHasta[le]`:
   *
   * - `servicio-recorridoSentido-vigenciaHasta[gt]`:
   *
   * - `servicio-recorridoSentido-vigenciaHasta[ge]`:
   *
   * - `servicio-recorridoSentido-vigenciaHasta`:
   *
   * - `servicio-recorridoSentido-vigenciaDesde[ne]`:
   *
   * - `servicio-recorridoSentido-vigenciaDesde[lt]`:
   *
   * - `servicio-recorridoSentido-vigenciaDesde[le]`:
   *
   * - `servicio-recorridoSentido-vigenciaDesde[gt]`:
   *
   * - `servicio-recorridoSentido-vigenciaDesde[ge]`:
   *
   * - `servicio-recorridoSentido-vigenciaDesde`:
   *
   * - `servicio-recorridoSentido-sentido-id[ne]`:
   *
   * - `servicio-recorridoSentido-sentido-id[lt]`:
   *
   * - `servicio-recorridoSentido-sentido-id[le]`:
   *
   * - `servicio-recorridoSentido-sentido-id[gt]`:
   *
   * - `servicio-recorridoSentido-sentido-id[ge]`:
   *
   * - `servicio-recorridoSentido-sentido-id`:
   *
   * - `servicio-recorridoSentido-sentido-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-sentido-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-sentido-descripcion`:
   *
   * - `servicio-recorridoSentido-sentido-activo`:
   *
   * - `servicio-recorridoSentido-sentido-abrev[ne]`:
   *
   * - `servicio-recorridoSentido-sentido-abrev[like]`:
   *
   * - `servicio-recorridoSentido-sentido-abrev`:
   *
   * - `servicio-recorridoSentido-recorrido-vinculacionCaminera[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-vinculacionCaminera[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-vinculacionCaminera`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaHasta[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaHasta[lt]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaHasta[le]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaHasta[gt]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaHasta[ge]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaHasta`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaDesde[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaDesde[lt]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaDesde[le]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaDesde[gt]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaDesde[ge]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaDesde`:
   *
   * - `servicio-recorridoSentido-recorrido-transito[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-transito[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-transito`:
   *
   * - `servicio-recorridoSentido-recorrido-sitioOrigen[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-sitioOrigen[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-sitioOrigen`:
   *
   * - `servicio-recorridoSentido-recorrido-sitioDestino[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-sitioDestino[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-sitioDestino`:
   *
   * - `servicio-recorridoSentido-recorrido-recorrido[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-recorrido[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-recorrido`:
   *
   * - `servicio-recorridoSentido-recorrido-origen[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-origen[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-origen`:
   *
   * - `servicio-recorridoSentido-recorrido-itinerarioVuelta[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-itinerarioVuelta[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-itinerarioVuelta`:
   *
   * - `servicio-recorridoSentido-recorrido-itinerarioIda[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-itinerarioIda[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-itinerarioIda`:
   *
   * - `servicio-recorridoSentido-recorrido-id[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-id[lt]`:
   *
   * - `servicio-recorridoSentido-recorrido-id[le]`:
   *
   * - `servicio-recorridoSentido-recorrido-id[gt]`:
   *
   * - `servicio-recorridoSentido-recorrido-id[ge]`:
   *
   * - `servicio-recorridoSentido-recorrido-id`:
   *
   * - `servicio-recorridoSentido-recorrido-destino[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-destino[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-destino`:
   *
   * - `servicio-recorridoSentido-recorrido-aplicaGasoil`:
   *
   * - `servicio-recorridoSentido-origen[ne]`:
   *
   * - `servicio-recorridoSentido-origen[like]`:
   *
   * - `servicio-recorridoSentido-origen`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-updatedAt[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-updatedAt[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-updatedAt[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-updatedAt[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-updatedAt[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-updatedAt`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-id[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-id[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-id[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-id[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-id[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-id`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-descripcion`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-codigo[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-codigo[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-codigo`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-activo`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-abrev[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-abrev[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-abrev`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-id[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-id[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-id[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-id[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-id[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-id`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-descripcion`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-codigo[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-codigo[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-codigoINDEC[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-codigoINDEC[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-codigoINDEC`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-codigo`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-activo`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-parada`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lon[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lon[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lon[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lon[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lon[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lon`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lat[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lat[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lat[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lat[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lat[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lat`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-id[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-id[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-id[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-id[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-id[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-id`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-fuente[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-fuente[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-fuente`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-descripcion`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-createdAt[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-createdAt[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-createdAt[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-createdAt[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-createdAt[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-createdAt`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-codigoINDEC[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-codigoINDEC[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-codigoINDEC`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-activo`:
   *
   * - `servicio-recorridoSentido-localidadDestino-updatedAt[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-updatedAt[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-updatedAt[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-updatedAt[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-updatedAt[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-updatedAt`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-id[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-id[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-id[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-id[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-id[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-id`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-descripcion`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-codigo[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-codigo[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-codigo`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-activo`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-abrev[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-abrev[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-abrev`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-id[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-id[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-id[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-id[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-id[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-id`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-descripcion`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-codigo[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-codigo[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-codigoINDEC[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-codigoINDEC[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-codigoINDEC`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-codigo`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-activo`:
   *
   * - `servicio-recorridoSentido-localidadDestino-parada`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lon[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lon[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lon[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lon[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lon[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lon`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lat[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lat[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lat[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lat[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lat[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lat`:
   *
   * - `servicio-recorridoSentido-localidadDestino-id[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-id[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-id[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-id[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-id[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-id`:
   *
   * - `servicio-recorridoSentido-localidadDestino-fuente[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-fuente[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-fuente`:
   *
   * - `servicio-recorridoSentido-localidadDestino-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-descripcion`:
   *
   * - `servicio-recorridoSentido-localidadDestino-createdAt[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-createdAt[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-createdAt[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-createdAt[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-createdAt[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-createdAt`:
   *
   * - `servicio-recorridoSentido-localidadDestino-codigoINDEC[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-codigoINDEC[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-codigoINDEC`:
   *
   * - `servicio-recorridoSentido-localidadDestino-activo`:
   *
   * - `servicio-recorridoSentido-itinerario[ne]`:
   *
   * - `servicio-recorridoSentido-itinerario[like]`:
   *
   * - `servicio-recorridoSentido-itinerario`:
   *
   * - `servicio-recorridoSentido-id[ne]`:
   *
   * - `servicio-recorridoSentido-id[lt]`:
   *
   * - `servicio-recorridoSentido-id[le]`:
   *
   * - `servicio-recorridoSentido-id[gt]`:
   *
   * - `servicio-recorridoSentido-id[ge]`:
   *
   * - `servicio-recorridoSentido-id`:
   *
   * - `servicio-recorridoSentido-destino[ne]`:
   *
   * - `servicio-recorridoSentido-destino[like]`:
   *
   * - `servicio-recorridoSentido-destino`:
   *
   * - `servicio-recorrido-vinculacionCaminera[ne]`:
   *
   * - `servicio-recorrido-vinculacionCaminera[like]`:
   *
   * - `servicio-recorrido-vinculacionCaminera`:
   *
   * - `servicio-recorrido-vigenciaHasta[ne]`:
   *
   * - `servicio-recorrido-vigenciaHasta[lt]`:
   *
   * - `servicio-recorrido-vigenciaHasta[le]`:
   *
   * - `servicio-recorrido-vigenciaHasta[gt]`:
   *
   * - `servicio-recorrido-vigenciaHasta[ge]`:
   *
   * - `servicio-recorrido-vigenciaHasta`:
   *
   * - `servicio-recorrido-vigenciaDesde[ne]`:
   *
   * - `servicio-recorrido-vigenciaDesde[lt]`:
   *
   * - `servicio-recorrido-vigenciaDesde[le]`:
   *
   * - `servicio-recorrido-vigenciaDesde[gt]`:
   *
   * - `servicio-recorrido-vigenciaDesde[ge]`:
   *
   * - `servicio-recorrido-vigenciaDesde`:
   *
   * - `servicio-recorrido-transito[ne]`:
   *
   * - `servicio-recorrido-transito[like]`:
   *
   * - `servicio-recorrido-transito`:
   *
   * - `servicio-recorrido-sitioOrigen[ne]`:
   *
   * - `servicio-recorrido-sitioOrigen[like]`:
   *
   * - `servicio-recorrido-sitioOrigen`:
   *
   * - `servicio-recorrido-sitioDestino[ne]`:
   *
   * - `servicio-recorrido-sitioDestino[like]`:
   *
   * - `servicio-recorrido-sitioDestino`:
   *
   * - `servicio-recorrido-recorrido[ne]`:
   *
   * - `servicio-recorrido-recorrido[like]`:
   *
   * - `servicio-recorrido-recorrido`:
   *
   * - `servicio-recorrido-origen[ne]`:
   *
   * - `servicio-recorrido-origen[like]`:
   *
   * - `servicio-recorrido-origen`:
   *
   * - `servicio-recorrido-itinerarioVuelta[ne]`:
   *
   * - `servicio-recorrido-itinerarioVuelta[like]`:
   *
   * - `servicio-recorrido-itinerarioVuelta`:
   *
   * - `servicio-recorrido-itinerarioIda[ne]`:
   *
   * - `servicio-recorrido-itinerarioIda[like]`:
   *
   * - `servicio-recorrido-itinerarioIda`:
   *
   * - `servicio-recorrido-id[ne]`:
   *
   * - `servicio-recorrido-id[lt]`:
   *
   * - `servicio-recorrido-id[le]`:
   *
   * - `servicio-recorrido-id[gt]`:
   *
   * - `servicio-recorrido-id[ge]`:
   *
   * - `servicio-recorrido-id`:
   *
   * - `servicio-recorrido-destino[ne]`:
   *
   * - `servicio-recorrido-destino[like]`:
   *
   * - `servicio-recorrido-destino`:
   *
   * - `servicio-recorrido-aplicaGasoil`:
   *
   * - `servicio-operador-vigenciaHasta[ne]`:
   *
   * - `servicio-operador-vigenciaHasta[lt]`:
   *
   * - `servicio-operador-vigenciaHasta[le]`:
   *
   * - `servicio-operador-vigenciaHasta[gt]`:
   *
   * - `servicio-operador-vigenciaHasta[ge]`:
   *
   * - `servicio-operador-vigenciaHasta`:
   *
   * - `servicio-operador-vigenciaDesde[ne]`:
   *
   * - `servicio-operador-vigenciaDesde[lt]`:
   *
   * - `servicio-operador-vigenciaDesde[le]`:
   *
   * - `servicio-operador-vigenciaDesde[gt]`:
   *
   * - `servicio-operador-vigenciaDesde[ge]`:
   *
   * - `servicio-operador-vigenciaDesde`:
   *
   * - `servicio-operador-localidadOrigen-updatedAt[ne]`:
   *
   * - `servicio-operador-localidadOrigen-updatedAt[lt]`:
   *
   * - `servicio-operador-localidadOrigen-updatedAt[le]`:
   *
   * - `servicio-operador-localidadOrigen-updatedAt[gt]`:
   *
   * - `servicio-operador-localidadOrigen-updatedAt[ge]`:
   *
   * - `servicio-operador-localidadOrigen-updatedAt`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-id[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-id[lt]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-id[le]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-id[gt]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-id[ge]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-id`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-descripcion[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-descripcion[like]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-descripcion`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-codigo[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-codigo[like]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-codigo`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-activo`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-abrev[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-abrev[like]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-abrev`:
   *
   * - `servicio-operador-localidadOrigen-provincia-id[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-id[lt]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-id[le]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-id[gt]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-id[ge]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-id`:
   *
   * - `servicio-operador-localidadOrigen-provincia-descripcion[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-descripcion[like]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-descripcion`:
   *
   * - `servicio-operador-localidadOrigen-provincia-codigo[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-codigo[like]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-codigoINDEC[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-codigoINDEC[like]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-codigoINDEC`:
   *
   * - `servicio-operador-localidadOrigen-provincia-codigo`:
   *
   * - `servicio-operador-localidadOrigen-provincia-activo`:
   *
   * - `servicio-operador-localidadOrigen-parada`:
   *
   * - `servicio-operador-localidadOrigen-lon[ne]`:
   *
   * - `servicio-operador-localidadOrigen-lon[lt]`:
   *
   * - `servicio-operador-localidadOrigen-lon[le]`:
   *
   * - `servicio-operador-localidadOrigen-lon[gt]`:
   *
   * - `servicio-operador-localidadOrigen-lon[ge]`:
   *
   * - `servicio-operador-localidadOrigen-lon`:
   *
   * - `servicio-operador-localidadOrigen-lat[ne]`:
   *
   * - `servicio-operador-localidadOrigen-lat[lt]`:
   *
   * - `servicio-operador-localidadOrigen-lat[le]`:
   *
   * - `servicio-operador-localidadOrigen-lat[gt]`:
   *
   * - `servicio-operador-localidadOrigen-lat[ge]`:
   *
   * - `servicio-operador-localidadOrigen-lat`:
   *
   * - `servicio-operador-localidadOrigen-id[ne]`:
   *
   * - `servicio-operador-localidadOrigen-id[lt]`:
   *
   * - `servicio-operador-localidadOrigen-id[le]`:
   *
   * - `servicio-operador-localidadOrigen-id[gt]`:
   *
   * - `servicio-operador-localidadOrigen-id[ge]`:
   *
   * - `servicio-operador-localidadOrigen-id`:
   *
   * - `servicio-operador-localidadOrigen-fuente[ne]`:
   *
   * - `servicio-operador-localidadOrigen-fuente[like]`:
   *
   * - `servicio-operador-localidadOrigen-fuente`:
   *
   * - `servicio-operador-localidadOrigen-descripcion[ne]`:
   *
   * - `servicio-operador-localidadOrigen-descripcion[like]`:
   *
   * - `servicio-operador-localidadOrigen-descripcion`:
   *
   * - `servicio-operador-localidadOrigen-createdAt[ne]`:
   *
   * - `servicio-operador-localidadOrigen-createdAt[lt]`:
   *
   * - `servicio-operador-localidadOrigen-createdAt[le]`:
   *
   * - `servicio-operador-localidadOrigen-createdAt[gt]`:
   *
   * - `servicio-operador-localidadOrigen-createdAt[ge]`:
   *
   * - `servicio-operador-localidadOrigen-createdAt`:
   *
   * - `servicio-operador-localidadOrigen-codigoINDEC[ne]`:
   *
   * - `servicio-operador-localidadOrigen-codigoINDEC[like]`:
   *
   * - `servicio-operador-localidadOrigen-codigoINDEC`:
   *
   * - `servicio-operador-localidadOrigen-activo`:
   *
   * - `servicio-operador-localidadDestino-updatedAt[ne]`:
   *
   * - `servicio-operador-localidadDestino-updatedAt[lt]`:
   *
   * - `servicio-operador-localidadDestino-updatedAt[le]`:
   *
   * - `servicio-operador-localidadDestino-updatedAt[gt]`:
   *
   * - `servicio-operador-localidadDestino-updatedAt[ge]`:
   *
   * - `servicio-operador-localidadDestino-updatedAt`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-id[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-id[lt]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-id[le]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-id[gt]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-id[ge]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-id`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-descripcion[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-descripcion[like]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-descripcion`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-codigo[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-codigo[like]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-codigo`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-activo`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-abrev[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-abrev[like]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-abrev`:
   *
   * - `servicio-operador-localidadDestino-provincia-id[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-id[lt]`:
   *
   * - `servicio-operador-localidadDestino-provincia-id[le]`:
   *
   * - `servicio-operador-localidadDestino-provincia-id[gt]`:
   *
   * - `servicio-operador-localidadDestino-provincia-id[ge]`:
   *
   * - `servicio-operador-localidadDestino-provincia-id`:
   *
   * - `servicio-operador-localidadDestino-provincia-descripcion[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-descripcion[like]`:
   *
   * - `servicio-operador-localidadDestino-provincia-descripcion`:
   *
   * - `servicio-operador-localidadDestino-provincia-codigo[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-codigo[like]`:
   *
   * - `servicio-operador-localidadDestino-provincia-codigoINDEC[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-codigoINDEC[like]`:
   *
   * - `servicio-operador-localidadDestino-provincia-codigoINDEC`:
   *
   * - `servicio-operador-localidadDestino-provincia-codigo`:
   *
   * - `servicio-operador-localidadDestino-provincia-activo`:
   *
   * - `servicio-operador-localidadDestino-parada`:
   *
   * - `servicio-operador-localidadDestino-lon[ne]`:
   *
   * - `servicio-operador-localidadDestino-lon[lt]`:
   *
   * - `servicio-operador-localidadDestino-lon[le]`:
   *
   * - `servicio-operador-localidadDestino-lon[gt]`:
   *
   * - `servicio-operador-localidadDestino-lon[ge]`:
   *
   * - `servicio-operador-localidadDestino-lon`:
   *
   * - `servicio-operador-localidadDestino-lat[ne]`:
   *
   * - `servicio-operador-localidadDestino-lat[lt]`:
   *
   * - `servicio-operador-localidadDestino-lat[le]`:
   *
   * - `servicio-operador-localidadDestino-lat[gt]`:
   *
   * - `servicio-operador-localidadDestino-lat[ge]`:
   *
   * - `servicio-operador-localidadDestino-lat`:
   *
   * - `servicio-operador-localidadDestino-id[ne]`:
   *
   * - `servicio-operador-localidadDestino-id[lt]`:
   *
   * - `servicio-operador-localidadDestino-id[le]`:
   *
   * - `servicio-operador-localidadDestino-id[gt]`:
   *
   * - `servicio-operador-localidadDestino-id[ge]`:
   *
   * - `servicio-operador-localidadDestino-id`:
   *
   * - `servicio-operador-localidadDestino-fuente[ne]`:
   *
   * - `servicio-operador-localidadDestino-fuente[like]`:
   *
   * - `servicio-operador-localidadDestino-fuente`:
   *
   * - `servicio-operador-localidadDestino-descripcion[ne]`:
   *
   * - `servicio-operador-localidadDestino-descripcion[like]`:
   *
   * - `servicio-operador-localidadDestino-descripcion`:
   *
   * - `servicio-operador-localidadDestino-createdAt[ne]`:
   *
   * - `servicio-operador-localidadDestino-createdAt[lt]`:
   *
   * - `servicio-operador-localidadDestino-createdAt[le]`:
   *
   * - `servicio-operador-localidadDestino-createdAt[gt]`:
   *
   * - `servicio-operador-localidadDestino-createdAt[ge]`:
   *
   * - `servicio-operador-localidadDestino-createdAt`:
   *
   * - `servicio-operador-localidadDestino-codigoINDEC[ne]`:
   *
   * - `servicio-operador-localidadDestino-codigoINDEC[like]`:
   *
   * - `servicio-operador-localidadDestino-codigoINDEC`:
   *
   * - `servicio-operador-localidadDestino-activo`:
   *
   * - `servicio-operador-linea[ne]`:
   *
   * - `servicio-operador-linea[like]`:
   *
   * - `servicio-operador-linea`:
   *
   * - `servicio-operador-id[ne]`:
   *
   * - `servicio-operador-id[lt]`:
   *
   * - `servicio-operador-id[le]`:
   *
   * - `servicio-operador-id[gt]`:
   *
   * - `servicio-operador-id[ge]`:
   *
   * - `servicio-operador-id`:
   *
   * - `servicio-operador-empresaContratante[ne]`:
   *
   * - `servicio-operador-empresaContratante[like]`:
   *
   * - `servicio-operador-empresaContratanteCuit[ne]`:
   *
   * - `servicio-operador-empresaContratanteCuit[like]`:
   *
   * - `servicio-operador-empresaContratanteCuit`:
   *
   * - `servicio-operador-empresaContratante`:
   *
   * - `servicio-operador-empresa-vigenciaHasta[ne]`:
   *
   * - `servicio-operador-empresa-vigenciaHasta[lt]`:
   *
   * - `servicio-operador-empresa-vigenciaHasta[le]`:
   *
   * - `servicio-operador-empresa-vigenciaHasta[gt]`:
   *
   * - `servicio-operador-empresa-vigenciaHasta[ge]`:
   *
   * - `servicio-operador-empresa-vigenciaHasta`:
   *
   * - `servicio-operador-empresa-vigenciaDesde[ne]`:
   *
   * - `servicio-operador-empresa-vigenciaDesde[lt]`:
   *
   * - `servicio-operador-empresa-vigenciaDesde[le]`:
   *
   * - `servicio-operador-empresa-vigenciaDesde[gt]`:
   *
   * - `servicio-operador-empresa-vigenciaDesde[ge]`:
   *
   * - `servicio-operador-empresa-vigenciaDesde`:
   *
   * - `servicio-operador-empresa-tipoSociedad[ne]`:
   *
   * - `servicio-operador-empresa-tipoSociedad[lt]`:
   *
   * - `servicio-operador-empresa-tipoSociedad[le]`:
   *
   * - `servicio-operador-empresa-tipoSociedad[gt]`:
   *
   * - `servicio-operador-empresa-tipoSociedad[ge]`:
   *
   * - `servicio-operador-empresa-tipoSociedad`:
   *
   * - `servicio-operador-empresa-tipoDocumento-id[ne]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-id[lt]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-id[le]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-id[gt]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-id[ge]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-id`:
   *
   * - `servicio-operador-empresa-tipoDocumento-descripcion[ne]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-descripcion[like]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-descripcion`:
   *
   * - `servicio-operador-empresa-tipoDocumento-abrev[ne]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-abrev[like]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-abrev`:
   *
   * - `servicio-operador-empresa-textoHash`:
   *
   * - `servicio-operador-empresa-razonSocial[ne]`:
   *
   * - `servicio-operador-empresa-razonSocial[like]`:
   *
   * - `servicio-operador-empresa-razonSocial`:
   *
   * - `servicio-operador-empresa-paut[ne]`:
   *
   * - `servicio-operador-empresa-paut[like]`:
   *
   * - `servicio-operador-empresa-paut`:
   *
   * - `servicio-operador-empresa-observacion[ne]`:
   *
   * - `servicio-operador-empresa-observacion[like]`:
   *
   * - `servicio-operador-empresa-observacion`:
   *
   * - `servicio-operador-empresa-nroDocumento[ne]`:
   *
   * - `servicio-operador-empresa-nroDocumento[like]`:
   *
   * - `servicio-operador-empresa-nroDocumento`:
   *
   * - `servicio-operador-empresa-nombreFantasia[ne]`:
   *
   * - `servicio-operador-empresa-nombreFantasia[like]`:
   *
   * - `servicio-operador-empresa-nombreFantasia`:
   *
   * - `servicio-operador-empresa-id[ne]`:
   *
   * - `servicio-operador-empresa-id[lt]`:
   *
   * - `servicio-operador-empresa-id[le]`:
   *
   * - `servicio-operador-empresa-id[gt]`:
   *
   * - `servicio-operador-empresa-id[ge]`:
   *
   * - `servicio-operador-empresa-id`:
   *
   * - `servicio-operador-empresa-estadoEmpresa[ne]`:
   *
   * - `servicio-operador-empresa-estadoEmpresa[lt]`:
   *
   * - `servicio-operador-empresa-estadoEmpresa[le]`:
   *
   * - `servicio-operador-empresa-estadoEmpresa[gt]`:
   *
   * - `servicio-operador-empresa-estadoEmpresa[ge]`:
   *
   * - `servicio-operador-empresa-estadoEmpresa`:
   *
   * - `servicio-operador-empresa-esPersonaFisica`:
   *
   * - `servicio-operador-empresa-email[ne]`:
   *
   * - `servicio-operador-empresa-email[like]`:
   *
   * - `servicio-operador-empresa-email`:
   *
   * - `servicio-operador-empresa-cuitEmpresaAnterior[ne]`:
   *
   * - `servicio-operador-empresa-cuitEmpresaAnterior[like]`:
   *
   * - `servicio-operador-empresa-cuitEmpresaAnterior`:
   *
   * - `servicio-operador-claseModalidad-id[ne]`:
   *
   * - `servicio-operador-claseModalidad-id[lt]`:
   *
   * - `servicio-operador-claseModalidad-id[le]`:
   *
   * - `servicio-operador-claseModalidad-id[gt]`:
   *
   * - `servicio-operador-claseModalidad-id[ge]`:
   *
   * - `servicio-operador-claseModalidad-id`:
   *
   * - `servicio-operador-claseModalidad-descripcion[ne]`:
   *
   * - `servicio-operador-claseModalidad-descripcion[like]`:
   *
   * - `servicio-operador-claseModalidad-descripcion`:
   *
   * - `servicio-id[ne]`:
   *
   * - `servicio-id[lt]`:
   *
   * - `servicio-id[le]`:
   *
   * - `servicio-id[gt]`:
   *
   * - `servicio-id[ge]`:
   *
   * - `servicio-id`:
   *
   * - `servicio-horaSalida[ne]`:
   *
   * - `servicio-horaSalida[lt]`:
   *
   * - `servicio-horaSalida[le]`:
   *
   * - `servicio-horaSalida[gt]`:
   *
   * - `servicio-horaSalida[ge]`:
   *
   * - `servicio-horaSalida`:
   *
   * - `servicio-horaLlegada[ne]`:
   *
   * - `servicio-horaLlegada[lt]`:
   *
   * - `servicio-horaLlegada[le]`:
   *
   * - `servicio-horaLlegada[gt]`:
   *
   * - `servicio-horaLlegada[ge]`:
   *
   * - `servicio-horaLlegada`:
   *
   * - `servicio-aprobado`:
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `categoriaServicioPrimerPiso-id[ne]`:
   *
   * - `categoriaServicioPrimerPiso-id[lt]`:
   *
   * - `categoriaServicioPrimerPiso-id[le]`:
   *
   * - `categoriaServicioPrimerPiso-id[gt]`:
   *
   * - `categoriaServicioPrimerPiso-id[ge]`:
   *
   * - `categoriaServicioPrimerPiso-id`:
   *
   * - `categoriaServicioPrimerPiso-descripcion[ne]`:
   *
   * - `categoriaServicioPrimerPiso-descripcion[like]`:
   *
   * - `categoriaServicioPrimerPiso-descripcion`:
   *
   * - `categoriaServicioPrimerPiso-activo`:
   *
   * - `activo`:
   *
   * @return Respuesta exitosa.
   */
  getApiV2DiasServiciosResponse(params: DiaServicioService.GetApiV2DiasServiciosParams): __Observable<__StrictHttpResponse<DiaServicioAPIResponsePaginacion>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.sort != null) __params = __params.set('sort', params.sort.toString());
    if (params.servicioVigenciaHastaNe != null) __params = __params.set('servicio-vigenciaHasta[ne]', params.servicioVigenciaHastaNe.toString());
    if (params.servicioVigenciaHastaLt != null) __params = __params.set('servicio-vigenciaHasta[lt]', params.servicioVigenciaHastaLt.toString());
    if (params.servicioVigenciaHastaLe != null) __params = __params.set('servicio-vigenciaHasta[le]', params.servicioVigenciaHastaLe.toString());
    if (params.servicioVigenciaHastaGt != null) __params = __params.set('servicio-vigenciaHasta[gt]', params.servicioVigenciaHastaGt.toString());
    if (params.servicioVigenciaHastaGe != null) __params = __params.set('servicio-vigenciaHasta[ge]', params.servicioVigenciaHastaGe.toString());
    if (params.servicioVigenciaHasta != null) __params = __params.set('servicio-vigenciaHasta', params.servicioVigenciaHasta.toString());
    if (params.servicioVigenciaDesdeNe != null) __params = __params.set('servicio-vigenciaDesde[ne]', params.servicioVigenciaDesdeNe.toString());
    if (params.servicioVigenciaDesdeLt != null) __params = __params.set('servicio-vigenciaDesde[lt]', params.servicioVigenciaDesdeLt.toString());
    if (params.servicioVigenciaDesdeLe != null) __params = __params.set('servicio-vigenciaDesde[le]', params.servicioVigenciaDesdeLe.toString());
    if (params.servicioVigenciaDesdeGt != null) __params = __params.set('servicio-vigenciaDesde[gt]', params.servicioVigenciaDesdeGt.toString());
    if (params.servicioVigenciaDesdeGe != null) __params = __params.set('servicio-vigenciaDesde[ge]', params.servicioVigenciaDesdeGe.toString());
    if (params.servicioVigenciaDesde != null) __params = __params.set('servicio-vigenciaDesde', params.servicioVigenciaDesde.toString());
    if (params.servicioVelocidadPromedioTotalNe != null) __params = __params.set('servicio-velocidadPromedioTotal[ne]', params.servicioVelocidadPromedioTotalNe.toString());
    if (params.servicioVelocidadPromedioTotalLt != null) __params = __params.set('servicio-velocidadPromedioTotal[lt]', params.servicioVelocidadPromedioTotalLt.toString());
    if (params.servicioVelocidadPromedioTotalLe != null) __params = __params.set('servicio-velocidadPromedioTotal[le]', params.servicioVelocidadPromedioTotalLe.toString());
    if (params.servicioVelocidadPromedioTotalGt != null) __params = __params.set('servicio-velocidadPromedioTotal[gt]', params.servicioVelocidadPromedioTotalGt.toString());
    if (params.servicioVelocidadPromedioTotalGe != null) __params = __params.set('servicio-velocidadPromedioTotal[ge]', params.servicioVelocidadPromedioTotalGe.toString());
    if (params.servicioVelocidadPromedioTotal != null) __params = __params.set('servicio-velocidadPromedioTotal', params.servicioVelocidadPromedioTotal.toString());
    if (params.servicioTramiteUsuarioNe != null) __params = __params.set('servicio-tramite-usuario[ne]', params.servicioTramiteUsuarioNe.toString());
    if (params.servicioTramiteUsuarioLike != null) __params = __params.set('servicio-tramite-usuario[like]', params.servicioTramiteUsuarioLike.toString());
    if (params.servicioTramiteUsuario != null) __params = __params.set('servicio-tramite-usuario', params.servicioTramiteUsuario.toString());
    if (params.servicioTramiteTipoTramiteIdNe != null) __params = __params.set('servicio-tramite-tipoTramite-id[ne]', params.servicioTramiteTipoTramiteIdNe.toString());
    if (params.servicioTramiteTipoTramiteIdLt != null) __params = __params.set('servicio-tramite-tipoTramite-id[lt]', params.servicioTramiteTipoTramiteIdLt.toString());
    if (params.servicioTramiteTipoTramiteIdLe != null) __params = __params.set('servicio-tramite-tipoTramite-id[le]', params.servicioTramiteTipoTramiteIdLe.toString());
    if (params.servicioTramiteTipoTramiteIdGt != null) __params = __params.set('servicio-tramite-tipoTramite-id[gt]', params.servicioTramiteTipoTramiteIdGt.toString());
    if (params.servicioTramiteTipoTramiteIdGe != null) __params = __params.set('servicio-tramite-tipoTramite-id[ge]', params.servicioTramiteTipoTramiteIdGe.toString());
    if (params.servicioTramiteTipoTramiteId != null) __params = __params.set('servicio-tramite-tipoTramite-id', params.servicioTramiteTipoTramiteId.toString());
    if (params.servicioTramiteTipoTramiteDescripcionNe != null) __params = __params.set('servicio-tramite-tipoTramite-descripcion[ne]', params.servicioTramiteTipoTramiteDescripcionNe.toString());
    if (params.servicioTramiteTipoTramiteDescripcionLike != null) __params = __params.set('servicio-tramite-tipoTramite-descripcion[like]', params.servicioTramiteTipoTramiteDescripcionLike.toString());
    if (params.servicioTramiteTipoTramiteDescripcion != null) __params = __params.set('servicio-tramite-tipoTramite-descripcion', params.servicioTramiteTipoTramiteDescripcion.toString());
    if (params.servicioTramiteTipoTramiteActivo != null) __params = __params.set('servicio-tramite-tipoTramite-activo', params.servicioTramiteTipoTramiteActivo.toString());
    if (params.servicioTramiteSubtipoTramiteIdNe != null) __params = __params.set('servicio-tramite-subtipoTramite-id[ne]', params.servicioTramiteSubtipoTramiteIdNe.toString());
    if (params.servicioTramiteSubtipoTramiteIdLt != null) __params = __params.set('servicio-tramite-subtipoTramite-id[lt]', params.servicioTramiteSubtipoTramiteIdLt.toString());
    if (params.servicioTramiteSubtipoTramiteIdLe != null) __params = __params.set('servicio-tramite-subtipoTramite-id[le]', params.servicioTramiteSubtipoTramiteIdLe.toString());
    if (params.servicioTramiteSubtipoTramiteIdGt != null) __params = __params.set('servicio-tramite-subtipoTramite-id[gt]', params.servicioTramiteSubtipoTramiteIdGt.toString());
    if (params.servicioTramiteSubtipoTramiteIdGe != null) __params = __params.set('servicio-tramite-subtipoTramite-id[ge]', params.servicioTramiteSubtipoTramiteIdGe.toString());
    if (params.servicioTramiteSubtipoTramiteId != null) __params = __params.set('servicio-tramite-subtipoTramite-id', params.servicioTramiteSubtipoTramiteId.toString());
    if (params.servicioTramiteSubtipoTramiteDescripcionNe != null) __params = __params.set('servicio-tramite-subtipoTramite-descripcion[ne]', params.servicioTramiteSubtipoTramiteDescripcionNe.toString());
    if (params.servicioTramiteSubtipoTramiteDescripcionLike != null) __params = __params.set('servicio-tramite-subtipoTramite-descripcion[like]', params.servicioTramiteSubtipoTramiteDescripcionLike.toString());
    if (params.servicioTramiteSubtipoTramiteDescripcion != null) __params = __params.set('servicio-tramite-subtipoTramite-descripcion', params.servicioTramiteSubtipoTramiteDescripcion.toString());
    if (params.servicioTramiteSubtipoTramiteActivo != null) __params = __params.set('servicio-tramite-subtipoTramite-activo', params.servicioTramiteSubtipoTramiteActivo.toString());
    if (params.servicioTramiteNroBoletaPagoNe != null) __params = __params.set('servicio-tramite-nroBoletaPago[ne]', params.servicioTramiteNroBoletaPagoNe.toString());
    if (params.servicioTramiteNroBoletaPagoLt != null) __params = __params.set('servicio-tramite-nroBoletaPago[lt]', params.servicioTramiteNroBoletaPagoLt.toString());
    if (params.servicioTramiteNroBoletaPagoLe != null) __params = __params.set('servicio-tramite-nroBoletaPago[le]', params.servicioTramiteNroBoletaPagoLe.toString());
    if (params.servicioTramiteNroBoletaPagoGt != null) __params = __params.set('servicio-tramite-nroBoletaPago[gt]', params.servicioTramiteNroBoletaPagoGt.toString());
    if (params.servicioTramiteNroBoletaPagoGe != null) __params = __params.set('servicio-tramite-nroBoletaPago[ge]', params.servicioTramiteNroBoletaPagoGe.toString());
    if (params.servicioTramiteNroBoletaPago != null) __params = __params.set('servicio-tramite-nroBoletaPago', params.servicioTramiteNroBoletaPago.toString());
    if (params.servicioTramiteIdNe != null) __params = __params.set('servicio-tramite-id[ne]', params.servicioTramiteIdNe.toString());
    if (params.servicioTramiteIdLt != null) __params = __params.set('servicio-tramite-id[lt]', params.servicioTramiteIdLt.toString());
    if (params.servicioTramiteIdLe != null) __params = __params.set('servicio-tramite-id[le]', params.servicioTramiteIdLe.toString());
    if (params.servicioTramiteIdGt != null) __params = __params.set('servicio-tramite-id[gt]', params.servicioTramiteIdGt.toString());
    if (params.servicioTramiteIdGe != null) __params = __params.set('servicio-tramite-id[ge]', params.servicioTramiteIdGe.toString());
    if (params.servicioTramiteId != null) __params = __params.set('servicio-tramite-id', params.servicioTramiteId.toString());
    if (params.servicioTramiteEstadoTramiteIdNe != null) __params = __params.set('servicio-tramite-estadoTramite-id[ne]', params.servicioTramiteEstadoTramiteIdNe.toString());
    if (params.servicioTramiteEstadoTramiteIdLt != null) __params = __params.set('servicio-tramite-estadoTramite-id[lt]', params.servicioTramiteEstadoTramiteIdLt.toString());
    if (params.servicioTramiteEstadoTramiteIdLe != null) __params = __params.set('servicio-tramite-estadoTramite-id[le]', params.servicioTramiteEstadoTramiteIdLe.toString());
    if (params.servicioTramiteEstadoTramiteIdGt != null) __params = __params.set('servicio-tramite-estadoTramite-id[gt]', params.servicioTramiteEstadoTramiteIdGt.toString());
    if (params.servicioTramiteEstadoTramiteIdGe != null) __params = __params.set('servicio-tramite-estadoTramite-id[ge]', params.servicioTramiteEstadoTramiteIdGe.toString());
    if (params.servicioTramiteEstadoTramiteId != null) __params = __params.set('servicio-tramite-estadoTramite-id', params.servicioTramiteEstadoTramiteId.toString());
    if (params.servicioTramiteEstadoTramiteDescripcionNe != null) __params = __params.set('servicio-tramite-estadoTramite-descripcion[ne]', params.servicioTramiteEstadoTramiteDescripcionNe.toString());
    if (params.servicioTramiteEstadoTramiteDescripcionLike != null) __params = __params.set('servicio-tramite-estadoTramite-descripcion[like]', params.servicioTramiteEstadoTramiteDescripcionLike.toString());
    if (params.servicioTramiteEstadoTramiteDescripcion != null) __params = __params.set('servicio-tramite-estadoTramite-descripcion', params.servicioTramiteEstadoTramiteDescripcion.toString());
    if (params.servicioTramiteEstadoTramiteActivo != null) __params = __params.set('servicio-tramite-estadoTramite-activo', params.servicioTramiteEstadoTramiteActivo.toString());
    if (params.servicioTramiteEstadoTramiteAbrevNe != null) __params = __params.set('servicio-tramite-estadoTramite-abrev[ne]', params.servicioTramiteEstadoTramiteAbrevNe.toString());
    if (params.servicioTramiteEstadoTramiteAbrevLike != null) __params = __params.set('servicio-tramite-estadoTramite-abrev[like]', params.servicioTramiteEstadoTramiteAbrevLike.toString());
    if (params.servicioTramiteEstadoTramiteAbrev != null) __params = __params.set('servicio-tramite-estadoTramite-abrev', params.servicioTramiteEstadoTramiteAbrev.toString());
    if (params.servicioTramiteEmpresaVigenciaHastaNe != null) __params = __params.set('servicio-tramite-empresa-vigenciaHasta[ne]', params.servicioTramiteEmpresaVigenciaHastaNe.toString());
    if (params.servicioTramiteEmpresaVigenciaHastaLt != null) __params = __params.set('servicio-tramite-empresa-vigenciaHasta[lt]', params.servicioTramiteEmpresaVigenciaHastaLt.toString());
    if (params.servicioTramiteEmpresaVigenciaHastaLe != null) __params = __params.set('servicio-tramite-empresa-vigenciaHasta[le]', params.servicioTramiteEmpresaVigenciaHastaLe.toString());
    if (params.servicioTramiteEmpresaVigenciaHastaGt != null) __params = __params.set('servicio-tramite-empresa-vigenciaHasta[gt]', params.servicioTramiteEmpresaVigenciaHastaGt.toString());
    if (params.servicioTramiteEmpresaVigenciaHastaGe != null) __params = __params.set('servicio-tramite-empresa-vigenciaHasta[ge]', params.servicioTramiteEmpresaVigenciaHastaGe.toString());
    if (params.servicioTramiteEmpresaVigenciaHasta != null) __params = __params.set('servicio-tramite-empresa-vigenciaHasta', params.servicioTramiteEmpresaVigenciaHasta.toString());
    if (params.servicioTramiteEmpresaVigenciaDesdeNe != null) __params = __params.set('servicio-tramite-empresa-vigenciaDesde[ne]', params.servicioTramiteEmpresaVigenciaDesdeNe.toString());
    if (params.servicioTramiteEmpresaVigenciaDesdeLt != null) __params = __params.set('servicio-tramite-empresa-vigenciaDesde[lt]', params.servicioTramiteEmpresaVigenciaDesdeLt.toString());
    if (params.servicioTramiteEmpresaVigenciaDesdeLe != null) __params = __params.set('servicio-tramite-empresa-vigenciaDesde[le]', params.servicioTramiteEmpresaVigenciaDesdeLe.toString());
    if (params.servicioTramiteEmpresaVigenciaDesdeGt != null) __params = __params.set('servicio-tramite-empresa-vigenciaDesde[gt]', params.servicioTramiteEmpresaVigenciaDesdeGt.toString());
    if (params.servicioTramiteEmpresaVigenciaDesdeGe != null) __params = __params.set('servicio-tramite-empresa-vigenciaDesde[ge]', params.servicioTramiteEmpresaVigenciaDesdeGe.toString());
    if (params.servicioTramiteEmpresaVigenciaDesde != null) __params = __params.set('servicio-tramite-empresa-vigenciaDesde', params.servicioTramiteEmpresaVigenciaDesde.toString());
    if (params.servicioTramiteEmpresaTipoSociedadNe != null) __params = __params.set('servicio-tramite-empresa-tipoSociedad[ne]', params.servicioTramiteEmpresaTipoSociedadNe.toString());
    if (params.servicioTramiteEmpresaTipoSociedadLt != null) __params = __params.set('servicio-tramite-empresa-tipoSociedad[lt]', params.servicioTramiteEmpresaTipoSociedadLt.toString());
    if (params.servicioTramiteEmpresaTipoSociedadLe != null) __params = __params.set('servicio-tramite-empresa-tipoSociedad[le]', params.servicioTramiteEmpresaTipoSociedadLe.toString());
    if (params.servicioTramiteEmpresaTipoSociedadGt != null) __params = __params.set('servicio-tramite-empresa-tipoSociedad[gt]', params.servicioTramiteEmpresaTipoSociedadGt.toString());
    if (params.servicioTramiteEmpresaTipoSociedadGe != null) __params = __params.set('servicio-tramite-empresa-tipoSociedad[ge]', params.servicioTramiteEmpresaTipoSociedadGe.toString());
    if (params.servicioTramiteEmpresaTipoSociedad != null) __params = __params.set('servicio-tramite-empresa-tipoSociedad', params.servicioTramiteEmpresaTipoSociedad.toString());
    if (params.servicioTramiteEmpresaTipoDocumentoIdNe != null) __params = __params.set('servicio-tramite-empresa-tipoDocumento-id[ne]', params.servicioTramiteEmpresaTipoDocumentoIdNe.toString());
    if (params.servicioTramiteEmpresaTipoDocumentoIdLt != null) __params = __params.set('servicio-tramite-empresa-tipoDocumento-id[lt]', params.servicioTramiteEmpresaTipoDocumentoIdLt.toString());
    if (params.servicioTramiteEmpresaTipoDocumentoIdLe != null) __params = __params.set('servicio-tramite-empresa-tipoDocumento-id[le]', params.servicioTramiteEmpresaTipoDocumentoIdLe.toString());
    if (params.servicioTramiteEmpresaTipoDocumentoIdGt != null) __params = __params.set('servicio-tramite-empresa-tipoDocumento-id[gt]', params.servicioTramiteEmpresaTipoDocumentoIdGt.toString());
    if (params.servicioTramiteEmpresaTipoDocumentoIdGe != null) __params = __params.set('servicio-tramite-empresa-tipoDocumento-id[ge]', params.servicioTramiteEmpresaTipoDocumentoIdGe.toString());
    if (params.servicioTramiteEmpresaTipoDocumentoId != null) __params = __params.set('servicio-tramite-empresa-tipoDocumento-id', params.servicioTramiteEmpresaTipoDocumentoId.toString());
    if (params.servicioTramiteEmpresaTipoDocumentoDescripcionNe != null) __params = __params.set('servicio-tramite-empresa-tipoDocumento-descripcion[ne]', params.servicioTramiteEmpresaTipoDocumentoDescripcionNe.toString());
    if (params.servicioTramiteEmpresaTipoDocumentoDescripcionLike != null) __params = __params.set('servicio-tramite-empresa-tipoDocumento-descripcion[like]', params.servicioTramiteEmpresaTipoDocumentoDescripcionLike.toString());
    if (params.servicioTramiteEmpresaTipoDocumentoDescripcion != null) __params = __params.set('servicio-tramite-empresa-tipoDocumento-descripcion', params.servicioTramiteEmpresaTipoDocumentoDescripcion.toString());
    if (params.servicioTramiteEmpresaTipoDocumentoAbrevNe != null) __params = __params.set('servicio-tramite-empresa-tipoDocumento-abrev[ne]', params.servicioTramiteEmpresaTipoDocumentoAbrevNe.toString());
    if (params.servicioTramiteEmpresaTipoDocumentoAbrevLike != null) __params = __params.set('servicio-tramite-empresa-tipoDocumento-abrev[like]', params.servicioTramiteEmpresaTipoDocumentoAbrevLike.toString());
    if (params.servicioTramiteEmpresaTipoDocumentoAbrev != null) __params = __params.set('servicio-tramite-empresa-tipoDocumento-abrev', params.servicioTramiteEmpresaTipoDocumentoAbrev.toString());
    if (params.servicioTramiteEmpresaTextoHash != null) __params = __params.set('servicio-tramite-empresa-textoHash', params.servicioTramiteEmpresaTextoHash.toString());
    if (params.servicioTramiteEmpresaRazonSocialNe != null) __params = __params.set('servicio-tramite-empresa-razonSocial[ne]', params.servicioTramiteEmpresaRazonSocialNe.toString());
    if (params.servicioTramiteEmpresaRazonSocialLike != null) __params = __params.set('servicio-tramite-empresa-razonSocial[like]', params.servicioTramiteEmpresaRazonSocialLike.toString());
    if (params.servicioTramiteEmpresaRazonSocial != null) __params = __params.set('servicio-tramite-empresa-razonSocial', params.servicioTramiteEmpresaRazonSocial.toString());
    if (params.servicioTramiteEmpresaPautNe != null) __params = __params.set('servicio-tramite-empresa-paut[ne]', params.servicioTramiteEmpresaPautNe.toString());
    if (params.servicioTramiteEmpresaPautLike != null) __params = __params.set('servicio-tramite-empresa-paut[like]', params.servicioTramiteEmpresaPautLike.toString());
    if (params.servicioTramiteEmpresaPaut != null) __params = __params.set('servicio-tramite-empresa-paut', params.servicioTramiteEmpresaPaut.toString());
    if (params.servicioTramiteEmpresaObservacionNe != null) __params = __params.set('servicio-tramite-empresa-observacion[ne]', params.servicioTramiteEmpresaObservacionNe.toString());
    if (params.servicioTramiteEmpresaObservacionLike != null) __params = __params.set('servicio-tramite-empresa-observacion[like]', params.servicioTramiteEmpresaObservacionLike.toString());
    if (params.servicioTramiteEmpresaObservacion != null) __params = __params.set('servicio-tramite-empresa-observacion', params.servicioTramiteEmpresaObservacion.toString());
    if (params.servicioTramiteEmpresaNroDocumentoNe != null) __params = __params.set('servicio-tramite-empresa-nroDocumento[ne]', params.servicioTramiteEmpresaNroDocumentoNe.toString());
    if (params.servicioTramiteEmpresaNroDocumentoLike != null) __params = __params.set('servicio-tramite-empresa-nroDocumento[like]', params.servicioTramiteEmpresaNroDocumentoLike.toString());
    if (params.servicioTramiteEmpresaNroDocumento != null) __params = __params.set('servicio-tramite-empresa-nroDocumento', params.servicioTramiteEmpresaNroDocumento.toString());
    if (params.servicioTramiteEmpresaNombreFantasiaNe != null) __params = __params.set('servicio-tramite-empresa-nombreFantasia[ne]', params.servicioTramiteEmpresaNombreFantasiaNe.toString());
    if (params.servicioTramiteEmpresaNombreFantasiaLike != null) __params = __params.set('servicio-tramite-empresa-nombreFantasia[like]', params.servicioTramiteEmpresaNombreFantasiaLike.toString());
    if (params.servicioTramiteEmpresaNombreFantasia != null) __params = __params.set('servicio-tramite-empresa-nombreFantasia', params.servicioTramiteEmpresaNombreFantasia.toString());
    if (params.servicioTramiteEmpresaIdNe != null) __params = __params.set('servicio-tramite-empresa-id[ne]', params.servicioTramiteEmpresaIdNe.toString());
    if (params.servicioTramiteEmpresaIdLt != null) __params = __params.set('servicio-tramite-empresa-id[lt]', params.servicioTramiteEmpresaIdLt.toString());
    if (params.servicioTramiteEmpresaIdLe != null) __params = __params.set('servicio-tramite-empresa-id[le]', params.servicioTramiteEmpresaIdLe.toString());
    if (params.servicioTramiteEmpresaIdGt != null) __params = __params.set('servicio-tramite-empresa-id[gt]', params.servicioTramiteEmpresaIdGt.toString());
    if (params.servicioTramiteEmpresaIdGe != null) __params = __params.set('servicio-tramite-empresa-id[ge]', params.servicioTramiteEmpresaIdGe.toString());
    if (params.servicioTramiteEmpresaId != null) __params = __params.set('servicio-tramite-empresa-id', params.servicioTramiteEmpresaId.toString());
    if (params.servicioTramiteEmpresaEstadoEmpresaNe != null) __params = __params.set('servicio-tramite-empresa-estadoEmpresa[ne]', params.servicioTramiteEmpresaEstadoEmpresaNe.toString());
    if (params.servicioTramiteEmpresaEstadoEmpresaLt != null) __params = __params.set('servicio-tramite-empresa-estadoEmpresa[lt]', params.servicioTramiteEmpresaEstadoEmpresaLt.toString());
    if (params.servicioTramiteEmpresaEstadoEmpresaLe != null) __params = __params.set('servicio-tramite-empresa-estadoEmpresa[le]', params.servicioTramiteEmpresaEstadoEmpresaLe.toString());
    if (params.servicioTramiteEmpresaEstadoEmpresaGt != null) __params = __params.set('servicio-tramite-empresa-estadoEmpresa[gt]', params.servicioTramiteEmpresaEstadoEmpresaGt.toString());
    if (params.servicioTramiteEmpresaEstadoEmpresaGe != null) __params = __params.set('servicio-tramite-empresa-estadoEmpresa[ge]', params.servicioTramiteEmpresaEstadoEmpresaGe.toString());
    if (params.servicioTramiteEmpresaEstadoEmpresa != null) __params = __params.set('servicio-tramite-empresa-estadoEmpresa', params.servicioTramiteEmpresaEstadoEmpresa.toString());
    if (params.servicioTramiteEmpresaEsPersonaFisica != null) __params = __params.set('servicio-tramite-empresa-esPersonaFisica', params.servicioTramiteEmpresaEsPersonaFisica.toString());
    if (params.servicioTramiteEmpresaEmailNe != null) __params = __params.set('servicio-tramite-empresa-email[ne]', params.servicioTramiteEmpresaEmailNe.toString());
    if (params.servicioTramiteEmpresaEmailLike != null) __params = __params.set('servicio-tramite-empresa-email[like]', params.servicioTramiteEmpresaEmailLike.toString());
    if (params.servicioTramiteEmpresaEmail != null) __params = __params.set('servicio-tramite-empresa-email', params.servicioTramiteEmpresaEmail.toString());
    if (params.servicioTramiteEmpresaCuitEmpresaAnteriorNe != null) __params = __params.set('servicio-tramite-empresa-cuitEmpresaAnterior[ne]', params.servicioTramiteEmpresaCuitEmpresaAnteriorNe.toString());
    if (params.servicioTramiteEmpresaCuitEmpresaAnteriorLike != null) __params = __params.set('servicio-tramite-empresa-cuitEmpresaAnterior[like]', params.servicioTramiteEmpresaCuitEmpresaAnteriorLike.toString());
    if (params.servicioTramiteEmpresaCuitEmpresaAnterior != null) __params = __params.set('servicio-tramite-empresa-cuitEmpresaAnterior', params.servicioTramiteEmpresaCuitEmpresaAnterior.toString());
    if (params.servicioRecorridoSentidoVinculacionCamineraNe != null) __params = __params.set('servicio-recorridoSentido-vinculacionCaminera[ne]', params.servicioRecorridoSentidoVinculacionCamineraNe.toString());
    if (params.servicioRecorridoSentidoVinculacionCamineraLike != null) __params = __params.set('servicio-recorridoSentido-vinculacionCaminera[like]', params.servicioRecorridoSentidoVinculacionCamineraLike.toString());
    if (params.servicioRecorridoSentidoVinculacionCaminera != null) __params = __params.set('servicio-recorridoSentido-vinculacionCaminera', params.servicioRecorridoSentidoVinculacionCaminera.toString());
    if (params.servicioRecorridoSentidoVigenciaHastaNe != null) __params = __params.set('servicio-recorridoSentido-vigenciaHasta[ne]', params.servicioRecorridoSentidoVigenciaHastaNe.toString());
    if (params.servicioRecorridoSentidoVigenciaHastaLt != null) __params = __params.set('servicio-recorridoSentido-vigenciaHasta[lt]', params.servicioRecorridoSentidoVigenciaHastaLt.toString());
    if (params.servicioRecorridoSentidoVigenciaHastaLe != null) __params = __params.set('servicio-recorridoSentido-vigenciaHasta[le]', params.servicioRecorridoSentidoVigenciaHastaLe.toString());
    if (params.servicioRecorridoSentidoVigenciaHastaGt != null) __params = __params.set('servicio-recorridoSentido-vigenciaHasta[gt]', params.servicioRecorridoSentidoVigenciaHastaGt.toString());
    if (params.servicioRecorridoSentidoVigenciaHastaGe != null) __params = __params.set('servicio-recorridoSentido-vigenciaHasta[ge]', params.servicioRecorridoSentidoVigenciaHastaGe.toString());
    if (params.servicioRecorridoSentidoVigenciaHasta != null) __params = __params.set('servicio-recorridoSentido-vigenciaHasta', params.servicioRecorridoSentidoVigenciaHasta.toString());
    if (params.servicioRecorridoSentidoVigenciaDesdeNe != null) __params = __params.set('servicio-recorridoSentido-vigenciaDesde[ne]', params.servicioRecorridoSentidoVigenciaDesdeNe.toString());
    if (params.servicioRecorridoSentidoVigenciaDesdeLt != null) __params = __params.set('servicio-recorridoSentido-vigenciaDesde[lt]', params.servicioRecorridoSentidoVigenciaDesdeLt.toString());
    if (params.servicioRecorridoSentidoVigenciaDesdeLe != null) __params = __params.set('servicio-recorridoSentido-vigenciaDesde[le]', params.servicioRecorridoSentidoVigenciaDesdeLe.toString());
    if (params.servicioRecorridoSentidoVigenciaDesdeGt != null) __params = __params.set('servicio-recorridoSentido-vigenciaDesde[gt]', params.servicioRecorridoSentidoVigenciaDesdeGt.toString());
    if (params.servicioRecorridoSentidoVigenciaDesdeGe != null) __params = __params.set('servicio-recorridoSentido-vigenciaDesde[ge]', params.servicioRecorridoSentidoVigenciaDesdeGe.toString());
    if (params.servicioRecorridoSentidoVigenciaDesde != null) __params = __params.set('servicio-recorridoSentido-vigenciaDesde', params.servicioRecorridoSentidoVigenciaDesde.toString());
    if (params.servicioRecorridoSentidoSentidoIdNe != null) __params = __params.set('servicio-recorridoSentido-sentido-id[ne]', params.servicioRecorridoSentidoSentidoIdNe.toString());
    if (params.servicioRecorridoSentidoSentidoIdLt != null) __params = __params.set('servicio-recorridoSentido-sentido-id[lt]', params.servicioRecorridoSentidoSentidoIdLt.toString());
    if (params.servicioRecorridoSentidoSentidoIdLe != null) __params = __params.set('servicio-recorridoSentido-sentido-id[le]', params.servicioRecorridoSentidoSentidoIdLe.toString());
    if (params.servicioRecorridoSentidoSentidoIdGt != null) __params = __params.set('servicio-recorridoSentido-sentido-id[gt]', params.servicioRecorridoSentidoSentidoIdGt.toString());
    if (params.servicioRecorridoSentidoSentidoIdGe != null) __params = __params.set('servicio-recorridoSentido-sentido-id[ge]', params.servicioRecorridoSentidoSentidoIdGe.toString());
    if (params.servicioRecorridoSentidoSentidoId != null) __params = __params.set('servicio-recorridoSentido-sentido-id', params.servicioRecorridoSentidoSentidoId.toString());
    if (params.servicioRecorridoSentidoSentidoDescripcionNe != null) __params = __params.set('servicio-recorridoSentido-sentido-descripcion[ne]', params.servicioRecorridoSentidoSentidoDescripcionNe.toString());
    if (params.servicioRecorridoSentidoSentidoDescripcionLike != null) __params = __params.set('servicio-recorridoSentido-sentido-descripcion[like]', params.servicioRecorridoSentidoSentidoDescripcionLike.toString());
    if (params.servicioRecorridoSentidoSentidoDescripcion != null) __params = __params.set('servicio-recorridoSentido-sentido-descripcion', params.servicioRecorridoSentidoSentidoDescripcion.toString());
    if (params.servicioRecorridoSentidoSentidoActivo != null) __params = __params.set('servicio-recorridoSentido-sentido-activo', params.servicioRecorridoSentidoSentidoActivo.toString());
    if (params.servicioRecorridoSentidoSentidoAbrevNe != null) __params = __params.set('servicio-recorridoSentido-sentido-abrev[ne]', params.servicioRecorridoSentidoSentidoAbrevNe.toString());
    if (params.servicioRecorridoSentidoSentidoAbrevLike != null) __params = __params.set('servicio-recorridoSentido-sentido-abrev[like]', params.servicioRecorridoSentidoSentidoAbrevLike.toString());
    if (params.servicioRecorridoSentidoSentidoAbrev != null) __params = __params.set('servicio-recorridoSentido-sentido-abrev', params.servicioRecorridoSentidoSentidoAbrev.toString());
    if (params.servicioRecorridoSentidoRecorridoVinculacionCamineraNe != null) __params = __params.set('servicio-recorridoSentido-recorrido-vinculacionCaminera[ne]', params.servicioRecorridoSentidoRecorridoVinculacionCamineraNe.toString());
    if (params.servicioRecorridoSentidoRecorridoVinculacionCamineraLike != null) __params = __params.set('servicio-recorridoSentido-recorrido-vinculacionCaminera[like]', params.servicioRecorridoSentidoRecorridoVinculacionCamineraLike.toString());
    if (params.servicioRecorridoSentidoRecorridoVinculacionCaminera != null) __params = __params.set('servicio-recorridoSentido-recorrido-vinculacionCaminera', params.servicioRecorridoSentidoRecorridoVinculacionCaminera.toString());
    if (params.servicioRecorridoSentidoRecorridoVigenciaHastaNe != null) __params = __params.set('servicio-recorridoSentido-recorrido-vigenciaHasta[ne]', params.servicioRecorridoSentidoRecorridoVigenciaHastaNe.toString());
    if (params.servicioRecorridoSentidoRecorridoVigenciaHastaLt != null) __params = __params.set('servicio-recorridoSentido-recorrido-vigenciaHasta[lt]', params.servicioRecorridoSentidoRecorridoVigenciaHastaLt.toString());
    if (params.servicioRecorridoSentidoRecorridoVigenciaHastaLe != null) __params = __params.set('servicio-recorridoSentido-recorrido-vigenciaHasta[le]', params.servicioRecorridoSentidoRecorridoVigenciaHastaLe.toString());
    if (params.servicioRecorridoSentidoRecorridoVigenciaHastaGt != null) __params = __params.set('servicio-recorridoSentido-recorrido-vigenciaHasta[gt]', params.servicioRecorridoSentidoRecorridoVigenciaHastaGt.toString());
    if (params.servicioRecorridoSentidoRecorridoVigenciaHastaGe != null) __params = __params.set('servicio-recorridoSentido-recorrido-vigenciaHasta[ge]', params.servicioRecorridoSentidoRecorridoVigenciaHastaGe.toString());
    if (params.servicioRecorridoSentidoRecorridoVigenciaHasta != null) __params = __params.set('servicio-recorridoSentido-recorrido-vigenciaHasta', params.servicioRecorridoSentidoRecorridoVigenciaHasta.toString());
    if (params.servicioRecorridoSentidoRecorridoVigenciaDesdeNe != null) __params = __params.set('servicio-recorridoSentido-recorrido-vigenciaDesde[ne]', params.servicioRecorridoSentidoRecorridoVigenciaDesdeNe.toString());
    if (params.servicioRecorridoSentidoRecorridoVigenciaDesdeLt != null) __params = __params.set('servicio-recorridoSentido-recorrido-vigenciaDesde[lt]', params.servicioRecorridoSentidoRecorridoVigenciaDesdeLt.toString());
    if (params.servicioRecorridoSentidoRecorridoVigenciaDesdeLe != null) __params = __params.set('servicio-recorridoSentido-recorrido-vigenciaDesde[le]', params.servicioRecorridoSentidoRecorridoVigenciaDesdeLe.toString());
    if (params.servicioRecorridoSentidoRecorridoVigenciaDesdeGt != null) __params = __params.set('servicio-recorridoSentido-recorrido-vigenciaDesde[gt]', params.servicioRecorridoSentidoRecorridoVigenciaDesdeGt.toString());
    if (params.servicioRecorridoSentidoRecorridoVigenciaDesdeGe != null) __params = __params.set('servicio-recorridoSentido-recorrido-vigenciaDesde[ge]', params.servicioRecorridoSentidoRecorridoVigenciaDesdeGe.toString());
    if (params.servicioRecorridoSentidoRecorridoVigenciaDesde != null) __params = __params.set('servicio-recorridoSentido-recorrido-vigenciaDesde', params.servicioRecorridoSentidoRecorridoVigenciaDesde.toString());
    if (params.servicioRecorridoSentidoRecorridoTransitoNe != null) __params = __params.set('servicio-recorridoSentido-recorrido-transito[ne]', params.servicioRecorridoSentidoRecorridoTransitoNe.toString());
    if (params.servicioRecorridoSentidoRecorridoTransitoLike != null) __params = __params.set('servicio-recorridoSentido-recorrido-transito[like]', params.servicioRecorridoSentidoRecorridoTransitoLike.toString());
    if (params.servicioRecorridoSentidoRecorridoTransito != null) __params = __params.set('servicio-recorridoSentido-recorrido-transito', params.servicioRecorridoSentidoRecorridoTransito.toString());
    if (params.servicioRecorridoSentidoRecorridoSitioOrigenNe != null) __params = __params.set('servicio-recorridoSentido-recorrido-sitioOrigen[ne]', params.servicioRecorridoSentidoRecorridoSitioOrigenNe.toString());
    if (params.servicioRecorridoSentidoRecorridoSitioOrigenLike != null) __params = __params.set('servicio-recorridoSentido-recorrido-sitioOrigen[like]', params.servicioRecorridoSentidoRecorridoSitioOrigenLike.toString());
    if (params.servicioRecorridoSentidoRecorridoSitioOrigen != null) __params = __params.set('servicio-recorridoSentido-recorrido-sitioOrigen', params.servicioRecorridoSentidoRecorridoSitioOrigen.toString());
    if (params.servicioRecorridoSentidoRecorridoSitioDestinoNe != null) __params = __params.set('servicio-recorridoSentido-recorrido-sitioDestino[ne]', params.servicioRecorridoSentidoRecorridoSitioDestinoNe.toString());
    if (params.servicioRecorridoSentidoRecorridoSitioDestinoLike != null) __params = __params.set('servicio-recorridoSentido-recorrido-sitioDestino[like]', params.servicioRecorridoSentidoRecorridoSitioDestinoLike.toString());
    if (params.servicioRecorridoSentidoRecorridoSitioDestino != null) __params = __params.set('servicio-recorridoSentido-recorrido-sitioDestino', params.servicioRecorridoSentidoRecorridoSitioDestino.toString());
    if (params.servicioRecorridoSentidoRecorridoRecorridoNe != null) __params = __params.set('servicio-recorridoSentido-recorrido-recorrido[ne]', params.servicioRecorridoSentidoRecorridoRecorridoNe.toString());
    if (params.servicioRecorridoSentidoRecorridoRecorridoLike != null) __params = __params.set('servicio-recorridoSentido-recorrido-recorrido[like]', params.servicioRecorridoSentidoRecorridoRecorridoLike.toString());
    if (params.servicioRecorridoSentidoRecorridoRecorrido != null) __params = __params.set('servicio-recorridoSentido-recorrido-recorrido', params.servicioRecorridoSentidoRecorridoRecorrido.toString());
    if (params.servicioRecorridoSentidoRecorridoOrigenNe != null) __params = __params.set('servicio-recorridoSentido-recorrido-origen[ne]', params.servicioRecorridoSentidoRecorridoOrigenNe.toString());
    if (params.servicioRecorridoSentidoRecorridoOrigenLike != null) __params = __params.set('servicio-recorridoSentido-recorrido-origen[like]', params.servicioRecorridoSentidoRecorridoOrigenLike.toString());
    if (params.servicioRecorridoSentidoRecorridoOrigen != null) __params = __params.set('servicio-recorridoSentido-recorrido-origen', params.servicioRecorridoSentidoRecorridoOrigen.toString());
    if (params.servicioRecorridoSentidoRecorridoItinerarioVueltaNe != null) __params = __params.set('servicio-recorridoSentido-recorrido-itinerarioVuelta[ne]', params.servicioRecorridoSentidoRecorridoItinerarioVueltaNe.toString());
    if (params.servicioRecorridoSentidoRecorridoItinerarioVueltaLike != null) __params = __params.set('servicio-recorridoSentido-recorrido-itinerarioVuelta[like]', params.servicioRecorridoSentidoRecorridoItinerarioVueltaLike.toString());
    if (params.servicioRecorridoSentidoRecorridoItinerarioVuelta != null) __params = __params.set('servicio-recorridoSentido-recorrido-itinerarioVuelta', params.servicioRecorridoSentidoRecorridoItinerarioVuelta.toString());
    if (params.servicioRecorridoSentidoRecorridoItinerarioIdaNe != null) __params = __params.set('servicio-recorridoSentido-recorrido-itinerarioIda[ne]', params.servicioRecorridoSentidoRecorridoItinerarioIdaNe.toString());
    if (params.servicioRecorridoSentidoRecorridoItinerarioIdaLike != null) __params = __params.set('servicio-recorridoSentido-recorrido-itinerarioIda[like]', params.servicioRecorridoSentidoRecorridoItinerarioIdaLike.toString());
    if (params.servicioRecorridoSentidoRecorridoItinerarioIda != null) __params = __params.set('servicio-recorridoSentido-recorrido-itinerarioIda', params.servicioRecorridoSentidoRecorridoItinerarioIda.toString());
    if (params.servicioRecorridoSentidoRecorridoIdNe != null) __params = __params.set('servicio-recorridoSentido-recorrido-id[ne]', params.servicioRecorridoSentidoRecorridoIdNe.toString());
    if (params.servicioRecorridoSentidoRecorridoIdLt != null) __params = __params.set('servicio-recorridoSentido-recorrido-id[lt]', params.servicioRecorridoSentidoRecorridoIdLt.toString());
    if (params.servicioRecorridoSentidoRecorridoIdLe != null) __params = __params.set('servicio-recorridoSentido-recorrido-id[le]', params.servicioRecorridoSentidoRecorridoIdLe.toString());
    if (params.servicioRecorridoSentidoRecorridoIdGt != null) __params = __params.set('servicio-recorridoSentido-recorrido-id[gt]', params.servicioRecorridoSentidoRecorridoIdGt.toString());
    if (params.servicioRecorridoSentidoRecorridoIdGe != null) __params = __params.set('servicio-recorridoSentido-recorrido-id[ge]', params.servicioRecorridoSentidoRecorridoIdGe.toString());
    if (params.servicioRecorridoSentidoRecorridoId != null) __params = __params.set('servicio-recorridoSentido-recorrido-id', params.servicioRecorridoSentidoRecorridoId.toString());
    if (params.servicioRecorridoSentidoRecorridoDestinoNe != null) __params = __params.set('servicio-recorridoSentido-recorrido-destino[ne]', params.servicioRecorridoSentidoRecorridoDestinoNe.toString());
    if (params.servicioRecorridoSentidoRecorridoDestinoLike != null) __params = __params.set('servicio-recorridoSentido-recorrido-destino[like]', params.servicioRecorridoSentidoRecorridoDestinoLike.toString());
    if (params.servicioRecorridoSentidoRecorridoDestino != null) __params = __params.set('servicio-recorridoSentido-recorrido-destino', params.servicioRecorridoSentidoRecorridoDestino.toString());
    if (params.servicioRecorridoSentidoRecorridoAplicaGasoil != null) __params = __params.set('servicio-recorridoSentido-recorrido-aplicaGasoil', params.servicioRecorridoSentidoRecorridoAplicaGasoil.toString());
    if (params.servicioRecorridoSentidoOrigenNe != null) __params = __params.set('servicio-recorridoSentido-origen[ne]', params.servicioRecorridoSentidoOrigenNe.toString());
    if (params.servicioRecorridoSentidoOrigenLike != null) __params = __params.set('servicio-recorridoSentido-origen[like]', params.servicioRecorridoSentidoOrigenLike.toString());
    if (params.servicioRecorridoSentidoOrigen != null) __params = __params.set('servicio-recorridoSentido-origen', params.servicioRecorridoSentidoOrigen.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenUpdatedAtNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-updatedAt[ne]', params.servicioRecorridoSentidoLocalidadOrigenUpdatedAtNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenUpdatedAtLt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-updatedAt[lt]', params.servicioRecorridoSentidoLocalidadOrigenUpdatedAtLt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenUpdatedAtLe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-updatedAt[le]', params.servicioRecorridoSentidoLocalidadOrigenUpdatedAtLe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenUpdatedAtGt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-updatedAt[gt]', params.servicioRecorridoSentidoLocalidadOrigenUpdatedAtGt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenUpdatedAtGe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-updatedAt[ge]', params.servicioRecorridoSentidoLocalidadOrigenUpdatedAtGe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenUpdatedAt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-updatedAt', params.servicioRecorridoSentidoLocalidadOrigenUpdatedAt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-id[ne]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdLt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-id[lt]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdLt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdLe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-id[le]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdLe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdGt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-id[gt]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdGt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdGe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-id[ge]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdGe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisId != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-id', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisId.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisDescripcionNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-descripcion[ne]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisDescripcionNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisDescripcionLike != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-descripcion[like]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisDescripcionLike.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisDescripcion != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-descripcion', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisDescripcion.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisCodigoNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-codigo[ne]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisCodigoNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisCodigoLike != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-codigo[like]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisCodigoLike.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisCodigo != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-codigo', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisCodigo.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisActivo != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-activo', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisActivo.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisAbrevNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-abrev[ne]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisAbrevNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisAbrevLike != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-abrev[like]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisAbrevLike.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisAbrev != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-pais-abrev', params.servicioRecorridoSentidoLocalidadOrigenProvinciaPaisAbrev.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaIdNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-id[ne]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaIdNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaIdLt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-id[lt]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaIdLt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaIdLe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-id[le]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaIdLe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaIdGt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-id[gt]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaIdGt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaIdGe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-id[ge]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaIdGe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaId != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-id', params.servicioRecorridoSentidoLocalidadOrigenProvinciaId.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaDescripcionNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-descripcion[ne]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaDescripcionNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaDescripcionLike != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-descripcion[like]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaDescripcionLike.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaDescripcion != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-descripcion', params.servicioRecorridoSentidoLocalidadOrigenProvinciaDescripcion.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-codigo[ne]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoLike != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-codigo[like]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoLike.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoINDECNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-codigoINDEC[ne]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoINDECNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoINDECLike != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-codigoINDEC[like]', params.servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoINDECLike.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoINDEC != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-codigoINDEC', params.servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoINDEC.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaCodigo != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-codigo', params.servicioRecorridoSentidoLocalidadOrigenProvinciaCodigo.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenProvinciaActivo != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-provincia-activo', params.servicioRecorridoSentidoLocalidadOrigenProvinciaActivo.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenParada != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-parada', params.servicioRecorridoSentidoLocalidadOrigenParada.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenLonNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-lon[ne]', params.servicioRecorridoSentidoLocalidadOrigenLonNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenLonLt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-lon[lt]', params.servicioRecorridoSentidoLocalidadOrigenLonLt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenLonLe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-lon[le]', params.servicioRecorridoSentidoLocalidadOrigenLonLe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenLonGt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-lon[gt]', params.servicioRecorridoSentidoLocalidadOrigenLonGt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenLonGe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-lon[ge]', params.servicioRecorridoSentidoLocalidadOrigenLonGe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenLon != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-lon', params.servicioRecorridoSentidoLocalidadOrigenLon.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenLatNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-lat[ne]', params.servicioRecorridoSentidoLocalidadOrigenLatNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenLatLt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-lat[lt]', params.servicioRecorridoSentidoLocalidadOrigenLatLt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenLatLe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-lat[le]', params.servicioRecorridoSentidoLocalidadOrigenLatLe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenLatGt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-lat[gt]', params.servicioRecorridoSentidoLocalidadOrigenLatGt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenLatGe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-lat[ge]', params.servicioRecorridoSentidoLocalidadOrigenLatGe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenLat != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-lat', params.servicioRecorridoSentidoLocalidadOrigenLat.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenIdNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-id[ne]', params.servicioRecorridoSentidoLocalidadOrigenIdNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenIdLt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-id[lt]', params.servicioRecorridoSentidoLocalidadOrigenIdLt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenIdLe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-id[le]', params.servicioRecorridoSentidoLocalidadOrigenIdLe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenIdGt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-id[gt]', params.servicioRecorridoSentidoLocalidadOrigenIdGt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenIdGe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-id[ge]', params.servicioRecorridoSentidoLocalidadOrigenIdGe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenId != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-id', params.servicioRecorridoSentidoLocalidadOrigenId.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenFuenteNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-fuente[ne]', params.servicioRecorridoSentidoLocalidadOrigenFuenteNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenFuenteLike != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-fuente[like]', params.servicioRecorridoSentidoLocalidadOrigenFuenteLike.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenFuente != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-fuente', params.servicioRecorridoSentidoLocalidadOrigenFuente.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenDescripcionNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-descripcion[ne]', params.servicioRecorridoSentidoLocalidadOrigenDescripcionNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenDescripcionLike != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-descripcion[like]', params.servicioRecorridoSentidoLocalidadOrigenDescripcionLike.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenDescripcion != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-descripcion', params.servicioRecorridoSentidoLocalidadOrigenDescripcion.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenCreatedAtNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-createdAt[ne]', params.servicioRecorridoSentidoLocalidadOrigenCreatedAtNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenCreatedAtLt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-createdAt[lt]', params.servicioRecorridoSentidoLocalidadOrigenCreatedAtLt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenCreatedAtLe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-createdAt[le]', params.servicioRecorridoSentidoLocalidadOrigenCreatedAtLe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenCreatedAtGt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-createdAt[gt]', params.servicioRecorridoSentidoLocalidadOrigenCreatedAtGt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenCreatedAtGe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-createdAt[ge]', params.servicioRecorridoSentidoLocalidadOrigenCreatedAtGe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenCreatedAt != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-createdAt', params.servicioRecorridoSentidoLocalidadOrigenCreatedAt.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenCodigoINDECNe != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-codigoINDEC[ne]', params.servicioRecorridoSentidoLocalidadOrigenCodigoINDECNe.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenCodigoINDECLike != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-codigoINDEC[like]', params.servicioRecorridoSentidoLocalidadOrigenCodigoINDECLike.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenCodigoINDEC != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-codigoINDEC', params.servicioRecorridoSentidoLocalidadOrigenCodigoINDEC.toString());
    if (params.servicioRecorridoSentidoLocalidadOrigenActivo != null) __params = __params.set('servicio-recorridoSentido-localidadOrigen-activo', params.servicioRecorridoSentidoLocalidadOrigenActivo.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoUpdatedAtNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-updatedAt[ne]', params.servicioRecorridoSentidoLocalidadDestinoUpdatedAtNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoUpdatedAtLt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-updatedAt[lt]', params.servicioRecorridoSentidoLocalidadDestinoUpdatedAtLt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoUpdatedAtLe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-updatedAt[le]', params.servicioRecorridoSentidoLocalidadDestinoUpdatedAtLe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoUpdatedAtGt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-updatedAt[gt]', params.servicioRecorridoSentidoLocalidadDestinoUpdatedAtGt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoUpdatedAtGe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-updatedAt[ge]', params.servicioRecorridoSentidoLocalidadDestinoUpdatedAtGe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoUpdatedAt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-updatedAt', params.servicioRecorridoSentidoLocalidadDestinoUpdatedAt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-id[ne]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdLt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-id[lt]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdLt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdLe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-id[le]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdLe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdGt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-id[gt]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdGt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdGe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-id[ge]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdGe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisId != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-id', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisId.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisDescripcionNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-descripcion[ne]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisDescripcionNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisDescripcionLike != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-descripcion[like]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisDescripcionLike.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisDescripcion != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-descripcion', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisDescripcion.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisCodigoNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-codigo[ne]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisCodigoNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisCodigoLike != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-codigo[like]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisCodigoLike.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisCodigo != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-codigo', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisCodigo.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisActivo != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-activo', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisActivo.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisAbrevNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-abrev[ne]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisAbrevNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisAbrevLike != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-abrev[like]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisAbrevLike.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisAbrev != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-pais-abrev', params.servicioRecorridoSentidoLocalidadDestinoProvinciaPaisAbrev.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaIdNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-id[ne]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaIdNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaIdLt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-id[lt]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaIdLt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaIdLe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-id[le]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaIdLe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaIdGt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-id[gt]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaIdGt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaIdGe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-id[ge]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaIdGe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaId != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-id', params.servicioRecorridoSentidoLocalidadDestinoProvinciaId.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaDescripcionNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-descripcion[ne]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaDescripcionNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaDescripcionLike != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-descripcion[like]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaDescripcionLike.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaDescripcion != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-descripcion', params.servicioRecorridoSentidoLocalidadDestinoProvinciaDescripcion.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-codigo[ne]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoLike != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-codigo[like]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoLike.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoINDECNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-codigoINDEC[ne]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoINDECNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoINDECLike != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-codigoINDEC[like]', params.servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoINDECLike.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoINDEC != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-codigoINDEC', params.servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoINDEC.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaCodigo != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-codigo', params.servicioRecorridoSentidoLocalidadDestinoProvinciaCodigo.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoProvinciaActivo != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-provincia-activo', params.servicioRecorridoSentidoLocalidadDestinoProvinciaActivo.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoParada != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-parada', params.servicioRecorridoSentidoLocalidadDestinoParada.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoLonNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-lon[ne]', params.servicioRecorridoSentidoLocalidadDestinoLonNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoLonLt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-lon[lt]', params.servicioRecorridoSentidoLocalidadDestinoLonLt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoLonLe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-lon[le]', params.servicioRecorridoSentidoLocalidadDestinoLonLe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoLonGt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-lon[gt]', params.servicioRecorridoSentidoLocalidadDestinoLonGt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoLonGe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-lon[ge]', params.servicioRecorridoSentidoLocalidadDestinoLonGe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoLon != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-lon', params.servicioRecorridoSentidoLocalidadDestinoLon.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoLatNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-lat[ne]', params.servicioRecorridoSentidoLocalidadDestinoLatNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoLatLt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-lat[lt]', params.servicioRecorridoSentidoLocalidadDestinoLatLt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoLatLe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-lat[le]', params.servicioRecorridoSentidoLocalidadDestinoLatLe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoLatGt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-lat[gt]', params.servicioRecorridoSentidoLocalidadDestinoLatGt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoLatGe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-lat[ge]', params.servicioRecorridoSentidoLocalidadDestinoLatGe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoLat != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-lat', params.servicioRecorridoSentidoLocalidadDestinoLat.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoIdNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-id[ne]', params.servicioRecorridoSentidoLocalidadDestinoIdNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoIdLt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-id[lt]', params.servicioRecorridoSentidoLocalidadDestinoIdLt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoIdLe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-id[le]', params.servicioRecorridoSentidoLocalidadDestinoIdLe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoIdGt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-id[gt]', params.servicioRecorridoSentidoLocalidadDestinoIdGt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoIdGe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-id[ge]', params.servicioRecorridoSentidoLocalidadDestinoIdGe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoId != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-id', params.servicioRecorridoSentidoLocalidadDestinoId.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoFuenteNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-fuente[ne]', params.servicioRecorridoSentidoLocalidadDestinoFuenteNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoFuenteLike != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-fuente[like]', params.servicioRecorridoSentidoLocalidadDestinoFuenteLike.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoFuente != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-fuente', params.servicioRecorridoSentidoLocalidadDestinoFuente.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoDescripcionNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-descripcion[ne]', params.servicioRecorridoSentidoLocalidadDestinoDescripcionNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoDescripcionLike != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-descripcion[like]', params.servicioRecorridoSentidoLocalidadDestinoDescripcionLike.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoDescripcion != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-descripcion', params.servicioRecorridoSentidoLocalidadDestinoDescripcion.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoCreatedAtNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-createdAt[ne]', params.servicioRecorridoSentidoLocalidadDestinoCreatedAtNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoCreatedAtLt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-createdAt[lt]', params.servicioRecorridoSentidoLocalidadDestinoCreatedAtLt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoCreatedAtLe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-createdAt[le]', params.servicioRecorridoSentidoLocalidadDestinoCreatedAtLe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoCreatedAtGt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-createdAt[gt]', params.servicioRecorridoSentidoLocalidadDestinoCreatedAtGt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoCreatedAtGe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-createdAt[ge]', params.servicioRecorridoSentidoLocalidadDestinoCreatedAtGe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoCreatedAt != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-createdAt', params.servicioRecorridoSentidoLocalidadDestinoCreatedAt.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoCodigoINDECNe != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-codigoINDEC[ne]', params.servicioRecorridoSentidoLocalidadDestinoCodigoINDECNe.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoCodigoINDECLike != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-codigoINDEC[like]', params.servicioRecorridoSentidoLocalidadDestinoCodigoINDECLike.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoCodigoINDEC != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-codigoINDEC', params.servicioRecorridoSentidoLocalidadDestinoCodigoINDEC.toString());
    if (params.servicioRecorridoSentidoLocalidadDestinoActivo != null) __params = __params.set('servicio-recorridoSentido-localidadDestino-activo', params.servicioRecorridoSentidoLocalidadDestinoActivo.toString());
    if (params.servicioRecorridoSentidoItinerarioNe != null) __params = __params.set('servicio-recorridoSentido-itinerario[ne]', params.servicioRecorridoSentidoItinerarioNe.toString());
    if (params.servicioRecorridoSentidoItinerarioLike != null) __params = __params.set('servicio-recorridoSentido-itinerario[like]', params.servicioRecorridoSentidoItinerarioLike.toString());
    if (params.servicioRecorridoSentidoItinerario != null) __params = __params.set('servicio-recorridoSentido-itinerario', params.servicioRecorridoSentidoItinerario.toString());
    if (params.servicioRecorridoSentidoIdNe != null) __params = __params.set('servicio-recorridoSentido-id[ne]', params.servicioRecorridoSentidoIdNe.toString());
    if (params.servicioRecorridoSentidoIdLt != null) __params = __params.set('servicio-recorridoSentido-id[lt]', params.servicioRecorridoSentidoIdLt.toString());
    if (params.servicioRecorridoSentidoIdLe != null) __params = __params.set('servicio-recorridoSentido-id[le]', params.servicioRecorridoSentidoIdLe.toString());
    if (params.servicioRecorridoSentidoIdGt != null) __params = __params.set('servicio-recorridoSentido-id[gt]', params.servicioRecorridoSentidoIdGt.toString());
    if (params.servicioRecorridoSentidoIdGe != null) __params = __params.set('servicio-recorridoSentido-id[ge]', params.servicioRecorridoSentidoIdGe.toString());
    if (params.servicioRecorridoSentidoId != null) __params = __params.set('servicio-recorridoSentido-id', params.servicioRecorridoSentidoId.toString());
    if (params.servicioRecorridoSentidoDestinoNe != null) __params = __params.set('servicio-recorridoSentido-destino[ne]', params.servicioRecorridoSentidoDestinoNe.toString());
    if (params.servicioRecorridoSentidoDestinoLike != null) __params = __params.set('servicio-recorridoSentido-destino[like]', params.servicioRecorridoSentidoDestinoLike.toString());
    if (params.servicioRecorridoSentidoDestino != null) __params = __params.set('servicio-recorridoSentido-destino', params.servicioRecorridoSentidoDestino.toString());
    if (params.servicioRecorridoVinculacionCamineraNe != null) __params = __params.set('servicio-recorrido-vinculacionCaminera[ne]', params.servicioRecorridoVinculacionCamineraNe.toString());
    if (params.servicioRecorridoVinculacionCamineraLike != null) __params = __params.set('servicio-recorrido-vinculacionCaminera[like]', params.servicioRecorridoVinculacionCamineraLike.toString());
    if (params.servicioRecorridoVinculacionCaminera != null) __params = __params.set('servicio-recorrido-vinculacionCaminera', params.servicioRecorridoVinculacionCaminera.toString());
    if (params.servicioRecorridoVigenciaHastaNe != null) __params = __params.set('servicio-recorrido-vigenciaHasta[ne]', params.servicioRecorridoVigenciaHastaNe.toString());
    if (params.servicioRecorridoVigenciaHastaLt != null) __params = __params.set('servicio-recorrido-vigenciaHasta[lt]', params.servicioRecorridoVigenciaHastaLt.toString());
    if (params.servicioRecorridoVigenciaHastaLe != null) __params = __params.set('servicio-recorrido-vigenciaHasta[le]', params.servicioRecorridoVigenciaHastaLe.toString());
    if (params.servicioRecorridoVigenciaHastaGt != null) __params = __params.set('servicio-recorrido-vigenciaHasta[gt]', params.servicioRecorridoVigenciaHastaGt.toString());
    if (params.servicioRecorridoVigenciaHastaGe != null) __params = __params.set('servicio-recorrido-vigenciaHasta[ge]', params.servicioRecorridoVigenciaHastaGe.toString());
    if (params.servicioRecorridoVigenciaHasta != null) __params = __params.set('servicio-recorrido-vigenciaHasta', params.servicioRecorridoVigenciaHasta.toString());
    if (params.servicioRecorridoVigenciaDesdeNe != null) __params = __params.set('servicio-recorrido-vigenciaDesde[ne]', params.servicioRecorridoVigenciaDesdeNe.toString());
    if (params.servicioRecorridoVigenciaDesdeLt != null) __params = __params.set('servicio-recorrido-vigenciaDesde[lt]', params.servicioRecorridoVigenciaDesdeLt.toString());
    if (params.servicioRecorridoVigenciaDesdeLe != null) __params = __params.set('servicio-recorrido-vigenciaDesde[le]', params.servicioRecorridoVigenciaDesdeLe.toString());
    if (params.servicioRecorridoVigenciaDesdeGt != null) __params = __params.set('servicio-recorrido-vigenciaDesde[gt]', params.servicioRecorridoVigenciaDesdeGt.toString());
    if (params.servicioRecorridoVigenciaDesdeGe != null) __params = __params.set('servicio-recorrido-vigenciaDesde[ge]', params.servicioRecorridoVigenciaDesdeGe.toString());
    if (params.servicioRecorridoVigenciaDesde != null) __params = __params.set('servicio-recorrido-vigenciaDesde', params.servicioRecorridoVigenciaDesde.toString());
    if (params.servicioRecorridoTransitoNe != null) __params = __params.set('servicio-recorrido-transito[ne]', params.servicioRecorridoTransitoNe.toString());
    if (params.servicioRecorridoTransitoLike != null) __params = __params.set('servicio-recorrido-transito[like]', params.servicioRecorridoTransitoLike.toString());
    if (params.servicioRecorridoTransito != null) __params = __params.set('servicio-recorrido-transito', params.servicioRecorridoTransito.toString());
    if (params.servicioRecorridoSitioOrigenNe != null) __params = __params.set('servicio-recorrido-sitioOrigen[ne]', params.servicioRecorridoSitioOrigenNe.toString());
    if (params.servicioRecorridoSitioOrigenLike != null) __params = __params.set('servicio-recorrido-sitioOrigen[like]', params.servicioRecorridoSitioOrigenLike.toString());
    if (params.servicioRecorridoSitioOrigen != null) __params = __params.set('servicio-recorrido-sitioOrigen', params.servicioRecorridoSitioOrigen.toString());
    if (params.servicioRecorridoSitioDestinoNe != null) __params = __params.set('servicio-recorrido-sitioDestino[ne]', params.servicioRecorridoSitioDestinoNe.toString());
    if (params.servicioRecorridoSitioDestinoLike != null) __params = __params.set('servicio-recorrido-sitioDestino[like]', params.servicioRecorridoSitioDestinoLike.toString());
    if (params.servicioRecorridoSitioDestino != null) __params = __params.set('servicio-recorrido-sitioDestino', params.servicioRecorridoSitioDestino.toString());
    if (params.servicioRecorridoRecorridoNe != null) __params = __params.set('servicio-recorrido-recorrido[ne]', params.servicioRecorridoRecorridoNe.toString());
    if (params.servicioRecorridoRecorridoLike != null) __params = __params.set('servicio-recorrido-recorrido[like]', params.servicioRecorridoRecorridoLike.toString());
    if (params.servicioRecorridoRecorrido != null) __params = __params.set('servicio-recorrido-recorrido', params.servicioRecorridoRecorrido.toString());
    if (params.servicioRecorridoOrigenNe != null) __params = __params.set('servicio-recorrido-origen[ne]', params.servicioRecorridoOrigenNe.toString());
    if (params.servicioRecorridoOrigenLike != null) __params = __params.set('servicio-recorrido-origen[like]', params.servicioRecorridoOrigenLike.toString());
    if (params.servicioRecorridoOrigen != null) __params = __params.set('servicio-recorrido-origen', params.servicioRecorridoOrigen.toString());
    if (params.servicioRecorridoItinerarioVueltaNe != null) __params = __params.set('servicio-recorrido-itinerarioVuelta[ne]', params.servicioRecorridoItinerarioVueltaNe.toString());
    if (params.servicioRecorridoItinerarioVueltaLike != null) __params = __params.set('servicio-recorrido-itinerarioVuelta[like]', params.servicioRecorridoItinerarioVueltaLike.toString());
    if (params.servicioRecorridoItinerarioVuelta != null) __params = __params.set('servicio-recorrido-itinerarioVuelta', params.servicioRecorridoItinerarioVuelta.toString());
    if (params.servicioRecorridoItinerarioIdaNe != null) __params = __params.set('servicio-recorrido-itinerarioIda[ne]', params.servicioRecorridoItinerarioIdaNe.toString());
    if (params.servicioRecorridoItinerarioIdaLike != null) __params = __params.set('servicio-recorrido-itinerarioIda[like]', params.servicioRecorridoItinerarioIdaLike.toString());
    if (params.servicioRecorridoItinerarioIda != null) __params = __params.set('servicio-recorrido-itinerarioIda', params.servicioRecorridoItinerarioIda.toString());
    if (params.servicioRecorridoIdNe != null) __params = __params.set('servicio-recorrido-id[ne]', params.servicioRecorridoIdNe.toString());
    if (params.servicioRecorridoIdLt != null) __params = __params.set('servicio-recorrido-id[lt]', params.servicioRecorridoIdLt.toString());
    if (params.servicioRecorridoIdLe != null) __params = __params.set('servicio-recorrido-id[le]', params.servicioRecorridoIdLe.toString());
    if (params.servicioRecorridoIdGt != null) __params = __params.set('servicio-recorrido-id[gt]', params.servicioRecorridoIdGt.toString());
    if (params.servicioRecorridoIdGe != null) __params = __params.set('servicio-recorrido-id[ge]', params.servicioRecorridoIdGe.toString());
    if (params.servicioRecorridoId != null) __params = __params.set('servicio-recorrido-id', params.servicioRecorridoId.toString());
    if (params.servicioRecorridoDestinoNe != null) __params = __params.set('servicio-recorrido-destino[ne]', params.servicioRecorridoDestinoNe.toString());
    if (params.servicioRecorridoDestinoLike != null) __params = __params.set('servicio-recorrido-destino[like]', params.servicioRecorridoDestinoLike.toString());
    if (params.servicioRecorridoDestino != null) __params = __params.set('servicio-recorrido-destino', params.servicioRecorridoDestino.toString());
    if (params.servicioRecorridoAplicaGasoil != null) __params = __params.set('servicio-recorrido-aplicaGasoil', params.servicioRecorridoAplicaGasoil.toString());
    if (params.servicioOperadorVigenciaHastaNe != null) __params = __params.set('servicio-operador-vigenciaHasta[ne]', params.servicioOperadorVigenciaHastaNe.toString());
    if (params.servicioOperadorVigenciaHastaLt != null) __params = __params.set('servicio-operador-vigenciaHasta[lt]', params.servicioOperadorVigenciaHastaLt.toString());
    if (params.servicioOperadorVigenciaHastaLe != null) __params = __params.set('servicio-operador-vigenciaHasta[le]', params.servicioOperadorVigenciaHastaLe.toString());
    if (params.servicioOperadorVigenciaHastaGt != null) __params = __params.set('servicio-operador-vigenciaHasta[gt]', params.servicioOperadorVigenciaHastaGt.toString());
    if (params.servicioOperadorVigenciaHastaGe != null) __params = __params.set('servicio-operador-vigenciaHasta[ge]', params.servicioOperadorVigenciaHastaGe.toString());
    if (params.servicioOperadorVigenciaHasta != null) __params = __params.set('servicio-operador-vigenciaHasta', params.servicioOperadorVigenciaHasta.toString());
    if (params.servicioOperadorVigenciaDesdeNe != null) __params = __params.set('servicio-operador-vigenciaDesde[ne]', params.servicioOperadorVigenciaDesdeNe.toString());
    if (params.servicioOperadorVigenciaDesdeLt != null) __params = __params.set('servicio-operador-vigenciaDesde[lt]', params.servicioOperadorVigenciaDesdeLt.toString());
    if (params.servicioOperadorVigenciaDesdeLe != null) __params = __params.set('servicio-operador-vigenciaDesde[le]', params.servicioOperadorVigenciaDesdeLe.toString());
    if (params.servicioOperadorVigenciaDesdeGt != null) __params = __params.set('servicio-operador-vigenciaDesde[gt]', params.servicioOperadorVigenciaDesdeGt.toString());
    if (params.servicioOperadorVigenciaDesdeGe != null) __params = __params.set('servicio-operador-vigenciaDesde[ge]', params.servicioOperadorVigenciaDesdeGe.toString());
    if (params.servicioOperadorVigenciaDesde != null) __params = __params.set('servicio-operador-vigenciaDesde', params.servicioOperadorVigenciaDesde.toString());
    if (params.servicioOperadorLocalidadOrigenUpdatedAtNe != null) __params = __params.set('servicio-operador-localidadOrigen-updatedAt[ne]', params.servicioOperadorLocalidadOrigenUpdatedAtNe.toString());
    if (params.servicioOperadorLocalidadOrigenUpdatedAtLt != null) __params = __params.set('servicio-operador-localidadOrigen-updatedAt[lt]', params.servicioOperadorLocalidadOrigenUpdatedAtLt.toString());
    if (params.servicioOperadorLocalidadOrigenUpdatedAtLe != null) __params = __params.set('servicio-operador-localidadOrigen-updatedAt[le]', params.servicioOperadorLocalidadOrigenUpdatedAtLe.toString());
    if (params.servicioOperadorLocalidadOrigenUpdatedAtGt != null) __params = __params.set('servicio-operador-localidadOrigen-updatedAt[gt]', params.servicioOperadorLocalidadOrigenUpdatedAtGt.toString());
    if (params.servicioOperadorLocalidadOrigenUpdatedAtGe != null) __params = __params.set('servicio-operador-localidadOrigen-updatedAt[ge]', params.servicioOperadorLocalidadOrigenUpdatedAtGe.toString());
    if (params.servicioOperadorLocalidadOrigenUpdatedAt != null) __params = __params.set('servicio-operador-localidadOrigen-updatedAt', params.servicioOperadorLocalidadOrigenUpdatedAt.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisIdNe != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-id[ne]', params.servicioOperadorLocalidadOrigenProvinciaPaisIdNe.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisIdLt != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-id[lt]', params.servicioOperadorLocalidadOrigenProvinciaPaisIdLt.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisIdLe != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-id[le]', params.servicioOperadorLocalidadOrigenProvinciaPaisIdLe.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisIdGt != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-id[gt]', params.servicioOperadorLocalidadOrigenProvinciaPaisIdGt.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisIdGe != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-id[ge]', params.servicioOperadorLocalidadOrigenProvinciaPaisIdGe.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisId != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-id', params.servicioOperadorLocalidadOrigenProvinciaPaisId.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisDescripcionNe != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-descripcion[ne]', params.servicioOperadorLocalidadOrigenProvinciaPaisDescripcionNe.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisDescripcionLike != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-descripcion[like]', params.servicioOperadorLocalidadOrigenProvinciaPaisDescripcionLike.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisDescripcion != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-descripcion', params.servicioOperadorLocalidadOrigenProvinciaPaisDescripcion.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisCodigoNe != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-codigo[ne]', params.servicioOperadorLocalidadOrigenProvinciaPaisCodigoNe.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisCodigoLike != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-codigo[like]', params.servicioOperadorLocalidadOrigenProvinciaPaisCodigoLike.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisCodigo != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-codigo', params.servicioOperadorLocalidadOrigenProvinciaPaisCodigo.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisActivo != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-activo', params.servicioOperadorLocalidadOrigenProvinciaPaisActivo.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisAbrevNe != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-abrev[ne]', params.servicioOperadorLocalidadOrigenProvinciaPaisAbrevNe.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisAbrevLike != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-abrev[like]', params.servicioOperadorLocalidadOrigenProvinciaPaisAbrevLike.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaPaisAbrev != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-pais-abrev', params.servicioOperadorLocalidadOrigenProvinciaPaisAbrev.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaIdNe != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-id[ne]', params.servicioOperadorLocalidadOrigenProvinciaIdNe.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaIdLt != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-id[lt]', params.servicioOperadorLocalidadOrigenProvinciaIdLt.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaIdLe != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-id[le]', params.servicioOperadorLocalidadOrigenProvinciaIdLe.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaIdGt != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-id[gt]', params.servicioOperadorLocalidadOrigenProvinciaIdGt.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaIdGe != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-id[ge]', params.servicioOperadorLocalidadOrigenProvinciaIdGe.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaId != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-id', params.servicioOperadorLocalidadOrigenProvinciaId.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaDescripcionNe != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-descripcion[ne]', params.servicioOperadorLocalidadOrigenProvinciaDescripcionNe.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaDescripcionLike != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-descripcion[like]', params.servicioOperadorLocalidadOrigenProvinciaDescripcionLike.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaDescripcion != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-descripcion', params.servicioOperadorLocalidadOrigenProvinciaDescripcion.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaCodigoNe != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-codigo[ne]', params.servicioOperadorLocalidadOrigenProvinciaCodigoNe.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaCodigoLike != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-codigo[like]', params.servicioOperadorLocalidadOrigenProvinciaCodigoLike.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaCodigoINDECNe != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-codigoINDEC[ne]', params.servicioOperadorLocalidadOrigenProvinciaCodigoINDECNe.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaCodigoINDECLike != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-codigoINDEC[like]', params.servicioOperadorLocalidadOrigenProvinciaCodigoINDECLike.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaCodigoINDEC != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-codigoINDEC', params.servicioOperadorLocalidadOrigenProvinciaCodigoINDEC.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaCodigo != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-codigo', params.servicioOperadorLocalidadOrigenProvinciaCodigo.toString());
    if (params.servicioOperadorLocalidadOrigenProvinciaActivo != null) __params = __params.set('servicio-operador-localidadOrigen-provincia-activo', params.servicioOperadorLocalidadOrigenProvinciaActivo.toString());
    if (params.servicioOperadorLocalidadOrigenParada != null) __params = __params.set('servicio-operador-localidadOrigen-parada', params.servicioOperadorLocalidadOrigenParada.toString());
    if (params.servicioOperadorLocalidadOrigenLonNe != null) __params = __params.set('servicio-operador-localidadOrigen-lon[ne]', params.servicioOperadorLocalidadOrigenLonNe.toString());
    if (params.servicioOperadorLocalidadOrigenLonLt != null) __params = __params.set('servicio-operador-localidadOrigen-lon[lt]', params.servicioOperadorLocalidadOrigenLonLt.toString());
    if (params.servicioOperadorLocalidadOrigenLonLe != null) __params = __params.set('servicio-operador-localidadOrigen-lon[le]', params.servicioOperadorLocalidadOrigenLonLe.toString());
    if (params.servicioOperadorLocalidadOrigenLonGt != null) __params = __params.set('servicio-operador-localidadOrigen-lon[gt]', params.servicioOperadorLocalidadOrigenLonGt.toString());
    if (params.servicioOperadorLocalidadOrigenLonGe != null) __params = __params.set('servicio-operador-localidadOrigen-lon[ge]', params.servicioOperadorLocalidadOrigenLonGe.toString());
    if (params.servicioOperadorLocalidadOrigenLon != null) __params = __params.set('servicio-operador-localidadOrigen-lon', params.servicioOperadorLocalidadOrigenLon.toString());
    if (params.servicioOperadorLocalidadOrigenLatNe != null) __params = __params.set('servicio-operador-localidadOrigen-lat[ne]', params.servicioOperadorLocalidadOrigenLatNe.toString());
    if (params.servicioOperadorLocalidadOrigenLatLt != null) __params = __params.set('servicio-operador-localidadOrigen-lat[lt]', params.servicioOperadorLocalidadOrigenLatLt.toString());
    if (params.servicioOperadorLocalidadOrigenLatLe != null) __params = __params.set('servicio-operador-localidadOrigen-lat[le]', params.servicioOperadorLocalidadOrigenLatLe.toString());
    if (params.servicioOperadorLocalidadOrigenLatGt != null) __params = __params.set('servicio-operador-localidadOrigen-lat[gt]', params.servicioOperadorLocalidadOrigenLatGt.toString());
    if (params.servicioOperadorLocalidadOrigenLatGe != null) __params = __params.set('servicio-operador-localidadOrigen-lat[ge]', params.servicioOperadorLocalidadOrigenLatGe.toString());
    if (params.servicioOperadorLocalidadOrigenLat != null) __params = __params.set('servicio-operador-localidadOrigen-lat', params.servicioOperadorLocalidadOrigenLat.toString());
    if (params.servicioOperadorLocalidadOrigenIdNe != null) __params = __params.set('servicio-operador-localidadOrigen-id[ne]', params.servicioOperadorLocalidadOrigenIdNe.toString());
    if (params.servicioOperadorLocalidadOrigenIdLt != null) __params = __params.set('servicio-operador-localidadOrigen-id[lt]', params.servicioOperadorLocalidadOrigenIdLt.toString());
    if (params.servicioOperadorLocalidadOrigenIdLe != null) __params = __params.set('servicio-operador-localidadOrigen-id[le]', params.servicioOperadorLocalidadOrigenIdLe.toString());
    if (params.servicioOperadorLocalidadOrigenIdGt != null) __params = __params.set('servicio-operador-localidadOrigen-id[gt]', params.servicioOperadorLocalidadOrigenIdGt.toString());
    if (params.servicioOperadorLocalidadOrigenIdGe != null) __params = __params.set('servicio-operador-localidadOrigen-id[ge]', params.servicioOperadorLocalidadOrigenIdGe.toString());
    if (params.servicioOperadorLocalidadOrigenId != null) __params = __params.set('servicio-operador-localidadOrigen-id', params.servicioOperadorLocalidadOrigenId.toString());
    if (params.servicioOperadorLocalidadOrigenFuenteNe != null) __params = __params.set('servicio-operador-localidadOrigen-fuente[ne]', params.servicioOperadorLocalidadOrigenFuenteNe.toString());
    if (params.servicioOperadorLocalidadOrigenFuenteLike != null) __params = __params.set('servicio-operador-localidadOrigen-fuente[like]', params.servicioOperadorLocalidadOrigenFuenteLike.toString());
    if (params.servicioOperadorLocalidadOrigenFuente != null) __params = __params.set('servicio-operador-localidadOrigen-fuente', params.servicioOperadorLocalidadOrigenFuente.toString());
    if (params.servicioOperadorLocalidadOrigenDescripcionNe != null) __params = __params.set('servicio-operador-localidadOrigen-descripcion[ne]', params.servicioOperadorLocalidadOrigenDescripcionNe.toString());
    if (params.servicioOperadorLocalidadOrigenDescripcionLike != null) __params = __params.set('servicio-operador-localidadOrigen-descripcion[like]', params.servicioOperadorLocalidadOrigenDescripcionLike.toString());
    if (params.servicioOperadorLocalidadOrigenDescripcion != null) __params = __params.set('servicio-operador-localidadOrigen-descripcion', params.servicioOperadorLocalidadOrigenDescripcion.toString());
    if (params.servicioOperadorLocalidadOrigenCreatedAtNe != null) __params = __params.set('servicio-operador-localidadOrigen-createdAt[ne]', params.servicioOperadorLocalidadOrigenCreatedAtNe.toString());
    if (params.servicioOperadorLocalidadOrigenCreatedAtLt != null) __params = __params.set('servicio-operador-localidadOrigen-createdAt[lt]', params.servicioOperadorLocalidadOrigenCreatedAtLt.toString());
    if (params.servicioOperadorLocalidadOrigenCreatedAtLe != null) __params = __params.set('servicio-operador-localidadOrigen-createdAt[le]', params.servicioOperadorLocalidadOrigenCreatedAtLe.toString());
    if (params.servicioOperadorLocalidadOrigenCreatedAtGt != null) __params = __params.set('servicio-operador-localidadOrigen-createdAt[gt]', params.servicioOperadorLocalidadOrigenCreatedAtGt.toString());
    if (params.servicioOperadorLocalidadOrigenCreatedAtGe != null) __params = __params.set('servicio-operador-localidadOrigen-createdAt[ge]', params.servicioOperadorLocalidadOrigenCreatedAtGe.toString());
    if (params.servicioOperadorLocalidadOrigenCreatedAt != null) __params = __params.set('servicio-operador-localidadOrigen-createdAt', params.servicioOperadorLocalidadOrigenCreatedAt.toString());
    if (params.servicioOperadorLocalidadOrigenCodigoINDECNe != null) __params = __params.set('servicio-operador-localidadOrigen-codigoINDEC[ne]', params.servicioOperadorLocalidadOrigenCodigoINDECNe.toString());
    if (params.servicioOperadorLocalidadOrigenCodigoINDECLike != null) __params = __params.set('servicio-operador-localidadOrigen-codigoINDEC[like]', params.servicioOperadorLocalidadOrigenCodigoINDECLike.toString());
    if (params.servicioOperadorLocalidadOrigenCodigoINDEC != null) __params = __params.set('servicio-operador-localidadOrigen-codigoINDEC', params.servicioOperadorLocalidadOrigenCodigoINDEC.toString());
    if (params.servicioOperadorLocalidadOrigenActivo != null) __params = __params.set('servicio-operador-localidadOrigen-activo', params.servicioOperadorLocalidadOrigenActivo.toString());
    if (params.servicioOperadorLocalidadDestinoUpdatedAtNe != null) __params = __params.set('servicio-operador-localidadDestino-updatedAt[ne]', params.servicioOperadorLocalidadDestinoUpdatedAtNe.toString());
    if (params.servicioOperadorLocalidadDestinoUpdatedAtLt != null) __params = __params.set('servicio-operador-localidadDestino-updatedAt[lt]', params.servicioOperadorLocalidadDestinoUpdatedAtLt.toString());
    if (params.servicioOperadorLocalidadDestinoUpdatedAtLe != null) __params = __params.set('servicio-operador-localidadDestino-updatedAt[le]', params.servicioOperadorLocalidadDestinoUpdatedAtLe.toString());
    if (params.servicioOperadorLocalidadDestinoUpdatedAtGt != null) __params = __params.set('servicio-operador-localidadDestino-updatedAt[gt]', params.servicioOperadorLocalidadDestinoUpdatedAtGt.toString());
    if (params.servicioOperadorLocalidadDestinoUpdatedAtGe != null) __params = __params.set('servicio-operador-localidadDestino-updatedAt[ge]', params.servicioOperadorLocalidadDestinoUpdatedAtGe.toString());
    if (params.servicioOperadorLocalidadDestinoUpdatedAt != null) __params = __params.set('servicio-operador-localidadDestino-updatedAt', params.servicioOperadorLocalidadDestinoUpdatedAt.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisIdNe != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-id[ne]', params.servicioOperadorLocalidadDestinoProvinciaPaisIdNe.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisIdLt != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-id[lt]', params.servicioOperadorLocalidadDestinoProvinciaPaisIdLt.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisIdLe != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-id[le]', params.servicioOperadorLocalidadDestinoProvinciaPaisIdLe.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisIdGt != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-id[gt]', params.servicioOperadorLocalidadDestinoProvinciaPaisIdGt.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisIdGe != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-id[ge]', params.servicioOperadorLocalidadDestinoProvinciaPaisIdGe.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisId != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-id', params.servicioOperadorLocalidadDestinoProvinciaPaisId.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisDescripcionNe != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-descripcion[ne]', params.servicioOperadorLocalidadDestinoProvinciaPaisDescripcionNe.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisDescripcionLike != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-descripcion[like]', params.servicioOperadorLocalidadDestinoProvinciaPaisDescripcionLike.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisDescripcion != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-descripcion', params.servicioOperadorLocalidadDestinoProvinciaPaisDescripcion.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisCodigoNe != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-codigo[ne]', params.servicioOperadorLocalidadDestinoProvinciaPaisCodigoNe.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisCodigoLike != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-codigo[like]', params.servicioOperadorLocalidadDestinoProvinciaPaisCodigoLike.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisCodigo != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-codigo', params.servicioOperadorLocalidadDestinoProvinciaPaisCodigo.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisActivo != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-activo', params.servicioOperadorLocalidadDestinoProvinciaPaisActivo.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisAbrevNe != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-abrev[ne]', params.servicioOperadorLocalidadDestinoProvinciaPaisAbrevNe.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisAbrevLike != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-abrev[like]', params.servicioOperadorLocalidadDestinoProvinciaPaisAbrevLike.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaPaisAbrev != null) __params = __params.set('servicio-operador-localidadDestino-provincia-pais-abrev', params.servicioOperadorLocalidadDestinoProvinciaPaisAbrev.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaIdNe != null) __params = __params.set('servicio-operador-localidadDestino-provincia-id[ne]', params.servicioOperadorLocalidadDestinoProvinciaIdNe.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaIdLt != null) __params = __params.set('servicio-operador-localidadDestino-provincia-id[lt]', params.servicioOperadorLocalidadDestinoProvinciaIdLt.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaIdLe != null) __params = __params.set('servicio-operador-localidadDestino-provincia-id[le]', params.servicioOperadorLocalidadDestinoProvinciaIdLe.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaIdGt != null) __params = __params.set('servicio-operador-localidadDestino-provincia-id[gt]', params.servicioOperadorLocalidadDestinoProvinciaIdGt.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaIdGe != null) __params = __params.set('servicio-operador-localidadDestino-provincia-id[ge]', params.servicioOperadorLocalidadDestinoProvinciaIdGe.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaId != null) __params = __params.set('servicio-operador-localidadDestino-provincia-id', params.servicioOperadorLocalidadDestinoProvinciaId.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaDescripcionNe != null) __params = __params.set('servicio-operador-localidadDestino-provincia-descripcion[ne]', params.servicioOperadorLocalidadDestinoProvinciaDescripcionNe.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaDescripcionLike != null) __params = __params.set('servicio-operador-localidadDestino-provincia-descripcion[like]', params.servicioOperadorLocalidadDestinoProvinciaDescripcionLike.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaDescripcion != null) __params = __params.set('servicio-operador-localidadDestino-provincia-descripcion', params.servicioOperadorLocalidadDestinoProvinciaDescripcion.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaCodigoNe != null) __params = __params.set('servicio-operador-localidadDestino-provincia-codigo[ne]', params.servicioOperadorLocalidadDestinoProvinciaCodigoNe.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaCodigoLike != null) __params = __params.set('servicio-operador-localidadDestino-provincia-codigo[like]', params.servicioOperadorLocalidadDestinoProvinciaCodigoLike.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaCodigoINDECNe != null) __params = __params.set('servicio-operador-localidadDestino-provincia-codigoINDEC[ne]', params.servicioOperadorLocalidadDestinoProvinciaCodigoINDECNe.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaCodigoINDECLike != null) __params = __params.set('servicio-operador-localidadDestino-provincia-codigoINDEC[like]', params.servicioOperadorLocalidadDestinoProvinciaCodigoINDECLike.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaCodigoINDEC != null) __params = __params.set('servicio-operador-localidadDestino-provincia-codigoINDEC', params.servicioOperadorLocalidadDestinoProvinciaCodigoINDEC.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaCodigo != null) __params = __params.set('servicio-operador-localidadDestino-provincia-codigo', params.servicioOperadorLocalidadDestinoProvinciaCodigo.toString());
    if (params.servicioOperadorLocalidadDestinoProvinciaActivo != null) __params = __params.set('servicio-operador-localidadDestino-provincia-activo', params.servicioOperadorLocalidadDestinoProvinciaActivo.toString());
    if (params.servicioOperadorLocalidadDestinoParada != null) __params = __params.set('servicio-operador-localidadDestino-parada', params.servicioOperadorLocalidadDestinoParada.toString());
    if (params.servicioOperadorLocalidadDestinoLonNe != null) __params = __params.set('servicio-operador-localidadDestino-lon[ne]', params.servicioOperadorLocalidadDestinoLonNe.toString());
    if (params.servicioOperadorLocalidadDestinoLonLt != null) __params = __params.set('servicio-operador-localidadDestino-lon[lt]', params.servicioOperadorLocalidadDestinoLonLt.toString());
    if (params.servicioOperadorLocalidadDestinoLonLe != null) __params = __params.set('servicio-operador-localidadDestino-lon[le]', params.servicioOperadorLocalidadDestinoLonLe.toString());
    if (params.servicioOperadorLocalidadDestinoLonGt != null) __params = __params.set('servicio-operador-localidadDestino-lon[gt]', params.servicioOperadorLocalidadDestinoLonGt.toString());
    if (params.servicioOperadorLocalidadDestinoLonGe != null) __params = __params.set('servicio-operador-localidadDestino-lon[ge]', params.servicioOperadorLocalidadDestinoLonGe.toString());
    if (params.servicioOperadorLocalidadDestinoLon != null) __params = __params.set('servicio-operador-localidadDestino-lon', params.servicioOperadorLocalidadDestinoLon.toString());
    if (params.servicioOperadorLocalidadDestinoLatNe != null) __params = __params.set('servicio-operador-localidadDestino-lat[ne]', params.servicioOperadorLocalidadDestinoLatNe.toString());
    if (params.servicioOperadorLocalidadDestinoLatLt != null) __params = __params.set('servicio-operador-localidadDestino-lat[lt]', params.servicioOperadorLocalidadDestinoLatLt.toString());
    if (params.servicioOperadorLocalidadDestinoLatLe != null) __params = __params.set('servicio-operador-localidadDestino-lat[le]', params.servicioOperadorLocalidadDestinoLatLe.toString());
    if (params.servicioOperadorLocalidadDestinoLatGt != null) __params = __params.set('servicio-operador-localidadDestino-lat[gt]', params.servicioOperadorLocalidadDestinoLatGt.toString());
    if (params.servicioOperadorLocalidadDestinoLatGe != null) __params = __params.set('servicio-operador-localidadDestino-lat[ge]', params.servicioOperadorLocalidadDestinoLatGe.toString());
    if (params.servicioOperadorLocalidadDestinoLat != null) __params = __params.set('servicio-operador-localidadDestino-lat', params.servicioOperadorLocalidadDestinoLat.toString());
    if (params.servicioOperadorLocalidadDestinoIdNe != null) __params = __params.set('servicio-operador-localidadDestino-id[ne]', params.servicioOperadorLocalidadDestinoIdNe.toString());
    if (params.servicioOperadorLocalidadDestinoIdLt != null) __params = __params.set('servicio-operador-localidadDestino-id[lt]', params.servicioOperadorLocalidadDestinoIdLt.toString());
    if (params.servicioOperadorLocalidadDestinoIdLe != null) __params = __params.set('servicio-operador-localidadDestino-id[le]', params.servicioOperadorLocalidadDestinoIdLe.toString());
    if (params.servicioOperadorLocalidadDestinoIdGt != null) __params = __params.set('servicio-operador-localidadDestino-id[gt]', params.servicioOperadorLocalidadDestinoIdGt.toString());
    if (params.servicioOperadorLocalidadDestinoIdGe != null) __params = __params.set('servicio-operador-localidadDestino-id[ge]', params.servicioOperadorLocalidadDestinoIdGe.toString());
    if (params.servicioOperadorLocalidadDestinoId != null) __params = __params.set('servicio-operador-localidadDestino-id', params.servicioOperadorLocalidadDestinoId.toString());
    if (params.servicioOperadorLocalidadDestinoFuenteNe != null) __params = __params.set('servicio-operador-localidadDestino-fuente[ne]', params.servicioOperadorLocalidadDestinoFuenteNe.toString());
    if (params.servicioOperadorLocalidadDestinoFuenteLike != null) __params = __params.set('servicio-operador-localidadDestino-fuente[like]', params.servicioOperadorLocalidadDestinoFuenteLike.toString());
    if (params.servicioOperadorLocalidadDestinoFuente != null) __params = __params.set('servicio-operador-localidadDestino-fuente', params.servicioOperadorLocalidadDestinoFuente.toString());
    if (params.servicioOperadorLocalidadDestinoDescripcionNe != null) __params = __params.set('servicio-operador-localidadDestino-descripcion[ne]', params.servicioOperadorLocalidadDestinoDescripcionNe.toString());
    if (params.servicioOperadorLocalidadDestinoDescripcionLike != null) __params = __params.set('servicio-operador-localidadDestino-descripcion[like]', params.servicioOperadorLocalidadDestinoDescripcionLike.toString());
    if (params.servicioOperadorLocalidadDestinoDescripcion != null) __params = __params.set('servicio-operador-localidadDestino-descripcion', params.servicioOperadorLocalidadDestinoDescripcion.toString());
    if (params.servicioOperadorLocalidadDestinoCreatedAtNe != null) __params = __params.set('servicio-operador-localidadDestino-createdAt[ne]', params.servicioOperadorLocalidadDestinoCreatedAtNe.toString());
    if (params.servicioOperadorLocalidadDestinoCreatedAtLt != null) __params = __params.set('servicio-operador-localidadDestino-createdAt[lt]', params.servicioOperadorLocalidadDestinoCreatedAtLt.toString());
    if (params.servicioOperadorLocalidadDestinoCreatedAtLe != null) __params = __params.set('servicio-operador-localidadDestino-createdAt[le]', params.servicioOperadorLocalidadDestinoCreatedAtLe.toString());
    if (params.servicioOperadorLocalidadDestinoCreatedAtGt != null) __params = __params.set('servicio-operador-localidadDestino-createdAt[gt]', params.servicioOperadorLocalidadDestinoCreatedAtGt.toString());
    if (params.servicioOperadorLocalidadDestinoCreatedAtGe != null) __params = __params.set('servicio-operador-localidadDestino-createdAt[ge]', params.servicioOperadorLocalidadDestinoCreatedAtGe.toString());
    if (params.servicioOperadorLocalidadDestinoCreatedAt != null) __params = __params.set('servicio-operador-localidadDestino-createdAt', params.servicioOperadorLocalidadDestinoCreatedAt.toString());
    if (params.servicioOperadorLocalidadDestinoCodigoINDECNe != null) __params = __params.set('servicio-operador-localidadDestino-codigoINDEC[ne]', params.servicioOperadorLocalidadDestinoCodigoINDECNe.toString());
    if (params.servicioOperadorLocalidadDestinoCodigoINDECLike != null) __params = __params.set('servicio-operador-localidadDestino-codigoINDEC[like]', params.servicioOperadorLocalidadDestinoCodigoINDECLike.toString());
    if (params.servicioOperadorLocalidadDestinoCodigoINDEC != null) __params = __params.set('servicio-operador-localidadDestino-codigoINDEC', params.servicioOperadorLocalidadDestinoCodigoINDEC.toString());
    if (params.servicioOperadorLocalidadDestinoActivo != null) __params = __params.set('servicio-operador-localidadDestino-activo', params.servicioOperadorLocalidadDestinoActivo.toString());
    if (params.servicioOperadorLineaNe != null) __params = __params.set('servicio-operador-linea[ne]', params.servicioOperadorLineaNe.toString());
    if (params.servicioOperadorLineaLike != null) __params = __params.set('servicio-operador-linea[like]', params.servicioOperadorLineaLike.toString());
    if (params.servicioOperadorLinea != null) __params = __params.set('servicio-operador-linea', params.servicioOperadorLinea.toString());
    if (params.servicioOperadorIdNe != null) __params = __params.set('servicio-operador-id[ne]', params.servicioOperadorIdNe.toString());
    if (params.servicioOperadorIdLt != null) __params = __params.set('servicio-operador-id[lt]', params.servicioOperadorIdLt.toString());
    if (params.servicioOperadorIdLe != null) __params = __params.set('servicio-operador-id[le]', params.servicioOperadorIdLe.toString());
    if (params.servicioOperadorIdGt != null) __params = __params.set('servicio-operador-id[gt]', params.servicioOperadorIdGt.toString());
    if (params.servicioOperadorIdGe != null) __params = __params.set('servicio-operador-id[ge]', params.servicioOperadorIdGe.toString());
    if (params.servicioOperadorId != null) __params = __params.set('servicio-operador-id', params.servicioOperadorId.toString());
    if (params.servicioOperadorEmpresaContratanteNe != null) __params = __params.set('servicio-operador-empresaContratante[ne]', params.servicioOperadorEmpresaContratanteNe.toString());
    if (params.servicioOperadorEmpresaContratanteLike != null) __params = __params.set('servicio-operador-empresaContratante[like]', params.servicioOperadorEmpresaContratanteLike.toString());
    if (params.servicioOperadorEmpresaContratanteCuitNe != null) __params = __params.set('servicio-operador-empresaContratanteCuit[ne]', params.servicioOperadorEmpresaContratanteCuitNe.toString());
    if (params.servicioOperadorEmpresaContratanteCuitLike != null) __params = __params.set('servicio-operador-empresaContratanteCuit[like]', params.servicioOperadorEmpresaContratanteCuitLike.toString());
    if (params.servicioOperadorEmpresaContratanteCuit != null) __params = __params.set('servicio-operador-empresaContratanteCuit', params.servicioOperadorEmpresaContratanteCuit.toString());
    if (params.servicioOperadorEmpresaContratante != null) __params = __params.set('servicio-operador-empresaContratante', params.servicioOperadorEmpresaContratante.toString());
    if (params.servicioOperadorEmpresaVigenciaHastaNe != null) __params = __params.set('servicio-operador-empresa-vigenciaHasta[ne]', params.servicioOperadorEmpresaVigenciaHastaNe.toString());
    if (params.servicioOperadorEmpresaVigenciaHastaLt != null) __params = __params.set('servicio-operador-empresa-vigenciaHasta[lt]', params.servicioOperadorEmpresaVigenciaHastaLt.toString());
    if (params.servicioOperadorEmpresaVigenciaHastaLe != null) __params = __params.set('servicio-operador-empresa-vigenciaHasta[le]', params.servicioOperadorEmpresaVigenciaHastaLe.toString());
    if (params.servicioOperadorEmpresaVigenciaHastaGt != null) __params = __params.set('servicio-operador-empresa-vigenciaHasta[gt]', params.servicioOperadorEmpresaVigenciaHastaGt.toString());
    if (params.servicioOperadorEmpresaVigenciaHastaGe != null) __params = __params.set('servicio-operador-empresa-vigenciaHasta[ge]', params.servicioOperadorEmpresaVigenciaHastaGe.toString());
    if (params.servicioOperadorEmpresaVigenciaHasta != null) __params = __params.set('servicio-operador-empresa-vigenciaHasta', params.servicioOperadorEmpresaVigenciaHasta.toString());
    if (params.servicioOperadorEmpresaVigenciaDesdeNe != null) __params = __params.set('servicio-operador-empresa-vigenciaDesde[ne]', params.servicioOperadorEmpresaVigenciaDesdeNe.toString());
    if (params.servicioOperadorEmpresaVigenciaDesdeLt != null) __params = __params.set('servicio-operador-empresa-vigenciaDesde[lt]', params.servicioOperadorEmpresaVigenciaDesdeLt.toString());
    if (params.servicioOperadorEmpresaVigenciaDesdeLe != null) __params = __params.set('servicio-operador-empresa-vigenciaDesde[le]', params.servicioOperadorEmpresaVigenciaDesdeLe.toString());
    if (params.servicioOperadorEmpresaVigenciaDesdeGt != null) __params = __params.set('servicio-operador-empresa-vigenciaDesde[gt]', params.servicioOperadorEmpresaVigenciaDesdeGt.toString());
    if (params.servicioOperadorEmpresaVigenciaDesdeGe != null) __params = __params.set('servicio-operador-empresa-vigenciaDesde[ge]', params.servicioOperadorEmpresaVigenciaDesdeGe.toString());
    if (params.servicioOperadorEmpresaVigenciaDesde != null) __params = __params.set('servicio-operador-empresa-vigenciaDesde', params.servicioOperadorEmpresaVigenciaDesde.toString());
    if (params.servicioOperadorEmpresaTipoSociedadNe != null) __params = __params.set('servicio-operador-empresa-tipoSociedad[ne]', params.servicioOperadorEmpresaTipoSociedadNe.toString());
    if (params.servicioOperadorEmpresaTipoSociedadLt != null) __params = __params.set('servicio-operador-empresa-tipoSociedad[lt]', params.servicioOperadorEmpresaTipoSociedadLt.toString());
    if (params.servicioOperadorEmpresaTipoSociedadLe != null) __params = __params.set('servicio-operador-empresa-tipoSociedad[le]', params.servicioOperadorEmpresaTipoSociedadLe.toString());
    if (params.servicioOperadorEmpresaTipoSociedadGt != null) __params = __params.set('servicio-operador-empresa-tipoSociedad[gt]', params.servicioOperadorEmpresaTipoSociedadGt.toString());
    if (params.servicioOperadorEmpresaTipoSociedadGe != null) __params = __params.set('servicio-operador-empresa-tipoSociedad[ge]', params.servicioOperadorEmpresaTipoSociedadGe.toString());
    if (params.servicioOperadorEmpresaTipoSociedad != null) __params = __params.set('servicio-operador-empresa-tipoSociedad', params.servicioOperadorEmpresaTipoSociedad.toString());
    if (params.servicioOperadorEmpresaTipoDocumentoIdNe != null) __params = __params.set('servicio-operador-empresa-tipoDocumento-id[ne]', params.servicioOperadorEmpresaTipoDocumentoIdNe.toString());
    if (params.servicioOperadorEmpresaTipoDocumentoIdLt != null) __params = __params.set('servicio-operador-empresa-tipoDocumento-id[lt]', params.servicioOperadorEmpresaTipoDocumentoIdLt.toString());
    if (params.servicioOperadorEmpresaTipoDocumentoIdLe != null) __params = __params.set('servicio-operador-empresa-tipoDocumento-id[le]', params.servicioOperadorEmpresaTipoDocumentoIdLe.toString());
    if (params.servicioOperadorEmpresaTipoDocumentoIdGt != null) __params = __params.set('servicio-operador-empresa-tipoDocumento-id[gt]', params.servicioOperadorEmpresaTipoDocumentoIdGt.toString());
    if (params.servicioOperadorEmpresaTipoDocumentoIdGe != null) __params = __params.set('servicio-operador-empresa-tipoDocumento-id[ge]', params.servicioOperadorEmpresaTipoDocumentoIdGe.toString());
    if (params.servicioOperadorEmpresaTipoDocumentoId != null) __params = __params.set('servicio-operador-empresa-tipoDocumento-id', params.servicioOperadorEmpresaTipoDocumentoId.toString());
    if (params.servicioOperadorEmpresaTipoDocumentoDescripcionNe != null) __params = __params.set('servicio-operador-empresa-tipoDocumento-descripcion[ne]', params.servicioOperadorEmpresaTipoDocumentoDescripcionNe.toString());
    if (params.servicioOperadorEmpresaTipoDocumentoDescripcionLike != null) __params = __params.set('servicio-operador-empresa-tipoDocumento-descripcion[like]', params.servicioOperadorEmpresaTipoDocumentoDescripcionLike.toString());
    if (params.servicioOperadorEmpresaTipoDocumentoDescripcion != null) __params = __params.set('servicio-operador-empresa-tipoDocumento-descripcion', params.servicioOperadorEmpresaTipoDocumentoDescripcion.toString());
    if (params.servicioOperadorEmpresaTipoDocumentoAbrevNe != null) __params = __params.set('servicio-operador-empresa-tipoDocumento-abrev[ne]', params.servicioOperadorEmpresaTipoDocumentoAbrevNe.toString());
    if (params.servicioOperadorEmpresaTipoDocumentoAbrevLike != null) __params = __params.set('servicio-operador-empresa-tipoDocumento-abrev[like]', params.servicioOperadorEmpresaTipoDocumentoAbrevLike.toString());
    if (params.servicioOperadorEmpresaTipoDocumentoAbrev != null) __params = __params.set('servicio-operador-empresa-tipoDocumento-abrev', params.servicioOperadorEmpresaTipoDocumentoAbrev.toString());
    if (params.servicioOperadorEmpresaTextoHash != null) __params = __params.set('servicio-operador-empresa-textoHash', params.servicioOperadorEmpresaTextoHash.toString());
    if (params.servicioOperadorEmpresaRazonSocialNe != null) __params = __params.set('servicio-operador-empresa-razonSocial[ne]', params.servicioOperadorEmpresaRazonSocialNe.toString());
    if (params.servicioOperadorEmpresaRazonSocialLike != null) __params = __params.set('servicio-operador-empresa-razonSocial[like]', params.servicioOperadorEmpresaRazonSocialLike.toString());
    if (params.servicioOperadorEmpresaRazonSocial != null) __params = __params.set('servicio-operador-empresa-razonSocial', params.servicioOperadorEmpresaRazonSocial.toString());
    if (params.servicioOperadorEmpresaPautNe != null) __params = __params.set('servicio-operador-empresa-paut[ne]', params.servicioOperadorEmpresaPautNe.toString());
    if (params.servicioOperadorEmpresaPautLike != null) __params = __params.set('servicio-operador-empresa-paut[like]', params.servicioOperadorEmpresaPautLike.toString());
    if (params.servicioOperadorEmpresaPaut != null) __params = __params.set('servicio-operador-empresa-paut', params.servicioOperadorEmpresaPaut.toString());
    if (params.servicioOperadorEmpresaObservacionNe != null) __params = __params.set('servicio-operador-empresa-observacion[ne]', params.servicioOperadorEmpresaObservacionNe.toString());
    if (params.servicioOperadorEmpresaObservacionLike != null) __params = __params.set('servicio-operador-empresa-observacion[like]', params.servicioOperadorEmpresaObservacionLike.toString());
    if (params.servicioOperadorEmpresaObservacion != null) __params = __params.set('servicio-operador-empresa-observacion', params.servicioOperadorEmpresaObservacion.toString());
    if (params.servicioOperadorEmpresaNroDocumentoNe != null) __params = __params.set('servicio-operador-empresa-nroDocumento[ne]', params.servicioOperadorEmpresaNroDocumentoNe.toString());
    if (params.servicioOperadorEmpresaNroDocumentoLike != null) __params = __params.set('servicio-operador-empresa-nroDocumento[like]', params.servicioOperadorEmpresaNroDocumentoLike.toString());
    if (params.servicioOperadorEmpresaNroDocumento != null) __params = __params.set('servicio-operador-empresa-nroDocumento', params.servicioOperadorEmpresaNroDocumento.toString());
    if (params.servicioOperadorEmpresaNombreFantasiaNe != null) __params = __params.set('servicio-operador-empresa-nombreFantasia[ne]', params.servicioOperadorEmpresaNombreFantasiaNe.toString());
    if (params.servicioOperadorEmpresaNombreFantasiaLike != null) __params = __params.set('servicio-operador-empresa-nombreFantasia[like]', params.servicioOperadorEmpresaNombreFantasiaLike.toString());
    if (params.servicioOperadorEmpresaNombreFantasia != null) __params = __params.set('servicio-operador-empresa-nombreFantasia', params.servicioOperadorEmpresaNombreFantasia.toString());
    if (params.servicioOperadorEmpresaIdNe != null) __params = __params.set('servicio-operador-empresa-id[ne]', params.servicioOperadorEmpresaIdNe.toString());
    if (params.servicioOperadorEmpresaIdLt != null) __params = __params.set('servicio-operador-empresa-id[lt]', params.servicioOperadorEmpresaIdLt.toString());
    if (params.servicioOperadorEmpresaIdLe != null) __params = __params.set('servicio-operador-empresa-id[le]', params.servicioOperadorEmpresaIdLe.toString());
    if (params.servicioOperadorEmpresaIdGt != null) __params = __params.set('servicio-operador-empresa-id[gt]', params.servicioOperadorEmpresaIdGt.toString());
    if (params.servicioOperadorEmpresaIdGe != null) __params = __params.set('servicio-operador-empresa-id[ge]', params.servicioOperadorEmpresaIdGe.toString());
    if (params.servicioOperadorEmpresaId != null) __params = __params.set('servicio-operador-empresa-id', params.servicioOperadorEmpresaId.toString());
    if (params.servicioOperadorEmpresaEstadoEmpresaNe != null) __params = __params.set('servicio-operador-empresa-estadoEmpresa[ne]', params.servicioOperadorEmpresaEstadoEmpresaNe.toString());
    if (params.servicioOperadorEmpresaEstadoEmpresaLt != null) __params = __params.set('servicio-operador-empresa-estadoEmpresa[lt]', params.servicioOperadorEmpresaEstadoEmpresaLt.toString());
    if (params.servicioOperadorEmpresaEstadoEmpresaLe != null) __params = __params.set('servicio-operador-empresa-estadoEmpresa[le]', params.servicioOperadorEmpresaEstadoEmpresaLe.toString());
    if (params.servicioOperadorEmpresaEstadoEmpresaGt != null) __params = __params.set('servicio-operador-empresa-estadoEmpresa[gt]', params.servicioOperadorEmpresaEstadoEmpresaGt.toString());
    if (params.servicioOperadorEmpresaEstadoEmpresaGe != null) __params = __params.set('servicio-operador-empresa-estadoEmpresa[ge]', params.servicioOperadorEmpresaEstadoEmpresaGe.toString());
    if (params.servicioOperadorEmpresaEstadoEmpresa != null) __params = __params.set('servicio-operador-empresa-estadoEmpresa', params.servicioOperadorEmpresaEstadoEmpresa.toString());
    if (params.servicioOperadorEmpresaEsPersonaFisica != null) __params = __params.set('servicio-operador-empresa-esPersonaFisica', params.servicioOperadorEmpresaEsPersonaFisica.toString());
    if (params.servicioOperadorEmpresaEmailNe != null) __params = __params.set('servicio-operador-empresa-email[ne]', params.servicioOperadorEmpresaEmailNe.toString());
    if (params.servicioOperadorEmpresaEmailLike != null) __params = __params.set('servicio-operador-empresa-email[like]', params.servicioOperadorEmpresaEmailLike.toString());
    if (params.servicioOperadorEmpresaEmail != null) __params = __params.set('servicio-operador-empresa-email', params.servicioOperadorEmpresaEmail.toString());
    if (params.servicioOperadorEmpresaCuitEmpresaAnteriorNe != null) __params = __params.set('servicio-operador-empresa-cuitEmpresaAnterior[ne]', params.servicioOperadorEmpresaCuitEmpresaAnteriorNe.toString());
    if (params.servicioOperadorEmpresaCuitEmpresaAnteriorLike != null) __params = __params.set('servicio-operador-empresa-cuitEmpresaAnterior[like]', params.servicioOperadorEmpresaCuitEmpresaAnteriorLike.toString());
    if (params.servicioOperadorEmpresaCuitEmpresaAnterior != null) __params = __params.set('servicio-operador-empresa-cuitEmpresaAnterior', params.servicioOperadorEmpresaCuitEmpresaAnterior.toString());
    if (params.servicioOperadorClaseModalidadIdNe != null) __params = __params.set('servicio-operador-claseModalidad-id[ne]', params.servicioOperadorClaseModalidadIdNe.toString());
    if (params.servicioOperadorClaseModalidadIdLt != null) __params = __params.set('servicio-operador-claseModalidad-id[lt]', params.servicioOperadorClaseModalidadIdLt.toString());
    if (params.servicioOperadorClaseModalidadIdLe != null) __params = __params.set('servicio-operador-claseModalidad-id[le]', params.servicioOperadorClaseModalidadIdLe.toString());
    if (params.servicioOperadorClaseModalidadIdGt != null) __params = __params.set('servicio-operador-claseModalidad-id[gt]', params.servicioOperadorClaseModalidadIdGt.toString());
    if (params.servicioOperadorClaseModalidadIdGe != null) __params = __params.set('servicio-operador-claseModalidad-id[ge]', params.servicioOperadorClaseModalidadIdGe.toString());
    if (params.servicioOperadorClaseModalidadId != null) __params = __params.set('servicio-operador-claseModalidad-id', params.servicioOperadorClaseModalidadId.toString());
    if (params.servicioOperadorClaseModalidadDescripcionNe != null) __params = __params.set('servicio-operador-claseModalidad-descripcion[ne]', params.servicioOperadorClaseModalidadDescripcionNe.toString());
    if (params.servicioOperadorClaseModalidadDescripcionLike != null) __params = __params.set('servicio-operador-claseModalidad-descripcion[like]', params.servicioOperadorClaseModalidadDescripcionLike.toString());
    if (params.servicioOperadorClaseModalidadDescripcion != null) __params = __params.set('servicio-operador-claseModalidad-descripcion', params.servicioOperadorClaseModalidadDescripcion.toString());
    if (params.servicioIdNe != null) __params = __params.set('servicio-id[ne]', params.servicioIdNe.toString());
    if (params.servicioIdLt != null) __params = __params.set('servicio-id[lt]', params.servicioIdLt.toString());
    if (params.servicioIdLe != null) __params = __params.set('servicio-id[le]', params.servicioIdLe.toString());
    if (params.servicioIdGt != null) __params = __params.set('servicio-id[gt]', params.servicioIdGt.toString());
    if (params.servicioIdGe != null) __params = __params.set('servicio-id[ge]', params.servicioIdGe.toString());
    if (params.servicioId != null) __params = __params.set('servicio-id', params.servicioId.toString());
    if (params.servicioHoraSalidaNe != null) __params = __params.set('servicio-horaSalida[ne]', params.servicioHoraSalidaNe.toString());
    if (params.servicioHoraSalidaLt != null) __params = __params.set('servicio-horaSalida[lt]', params.servicioHoraSalidaLt.toString());
    if (params.servicioHoraSalidaLe != null) __params = __params.set('servicio-horaSalida[le]', params.servicioHoraSalidaLe.toString());
    if (params.servicioHoraSalidaGt != null) __params = __params.set('servicio-horaSalida[gt]', params.servicioHoraSalidaGt.toString());
    if (params.servicioHoraSalidaGe != null) __params = __params.set('servicio-horaSalida[ge]', params.servicioHoraSalidaGe.toString());
    if (params.servicioHoraSalida != null) __params = __params.set('servicio-horaSalida', params.servicioHoraSalida.toString());
    if (params.servicioHoraLlegadaNe != null) __params = __params.set('servicio-horaLlegada[ne]', params.servicioHoraLlegadaNe.toString());
    if (params.servicioHoraLlegadaLt != null) __params = __params.set('servicio-horaLlegada[lt]', params.servicioHoraLlegadaLt.toString());
    if (params.servicioHoraLlegadaLe != null) __params = __params.set('servicio-horaLlegada[le]', params.servicioHoraLlegadaLe.toString());
    if (params.servicioHoraLlegadaGt != null) __params = __params.set('servicio-horaLlegada[gt]', params.servicioHoraLlegadaGt.toString());
    if (params.servicioHoraLlegadaGe != null) __params = __params.set('servicio-horaLlegada[ge]', params.servicioHoraLlegadaGe.toString());
    if (params.servicioHoraLlegada != null) __params = __params.set('servicio-horaLlegada', params.servicioHoraLlegada.toString());
    if (params.servicioAprobado != null) __params = __params.set('servicio-aprobado', params.servicioAprobado.toString());
    if (params.order != null) __params = __params.set('order', params.order.toString());
    if (params.offset != null) __params = __params.set('offset', params.offset.toString());
    if (params.limit != null) __params = __params.set('limit', params.limit.toString());
    if (params.idNe != null) __params = __params.set('id[ne]', params.idNe.toString());
    if (params.idLt != null) __params = __params.set('id[lt]', params.idLt.toString());
    if (params.idLe != null) __params = __params.set('id[le]', params.idLe.toString());
    if (params.idGt != null) __params = __params.set('id[gt]', params.idGt.toString());
    if (params.idGe != null) __params = __params.set('id[ge]', params.idGe.toString());
    if (params.id != null) __params = __params.set('id', params.id.toString());
    if (params.categoriaServicioPrimerPisoIdNe != null) __params = __params.set('categoriaServicioPrimerPiso-id[ne]', params.categoriaServicioPrimerPisoIdNe.toString());
    if (params.categoriaServicioPrimerPisoIdLt != null) __params = __params.set('categoriaServicioPrimerPiso-id[lt]', params.categoriaServicioPrimerPisoIdLt.toString());
    if (params.categoriaServicioPrimerPisoIdLe != null) __params = __params.set('categoriaServicioPrimerPiso-id[le]', params.categoriaServicioPrimerPisoIdLe.toString());
    if (params.categoriaServicioPrimerPisoIdGt != null) __params = __params.set('categoriaServicioPrimerPiso-id[gt]', params.categoriaServicioPrimerPisoIdGt.toString());
    if (params.categoriaServicioPrimerPisoIdGe != null) __params = __params.set('categoriaServicioPrimerPiso-id[ge]', params.categoriaServicioPrimerPisoIdGe.toString());
    if (params.categoriaServicioPrimerPisoId != null) __params = __params.set('categoriaServicioPrimerPiso-id', params.categoriaServicioPrimerPisoId.toString());
    if (params.categoriaServicioPrimerPisoDescripcionNe != null) __params = __params.set('categoriaServicioPrimerPiso-descripcion[ne]', params.categoriaServicioPrimerPisoDescripcionNe.toString());
    if (params.categoriaServicioPrimerPisoDescripcionLike != null) __params = __params.set('categoriaServicioPrimerPiso-descripcion[like]', params.categoriaServicioPrimerPisoDescripcionLike.toString());
    if (params.categoriaServicioPrimerPisoDescripcion != null) __params = __params.set('categoriaServicioPrimerPiso-descripcion', params.categoriaServicioPrimerPisoDescripcion.toString());
    if (params.categoriaServicioPrimerPisoActivo != null) __params = __params.set('categoriaServicioPrimerPiso-activo', params.categoriaServicioPrimerPisoActivo.toString());
    if (params.activo != null) __params = __params.set('activo', params.activo.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/diasServicios`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<DiaServicioAPIResponsePaginacion>;
      })
    );
  }
  /**
   * Obtiene una lista de Días de Servicio paginadas
   * @param params The `DiaServicioService.GetApiV2DiasServiciosParams` containing the following parameters:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `servicio-vigenciaHasta[ne]`:
   *
   * - `servicio-vigenciaHasta[lt]`:
   *
   * - `servicio-vigenciaHasta[le]`:
   *
   * - `servicio-vigenciaHasta[gt]`:
   *
   * - `servicio-vigenciaHasta[ge]`:
   *
   * - `servicio-vigenciaHasta`:
   *
   * - `servicio-vigenciaDesde[ne]`:
   *
   * - `servicio-vigenciaDesde[lt]`:
   *
   * - `servicio-vigenciaDesde[le]`:
   *
   * - `servicio-vigenciaDesde[gt]`:
   *
   * - `servicio-vigenciaDesde[ge]`:
   *
   * - `servicio-vigenciaDesde`:
   *
   * - `servicio-velocidadPromedioTotal[ne]`:
   *
   * - `servicio-velocidadPromedioTotal[lt]`:
   *
   * - `servicio-velocidadPromedioTotal[le]`:
   *
   * - `servicio-velocidadPromedioTotal[gt]`:
   *
   * - `servicio-velocidadPromedioTotal[ge]`:
   *
   * - `servicio-velocidadPromedioTotal`:
   *
   * - `servicio-tramite-usuario[ne]`:
   *
   * - `servicio-tramite-usuario[like]`:
   *
   * - `servicio-tramite-usuario`:
   *
   * - `servicio-tramite-tipoTramite-id[ne]`:
   *
   * - `servicio-tramite-tipoTramite-id[lt]`:
   *
   * - `servicio-tramite-tipoTramite-id[le]`:
   *
   * - `servicio-tramite-tipoTramite-id[gt]`:
   *
   * - `servicio-tramite-tipoTramite-id[ge]`:
   *
   * - `servicio-tramite-tipoTramite-id`:
   *
   * - `servicio-tramite-tipoTramite-descripcion[ne]`:
   *
   * - `servicio-tramite-tipoTramite-descripcion[like]`:
   *
   * - `servicio-tramite-tipoTramite-descripcion`:
   *
   * - `servicio-tramite-tipoTramite-activo`:
   *
   * - `servicio-tramite-subtipoTramite-id[ne]`:
   *
   * - `servicio-tramite-subtipoTramite-id[lt]`:
   *
   * - `servicio-tramite-subtipoTramite-id[le]`:
   *
   * - `servicio-tramite-subtipoTramite-id[gt]`:
   *
   * - `servicio-tramite-subtipoTramite-id[ge]`:
   *
   * - `servicio-tramite-subtipoTramite-id`:
   *
   * - `servicio-tramite-subtipoTramite-descripcion[ne]`:
   *
   * - `servicio-tramite-subtipoTramite-descripcion[like]`:
   *
   * - `servicio-tramite-subtipoTramite-descripcion`:
   *
   * - `servicio-tramite-subtipoTramite-activo`:
   *
   * - `servicio-tramite-nroBoletaPago[ne]`:
   *
   * - `servicio-tramite-nroBoletaPago[lt]`:
   *
   * - `servicio-tramite-nroBoletaPago[le]`:
   *
   * - `servicio-tramite-nroBoletaPago[gt]`:
   *
   * - `servicio-tramite-nroBoletaPago[ge]`:
   *
   * - `servicio-tramite-nroBoletaPago`:
   *
   * - `servicio-tramite-id[ne]`:
   *
   * - `servicio-tramite-id[lt]`:
   *
   * - `servicio-tramite-id[le]`:
   *
   * - `servicio-tramite-id[gt]`:
   *
   * - `servicio-tramite-id[ge]`:
   *
   * - `servicio-tramite-id`:
   *
   * - `servicio-tramite-estadoTramite-id[ne]`:
   *
   * - `servicio-tramite-estadoTramite-id[lt]`:
   *
   * - `servicio-tramite-estadoTramite-id[le]`:
   *
   * - `servicio-tramite-estadoTramite-id[gt]`:
   *
   * - `servicio-tramite-estadoTramite-id[ge]`:
   *
   * - `servicio-tramite-estadoTramite-id`:
   *
   * - `servicio-tramite-estadoTramite-descripcion[ne]`:
   *
   * - `servicio-tramite-estadoTramite-descripcion[like]`:
   *
   * - `servicio-tramite-estadoTramite-descripcion`:
   *
   * - `servicio-tramite-estadoTramite-activo`:
   *
   * - `servicio-tramite-estadoTramite-abrev[ne]`:
   *
   * - `servicio-tramite-estadoTramite-abrev[like]`:
   *
   * - `servicio-tramite-estadoTramite-abrev`:
   *
   * - `servicio-tramite-empresa-vigenciaHasta[ne]`:
   *
   * - `servicio-tramite-empresa-vigenciaHasta[lt]`:
   *
   * - `servicio-tramite-empresa-vigenciaHasta[le]`:
   *
   * - `servicio-tramite-empresa-vigenciaHasta[gt]`:
   *
   * - `servicio-tramite-empresa-vigenciaHasta[ge]`:
   *
   * - `servicio-tramite-empresa-vigenciaHasta`:
   *
   * - `servicio-tramite-empresa-vigenciaDesde[ne]`:
   *
   * - `servicio-tramite-empresa-vigenciaDesde[lt]`:
   *
   * - `servicio-tramite-empresa-vigenciaDesde[le]`:
   *
   * - `servicio-tramite-empresa-vigenciaDesde[gt]`:
   *
   * - `servicio-tramite-empresa-vigenciaDesde[ge]`:
   *
   * - `servicio-tramite-empresa-vigenciaDesde`:
   *
   * - `servicio-tramite-empresa-tipoSociedad[ne]`:
   *
   * - `servicio-tramite-empresa-tipoSociedad[lt]`:
   *
   * - `servicio-tramite-empresa-tipoSociedad[le]`:
   *
   * - `servicio-tramite-empresa-tipoSociedad[gt]`:
   *
   * - `servicio-tramite-empresa-tipoSociedad[ge]`:
   *
   * - `servicio-tramite-empresa-tipoSociedad`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-id[ne]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-id[lt]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-id[le]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-id[gt]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-id[ge]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-id`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-descripcion[ne]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-descripcion[like]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-descripcion`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-abrev[ne]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-abrev[like]`:
   *
   * - `servicio-tramite-empresa-tipoDocumento-abrev`:
   *
   * - `servicio-tramite-empresa-textoHash`:
   *
   * - `servicio-tramite-empresa-razonSocial[ne]`:
   *
   * - `servicio-tramite-empresa-razonSocial[like]`:
   *
   * - `servicio-tramite-empresa-razonSocial`:
   *
   * - `servicio-tramite-empresa-paut[ne]`:
   *
   * - `servicio-tramite-empresa-paut[like]`:
   *
   * - `servicio-tramite-empresa-paut`:
   *
   * - `servicio-tramite-empresa-observacion[ne]`:
   *
   * - `servicio-tramite-empresa-observacion[like]`:
   *
   * - `servicio-tramite-empresa-observacion`:
   *
   * - `servicio-tramite-empresa-nroDocumento[ne]`:
   *
   * - `servicio-tramite-empresa-nroDocumento[like]`:
   *
   * - `servicio-tramite-empresa-nroDocumento`:
   *
   * - `servicio-tramite-empresa-nombreFantasia[ne]`:
   *
   * - `servicio-tramite-empresa-nombreFantasia[like]`:
   *
   * - `servicio-tramite-empresa-nombreFantasia`:
   *
   * - `servicio-tramite-empresa-id[ne]`:
   *
   * - `servicio-tramite-empresa-id[lt]`:
   *
   * - `servicio-tramite-empresa-id[le]`:
   *
   * - `servicio-tramite-empresa-id[gt]`:
   *
   * - `servicio-tramite-empresa-id[ge]`:
   *
   * - `servicio-tramite-empresa-id`:
   *
   * - `servicio-tramite-empresa-estadoEmpresa[ne]`:
   *
   * - `servicio-tramite-empresa-estadoEmpresa[lt]`:
   *
   * - `servicio-tramite-empresa-estadoEmpresa[le]`:
   *
   * - `servicio-tramite-empresa-estadoEmpresa[gt]`:
   *
   * - `servicio-tramite-empresa-estadoEmpresa[ge]`:
   *
   * - `servicio-tramite-empresa-estadoEmpresa`:
   *
   * - `servicio-tramite-empresa-esPersonaFisica`:
   *
   * - `servicio-tramite-empresa-email[ne]`:
   *
   * - `servicio-tramite-empresa-email[like]`:
   *
   * - `servicio-tramite-empresa-email`:
   *
   * - `servicio-tramite-empresa-cuitEmpresaAnterior[ne]`:
   *
   * - `servicio-tramite-empresa-cuitEmpresaAnterior[like]`:
   *
   * - `servicio-tramite-empresa-cuitEmpresaAnterior`:
   *
   * - `servicio-recorridoSentido-vinculacionCaminera[ne]`:
   *
   * - `servicio-recorridoSentido-vinculacionCaminera[like]`:
   *
   * - `servicio-recorridoSentido-vinculacionCaminera`:
   *
   * - `servicio-recorridoSentido-vigenciaHasta[ne]`:
   *
   * - `servicio-recorridoSentido-vigenciaHasta[lt]`:
   *
   * - `servicio-recorridoSentido-vigenciaHasta[le]`:
   *
   * - `servicio-recorridoSentido-vigenciaHasta[gt]`:
   *
   * - `servicio-recorridoSentido-vigenciaHasta[ge]`:
   *
   * - `servicio-recorridoSentido-vigenciaHasta`:
   *
   * - `servicio-recorridoSentido-vigenciaDesde[ne]`:
   *
   * - `servicio-recorridoSentido-vigenciaDesde[lt]`:
   *
   * - `servicio-recorridoSentido-vigenciaDesde[le]`:
   *
   * - `servicio-recorridoSentido-vigenciaDesde[gt]`:
   *
   * - `servicio-recorridoSentido-vigenciaDesde[ge]`:
   *
   * - `servicio-recorridoSentido-vigenciaDesde`:
   *
   * - `servicio-recorridoSentido-sentido-id[ne]`:
   *
   * - `servicio-recorridoSentido-sentido-id[lt]`:
   *
   * - `servicio-recorridoSentido-sentido-id[le]`:
   *
   * - `servicio-recorridoSentido-sentido-id[gt]`:
   *
   * - `servicio-recorridoSentido-sentido-id[ge]`:
   *
   * - `servicio-recorridoSentido-sentido-id`:
   *
   * - `servicio-recorridoSentido-sentido-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-sentido-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-sentido-descripcion`:
   *
   * - `servicio-recorridoSentido-sentido-activo`:
   *
   * - `servicio-recorridoSentido-sentido-abrev[ne]`:
   *
   * - `servicio-recorridoSentido-sentido-abrev[like]`:
   *
   * - `servicio-recorridoSentido-sentido-abrev`:
   *
   * - `servicio-recorridoSentido-recorrido-vinculacionCaminera[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-vinculacionCaminera[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-vinculacionCaminera`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaHasta[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaHasta[lt]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaHasta[le]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaHasta[gt]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaHasta[ge]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaHasta`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaDesde[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaDesde[lt]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaDesde[le]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaDesde[gt]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaDesde[ge]`:
   *
   * - `servicio-recorridoSentido-recorrido-vigenciaDesde`:
   *
   * - `servicio-recorridoSentido-recorrido-transito[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-transito[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-transito`:
   *
   * - `servicio-recorridoSentido-recorrido-sitioOrigen[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-sitioOrigen[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-sitioOrigen`:
   *
   * - `servicio-recorridoSentido-recorrido-sitioDestino[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-sitioDestino[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-sitioDestino`:
   *
   * - `servicio-recorridoSentido-recorrido-recorrido[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-recorrido[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-recorrido`:
   *
   * - `servicio-recorridoSentido-recorrido-origen[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-origen[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-origen`:
   *
   * - `servicio-recorridoSentido-recorrido-itinerarioVuelta[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-itinerarioVuelta[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-itinerarioVuelta`:
   *
   * - `servicio-recorridoSentido-recorrido-itinerarioIda[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-itinerarioIda[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-itinerarioIda`:
   *
   * - `servicio-recorridoSentido-recorrido-id[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-id[lt]`:
   *
   * - `servicio-recorridoSentido-recorrido-id[le]`:
   *
   * - `servicio-recorridoSentido-recorrido-id[gt]`:
   *
   * - `servicio-recorridoSentido-recorrido-id[ge]`:
   *
   * - `servicio-recorridoSentido-recorrido-id`:
   *
   * - `servicio-recorridoSentido-recorrido-destino[ne]`:
   *
   * - `servicio-recorridoSentido-recorrido-destino[like]`:
   *
   * - `servicio-recorridoSentido-recorrido-destino`:
   *
   * - `servicio-recorridoSentido-recorrido-aplicaGasoil`:
   *
   * - `servicio-recorridoSentido-origen[ne]`:
   *
   * - `servicio-recorridoSentido-origen[like]`:
   *
   * - `servicio-recorridoSentido-origen`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-updatedAt[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-updatedAt[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-updatedAt[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-updatedAt[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-updatedAt[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-updatedAt`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-id[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-id[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-id[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-id[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-id[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-id`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-descripcion`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-codigo[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-codigo[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-codigo`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-activo`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-abrev[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-abrev[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-pais-abrev`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-id[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-id[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-id[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-id[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-id[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-id`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-descripcion`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-codigo[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-codigo[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-codigoINDEC[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-codigoINDEC[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-codigoINDEC`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-codigo`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-provincia-activo`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-parada`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lon[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lon[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lon[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lon[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lon[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lon`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lat[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lat[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lat[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lat[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lat[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-lat`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-id[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-id[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-id[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-id[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-id[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-id`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-fuente[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-fuente[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-fuente`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-descripcion`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-createdAt[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-createdAt[lt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-createdAt[le]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-createdAt[gt]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-createdAt[ge]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-createdAt`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-codigoINDEC[ne]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-codigoINDEC[like]`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-codigoINDEC`:
   *
   * - `servicio-recorridoSentido-localidadOrigen-activo`:
   *
   * - `servicio-recorridoSentido-localidadDestino-updatedAt[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-updatedAt[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-updatedAt[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-updatedAt[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-updatedAt[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-updatedAt`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-id[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-id[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-id[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-id[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-id[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-id`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-descripcion`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-codigo[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-codigo[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-codigo`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-activo`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-abrev[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-abrev[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-pais-abrev`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-id[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-id[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-id[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-id[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-id[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-id`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-descripcion`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-codigo[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-codigo[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-codigoINDEC[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-codigoINDEC[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-codigoINDEC`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-codigo`:
   *
   * - `servicio-recorridoSentido-localidadDestino-provincia-activo`:
   *
   * - `servicio-recorridoSentido-localidadDestino-parada`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lon[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lon[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lon[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lon[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lon[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lon`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lat[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lat[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lat[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lat[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lat[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-lat`:
   *
   * - `servicio-recorridoSentido-localidadDestino-id[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-id[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-id[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-id[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-id[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-id`:
   *
   * - `servicio-recorridoSentido-localidadDestino-fuente[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-fuente[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-fuente`:
   *
   * - `servicio-recorridoSentido-localidadDestino-descripcion[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-descripcion[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-descripcion`:
   *
   * - `servicio-recorridoSentido-localidadDestino-createdAt[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-createdAt[lt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-createdAt[le]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-createdAt[gt]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-createdAt[ge]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-createdAt`:
   *
   * - `servicio-recorridoSentido-localidadDestino-codigoINDEC[ne]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-codigoINDEC[like]`:
   *
   * - `servicio-recorridoSentido-localidadDestino-codigoINDEC`:
   *
   * - `servicio-recorridoSentido-localidadDestino-activo`:
   *
   * - `servicio-recorridoSentido-itinerario[ne]`:
   *
   * - `servicio-recorridoSentido-itinerario[like]`:
   *
   * - `servicio-recorridoSentido-itinerario`:
   *
   * - `servicio-recorridoSentido-id[ne]`:
   *
   * - `servicio-recorridoSentido-id[lt]`:
   *
   * - `servicio-recorridoSentido-id[le]`:
   *
   * - `servicio-recorridoSentido-id[gt]`:
   *
   * - `servicio-recorridoSentido-id[ge]`:
   *
   * - `servicio-recorridoSentido-id`:
   *
   * - `servicio-recorridoSentido-destino[ne]`:
   *
   * - `servicio-recorridoSentido-destino[like]`:
   *
   * - `servicio-recorridoSentido-destino`:
   *
   * - `servicio-recorrido-vinculacionCaminera[ne]`:
   *
   * - `servicio-recorrido-vinculacionCaminera[like]`:
   *
   * - `servicio-recorrido-vinculacionCaminera`:
   *
   * - `servicio-recorrido-vigenciaHasta[ne]`:
   *
   * - `servicio-recorrido-vigenciaHasta[lt]`:
   *
   * - `servicio-recorrido-vigenciaHasta[le]`:
   *
   * - `servicio-recorrido-vigenciaHasta[gt]`:
   *
   * - `servicio-recorrido-vigenciaHasta[ge]`:
   *
   * - `servicio-recorrido-vigenciaHasta`:
   *
   * - `servicio-recorrido-vigenciaDesde[ne]`:
   *
   * - `servicio-recorrido-vigenciaDesde[lt]`:
   *
   * - `servicio-recorrido-vigenciaDesde[le]`:
   *
   * - `servicio-recorrido-vigenciaDesde[gt]`:
   *
   * - `servicio-recorrido-vigenciaDesde[ge]`:
   *
   * - `servicio-recorrido-vigenciaDesde`:
   *
   * - `servicio-recorrido-transito[ne]`:
   *
   * - `servicio-recorrido-transito[like]`:
   *
   * - `servicio-recorrido-transito`:
   *
   * - `servicio-recorrido-sitioOrigen[ne]`:
   *
   * - `servicio-recorrido-sitioOrigen[like]`:
   *
   * - `servicio-recorrido-sitioOrigen`:
   *
   * - `servicio-recorrido-sitioDestino[ne]`:
   *
   * - `servicio-recorrido-sitioDestino[like]`:
   *
   * - `servicio-recorrido-sitioDestino`:
   *
   * - `servicio-recorrido-recorrido[ne]`:
   *
   * - `servicio-recorrido-recorrido[like]`:
   *
   * - `servicio-recorrido-recorrido`:
   *
   * - `servicio-recorrido-origen[ne]`:
   *
   * - `servicio-recorrido-origen[like]`:
   *
   * - `servicio-recorrido-origen`:
   *
   * - `servicio-recorrido-itinerarioVuelta[ne]`:
   *
   * - `servicio-recorrido-itinerarioVuelta[like]`:
   *
   * - `servicio-recorrido-itinerarioVuelta`:
   *
   * - `servicio-recorrido-itinerarioIda[ne]`:
   *
   * - `servicio-recorrido-itinerarioIda[like]`:
   *
   * - `servicio-recorrido-itinerarioIda`:
   *
   * - `servicio-recorrido-id[ne]`:
   *
   * - `servicio-recorrido-id[lt]`:
   *
   * - `servicio-recorrido-id[le]`:
   *
   * - `servicio-recorrido-id[gt]`:
   *
   * - `servicio-recorrido-id[ge]`:
   *
   * - `servicio-recorrido-id`:
   *
   * - `servicio-recorrido-destino[ne]`:
   *
   * - `servicio-recorrido-destino[like]`:
   *
   * - `servicio-recorrido-destino`:
   *
   * - `servicio-recorrido-aplicaGasoil`:
   *
   * - `servicio-operador-vigenciaHasta[ne]`:
   *
   * - `servicio-operador-vigenciaHasta[lt]`:
   *
   * - `servicio-operador-vigenciaHasta[le]`:
   *
   * - `servicio-operador-vigenciaHasta[gt]`:
   *
   * - `servicio-operador-vigenciaHasta[ge]`:
   *
   * - `servicio-operador-vigenciaHasta`:
   *
   * - `servicio-operador-vigenciaDesde[ne]`:
   *
   * - `servicio-operador-vigenciaDesde[lt]`:
   *
   * - `servicio-operador-vigenciaDesde[le]`:
   *
   * - `servicio-operador-vigenciaDesde[gt]`:
   *
   * - `servicio-operador-vigenciaDesde[ge]`:
   *
   * - `servicio-operador-vigenciaDesde`:
   *
   * - `servicio-operador-localidadOrigen-updatedAt[ne]`:
   *
   * - `servicio-operador-localidadOrigen-updatedAt[lt]`:
   *
   * - `servicio-operador-localidadOrigen-updatedAt[le]`:
   *
   * - `servicio-operador-localidadOrigen-updatedAt[gt]`:
   *
   * - `servicio-operador-localidadOrigen-updatedAt[ge]`:
   *
   * - `servicio-operador-localidadOrigen-updatedAt`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-id[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-id[lt]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-id[le]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-id[gt]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-id[ge]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-id`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-descripcion[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-descripcion[like]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-descripcion`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-codigo[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-codigo[like]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-codigo`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-activo`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-abrev[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-abrev[like]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-pais-abrev`:
   *
   * - `servicio-operador-localidadOrigen-provincia-id[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-id[lt]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-id[le]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-id[gt]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-id[ge]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-id`:
   *
   * - `servicio-operador-localidadOrigen-provincia-descripcion[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-descripcion[like]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-descripcion`:
   *
   * - `servicio-operador-localidadOrigen-provincia-codigo[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-codigo[like]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-codigoINDEC[ne]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-codigoINDEC[like]`:
   *
   * - `servicio-operador-localidadOrigen-provincia-codigoINDEC`:
   *
   * - `servicio-operador-localidadOrigen-provincia-codigo`:
   *
   * - `servicio-operador-localidadOrigen-provincia-activo`:
   *
   * - `servicio-operador-localidadOrigen-parada`:
   *
   * - `servicio-operador-localidadOrigen-lon[ne]`:
   *
   * - `servicio-operador-localidadOrigen-lon[lt]`:
   *
   * - `servicio-operador-localidadOrigen-lon[le]`:
   *
   * - `servicio-operador-localidadOrigen-lon[gt]`:
   *
   * - `servicio-operador-localidadOrigen-lon[ge]`:
   *
   * - `servicio-operador-localidadOrigen-lon`:
   *
   * - `servicio-operador-localidadOrigen-lat[ne]`:
   *
   * - `servicio-operador-localidadOrigen-lat[lt]`:
   *
   * - `servicio-operador-localidadOrigen-lat[le]`:
   *
   * - `servicio-operador-localidadOrigen-lat[gt]`:
   *
   * - `servicio-operador-localidadOrigen-lat[ge]`:
   *
   * - `servicio-operador-localidadOrigen-lat`:
   *
   * - `servicio-operador-localidadOrigen-id[ne]`:
   *
   * - `servicio-operador-localidadOrigen-id[lt]`:
   *
   * - `servicio-operador-localidadOrigen-id[le]`:
   *
   * - `servicio-operador-localidadOrigen-id[gt]`:
   *
   * - `servicio-operador-localidadOrigen-id[ge]`:
   *
   * - `servicio-operador-localidadOrigen-id`:
   *
   * - `servicio-operador-localidadOrigen-fuente[ne]`:
   *
   * - `servicio-operador-localidadOrigen-fuente[like]`:
   *
   * - `servicio-operador-localidadOrigen-fuente`:
   *
   * - `servicio-operador-localidadOrigen-descripcion[ne]`:
   *
   * - `servicio-operador-localidadOrigen-descripcion[like]`:
   *
   * - `servicio-operador-localidadOrigen-descripcion`:
   *
   * - `servicio-operador-localidadOrigen-createdAt[ne]`:
   *
   * - `servicio-operador-localidadOrigen-createdAt[lt]`:
   *
   * - `servicio-operador-localidadOrigen-createdAt[le]`:
   *
   * - `servicio-operador-localidadOrigen-createdAt[gt]`:
   *
   * - `servicio-operador-localidadOrigen-createdAt[ge]`:
   *
   * - `servicio-operador-localidadOrigen-createdAt`:
   *
   * - `servicio-operador-localidadOrigen-codigoINDEC[ne]`:
   *
   * - `servicio-operador-localidadOrigen-codigoINDEC[like]`:
   *
   * - `servicio-operador-localidadOrigen-codigoINDEC`:
   *
   * - `servicio-operador-localidadOrigen-activo`:
   *
   * - `servicio-operador-localidadDestino-updatedAt[ne]`:
   *
   * - `servicio-operador-localidadDestino-updatedAt[lt]`:
   *
   * - `servicio-operador-localidadDestino-updatedAt[le]`:
   *
   * - `servicio-operador-localidadDestino-updatedAt[gt]`:
   *
   * - `servicio-operador-localidadDestino-updatedAt[ge]`:
   *
   * - `servicio-operador-localidadDestino-updatedAt`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-id[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-id[lt]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-id[le]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-id[gt]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-id[ge]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-id`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-descripcion[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-descripcion[like]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-descripcion`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-codigo[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-codigo[like]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-codigo`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-activo`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-abrev[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-abrev[like]`:
   *
   * - `servicio-operador-localidadDestino-provincia-pais-abrev`:
   *
   * - `servicio-operador-localidadDestino-provincia-id[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-id[lt]`:
   *
   * - `servicio-operador-localidadDestino-provincia-id[le]`:
   *
   * - `servicio-operador-localidadDestino-provincia-id[gt]`:
   *
   * - `servicio-operador-localidadDestino-provincia-id[ge]`:
   *
   * - `servicio-operador-localidadDestino-provincia-id`:
   *
   * - `servicio-operador-localidadDestino-provincia-descripcion[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-descripcion[like]`:
   *
   * - `servicio-operador-localidadDestino-provincia-descripcion`:
   *
   * - `servicio-operador-localidadDestino-provincia-codigo[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-codigo[like]`:
   *
   * - `servicio-operador-localidadDestino-provincia-codigoINDEC[ne]`:
   *
   * - `servicio-operador-localidadDestino-provincia-codigoINDEC[like]`:
   *
   * - `servicio-operador-localidadDestino-provincia-codigoINDEC`:
   *
   * - `servicio-operador-localidadDestino-provincia-codigo`:
   *
   * - `servicio-operador-localidadDestino-provincia-activo`:
   *
   * - `servicio-operador-localidadDestino-parada`:
   *
   * - `servicio-operador-localidadDestino-lon[ne]`:
   *
   * - `servicio-operador-localidadDestino-lon[lt]`:
   *
   * - `servicio-operador-localidadDestino-lon[le]`:
   *
   * - `servicio-operador-localidadDestino-lon[gt]`:
   *
   * - `servicio-operador-localidadDestino-lon[ge]`:
   *
   * - `servicio-operador-localidadDestino-lon`:
   *
   * - `servicio-operador-localidadDestino-lat[ne]`:
   *
   * - `servicio-operador-localidadDestino-lat[lt]`:
   *
   * - `servicio-operador-localidadDestino-lat[le]`:
   *
   * - `servicio-operador-localidadDestino-lat[gt]`:
   *
   * - `servicio-operador-localidadDestino-lat[ge]`:
   *
   * - `servicio-operador-localidadDestino-lat`:
   *
   * - `servicio-operador-localidadDestino-id[ne]`:
   *
   * - `servicio-operador-localidadDestino-id[lt]`:
   *
   * - `servicio-operador-localidadDestino-id[le]`:
   *
   * - `servicio-operador-localidadDestino-id[gt]`:
   *
   * - `servicio-operador-localidadDestino-id[ge]`:
   *
   * - `servicio-operador-localidadDestino-id`:
   *
   * - `servicio-operador-localidadDestino-fuente[ne]`:
   *
   * - `servicio-operador-localidadDestino-fuente[like]`:
   *
   * - `servicio-operador-localidadDestino-fuente`:
   *
   * - `servicio-operador-localidadDestino-descripcion[ne]`:
   *
   * - `servicio-operador-localidadDestino-descripcion[like]`:
   *
   * - `servicio-operador-localidadDestino-descripcion`:
   *
   * - `servicio-operador-localidadDestino-createdAt[ne]`:
   *
   * - `servicio-operador-localidadDestino-createdAt[lt]`:
   *
   * - `servicio-operador-localidadDestino-createdAt[le]`:
   *
   * - `servicio-operador-localidadDestino-createdAt[gt]`:
   *
   * - `servicio-operador-localidadDestino-createdAt[ge]`:
   *
   * - `servicio-operador-localidadDestino-createdAt`:
   *
   * - `servicio-operador-localidadDestino-codigoINDEC[ne]`:
   *
   * - `servicio-operador-localidadDestino-codigoINDEC[like]`:
   *
   * - `servicio-operador-localidadDestino-codigoINDEC`:
   *
   * - `servicio-operador-localidadDestino-activo`:
   *
   * - `servicio-operador-linea[ne]`:
   *
   * - `servicio-operador-linea[like]`:
   *
   * - `servicio-operador-linea`:
   *
   * - `servicio-operador-id[ne]`:
   *
   * - `servicio-operador-id[lt]`:
   *
   * - `servicio-operador-id[le]`:
   *
   * - `servicio-operador-id[gt]`:
   *
   * - `servicio-operador-id[ge]`:
   *
   * - `servicio-operador-id`:
   *
   * - `servicio-operador-empresaContratante[ne]`:
   *
   * - `servicio-operador-empresaContratante[like]`:
   *
   * - `servicio-operador-empresaContratanteCuit[ne]`:
   *
   * - `servicio-operador-empresaContratanteCuit[like]`:
   *
   * - `servicio-operador-empresaContratanteCuit`:
   *
   * - `servicio-operador-empresaContratante`:
   *
   * - `servicio-operador-empresa-vigenciaHasta[ne]`:
   *
   * - `servicio-operador-empresa-vigenciaHasta[lt]`:
   *
   * - `servicio-operador-empresa-vigenciaHasta[le]`:
   *
   * - `servicio-operador-empresa-vigenciaHasta[gt]`:
   *
   * - `servicio-operador-empresa-vigenciaHasta[ge]`:
   *
   * - `servicio-operador-empresa-vigenciaHasta`:
   *
   * - `servicio-operador-empresa-vigenciaDesde[ne]`:
   *
   * - `servicio-operador-empresa-vigenciaDesde[lt]`:
   *
   * - `servicio-operador-empresa-vigenciaDesde[le]`:
   *
   * - `servicio-operador-empresa-vigenciaDesde[gt]`:
   *
   * - `servicio-operador-empresa-vigenciaDesde[ge]`:
   *
   * - `servicio-operador-empresa-vigenciaDesde`:
   *
   * - `servicio-operador-empresa-tipoSociedad[ne]`:
   *
   * - `servicio-operador-empresa-tipoSociedad[lt]`:
   *
   * - `servicio-operador-empresa-tipoSociedad[le]`:
   *
   * - `servicio-operador-empresa-tipoSociedad[gt]`:
   *
   * - `servicio-operador-empresa-tipoSociedad[ge]`:
   *
   * - `servicio-operador-empresa-tipoSociedad`:
   *
   * - `servicio-operador-empresa-tipoDocumento-id[ne]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-id[lt]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-id[le]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-id[gt]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-id[ge]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-id`:
   *
   * - `servicio-operador-empresa-tipoDocumento-descripcion[ne]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-descripcion[like]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-descripcion`:
   *
   * - `servicio-operador-empresa-tipoDocumento-abrev[ne]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-abrev[like]`:
   *
   * - `servicio-operador-empresa-tipoDocumento-abrev`:
   *
   * - `servicio-operador-empresa-textoHash`:
   *
   * - `servicio-operador-empresa-razonSocial[ne]`:
   *
   * - `servicio-operador-empresa-razonSocial[like]`:
   *
   * - `servicio-operador-empresa-razonSocial`:
   *
   * - `servicio-operador-empresa-paut[ne]`:
   *
   * - `servicio-operador-empresa-paut[like]`:
   *
   * - `servicio-operador-empresa-paut`:
   *
   * - `servicio-operador-empresa-observacion[ne]`:
   *
   * - `servicio-operador-empresa-observacion[like]`:
   *
   * - `servicio-operador-empresa-observacion`:
   *
   * - `servicio-operador-empresa-nroDocumento[ne]`:
   *
   * - `servicio-operador-empresa-nroDocumento[like]`:
   *
   * - `servicio-operador-empresa-nroDocumento`:
   *
   * - `servicio-operador-empresa-nombreFantasia[ne]`:
   *
   * - `servicio-operador-empresa-nombreFantasia[like]`:
   *
   * - `servicio-operador-empresa-nombreFantasia`:
   *
   * - `servicio-operador-empresa-id[ne]`:
   *
   * - `servicio-operador-empresa-id[lt]`:
   *
   * - `servicio-operador-empresa-id[le]`:
   *
   * - `servicio-operador-empresa-id[gt]`:
   *
   * - `servicio-operador-empresa-id[ge]`:
   *
   * - `servicio-operador-empresa-id`:
   *
   * - `servicio-operador-empresa-estadoEmpresa[ne]`:
   *
   * - `servicio-operador-empresa-estadoEmpresa[lt]`:
   *
   * - `servicio-operador-empresa-estadoEmpresa[le]`:
   *
   * - `servicio-operador-empresa-estadoEmpresa[gt]`:
   *
   * - `servicio-operador-empresa-estadoEmpresa[ge]`:
   *
   * - `servicio-operador-empresa-estadoEmpresa`:
   *
   * - `servicio-operador-empresa-esPersonaFisica`:
   *
   * - `servicio-operador-empresa-email[ne]`:
   *
   * - `servicio-operador-empresa-email[like]`:
   *
   * - `servicio-operador-empresa-email`:
   *
   * - `servicio-operador-empresa-cuitEmpresaAnterior[ne]`:
   *
   * - `servicio-operador-empresa-cuitEmpresaAnterior[like]`:
   *
   * - `servicio-operador-empresa-cuitEmpresaAnterior`:
   *
   * - `servicio-operador-claseModalidad-id[ne]`:
   *
   * - `servicio-operador-claseModalidad-id[lt]`:
   *
   * - `servicio-operador-claseModalidad-id[le]`:
   *
   * - `servicio-operador-claseModalidad-id[gt]`:
   *
   * - `servicio-operador-claseModalidad-id[ge]`:
   *
   * - `servicio-operador-claseModalidad-id`:
   *
   * - `servicio-operador-claseModalidad-descripcion[ne]`:
   *
   * - `servicio-operador-claseModalidad-descripcion[like]`:
   *
   * - `servicio-operador-claseModalidad-descripcion`:
   *
   * - `servicio-id[ne]`:
   *
   * - `servicio-id[lt]`:
   *
   * - `servicio-id[le]`:
   *
   * - `servicio-id[gt]`:
   *
   * - `servicio-id[ge]`:
   *
   * - `servicio-id`:
   *
   * - `servicio-horaSalida[ne]`:
   *
   * - `servicio-horaSalida[lt]`:
   *
   * - `servicio-horaSalida[le]`:
   *
   * - `servicio-horaSalida[gt]`:
   *
   * - `servicio-horaSalida[ge]`:
   *
   * - `servicio-horaSalida`:
   *
   * - `servicio-horaLlegada[ne]`:
   *
   * - `servicio-horaLlegada[lt]`:
   *
   * - `servicio-horaLlegada[le]`:
   *
   * - `servicio-horaLlegada[gt]`:
   *
   * - `servicio-horaLlegada[ge]`:
   *
   * - `servicio-horaLlegada`:
   *
   * - `servicio-aprobado`:
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `categoriaServicioPrimerPiso-id[ne]`:
   *
   * - `categoriaServicioPrimerPiso-id[lt]`:
   *
   * - `categoriaServicioPrimerPiso-id[le]`:
   *
   * - `categoriaServicioPrimerPiso-id[gt]`:
   *
   * - `categoriaServicioPrimerPiso-id[ge]`:
   *
   * - `categoriaServicioPrimerPiso-id`:
   *
   * - `categoriaServicioPrimerPiso-descripcion[ne]`:
   *
   * - `categoriaServicioPrimerPiso-descripcion[like]`:
   *
   * - `categoriaServicioPrimerPiso-descripcion`:
   *
   * - `categoriaServicioPrimerPiso-activo`:
   *
   * - `activo`:
   *
   * @return Respuesta exitosa.
   */
  getApiV2DiasServicios(params: DiaServicioService.GetApiV2DiasServiciosParams): __Observable<DiaServicioAPIResponsePaginacion> {
    return this.getApiV2DiasServiciosResponse(params).pipe(
      __map(_r => _r.body as DiaServicioAPIResponsePaginacion)
    );
  }

  /**
   * Obtiene un día servicio por ID
   * @param id Id de Día Servicio a buscar
   * @return Respuesta exitosa.
   */
  findDiaServicioResponse(id: number): __Observable<__StrictHttpResponse<DiaServicioAPIResponseData>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/diasServicios/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<DiaServicioAPIResponseData>;
      })
    );
  }
  /**
   * Obtiene un día servicio por ID
   * @param id Id de Día Servicio a buscar
   * @return Respuesta exitosa.
   */
  findDiaServicio(id: number): __Observable<DiaServicioAPIResponseData> {
    return this.findDiaServicioResponse(id).pipe(
      __map(_r => _r.body as DiaServicioAPIResponseData)
    );
  }
}

module DiaServicioService {

  /**
   * Parameters for getApiV2DiasServicios
   */
  export interface GetApiV2DiasServiciosParams {

    /**
     * Campo por el cual se ordena.
     */
    sort?: string;
    servicioVigenciaHastaNe?: any;
    servicioVigenciaHastaLt?: any;
    servicioVigenciaHastaLe?: any;
    servicioVigenciaHastaGt?: any;
    servicioVigenciaHastaGe?: any;
    servicioVigenciaHasta?: any;
    servicioVigenciaDesdeNe?: any;
    servicioVigenciaDesdeLt?: any;
    servicioVigenciaDesdeLe?: any;
    servicioVigenciaDesdeGt?: any;
    servicioVigenciaDesdeGe?: any;
    servicioVigenciaDesde?: any;
    servicioVelocidadPromedioTotalNe?: number;
    servicioVelocidadPromedioTotalLt?: number;
    servicioVelocidadPromedioTotalLe?: number;
    servicioVelocidadPromedioTotalGt?: number;
    servicioVelocidadPromedioTotalGe?: number;
    servicioVelocidadPromedioTotal?: number;
    servicioTramiteUsuarioNe?: string;
    servicioTramiteUsuarioLike?: string;
    servicioTramiteUsuario?: string;
    servicioTramiteTipoTramiteIdNe?: number;
    servicioTramiteTipoTramiteIdLt?: number;
    servicioTramiteTipoTramiteIdLe?: number;
    servicioTramiteTipoTramiteIdGt?: number;
    servicioTramiteTipoTramiteIdGe?: number;
    servicioTramiteTipoTramiteId?: number;
    servicioTramiteTipoTramiteDescripcionNe?: string;
    servicioTramiteTipoTramiteDescripcionLike?: string;
    servicioTramiteTipoTramiteDescripcion?: string;
    servicioTramiteTipoTramiteActivo?: boolean;
    servicioTramiteSubtipoTramiteIdNe?: number;
    servicioTramiteSubtipoTramiteIdLt?: number;
    servicioTramiteSubtipoTramiteIdLe?: number;
    servicioTramiteSubtipoTramiteIdGt?: number;
    servicioTramiteSubtipoTramiteIdGe?: number;
    servicioTramiteSubtipoTramiteId?: number;
    servicioTramiteSubtipoTramiteDescripcionNe?: string;
    servicioTramiteSubtipoTramiteDescripcionLike?: string;
    servicioTramiteSubtipoTramiteDescripcion?: string;
    servicioTramiteSubtipoTramiteActivo?: boolean;
    servicioTramiteNroBoletaPagoNe?: number;
    servicioTramiteNroBoletaPagoLt?: number;
    servicioTramiteNroBoletaPagoLe?: number;
    servicioTramiteNroBoletaPagoGt?: number;
    servicioTramiteNroBoletaPagoGe?: number;
    servicioTramiteNroBoletaPago?: number;
    servicioTramiteIdNe?: number;
    servicioTramiteIdLt?: number;
    servicioTramiteIdLe?: number;
    servicioTramiteIdGt?: number;
    servicioTramiteIdGe?: number;
    servicioTramiteId?: number;
    servicioTramiteEstadoTramiteIdNe?: any;
    servicioTramiteEstadoTramiteIdLt?: any;
    servicioTramiteEstadoTramiteIdLe?: any;
    servicioTramiteEstadoTramiteIdGt?: any;
    servicioTramiteEstadoTramiteIdGe?: any;
    servicioTramiteEstadoTramiteId?: any;
    servicioTramiteEstadoTramiteDescripcionNe?: string;
    servicioTramiteEstadoTramiteDescripcionLike?: string;
    servicioTramiteEstadoTramiteDescripcion?: string;
    servicioTramiteEstadoTramiteActivo?: boolean;
    servicioTramiteEstadoTramiteAbrevNe?: string;
    servicioTramiteEstadoTramiteAbrevLike?: string;
    servicioTramiteEstadoTramiteAbrev?: string;
    servicioTramiteEmpresaVigenciaHastaNe?: any;
    servicioTramiteEmpresaVigenciaHastaLt?: any;
    servicioTramiteEmpresaVigenciaHastaLe?: any;
    servicioTramiteEmpresaVigenciaHastaGt?: any;
    servicioTramiteEmpresaVigenciaHastaGe?: any;
    servicioTramiteEmpresaVigenciaHasta?: any;
    servicioTramiteEmpresaVigenciaDesdeNe?: any;
    servicioTramiteEmpresaVigenciaDesdeLt?: any;
    servicioTramiteEmpresaVigenciaDesdeLe?: any;
    servicioTramiteEmpresaVigenciaDesdeGt?: any;
    servicioTramiteEmpresaVigenciaDesdeGe?: any;
    servicioTramiteEmpresaVigenciaDesde?: any;
    servicioTramiteEmpresaTipoSociedadNe?: number;
    servicioTramiteEmpresaTipoSociedadLt?: number;
    servicioTramiteEmpresaTipoSociedadLe?: number;
    servicioTramiteEmpresaTipoSociedadGt?: number;
    servicioTramiteEmpresaTipoSociedadGe?: number;
    servicioTramiteEmpresaTipoSociedad?: number;
    servicioTramiteEmpresaTipoDocumentoIdNe?: number;
    servicioTramiteEmpresaTipoDocumentoIdLt?: number;
    servicioTramiteEmpresaTipoDocumentoIdLe?: number;
    servicioTramiteEmpresaTipoDocumentoIdGt?: number;
    servicioTramiteEmpresaTipoDocumentoIdGe?: number;
    servicioTramiteEmpresaTipoDocumentoId?: number;
    servicioTramiteEmpresaTipoDocumentoDescripcionNe?: string;
    servicioTramiteEmpresaTipoDocumentoDescripcionLike?: string;
    servicioTramiteEmpresaTipoDocumentoDescripcion?: string;
    servicioTramiteEmpresaTipoDocumentoAbrevNe?: any;
    servicioTramiteEmpresaTipoDocumentoAbrevLike?: any;
    servicioTramiteEmpresaTipoDocumentoAbrev?: any;
    servicioTramiteEmpresaTextoHash?: string;
    servicioTramiteEmpresaRazonSocialNe?: string;
    servicioTramiteEmpresaRazonSocialLike?: string;
    servicioTramiteEmpresaRazonSocial?: string;
    servicioTramiteEmpresaPautNe?: string;
    servicioTramiteEmpresaPautLike?: string;
    servicioTramiteEmpresaPaut?: string;
    servicioTramiteEmpresaObservacionNe?: string;
    servicioTramiteEmpresaObservacionLike?: string;
    servicioTramiteEmpresaObservacion?: string;
    servicioTramiteEmpresaNroDocumentoNe?: string;
    servicioTramiteEmpresaNroDocumentoLike?: string;
    servicioTramiteEmpresaNroDocumento?: string;
    servicioTramiteEmpresaNombreFantasiaNe?: string;
    servicioTramiteEmpresaNombreFantasiaLike?: string;
    servicioTramiteEmpresaNombreFantasia?: string;
    servicioTramiteEmpresaIdNe?: number;
    servicioTramiteEmpresaIdLt?: number;
    servicioTramiteEmpresaIdLe?: number;
    servicioTramiteEmpresaIdGt?: number;
    servicioTramiteEmpresaIdGe?: number;
    servicioTramiteEmpresaId?: number;
    servicioTramiteEmpresaEstadoEmpresaNe?: number;
    servicioTramiteEmpresaEstadoEmpresaLt?: number;
    servicioTramiteEmpresaEstadoEmpresaLe?: number;
    servicioTramiteEmpresaEstadoEmpresaGt?: number;
    servicioTramiteEmpresaEstadoEmpresaGe?: number;
    servicioTramiteEmpresaEstadoEmpresa?: number;
    servicioTramiteEmpresaEsPersonaFisica?: boolean;
    servicioTramiteEmpresaEmailNe?: string;
    servicioTramiteEmpresaEmailLike?: string;
    servicioTramiteEmpresaEmail?: string;
    servicioTramiteEmpresaCuitEmpresaAnteriorNe?: string;
    servicioTramiteEmpresaCuitEmpresaAnteriorLike?: string;
    servicioTramiteEmpresaCuitEmpresaAnterior?: string;
    servicioRecorridoSentidoVinculacionCamineraNe?: string;
    servicioRecorridoSentidoVinculacionCamineraLike?: string;
    servicioRecorridoSentidoVinculacionCaminera?: string;
    servicioRecorridoSentidoVigenciaHastaNe?: any;
    servicioRecorridoSentidoVigenciaHastaLt?: any;
    servicioRecorridoSentidoVigenciaHastaLe?: any;
    servicioRecorridoSentidoVigenciaHastaGt?: any;
    servicioRecorridoSentidoVigenciaHastaGe?: any;
    servicioRecorridoSentidoVigenciaHasta?: any;
    servicioRecorridoSentidoVigenciaDesdeNe?: any;
    servicioRecorridoSentidoVigenciaDesdeLt?: any;
    servicioRecorridoSentidoVigenciaDesdeLe?: any;
    servicioRecorridoSentidoVigenciaDesdeGt?: any;
    servicioRecorridoSentidoVigenciaDesdeGe?: any;
    servicioRecorridoSentidoVigenciaDesde?: any;
    servicioRecorridoSentidoSentidoIdNe?: number;
    servicioRecorridoSentidoSentidoIdLt?: number;
    servicioRecorridoSentidoSentidoIdLe?: number;
    servicioRecorridoSentidoSentidoIdGt?: number;
    servicioRecorridoSentidoSentidoIdGe?: number;
    servicioRecorridoSentidoSentidoId?: number;
    servicioRecorridoSentidoSentidoDescripcionNe?: string;
    servicioRecorridoSentidoSentidoDescripcionLike?: string;
    servicioRecorridoSentidoSentidoDescripcion?: string;
    servicioRecorridoSentidoSentidoActivo?: boolean;
    servicioRecorridoSentidoSentidoAbrevNe?: string;
    servicioRecorridoSentidoSentidoAbrevLike?: string;
    servicioRecorridoSentidoSentidoAbrev?: string;
    servicioRecorridoSentidoRecorridoVinculacionCamineraNe?: string;
    servicioRecorridoSentidoRecorridoVinculacionCamineraLike?: string;
    servicioRecorridoSentidoRecorridoVinculacionCaminera?: string;
    servicioRecorridoSentidoRecorridoVigenciaHastaNe?: any;
    servicioRecorridoSentidoRecorridoVigenciaHastaLt?: any;
    servicioRecorridoSentidoRecorridoVigenciaHastaLe?: any;
    servicioRecorridoSentidoRecorridoVigenciaHastaGt?: any;
    servicioRecorridoSentidoRecorridoVigenciaHastaGe?: any;
    servicioRecorridoSentidoRecorridoVigenciaHasta?: any;
    servicioRecorridoSentidoRecorridoVigenciaDesdeNe?: any;
    servicioRecorridoSentidoRecorridoVigenciaDesdeLt?: any;
    servicioRecorridoSentidoRecorridoVigenciaDesdeLe?: any;
    servicioRecorridoSentidoRecorridoVigenciaDesdeGt?: any;
    servicioRecorridoSentidoRecorridoVigenciaDesdeGe?: any;
    servicioRecorridoSentidoRecorridoVigenciaDesde?: any;
    servicioRecorridoSentidoRecorridoTransitoNe?: string;
    servicioRecorridoSentidoRecorridoTransitoLike?: string;
    servicioRecorridoSentidoRecorridoTransito?: string;
    servicioRecorridoSentidoRecorridoSitioOrigenNe?: string;
    servicioRecorridoSentidoRecorridoSitioOrigenLike?: string;
    servicioRecorridoSentidoRecorridoSitioOrigen?: string;
    servicioRecorridoSentidoRecorridoSitioDestinoNe?: string;
    servicioRecorridoSentidoRecorridoSitioDestinoLike?: string;
    servicioRecorridoSentidoRecorridoSitioDestino?: string;
    servicioRecorridoSentidoRecorridoRecorridoNe?: string;
    servicioRecorridoSentidoRecorridoRecorridoLike?: string;
    servicioRecorridoSentidoRecorridoRecorrido?: string;
    servicioRecorridoSentidoRecorridoOrigenNe?: string;
    servicioRecorridoSentidoRecorridoOrigenLike?: string;
    servicioRecorridoSentidoRecorridoOrigen?: string;
    servicioRecorridoSentidoRecorridoItinerarioVueltaNe?: string;
    servicioRecorridoSentidoRecorridoItinerarioVueltaLike?: string;
    servicioRecorridoSentidoRecorridoItinerarioVuelta?: string;
    servicioRecorridoSentidoRecorridoItinerarioIdaNe?: string;
    servicioRecorridoSentidoRecorridoItinerarioIdaLike?: string;
    servicioRecorridoSentidoRecorridoItinerarioIda?: string;
    servicioRecorridoSentidoRecorridoIdNe?: number;
    servicioRecorridoSentidoRecorridoIdLt?: number;
    servicioRecorridoSentidoRecorridoIdLe?: number;
    servicioRecorridoSentidoRecorridoIdGt?: number;
    servicioRecorridoSentidoRecorridoIdGe?: number;
    servicioRecorridoSentidoRecorridoId?: number;
    servicioRecorridoSentidoRecorridoDestinoNe?: string;
    servicioRecorridoSentidoRecorridoDestinoLike?: string;
    servicioRecorridoSentidoRecorridoDestino?: string;
    servicioRecorridoSentidoRecorridoAplicaGasoil?: boolean;
    servicioRecorridoSentidoOrigenNe?: string;
    servicioRecorridoSentidoOrigenLike?: string;
    servicioRecorridoSentidoOrigen?: string;
    servicioRecorridoSentidoLocalidadOrigenUpdatedAtNe?: any;
    servicioRecorridoSentidoLocalidadOrigenUpdatedAtLt?: any;
    servicioRecorridoSentidoLocalidadOrigenUpdatedAtLe?: any;
    servicioRecorridoSentidoLocalidadOrigenUpdatedAtGt?: any;
    servicioRecorridoSentidoLocalidadOrigenUpdatedAtGe?: any;
    servicioRecorridoSentidoLocalidadOrigenUpdatedAt?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdNe?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdLt?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdLe?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdGt?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisIdGe?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisId?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisDescripcionNe?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisDescripcionLike?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisDescripcion?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisCodigoNe?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisCodigoLike?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisCodigo?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisActivo?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisAbrevNe?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisAbrevLike?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaPaisAbrev?: any;
    servicioRecorridoSentidoLocalidadOrigenProvinciaIdNe?: number;
    servicioRecorridoSentidoLocalidadOrigenProvinciaIdLt?: number;
    servicioRecorridoSentidoLocalidadOrigenProvinciaIdLe?: number;
    servicioRecorridoSentidoLocalidadOrigenProvinciaIdGt?: number;
    servicioRecorridoSentidoLocalidadOrigenProvinciaIdGe?: number;
    servicioRecorridoSentidoLocalidadOrigenProvinciaId?: number;
    servicioRecorridoSentidoLocalidadOrigenProvinciaDescripcionNe?: string;
    servicioRecorridoSentidoLocalidadOrigenProvinciaDescripcionLike?: string;
    servicioRecorridoSentidoLocalidadOrigenProvinciaDescripcion?: string;
    servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoNe?: string;
    servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoLike?: string;
    servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoINDECNe?: string;
    servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoINDECLike?: string;
    servicioRecorridoSentidoLocalidadOrigenProvinciaCodigoINDEC?: string;
    servicioRecorridoSentidoLocalidadOrigenProvinciaCodigo?: string;
    servicioRecorridoSentidoLocalidadOrigenProvinciaActivo?: boolean;
    servicioRecorridoSentidoLocalidadOrigenParada?: any;
    servicioRecorridoSentidoLocalidadOrigenLonNe?: number;
    servicioRecorridoSentidoLocalidadOrigenLonLt?: number;
    servicioRecorridoSentidoLocalidadOrigenLonLe?: number;
    servicioRecorridoSentidoLocalidadOrigenLonGt?: number;
    servicioRecorridoSentidoLocalidadOrigenLonGe?: number;
    servicioRecorridoSentidoLocalidadOrigenLon?: number;
    servicioRecorridoSentidoLocalidadOrigenLatNe?: number;
    servicioRecorridoSentidoLocalidadOrigenLatLt?: number;
    servicioRecorridoSentidoLocalidadOrigenLatLe?: number;
    servicioRecorridoSentidoLocalidadOrigenLatGt?: number;
    servicioRecorridoSentidoLocalidadOrigenLatGe?: number;
    servicioRecorridoSentidoLocalidadOrigenLat?: number;
    servicioRecorridoSentidoLocalidadOrigenIdNe?: number;
    servicioRecorridoSentidoLocalidadOrigenIdLt?: number;
    servicioRecorridoSentidoLocalidadOrigenIdLe?: number;
    servicioRecorridoSentidoLocalidadOrigenIdGt?: number;
    servicioRecorridoSentidoLocalidadOrigenIdGe?: number;
    servicioRecorridoSentidoLocalidadOrigenId?: number;
    servicioRecorridoSentidoLocalidadOrigenFuenteNe?: string;
    servicioRecorridoSentidoLocalidadOrigenFuenteLike?: string;
    servicioRecorridoSentidoLocalidadOrigenFuente?: string;
    servicioRecorridoSentidoLocalidadOrigenDescripcionNe?: string;
    servicioRecorridoSentidoLocalidadOrigenDescripcionLike?: string;
    servicioRecorridoSentidoLocalidadOrigenDescripcion?: string;
    servicioRecorridoSentidoLocalidadOrigenCreatedAtNe?: any;
    servicioRecorridoSentidoLocalidadOrigenCreatedAtLt?: any;
    servicioRecorridoSentidoLocalidadOrigenCreatedAtLe?: any;
    servicioRecorridoSentidoLocalidadOrigenCreatedAtGt?: any;
    servicioRecorridoSentidoLocalidadOrigenCreatedAtGe?: any;
    servicioRecorridoSentidoLocalidadOrigenCreatedAt?: any;
    servicioRecorridoSentidoLocalidadOrigenCodigoINDECNe?: string;
    servicioRecorridoSentidoLocalidadOrigenCodigoINDECLike?: string;
    servicioRecorridoSentidoLocalidadOrigenCodigoINDEC?: string;
    servicioRecorridoSentidoLocalidadOrigenActivo?: boolean;
    servicioRecorridoSentidoLocalidadDestinoUpdatedAtNe?: any;
    servicioRecorridoSentidoLocalidadDestinoUpdatedAtLt?: any;
    servicioRecorridoSentidoLocalidadDestinoUpdatedAtLe?: any;
    servicioRecorridoSentidoLocalidadDestinoUpdatedAtGt?: any;
    servicioRecorridoSentidoLocalidadDestinoUpdatedAtGe?: any;
    servicioRecorridoSentidoLocalidadDestinoUpdatedAt?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdNe?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdLt?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdLe?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdGt?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisIdGe?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisId?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisDescripcionNe?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisDescripcionLike?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisDescripcion?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisCodigoNe?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisCodigoLike?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisCodigo?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisActivo?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisAbrevNe?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisAbrevLike?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaPaisAbrev?: any;
    servicioRecorridoSentidoLocalidadDestinoProvinciaIdNe?: number;
    servicioRecorridoSentidoLocalidadDestinoProvinciaIdLt?: number;
    servicioRecorridoSentidoLocalidadDestinoProvinciaIdLe?: number;
    servicioRecorridoSentidoLocalidadDestinoProvinciaIdGt?: number;
    servicioRecorridoSentidoLocalidadDestinoProvinciaIdGe?: number;
    servicioRecorridoSentidoLocalidadDestinoProvinciaId?: number;
    servicioRecorridoSentidoLocalidadDestinoProvinciaDescripcionNe?: string;
    servicioRecorridoSentidoLocalidadDestinoProvinciaDescripcionLike?: string;
    servicioRecorridoSentidoLocalidadDestinoProvinciaDescripcion?: string;
    servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoNe?: string;
    servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoLike?: string;
    servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoINDECNe?: string;
    servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoINDECLike?: string;
    servicioRecorridoSentidoLocalidadDestinoProvinciaCodigoINDEC?: string;
    servicioRecorridoSentidoLocalidadDestinoProvinciaCodigo?: string;
    servicioRecorridoSentidoLocalidadDestinoProvinciaActivo?: boolean;
    servicioRecorridoSentidoLocalidadDestinoParada?: any;
    servicioRecorridoSentidoLocalidadDestinoLonNe?: number;
    servicioRecorridoSentidoLocalidadDestinoLonLt?: number;
    servicioRecorridoSentidoLocalidadDestinoLonLe?: number;
    servicioRecorridoSentidoLocalidadDestinoLonGt?: number;
    servicioRecorridoSentidoLocalidadDestinoLonGe?: number;
    servicioRecorridoSentidoLocalidadDestinoLon?: number;
    servicioRecorridoSentidoLocalidadDestinoLatNe?: number;
    servicioRecorridoSentidoLocalidadDestinoLatLt?: number;
    servicioRecorridoSentidoLocalidadDestinoLatLe?: number;
    servicioRecorridoSentidoLocalidadDestinoLatGt?: number;
    servicioRecorridoSentidoLocalidadDestinoLatGe?: number;
    servicioRecorridoSentidoLocalidadDestinoLat?: number;
    servicioRecorridoSentidoLocalidadDestinoIdNe?: number;
    servicioRecorridoSentidoLocalidadDestinoIdLt?: number;
    servicioRecorridoSentidoLocalidadDestinoIdLe?: number;
    servicioRecorridoSentidoLocalidadDestinoIdGt?: number;
    servicioRecorridoSentidoLocalidadDestinoIdGe?: number;
    servicioRecorridoSentidoLocalidadDestinoId?: number;
    servicioRecorridoSentidoLocalidadDestinoFuenteNe?: string;
    servicioRecorridoSentidoLocalidadDestinoFuenteLike?: string;
    servicioRecorridoSentidoLocalidadDestinoFuente?: string;
    servicioRecorridoSentidoLocalidadDestinoDescripcionNe?: string;
    servicioRecorridoSentidoLocalidadDestinoDescripcionLike?: string;
    servicioRecorridoSentidoLocalidadDestinoDescripcion?: string;
    servicioRecorridoSentidoLocalidadDestinoCreatedAtNe?: any;
    servicioRecorridoSentidoLocalidadDestinoCreatedAtLt?: any;
    servicioRecorridoSentidoLocalidadDestinoCreatedAtLe?: any;
    servicioRecorridoSentidoLocalidadDestinoCreatedAtGt?: any;
    servicioRecorridoSentidoLocalidadDestinoCreatedAtGe?: any;
    servicioRecorridoSentidoLocalidadDestinoCreatedAt?: any;
    servicioRecorridoSentidoLocalidadDestinoCodigoINDECNe?: string;
    servicioRecorridoSentidoLocalidadDestinoCodigoINDECLike?: string;
    servicioRecorridoSentidoLocalidadDestinoCodigoINDEC?: string;
    servicioRecorridoSentidoLocalidadDestinoActivo?: boolean;
    servicioRecorridoSentidoItinerarioNe?: string;
    servicioRecorridoSentidoItinerarioLike?: string;
    servicioRecorridoSentidoItinerario?: string;
    servicioRecorridoSentidoIdNe?: number;
    servicioRecorridoSentidoIdLt?: number;
    servicioRecorridoSentidoIdLe?: number;
    servicioRecorridoSentidoIdGt?: number;
    servicioRecorridoSentidoIdGe?: number;
    servicioRecorridoSentidoId?: number;
    servicioRecorridoSentidoDestinoNe?: string;
    servicioRecorridoSentidoDestinoLike?: string;
    servicioRecorridoSentidoDestino?: string;
    servicioRecorridoVinculacionCamineraNe?: string;
    servicioRecorridoVinculacionCamineraLike?: string;
    servicioRecorridoVinculacionCaminera?: string;
    servicioRecorridoVigenciaHastaNe?: any;
    servicioRecorridoVigenciaHastaLt?: any;
    servicioRecorridoVigenciaHastaLe?: any;
    servicioRecorridoVigenciaHastaGt?: any;
    servicioRecorridoVigenciaHastaGe?: any;
    servicioRecorridoVigenciaHasta?: any;
    servicioRecorridoVigenciaDesdeNe?: any;
    servicioRecorridoVigenciaDesdeLt?: any;
    servicioRecorridoVigenciaDesdeLe?: any;
    servicioRecorridoVigenciaDesdeGt?: any;
    servicioRecorridoVigenciaDesdeGe?: any;
    servicioRecorridoVigenciaDesde?: any;
    servicioRecorridoTransitoNe?: string;
    servicioRecorridoTransitoLike?: string;
    servicioRecorridoTransito?: string;
    servicioRecorridoSitioOrigenNe?: string;
    servicioRecorridoSitioOrigenLike?: string;
    servicioRecorridoSitioOrigen?: string;
    servicioRecorridoSitioDestinoNe?: string;
    servicioRecorridoSitioDestinoLike?: string;
    servicioRecorridoSitioDestino?: string;
    servicioRecorridoRecorridoNe?: string;
    servicioRecorridoRecorridoLike?: string;
    servicioRecorridoRecorrido?: string;
    servicioRecorridoOrigenNe?: string;
    servicioRecorridoOrigenLike?: string;
    servicioRecorridoOrigen?: string;
    servicioRecorridoItinerarioVueltaNe?: string;
    servicioRecorridoItinerarioVueltaLike?: string;
    servicioRecorridoItinerarioVuelta?: string;
    servicioRecorridoItinerarioIdaNe?: string;
    servicioRecorridoItinerarioIdaLike?: string;
    servicioRecorridoItinerarioIda?: string;
    servicioRecorridoIdNe?: number;
    servicioRecorridoIdLt?: number;
    servicioRecorridoIdLe?: number;
    servicioRecorridoIdGt?: number;
    servicioRecorridoIdGe?: number;
    servicioRecorridoId?: number;
    servicioRecorridoDestinoNe?: string;
    servicioRecorridoDestinoLike?: string;
    servicioRecorridoDestino?: string;
    servicioRecorridoAplicaGasoil?: boolean;
    servicioOperadorVigenciaHastaNe?: any;
    servicioOperadorVigenciaHastaLt?: any;
    servicioOperadorVigenciaHastaLe?: any;
    servicioOperadorVigenciaHastaGt?: any;
    servicioOperadorVigenciaHastaGe?: any;
    servicioOperadorVigenciaHasta?: any;
    servicioOperadorVigenciaDesdeNe?: any;
    servicioOperadorVigenciaDesdeLt?: any;
    servicioOperadorVigenciaDesdeLe?: any;
    servicioOperadorVigenciaDesdeGt?: any;
    servicioOperadorVigenciaDesdeGe?: any;
    servicioOperadorVigenciaDesde?: any;
    servicioOperadorLocalidadOrigenUpdatedAtNe?: any;
    servicioOperadorLocalidadOrigenUpdatedAtLt?: any;
    servicioOperadorLocalidadOrigenUpdatedAtLe?: any;
    servicioOperadorLocalidadOrigenUpdatedAtGt?: any;
    servicioOperadorLocalidadOrigenUpdatedAtGe?: any;
    servicioOperadorLocalidadOrigenUpdatedAt?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisIdNe?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisIdLt?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisIdLe?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisIdGt?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisIdGe?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisId?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisDescripcionNe?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisDescripcionLike?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisDescripcion?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisCodigoNe?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisCodigoLike?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisCodigo?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisActivo?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisAbrevNe?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisAbrevLike?: any;
    servicioOperadorLocalidadOrigenProvinciaPaisAbrev?: any;
    servicioOperadorLocalidadOrigenProvinciaIdNe?: number;
    servicioOperadorLocalidadOrigenProvinciaIdLt?: number;
    servicioOperadorLocalidadOrigenProvinciaIdLe?: number;
    servicioOperadorLocalidadOrigenProvinciaIdGt?: number;
    servicioOperadorLocalidadOrigenProvinciaIdGe?: number;
    servicioOperadorLocalidadOrigenProvinciaId?: number;
    servicioOperadorLocalidadOrigenProvinciaDescripcionNe?: string;
    servicioOperadorLocalidadOrigenProvinciaDescripcionLike?: string;
    servicioOperadorLocalidadOrigenProvinciaDescripcion?: string;
    servicioOperadorLocalidadOrigenProvinciaCodigoNe?: string;
    servicioOperadorLocalidadOrigenProvinciaCodigoLike?: string;
    servicioOperadorLocalidadOrigenProvinciaCodigoINDECNe?: string;
    servicioOperadorLocalidadOrigenProvinciaCodigoINDECLike?: string;
    servicioOperadorLocalidadOrigenProvinciaCodigoINDEC?: string;
    servicioOperadorLocalidadOrigenProvinciaCodigo?: string;
    servicioOperadorLocalidadOrigenProvinciaActivo?: boolean;
    servicioOperadorLocalidadOrigenParada?: any;
    servicioOperadorLocalidadOrigenLonNe?: number;
    servicioOperadorLocalidadOrigenLonLt?: number;
    servicioOperadorLocalidadOrigenLonLe?: number;
    servicioOperadorLocalidadOrigenLonGt?: number;
    servicioOperadorLocalidadOrigenLonGe?: number;
    servicioOperadorLocalidadOrigenLon?: number;
    servicioOperadorLocalidadOrigenLatNe?: number;
    servicioOperadorLocalidadOrigenLatLt?: number;
    servicioOperadorLocalidadOrigenLatLe?: number;
    servicioOperadorLocalidadOrigenLatGt?: number;
    servicioOperadorLocalidadOrigenLatGe?: number;
    servicioOperadorLocalidadOrigenLat?: number;
    servicioOperadorLocalidadOrigenIdNe?: number;
    servicioOperadorLocalidadOrigenIdLt?: number;
    servicioOperadorLocalidadOrigenIdLe?: number;
    servicioOperadorLocalidadOrigenIdGt?: number;
    servicioOperadorLocalidadOrigenIdGe?: number;
    servicioOperadorLocalidadOrigenId?: number;
    servicioOperadorLocalidadOrigenFuenteNe?: string;
    servicioOperadorLocalidadOrigenFuenteLike?: string;
    servicioOperadorLocalidadOrigenFuente?: string;
    servicioOperadorLocalidadOrigenDescripcionNe?: string;
    servicioOperadorLocalidadOrigenDescripcionLike?: string;
    servicioOperadorLocalidadOrigenDescripcion?: string;
    servicioOperadorLocalidadOrigenCreatedAtNe?: any;
    servicioOperadorLocalidadOrigenCreatedAtLt?: any;
    servicioOperadorLocalidadOrigenCreatedAtLe?: any;
    servicioOperadorLocalidadOrigenCreatedAtGt?: any;
    servicioOperadorLocalidadOrigenCreatedAtGe?: any;
    servicioOperadorLocalidadOrigenCreatedAt?: any;
    servicioOperadorLocalidadOrigenCodigoINDECNe?: string;
    servicioOperadorLocalidadOrigenCodigoINDECLike?: string;
    servicioOperadorLocalidadOrigenCodigoINDEC?: string;
    servicioOperadorLocalidadOrigenActivo?: boolean;
    servicioOperadorLocalidadDestinoUpdatedAtNe?: any;
    servicioOperadorLocalidadDestinoUpdatedAtLt?: any;
    servicioOperadorLocalidadDestinoUpdatedAtLe?: any;
    servicioOperadorLocalidadDestinoUpdatedAtGt?: any;
    servicioOperadorLocalidadDestinoUpdatedAtGe?: any;
    servicioOperadorLocalidadDestinoUpdatedAt?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisIdNe?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisIdLt?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisIdLe?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisIdGt?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisIdGe?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisId?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisDescripcionNe?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisDescripcionLike?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisDescripcion?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisCodigoNe?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisCodigoLike?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisCodigo?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisActivo?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisAbrevNe?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisAbrevLike?: any;
    servicioOperadorLocalidadDestinoProvinciaPaisAbrev?: any;
    servicioOperadorLocalidadDestinoProvinciaIdNe?: number;
    servicioOperadorLocalidadDestinoProvinciaIdLt?: number;
    servicioOperadorLocalidadDestinoProvinciaIdLe?: number;
    servicioOperadorLocalidadDestinoProvinciaIdGt?: number;
    servicioOperadorLocalidadDestinoProvinciaIdGe?: number;
    servicioOperadorLocalidadDestinoProvinciaId?: number;
    servicioOperadorLocalidadDestinoProvinciaDescripcionNe?: string;
    servicioOperadorLocalidadDestinoProvinciaDescripcionLike?: string;
    servicioOperadorLocalidadDestinoProvinciaDescripcion?: string;
    servicioOperadorLocalidadDestinoProvinciaCodigoNe?: string;
    servicioOperadorLocalidadDestinoProvinciaCodigoLike?: string;
    servicioOperadorLocalidadDestinoProvinciaCodigoINDECNe?: string;
    servicioOperadorLocalidadDestinoProvinciaCodigoINDECLike?: string;
    servicioOperadorLocalidadDestinoProvinciaCodigoINDEC?: string;
    servicioOperadorLocalidadDestinoProvinciaCodigo?: string;
    servicioOperadorLocalidadDestinoProvinciaActivo?: boolean;
    servicioOperadorLocalidadDestinoParada?: any;
    servicioOperadorLocalidadDestinoLonNe?: number;
    servicioOperadorLocalidadDestinoLonLt?: number;
    servicioOperadorLocalidadDestinoLonLe?: number;
    servicioOperadorLocalidadDestinoLonGt?: number;
    servicioOperadorLocalidadDestinoLonGe?: number;
    servicioOperadorLocalidadDestinoLon?: number;
    servicioOperadorLocalidadDestinoLatNe?: number;
    servicioOperadorLocalidadDestinoLatLt?: number;
    servicioOperadorLocalidadDestinoLatLe?: number;
    servicioOperadorLocalidadDestinoLatGt?: number;
    servicioOperadorLocalidadDestinoLatGe?: number;
    servicioOperadorLocalidadDestinoLat?: number;
    servicioOperadorLocalidadDestinoIdNe?: number;
    servicioOperadorLocalidadDestinoIdLt?: number;
    servicioOperadorLocalidadDestinoIdLe?: number;
    servicioOperadorLocalidadDestinoIdGt?: number;
    servicioOperadorLocalidadDestinoIdGe?: number;
    servicioOperadorLocalidadDestinoId?: number;
    servicioOperadorLocalidadDestinoFuenteNe?: string;
    servicioOperadorLocalidadDestinoFuenteLike?: string;
    servicioOperadorLocalidadDestinoFuente?: string;
    servicioOperadorLocalidadDestinoDescripcionNe?: string;
    servicioOperadorLocalidadDestinoDescripcionLike?: string;
    servicioOperadorLocalidadDestinoDescripcion?: string;
    servicioOperadorLocalidadDestinoCreatedAtNe?: any;
    servicioOperadorLocalidadDestinoCreatedAtLt?: any;
    servicioOperadorLocalidadDestinoCreatedAtLe?: any;
    servicioOperadorLocalidadDestinoCreatedAtGt?: any;
    servicioOperadorLocalidadDestinoCreatedAtGe?: any;
    servicioOperadorLocalidadDestinoCreatedAt?: any;
    servicioOperadorLocalidadDestinoCodigoINDECNe?: string;
    servicioOperadorLocalidadDestinoCodigoINDECLike?: string;
    servicioOperadorLocalidadDestinoCodigoINDEC?: string;
    servicioOperadorLocalidadDestinoActivo?: boolean;
    servicioOperadorLineaNe?: string;
    servicioOperadorLineaLike?: string;
    servicioOperadorLinea?: string;
    servicioOperadorIdNe?: number;
    servicioOperadorIdLt?: number;
    servicioOperadorIdLe?: number;
    servicioOperadorIdGt?: number;
    servicioOperadorIdGe?: number;
    servicioOperadorId?: number;
    servicioOperadorEmpresaContratanteNe?: string;
    servicioOperadorEmpresaContratanteLike?: string;
    servicioOperadorEmpresaContratanteCuitNe?: string;
    servicioOperadorEmpresaContratanteCuitLike?: string;
    servicioOperadorEmpresaContratanteCuit?: string;
    servicioOperadorEmpresaContratante?: string;
    servicioOperadorEmpresaVigenciaHastaNe?: any;
    servicioOperadorEmpresaVigenciaHastaLt?: any;
    servicioOperadorEmpresaVigenciaHastaLe?: any;
    servicioOperadorEmpresaVigenciaHastaGt?: any;
    servicioOperadorEmpresaVigenciaHastaGe?: any;
    servicioOperadorEmpresaVigenciaHasta?: any;
    servicioOperadorEmpresaVigenciaDesdeNe?: any;
    servicioOperadorEmpresaVigenciaDesdeLt?: any;
    servicioOperadorEmpresaVigenciaDesdeLe?: any;
    servicioOperadorEmpresaVigenciaDesdeGt?: any;
    servicioOperadorEmpresaVigenciaDesdeGe?: any;
    servicioOperadorEmpresaVigenciaDesde?: any;
    servicioOperadorEmpresaTipoSociedadNe?: number;
    servicioOperadorEmpresaTipoSociedadLt?: number;
    servicioOperadorEmpresaTipoSociedadLe?: number;
    servicioOperadorEmpresaTipoSociedadGt?: number;
    servicioOperadorEmpresaTipoSociedadGe?: number;
    servicioOperadorEmpresaTipoSociedad?: number;
    servicioOperadorEmpresaTipoDocumentoIdNe?: number;
    servicioOperadorEmpresaTipoDocumentoIdLt?: number;
    servicioOperadorEmpresaTipoDocumentoIdLe?: number;
    servicioOperadorEmpresaTipoDocumentoIdGt?: number;
    servicioOperadorEmpresaTipoDocumentoIdGe?: number;
    servicioOperadorEmpresaTipoDocumentoId?: number;
    servicioOperadorEmpresaTipoDocumentoDescripcionNe?: string;
    servicioOperadorEmpresaTipoDocumentoDescripcionLike?: string;
    servicioOperadorEmpresaTipoDocumentoDescripcion?: string;
    servicioOperadorEmpresaTipoDocumentoAbrevNe?: any;
    servicioOperadorEmpresaTipoDocumentoAbrevLike?: any;
    servicioOperadorEmpresaTipoDocumentoAbrev?: any;
    servicioOperadorEmpresaTextoHash?: string;
    servicioOperadorEmpresaRazonSocialNe?: string;
    servicioOperadorEmpresaRazonSocialLike?: string;
    servicioOperadorEmpresaRazonSocial?: string;
    servicioOperadorEmpresaPautNe?: string;
    servicioOperadorEmpresaPautLike?: string;
    servicioOperadorEmpresaPaut?: string;
    servicioOperadorEmpresaObservacionNe?: string;
    servicioOperadorEmpresaObservacionLike?: string;
    servicioOperadorEmpresaObservacion?: string;
    servicioOperadorEmpresaNroDocumentoNe?: string;
    servicioOperadorEmpresaNroDocumentoLike?: string;
    servicioOperadorEmpresaNroDocumento?: string;
    servicioOperadorEmpresaNombreFantasiaNe?: string;
    servicioOperadorEmpresaNombreFantasiaLike?: string;
    servicioOperadorEmpresaNombreFantasia?: string;
    servicioOperadorEmpresaIdNe?: number;
    servicioOperadorEmpresaIdLt?: number;
    servicioOperadorEmpresaIdLe?: number;
    servicioOperadorEmpresaIdGt?: number;
    servicioOperadorEmpresaIdGe?: number;
    servicioOperadorEmpresaId?: number;
    servicioOperadorEmpresaEstadoEmpresaNe?: number;
    servicioOperadorEmpresaEstadoEmpresaLt?: number;
    servicioOperadorEmpresaEstadoEmpresaLe?: number;
    servicioOperadorEmpresaEstadoEmpresaGt?: number;
    servicioOperadorEmpresaEstadoEmpresaGe?: number;
    servicioOperadorEmpresaEstadoEmpresa?: number;
    servicioOperadorEmpresaEsPersonaFisica?: boolean;
    servicioOperadorEmpresaEmailNe?: string;
    servicioOperadorEmpresaEmailLike?: string;
    servicioOperadorEmpresaEmail?: string;
    servicioOperadorEmpresaCuitEmpresaAnteriorNe?: string;
    servicioOperadorEmpresaCuitEmpresaAnteriorLike?: string;
    servicioOperadorEmpresaCuitEmpresaAnterior?: string;
    servicioOperadorClaseModalidadIdNe?: number;
    servicioOperadorClaseModalidadIdLt?: number;
    servicioOperadorClaseModalidadIdLe?: number;
    servicioOperadorClaseModalidadIdGt?: number;
    servicioOperadorClaseModalidadIdGe?: number;
    servicioOperadorClaseModalidadId?: number;
    servicioOperadorClaseModalidadDescripcionNe?: string;
    servicioOperadorClaseModalidadDescripcionLike?: string;
    servicioOperadorClaseModalidadDescripcion?: string;
    servicioIdNe?: number;
    servicioIdLt?: number;
    servicioIdLe?: number;
    servicioIdGt?: number;
    servicioIdGe?: number;
    servicioId?: number;
    servicioHoraSalidaNe?: any;
    servicioHoraSalidaLt?: any;
    servicioHoraSalidaLe?: any;
    servicioHoraSalidaGt?: any;
    servicioHoraSalidaGe?: any;
    servicioHoraSalida?: any;
    servicioHoraLlegadaNe?: any;
    servicioHoraLlegadaLt?: any;
    servicioHoraLlegadaLe?: any;
    servicioHoraLlegadaGt?: any;
    servicioHoraLlegadaGe?: any;
    servicioHoraLlegada?: any;
    servicioAprobado?: boolean;

    /**
     * Método de ordenación, ascendente (ASC) o descendente (DESC).
     */
    order?: 'ASC' | 'DESC';

    /**
     * Cantidad de registros que deben omitirse en la consulta.
     */
    offset?: number;

    /**
     * Cantidad máxima de registros que debe regresar la consulta.
     */
    limit?: number;
    idNe?: number;
    idLt?: number;
    idLe?: number;
    idGt?: number;
    idGe?: number;
    id?: number;
    categoriaServicioPrimerPisoIdNe?: number;
    categoriaServicioPrimerPisoIdLt?: number;
    categoriaServicioPrimerPisoIdLe?: number;
    categoriaServicioPrimerPisoIdGt?: number;
    categoriaServicioPrimerPisoIdGe?: number;
    categoriaServicioPrimerPisoId?: number;
    categoriaServicioPrimerPisoDescripcionNe?: string;
    categoriaServicioPrimerPisoDescripcionLike?: string;
    categoriaServicioPrimerPisoDescripcion?: string;
    categoriaServicioPrimerPisoActivo?: boolean;
    activo?: boolean;
  }
}

export { DiaServicioService }
