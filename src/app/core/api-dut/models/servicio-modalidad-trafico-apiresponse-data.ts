/* tslint:disable */
import { ServicioModalidadTraficoAPI2 } from './servicio-modalidad-trafico-api2';
export interface ServicioModalidadTraficoAPIResponseData {
  actions?: string;
  data?: ServicioModalidadTraficoAPI2;
  result?: string;
  status?: number;
  userMessage?: string;
}
