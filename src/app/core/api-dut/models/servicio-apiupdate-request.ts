/* tslint:disable */
import { DiaServicioAPIUpdateRequest } from './dia-servicio-apiupdate-request';
import { ServicioHorarioAPIUpdateRequest } from './servicio-horario-apiupdate-request';
export interface ServicioAPIUpdateRequest {
  dias?: Array<DiaServicioAPIUpdateRequest>;
  diasDuracion?: number;
  horaLlegada?: string;
  horaSalida?: string;
  horarios?: Array<ServicioHorarioAPIUpdateRequest>;
  operador?: number;
  recorrido?: number;
  recorridoSentido?: number;
  velocidadPromedioTotal?: number;
  vigenciaDesde?: string;
  vigenciaHasta?: string;
}
