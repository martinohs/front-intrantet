/* tslint:disable */
import { EmpresaAPI } from './empresa-api';
import { EstadoTramiteAPI2 } from './estado-tramite-api2';
import { TramiteLogAPI } from './tramite-log-api';
import { SubtipoTramiteAPI2 } from './subtipo-tramite-api2';
import { TipoTramiteAPI } from './tipo-tramite-api';
export interface TramiteAPI {
  empresa?: EmpresaAPI;
  estadoTramite?: EstadoTramiteAPI2;
  fechaCreacion?: any;
  id?: number;
  movimientos?: Array<TramiteLogAPI>;
  nroBoletaPago?: number;
  subtipoTramite?: SubtipoTramiteAPI2;
  tipoTramite?: TipoTramiteAPI;
  usuario?: string;
}
