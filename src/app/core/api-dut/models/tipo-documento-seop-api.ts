/* tslint:disable */
export interface TipoDocumentoSeopAPI {
  abrev?: string;
  descripcion?: string;
  id?: number;
}
