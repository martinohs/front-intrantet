/* tslint:disable */

/**
 * Error de validación
 */
export interface ValidationError {
  message?: string;
  property_path?: string;
}
