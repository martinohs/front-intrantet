import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
	selector: 'app-alert-popup',
	templateUrl: './alert-popup.component.html',
	encapsulation: ViewEncapsulation.None,
	styles: [`
		.mat-snack-bar-container {
			height: auto;
			width: auto;
		}
	`],
})
export class AlertPopupComponent implements OnInit {
	constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) {}

	ngOnInit(): void {		
	}

	close(){
		this.data.preClose(); 
	  }
}
