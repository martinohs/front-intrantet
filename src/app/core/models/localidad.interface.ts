import { Provincia } from './provincia.interface';
export interface Localidad {
	id: number;
	descripcion: string;
	codigoINDEC: string;
	provincia: Provincia;
	departamento: number;
	lat: number;
	lon: number;
	fuente: string;
	activo: boolean;
	parada: boolean;
}
