import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCreadorComponent } from './post-creador.component';

describe('PostCreadorComponent', () => {
  let component: PostCreadorComponent;
  let fixture: ComponentFixture<PostCreadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostCreadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostCreadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
