/* tslint:disable */
import { LocalidadAPI } from './localidad-api';
export interface ParadaAPI {
  activa?: boolean;
  aprobada?: boolean;
  cityTown?: string;
  county?: string;
  createdAt?: any;
  createdBy?: string;
  displayName?: string;
  empresa?: number;
  id?: number;
  latitud?: number;
  localidad?: LocalidadAPI;
  longitud?: number;
  nombre?: string;
  nombreCompleto?: string;
  state?: string;
  stateDistrict?: string;
  updatedAt?: any;
}
