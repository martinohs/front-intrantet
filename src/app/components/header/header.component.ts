import { Component, OnInit } from '@angular/core';
// import { CoreFacade } from '@core/facades/core.facade';
// import { AuthService } from '@auth/services/auth.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
	constructor(
    // private coreFacade: CoreFacade, private authService: AuthService
    ) {}

	ngOnInit(): void {}

	get user$() {
    // return this.coreFacade.user$;
    return "USUARIO(header)";
	}

	logout() {
		// this.authService.logout();
	}

	goToLogin() {
		// this.authService.login();
	}
}
