export interface MediaContent {
  id?: number;
  imageOne?: File;
  imageTwo?: File;
}
