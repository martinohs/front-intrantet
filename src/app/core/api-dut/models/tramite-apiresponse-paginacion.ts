/* tslint:disable */
import { ResponseMetadata } from './response-metadata';
import { TramiteAPI } from './tramite-api';
export interface TramiteAPIResponsePaginacion {
  actions?: string;
  data?: {metadata?: ResponseMetadata, results?: Array<TramiteAPI>};
  result?: string;
  status?: number;
  userMessage?: string;
}
