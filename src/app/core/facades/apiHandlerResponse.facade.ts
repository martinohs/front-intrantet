import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApiHandlerAlertComponent } from '@shared/varios/components/api-handler-alert/api-handler-alert.component';
import { AbstractControl, FormArray } from '@angular/forms';

@Injectable()
export class ApiHandlerFacade {
	constructor(private snackBar: MatSnackBar) {}

	apiSuccessResponse(res: any) {
		this.snackBar.openFromComponent(ApiHandlerAlertComponent, {
			data: { msg: res.userMessage, icon: 'check-circle' },
			duration: 6000,
			horizontalPosition: 'center',
			verticalPosition: 'bottom',
			panelClass: ['bg-success', 'align-middle', 'snackbar'],
		});
	}
	/* IMPORTANTE: Importar el modelo ResponseError de los modelos generados por swagger y 
				cambiar el tipo del param "error" de any a ResponseError
*/
	apiErrorResponse(error: any, formGroup?: AbstractControl) {
		if ((error.status === 401 || error.status === 409) && formGroup) {
			error.validationErrors.map((err) => {
				if (err.propertyPath.includes('[')) {
					const star = err.propertyPath.search(/\[/);
					const end = err.propertyPath.search(/\]/);
					const index = parseInt(err.propertyPath.slice(star + 1, end));
					const paths = err.propertyPath.split(/\[\d+\]\./);

					const formArray = formGroup.get(paths[0]) as FormArray;
					formArray.controls[index].get(paths[1]).setErrors({ serverError: err.message });
				} else {
					formGroup.get(err.propertyPath).setErrors({ serverError: err.message });
				}
			});
		}
		//array de errores
		let validationErrors = error.validationErrors
			? error.validationErrors.map((value) => {
					return {
						validationErrorMessage: value.message,
						validationErrorProperty: value.propertyPath,
					};
			  })
			: [];

		this.snackBar.openFromComponent(ApiHandlerAlertComponent, {
			data: {
				msg:
					validationErrors.length > 0
						? 'Se detectaron errores en la validación. ' + error.userMessage
						: error.userMessage,
				icon: 'exclamation-triangle',
				// errMessagesArray: validationErrors,
				preClose: () => this.snackBar.dismiss(),
				btnCerrar: true,
			},
			duration: undefined,
			horizontalPosition: 'center',
			verticalPosition: 'bottom',
			panelClass: ['fondo-danger', 'snackbar'],
		});
	}
}
