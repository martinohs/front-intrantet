import { Injectable } from '@angular/core';
import { AuthService } from '@auth/services/auth.service';
import { BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, delay, takeWhile } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from '@auth/models/user.user';
import { Post } from '../models/post.interface';
import { PostService } from '../services/post.service';
import { R3TargetBinder } from '@angular/compiler';

@Injectable()
export class CoreFacade {
	cargandoBS = new BehaviorSubject<boolean>(false);

	constructor(private authService: AuthService, private router: Router,private PostService: PostService
    ) {}

	get isAuthtenticate$() {
		return this.authService.isAuthenticatedObs;
	}

	get user$() {
		return this.authService.currentUser;
  }


	set cargando(status: boolean) {
		this.cargandoBS.next(status);
	}

	get cargando$() {
		return this.cargandoBS.asObservable().pipe(distinctUntilChanged(), delay(0));
  }


// ******** METODOS PARA API DE POSTS ********

//Obtengo todos los post -> Devuelve un Observable
obtenerTodasLasNoticias(){
  return this.PostService.getAllPost();
}

//Nuevo post
nuevoPost(unPost:Post):void{
this.PostService.addPost(unPost).subscribe(rta => {console.log(rta)});
}

//Eliminar post -> [FALTA CHECKEAR]
eliminarPost(){
  return this.PostService.deletePost(4).subscribe(rta => {console.log('ELIMINADO CON EXITO')})
}

//Metodo que devuelve un observable tipo post
obtenerPost(postId: number){
  return this.PostService.getOnePost(postId);
}


// ******** METODOS DE PRUEBA/TEST ********

//metodo de test , eliminar su contraparte en app.component y html
getPostConsoleLog(postId: number){
  return this.PostService.getOnePost(postId).subscribe(rta => {(console.log(rta))});
}

//Creo un post para testear
// nuevoPostTest(): void{

//   const unPost: Post ={
//     title: 'testeo',
//     content: 'testeo',
//     creator: 'testeo',
//     // image: ,
//   };

//   //Inyecto el post y recibo por consola la respuesta
//   this.PostService.addPost(unPost).subscribe(
//     rta => {console.log(rta)}
//   )
// }

}
