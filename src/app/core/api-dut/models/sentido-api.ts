/* tslint:disable */
export interface SentidoAPI {
  abrev?: string;
  activo?: boolean;
  descripcion?: string;
  id?: number;
}
