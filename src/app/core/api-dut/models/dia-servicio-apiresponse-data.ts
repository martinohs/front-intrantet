/* tslint:disable */
import { DiaServicioAPI } from './dia-servicio-api';
export interface DiaServicioAPIResponseData {
  actions?: string;
  data?: DiaServicioAPI;
  result?: string;
  status?: number;
  userMessage?: string;
}
