/* tslint:disable */
import { DetalleModalidadTraficoAPISaveRequest } from './detalle-modalidad-trafico-apisave-request';
export interface ServicioModalidadTraficoAPISaveRequest {
  createdBy?: string;
  detalle?: Array<DetalleModalidadTraficoAPISaveRequest>;
  servicio?: number;
  tramite?: number;
  vigenciaDesde?: string;
  vigenciaHasta?: string;
}
