import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { catchError, map } from 'rxjs/operators';

import { Router, NavigationStart } from '@angular/router';

@Injectable()
export class GeorefApiService {
	constructor(private _router: Router, private http: HttpClient) {}

	get(path: string, params: Object = {}, showErrorMessages = true, absolutePath = false): Observable<any> {
		let httpParams: HttpParams = new HttpParams();
		const route = absolutePath ? path : this.parsePath(path);

		if (params) {
			Object.keys(params).forEach((key) => {
				httpParams = httpParams.append(key, params[key]);
			});
		}

		return this.http.get(route, { params: httpParams });
	}

	parsePath(path: string): string {
		return `${environment.georef_api_url}${path}`;
	}
}
