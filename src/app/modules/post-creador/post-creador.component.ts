import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CoreFacade } from '@core/facades/core.facade';
import { Post } from '../../core/models/post.interface';
import { MediaContent } from '../../core/models/mediaContent.interface';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-post-creador',
  templateUrl: './post-creador.component.html',
  styleUrls: ['./post-creador.component.scss']
})
export class PostCreadorComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private coreFacade: CoreFacade) { }



// DECLARACION CONTADOR DE CARACTERES Y UN AUX PARA TRABAJAR
  totalCount : number ;
  unPost : Post;
  mContent: MediaContent;
  file: File;

// -----------  CREADOR DEL FORMULARIO PARA LA NOTICIA
  creatorForm = this.formBuilder.group({
    title: ['',Validators.required],
    content: ['',Validators.required],
    creator: ['',Validators.required],
    isImportant: [''],
    likes:[1],
    from_area: ['test'] //DEBERIA SER SELECCIONABLE
  });

  contentForm = this.formBuilder.group({
    imageOne: [''],
    imageTwo: ['']
  })


  ngOnInit(): void {

  }

  submit(){
    this.unPost = this.creatorForm.value;
    console.log(this.creatorForm.value); // -> PARA CHECKEAR, LUEGO ELIMINAR.

    //////// PARA PROBAR /////////////

    this.mContent = this.contentForm.value;


    console.log(this.contentForm.value);
    this.crearPost();
    this.testeoJson();
  }

  crearPost():void {
    this.coreFacade.nuevoPost(this.unPost);
  }

  testeoJson(): void{
    var jsonS = {
      post: this.unPost,
      mContent: this.mContent,
        };
        console.log(jsonS);
  }

  onImageSelected(event){
    if (event.target.files && event.target.files[0]){
      this.file = <File>event.target.files[0];
    } else {
      console.log("problemas");
    }
  }

  // FUNCION QUE ME MUESTRA CANTIDAD DE CARACTERES UTILIZADOS HASTA EL MOMENTO
  onkeyup(){

    this.totalCount = this.eliminarFormato(this.creatorForm.value.content).length;


      this.totalCount = 450 - this.totalCount;
    if (this.totalCount < 0){
      this.totalCount = 0;
      console.log("asdf");
    }

    }

    // -----------  FUNCION QUE ELIMINA EL FORMATO DEL TEXTO
  eliminarFormato(texto){
    var tmp = document.createElement('div');
      tmp.innerHTML = texto;

    return tmp.textContent || tmp.innerText;
    }

    // ----------- CONFIGURACION DEL EDITOR DE TEXTO
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '15rem',
      minHeight: '5rem',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Ingrese el texto...',
      defaultParagraphSeparator: '',
      defaultFontName: 'Arial',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
    //   ['bold', 'italic'],
    //   ['fontSize']
      ['insertImage']
    ]
};
// -----------  FIN CONFIGURACION EDITOR DE TEXTO

}
