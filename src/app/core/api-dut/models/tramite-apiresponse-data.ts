/* tslint:disable */
import { TramiteAPI } from './tramite-api';
export interface TramiteAPIResponseData {
  actions?: string;
  data?: TramiteAPI;
  result?: string;
  status?: number;
  userMessage?: string;
}
