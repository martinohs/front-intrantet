import { Injectable } from '@angular/core';
import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc-codeflow-pkce';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { CNRTAuthConfig } from '../config/cnrt.auth.config';
import { authConfig } from '../config/authConfig';
import { UserFactory } from '../models/user.factory';
import { User } from '../models/user.user';
import { UsuarioService } from '../../core/services';
import { Router } from '@angular/router';
import { environment } from '@env/environment';
import { Usuario } from '../models/usuario.model';

@Injectable({
	providedIn: 'root',
})
export class AuthService {
	protected usuario: Usuario;

	private currentUserSubject = new BehaviorSubject<User>(null);
	public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());
	private isAuthenticatedSubject = new BehaviorSubject<boolean>(false);

	constructor(private oauthService: OAuthService, private usuarioService: UsuarioService, private router: Router) {}

	userChangeCallback() {
		const user = this.getUser();
		if (user && user.getId()) {
			if (!this.usuario || this.usuario.id !== user.getId()) {
				this.usuario = Usuario.fromUser(user);
			}

			// Validamos que el usuario posea algunos de los roles utilizados en este proyecto
			if (user && user.roles.length > 0 && !user.hasAnyRol([])) {
				// AppAlertService.swalError(
				//   "El usuario no posee ninguno de los roles necesarios para esta aplicación"
				// );
			}
			this.currentUserSubject.next(user);
		} else {
			this.usuario = null;
			this.currentUserSubject.next(null);
		}
	}

	/**
	 * Obtiene la configuración de autenticación actual.
	 * Cada una de estas representa la configuración del IDP.
	 *
	 * @return CNRTAuthConfig
	 */
	getCurrentConfig(): CNRTAuthConfig {
		return authConfig.find((c) => c.id === environment.current_idp);
	}

	/**
	 * Inicializa el Authenticator.
	 * @return AuthService this
	 */
	initialize(): AuthService {
		this.oauthService.configure(this.getCurrentConfig()['config']);
		this.oauthService.tokenValidationHandler = new JwksValidationHandler();
		this.oauthService.setStorage(localStorage);

		this.oauthService.events.subscribe((e) => {
			//console.log(e);
			if (['discovery_document_load_error', 'discovery_document_validation_error'].indexOf(e.type) >= 0) {
				this.clearUserInfo();
			} else if (e.type === 'session_terminated') {
				if (this.oauthService.getRefreshToken()) {
					this.oauthService.refreshToken();
				} else {
					this.clearUserInfo();
					this.router.navigate(['/login']);
				}
			} else if (e.type === 'token_refreshed') {
				this.oauthService.loadUserProfile().then(() => {});
			} else if (e.type === 'token_expires') {
				this.oauthService.refreshToken();
			} else if (e.type === 'user_profile_loaded') {
				this.userChangeCallback();
			} else if (e.type === 'logout') {
				this.router.navigate(['/login']);
			}
		});

		this.oauthService.loadDiscoveryDocument().then(() => {
			this.oauthService
				.tryLogin()
				.then(() => {
					if (this.oauthService.getRefreshToken()) {
						this.oauthService.refreshToken();
					}
				})
				.catch((msg) => {
					this.clearUserInfo();
				});
		});
		// Capturamos eventos de OAuth para tratarlos
		/* 	this.oauthService.events
			.pipe(
				filter(e => {
					console.log(e);
					return (
						e.type === 'token_received' ||
						e.type === 'token_expires' ||
						e.type === 'session_terminated' ||
						e.type === 'logout'
					);
				})
			)
			.subscribe(e => {
				if (e.type === 'token_received') {
					// Buscamos la Información del Usuario cada vez que se recibe el token
					this.oauthService.loadUserProfile().then(() => {
						this.userChangeCallback();
					});
				} else if (e.type === 'token_expires') {
					this.oauthService
						.refreshToken()
						.then(token => {
							console.log('REFRESH TOKEN:', token);
						})
						.catch(err => {
							console.log('Error al obtener refresh token', err);
							this.logout();
						});
				} else if (e.type === 'session_terminated') {
					this.logout();
				} else if (e.type === 'logout') {
					this.router.navigate(['/']);
				}
			});

		this.oauthService.loadDiscoveryDocument().then(() => {
			this.oauthService
				.tryLogin()
				.then(() => {
					if (!this.isAuthenticated() && this.oauthService.hasValidAccessToken()) {
						this.login();
					}

					this.userChangeCallback();
				})
				.catch(msg => {
					this.currentUserSubject.error(`Error mientras se intentaba loguear ${msg}`);
					this.logout();
				});
		});
 */
		return this;
	}

	/**
	 * Nos lleva a la pantalla de login
	 */
	login(): void {
		this.oauthService.initAuthorizationCodeFlow();
	}

	/**
	 * Cierra la sesión, tanto en el endpoint como en nuestro dispositivo.
	 * @return AuthService this
	 */
	logout(): AuthService {
		// @TODO #4573 Terminar la sessión al desloguear
		this.oauthService.logOut();

		return this;
	}

	clearUserInfo(noRedirect: boolean = false) {
		this.oauthService.logOut(false);
	}

	/**
	 * Retorna el id_token (JWT) de OAuth
	 */
	getIdToken(): string {
		return this.oauthService.getIdToken();
	}

	/**
	 * Retorna el access_token de OAuth
	 */
	getAccessToken(): string {
		return this.oauthService.getAccessToken();
	}

	/**
	 * Nos responde si el usuario está autenticado.
	 */
	isAuthenticated(): boolean {
		const authOk = this.oauthService.hasValidIdToken();
		this.isAuthenticatedSubject.next(authOk);
		return authOk;
	}

	get isAuthenticatedObs() {
		return this.isAuthenticatedSubject.asObservable().pipe(distinctUntilChanged());
	}

	/**
	 * Obtiene una instancia del usuario en cuestión. Puede ser un usuario de google,
	 * de AFIP. La clase usuario, básicamente, parsea la información devuelta por el
	 * IDP y la normaliza para nuestro uso.
	 * @return User
	 */
	getUser() {
		if (null !== this.getCurrentConfig()) {
			return UserFactory.getInstance(this.getCurrentConfig()['id'], this.getIdentityClaims());
		}

		return null;
	}

	/**
	 * Devuelve información sobre el usuario logueado.
	 */
	getIdentityClaims() {
		return this.oauthService.getIdentityClaims();
	}
}
