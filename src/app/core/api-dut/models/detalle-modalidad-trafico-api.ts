/* tslint:disable */
import { PaisAPI } from './pais-api';
import { ParadaAPI } from './parada-api';
import { ProvinciaAPI } from './provincia-api';
export interface DetalleModalidadTraficoAPI {
  id?: number;
  ofreceServicio?: boolean;
  paisDestino?: PaisAPI;
  paisOrigen?: PaisAPI;
  paradaDestino?: ParadaAPI;
  paradaOrigen?: ParadaAPI;
  provinciaDestino?: ProvinciaAPI;
  provinciaOrigen?: ProvinciaAPI;
  servicioModalidadTrafico?: number;
}
