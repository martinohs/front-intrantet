/* tslint:disable */
import { DiaAPI } from './dia-api';
export interface DiaAPIResponseData {
  actions?: string;
  data?: DiaAPI;
  result?: string;
  status?: number;
  userMessage?: string;
}
