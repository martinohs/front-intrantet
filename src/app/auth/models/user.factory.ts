import { SIAUser } from './sia.user';
import { User } from './user.user';

export class UserFactory {
	static getInstance(id: string, userInfo) {
		if (id === 'hydra') {
			return new SIAUser(userInfo);
		}
		return new User(userInfo);
	}
}
