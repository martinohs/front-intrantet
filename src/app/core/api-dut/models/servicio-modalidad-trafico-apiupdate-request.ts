/* tslint:disable */
import { DetalleModalidadTraficoAPIUpdateRequest } from './detalle-modalidad-trafico-apiupdate-request';
export interface ServicioModalidadTraficoAPIUpdateRequest {
  detalle?: Array<DetalleModalidadTraficoAPIUpdateRequest>;
  updatedLastBy?: string;
  vigenciaDesde?: string;
  vigenciaHasta?: string;
}
