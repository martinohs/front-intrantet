import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../models/post.interface';
import { UniqueSelectionDispatcher } from '@angular/cdk/collections';
import { Observable } from 'rxjs';
import { MediaContent } from '@core/models/mediaContent.interface';


@Injectable({
  providedIn:'root'
})

export class PostService {


  constructor(
    private http:HttpClient,
  ) {}

// Agregar post
addPost(unPost : Post){
  const path = 'http://localhost:8000/api/addpost';

  return this.http.post(path, JSON.stringify(unPost));
}

// Agregar post
addPostjTest(unPost : Post, mediaContent: MediaContent){
  const path = 'http://localhost:8000/api/addpost';
  var jsonPost = {
    post: unPost,
    mContent: mediaContent,
    };
  return this.http.post(path, jsonPost);
}

// Eliminar post
deletePost(unaId: number){
 const path = `http://localhost:8000/api/delpost/${unaId}`

 return this.http.delete(path);
}

// Modificar Post
modPost(unPost: Post){

  const path = `http://localhost:8000/api/modpost/${unPost.id}`;

  return this.http.put(path, JSON.stringify(unPost));

}

// Obtener todos los POST
getAllPost(){
  const path = 'http://localhost:8000/api/getpost/all';

  return this.http.get<Post[]>(path);
}

// Obtengo 1 post por id
getOnePost(unaId: number):Observable<Post>{

  const path = `http://localhost:8000/api/getpost/${unaId}`;

  return this.http.get<Post>(path);

}

//Likea un post
likePost (unaId: number){

  const path = `http://localhost:8000/api/likedpost/${unaId}`;

  return this.http.put(path,null);
}
























}

