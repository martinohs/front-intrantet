/* tslint:disable */
export interface CategoriaServicioAPI {
  activo?: boolean;
  descripcion?: string;
  id?: number;
}
