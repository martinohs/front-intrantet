import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

//modules
import { SharedModule } from '@shared/shared.module';
import { VariosModule } from '@shared/varios/varios.module';

// Auth
import { OAuthModule } from 'angular-oauth2-oidc-codeflow-pkce';
import { AuthService } from '@auth/services/auth.service';
import { AuthGuardService } from '@auth/guard/auth.guard';
import { BackendInterceptor } from '@auth/services/backend.interceptor';
import { UsuarioService, GeorefApiService, LocalidadService } from '@core/services';

import { CoreFacade } from './facades/core.facade';
import { ApiHandlerFacade } from './facades/apiHandlerResponse.facade';

@NgModule({
	declarations: [],
	imports: [SharedModule, OAuthModule.forRoot(), VariosModule],
	exports: [],
})
export class CoreModule {
	constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
		if (parentModule) {
			throw new Error('CoreModule ya fue importado');
		}
	}
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: CoreModule,
			providers: [
				AuthService,
				AuthGuardService,
				UsuarioService,
				GeorefApiService,
				LocalidadService,
				CoreFacade,
				ApiHandlerFacade,
				{ provide: HTTP_INTERCEPTORS, useClass: BackendInterceptor, multi: true },
			],
		};
	}
}
