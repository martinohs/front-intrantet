import { Injectable } from '@angular/core';
import { GeorefApiService } from './geo-ref.service';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class LocalidadService {
	constructor(private _georefApiServices: GeorefApiService) {}

	/* GET */

	get(params: Object = {}, absolutePath = false) {
		return this._georefApiServices.get(`/localidades`, params, absolutePath).pipe(
			map(res => {
				return res.data.results;
			}),
			catchError(err => of([]))
		);
	}
}
