/* tslint:disable */
import { ResponseMetadata } from './response-metadata';
import { ParadaAPI } from './parada-api';
export interface ParadaAPIResponsePaginacion {
  actions?: string;
  data?: {metadata?: ResponseMetadata, results?: Array<ParadaAPI>};
  result?: string;
  status?: number;
  userMessage?: string;
}
