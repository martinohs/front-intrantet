import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';

import { ApiHandlerAlertComponent } from './components/api-handler-alert/api-handler-alert.component';
import { VolverButtonComponent } from './components/volver-button/volver-button.component';
import { AlertPopupComponent } from './components/alert-popup/alert-popup.component';

import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
	declarations: [
		ApiHandlerAlertComponent,
		VolverButtonComponent,
		AlertPopupComponent,
	],
	imports: [
		SharedModule,
		MatSnackBarModule],
	entryComponents: [
		ApiHandlerAlertComponent,
		AlertPopupComponent,
	],
	exports: [
		ApiHandlerAlertComponent,
		MatSnackBarModule,
		VolverButtonComponent,
		AlertPopupComponent,
	],
	providers: [],
})
export class VariosModule { };
