/* tslint:disable */
import { ModalidadPrestacionAPI } from './modalidad-prestacion-api';
export interface ClaseModalidadAPI {
  descripcion?: string;
  id?: number;
  modalidadesPrestacion?: Array<ModalidadPrestacionAPI>;
}
