import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable()
export class UsuarioService {
	apiUrl = environment.authConfig.issuer;

	constructor(private http: HttpClient) {}

	/**
	 * https://api.cnrt.gob.ar/oidc/userinfo
	 */
	getByCuil(id: string): Observable<any> {
		return this.http.get(this.apiUrl + 'userinfo/' + id);
	}
}
