/* tslint:disable */
import { OperadorAPI } from './operador-api';
export interface PeriodoFrecuenciaAPI {
  activo?: boolean;
  frecuencia?: number;
  id?: number;
  operador?: OperadorAPI;
  periodoDesde?: any;
  periodoHasta?: any;
  vigenciaDesde?: any;
  vigenciaHasta?: any;
}
