/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiDutConfiguration as __Configuration } from '../api-dut-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ServicioAPIResponseData } from '../models/servicio-apiresponse-data';
import { ServicioAPISaveRequest } from '../models/servicio-apisave-request';
import { ServicioAPIUpdateRequest } from '../models/servicio-apiupdate-request';
import { ServicioHorarioAPIResponseLista } from '../models/servicio-horario-apiresponse-lista';
import { DiaServicioAPIResponseLista } from '../models/dia-servicio-apiresponse-lista';
import { ServicioAPIResponseLista } from '../models/servicio-apiresponse-lista';
@Injectable({
  providedIn: 'root',
})
class ServicioService extends __BaseService {
  static readonly findAllServiciosPath = '/api/v2/servicios';
  static readonly saveServicioPath = '/api/v2/servicios';
  static readonly findServicioPath = '/api/v2/servicios/{id}';
  static readonly putApiV2ServiciosIdPath = '/api/v2/servicios/{id}';
  static readonly deleteApiV2ServiciosIdPath = '/api/v2/servicios/{id}';
  static readonly findAllHorariosByServicioPath = '/api/v2/servicios/{id}/horarios';
  static readonly postApiV2ServiciosIdHorariosPath = '/api/v2/servicios/{id}/horarios';
  static readonly findAllDiasByServicioPath = '/api/v2/servicios/{id}/dias';
  static readonly postApiV2ServiciosIdDiasPath = '/api/v2/servicios/{id}/dias';
  static readonly getApiV2ServiciosIdOrigenIdDestinoOrigenDestinoPath = '/api/v2/servicios/{idOrigen}-{idDestino}/origenDestino';
  static readonly putApiV2ServiciosIdAprobarPath = '/api/v2/servicios/{id}/aprobar';
  static readonly putApiV2ServiciosIdRechazarPath = '/api/v2/servicios/{id}/rechazar';
  static readonly putApiV2ServiciosIdEnviaracnrtPath = '/api/v2/servicios/{id}/enviaracnrt';
  static readonly putApiV2ServiciosIdEnevaluacionPath = '/api/v2/servicios/{id}/enevaluacion';
  static readonly getApiV2ServiciosValidacionVepPath = '/api/v2/servicios/validacionVep';
  static readonly putApiV2ServiciosIdBajaPath = '/api/v2/servicios/{id}/baja';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Obtiene una lista de cuadros horarios paginados
   * @param params The `ServicioService.FindAllServiciosParams` containing the following parameters:
   *
   * - `vigente`: Filtra los cuadros horarios con vigenciaHasta mayor o menor a la fecha de hoy: vigente=true (devuelve los cuadros horarios con vigenciaHasta mayor a la fecha de hoy o con vigenciaHasta = null // vigente=false (devuelve los servicios con vigenciaHasta menor o igual a la fecha de hoy - es decir, cuadros horarios que perdieron su vigencia - ). Este filtro se puede usar junto con los atributos de criteriaBuilder. ejemplo: ...&id[eq]=23%&vigente=true
   *
   * - `vigenciaHasta[ne]`:
   *
   * - `vigenciaHasta[lt]`:
   *
   * - `vigenciaHasta[le]`:
   *
   * - `vigenciaHasta[gt]`:
   *
   * - `vigenciaHasta[ge]`:
   *
   * - `vigenciaHasta`:
   *
   * - `vigenciaDesde[ne]`:
   *
   * - `vigenciaDesde[lt]`:
   *
   * - `vigenciaDesde[le]`:
   *
   * - `vigenciaDesde[gt]`:
   *
   * - `vigenciaDesde[ge]`:
   *
   * - `vigenciaDesde`:
   *
   * - `velocidadPromedioTotal[ne]`:
   *
   * - `velocidadPromedioTotal[lt]`:
   *
   * - `velocidadPromedioTotal[le]`:
   *
   * - `velocidadPromedioTotal[gt]`:
   *
   * - `velocidadPromedioTotal[ge]`:
   *
   * - `velocidadPromedioTotal`:
   *
   * - `tramite-usuario[ne]`:
   *
   * - `tramite-usuario[like]`:
   *
   * - `tramite-usuario`:
   *
   * - `tramite-tipoTramite-id[ne]`:
   *
   * - `tramite-tipoTramite-id[lt]`:
   *
   * - `tramite-tipoTramite-id[le]`:
   *
   * - `tramite-tipoTramite-id[gt]`:
   *
   * - `tramite-tipoTramite-id[ge]`:
   *
   * - `tramite-tipoTramite-id`:
   *
   * - `tramite-tipoTramite-descripcion[ne]`:
   *
   * - `tramite-tipoTramite-descripcion[like]`:
   *
   * - `tramite-tipoTramite-descripcion`:
   *
   * - `tramite-tipoTramite-activo`:
   *
   * - `tramite-subtipoTramite-id[ne]`:
   *
   * - `tramite-subtipoTramite-id[lt]`:
   *
   * - `tramite-subtipoTramite-id[le]`:
   *
   * - `tramite-subtipoTramite-id[gt]`:
   *
   * - `tramite-subtipoTramite-id[ge]`:
   *
   * - `tramite-subtipoTramite-id`:
   *
   * - `tramite-subtipoTramite-descripcion[ne]`:
   *
   * - `tramite-subtipoTramite-descripcion[like]`:
   *
   * - `tramite-subtipoTramite-descripcion`:
   *
   * - `tramite-subtipoTramite-activo`:
   *
   * - `tramite-nroBoletaPago[ne]`:
   *
   * - `tramite-nroBoletaPago[lt]`:
   *
   * - `tramite-nroBoletaPago[le]`:
   *
   * - `tramite-nroBoletaPago[gt]`:
   *
   * - `tramite-nroBoletaPago[ge]`:
   *
   * - `tramite-nroBoletaPago`:
   *
   * - `tramite-id[ne]`:
   *
   * - `tramite-id[lt]`:
   *
   * - `tramite-id[le]`:
   *
   * - `tramite-id[gt]`:
   *
   * - `tramite-id[ge]`:
   *
   * - `tramite-id`:
   *
   * - `tramite-estadoTramite-id[ne]`:
   *
   * - `tramite-estadoTramite-id[lt]`:
   *
   * - `tramite-estadoTramite-id[le]`:
   *
   * - `tramite-estadoTramite-id[gt]`:
   *
   * - `tramite-estadoTramite-id[ge]`:
   *
   * - `tramite-estadoTramite-id`:
   *
   * - `tramite-estadoTramite-descripcion[ne]`:
   *
   * - `tramite-estadoTramite-descripcion[like]`:
   *
   * - `tramite-estadoTramite-descripcion`:
   *
   * - `tramite-estadoTramite-activo`:
   *
   * - `tramite-estadoTramite-abrev[ne]`:
   *
   * - `tramite-estadoTramite-abrev[like]`:
   *
   * - `tramite-estadoTramite-abrev`:
   *
   * - `tramite-empresa-vigenciaHasta[ne]`:
   *
   * - `tramite-empresa-vigenciaHasta[lt]`:
   *
   * - `tramite-empresa-vigenciaHasta[le]`:
   *
   * - `tramite-empresa-vigenciaHasta[gt]`:
   *
   * - `tramite-empresa-vigenciaHasta[ge]`:
   *
   * - `tramite-empresa-vigenciaHasta`:
   *
   * - `tramite-empresa-vigenciaDesde[ne]`:
   *
   * - `tramite-empresa-vigenciaDesde[lt]`:
   *
   * - `tramite-empresa-vigenciaDesde[le]`:
   *
   * - `tramite-empresa-vigenciaDesde[gt]`:
   *
   * - `tramite-empresa-vigenciaDesde[ge]`:
   *
   * - `tramite-empresa-vigenciaDesde`:
   *
   * - `tramite-empresa-tipoSociedad[ne]`:
   *
   * - `tramite-empresa-tipoSociedad[lt]`:
   *
   * - `tramite-empresa-tipoSociedad[le]`:
   *
   * - `tramite-empresa-tipoSociedad[gt]`:
   *
   * - `tramite-empresa-tipoSociedad[ge]`:
   *
   * - `tramite-empresa-tipoSociedad`:
   *
   * - `tramite-empresa-tipoDocumento-id[ne]`:
   *
   * - `tramite-empresa-tipoDocumento-id[lt]`:
   *
   * - `tramite-empresa-tipoDocumento-id[le]`:
   *
   * - `tramite-empresa-tipoDocumento-id[gt]`:
   *
   * - `tramite-empresa-tipoDocumento-id[ge]`:
   *
   * - `tramite-empresa-tipoDocumento-id`:
   *
   * - `tramite-empresa-tipoDocumento-descripcion[ne]`:
   *
   * - `tramite-empresa-tipoDocumento-descripcion[like]`:
   *
   * - `tramite-empresa-tipoDocumento-descripcion`:
   *
   * - `tramite-empresa-tipoDocumento-abrev[ne]`:
   *
   * - `tramite-empresa-tipoDocumento-abrev[like]`:
   *
   * - `tramite-empresa-tipoDocumento-abrev`:
   *
   * - `tramite-empresa-textoHash`:
   *
   * - `tramite-empresa-razonSocial[ne]`:
   *
   * - `tramite-empresa-razonSocial[like]`:
   *
   * - `tramite-empresa-razonSocial`:
   *
   * - `tramite-empresa-paut[ne]`:
   *
   * - `tramite-empresa-paut[like]`:
   *
   * - `tramite-empresa-paut`:
   *
   * - `tramite-empresa-observacion[ne]`:
   *
   * - `tramite-empresa-observacion[like]`:
   *
   * - `tramite-empresa-observacion`:
   *
   * - `tramite-empresa-nroDocumento[ne]`:
   *
   * - `tramite-empresa-nroDocumento[like]`:
   *
   * - `tramite-empresa-nroDocumento`:
   *
   * - `tramite-empresa-nombreFantasia[ne]`:
   *
   * - `tramite-empresa-nombreFantasia[like]`:
   *
   * - `tramite-empresa-nombreFantasia`:
   *
   * - `tramite-empresa-id[ne]`:
   *
   * - `tramite-empresa-id[lt]`:
   *
   * - `tramite-empresa-id[le]`:
   *
   * - `tramite-empresa-id[gt]`:
   *
   * - `tramite-empresa-id[ge]`:
   *
   * - `tramite-empresa-id`:
   *
   * - `tramite-empresa-estadoEmpresa[ne]`:
   *
   * - `tramite-empresa-estadoEmpresa[lt]`:
   *
   * - `tramite-empresa-estadoEmpresa[le]`:
   *
   * - `tramite-empresa-estadoEmpresa[gt]`:
   *
   * - `tramite-empresa-estadoEmpresa[ge]`:
   *
   * - `tramite-empresa-estadoEmpresa`:
   *
   * - `tramite-empresa-esPersonaFisica`:
   *
   * - `tramite-empresa-email[ne]`:
   *
   * - `tramite-empresa-email[like]`:
   *
   * - `tramite-empresa-email`:
   *
   * - `tramite-empresa-cuitEmpresaAnterior[ne]`:
   *
   * - `tramite-empresa-cuitEmpresaAnterior[like]`:
   *
   * - `tramite-empresa-cuitEmpresaAnterior`:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `recorridoSentido-vinculacionCaminera[ne]`:
   *
   * - `recorridoSentido-vinculacionCaminera[like]`:
   *
   * - `recorridoSentido-vinculacionCaminera`:
   *
   * - `recorridoSentido-vigenciaHasta[ne]`:
   *
   * - `recorridoSentido-vigenciaHasta[lt]`:
   *
   * - `recorridoSentido-vigenciaHasta[le]`:
   *
   * - `recorridoSentido-vigenciaHasta[gt]`:
   *
   * - `recorridoSentido-vigenciaHasta[ge]`:
   *
   * - `recorridoSentido-vigenciaHasta`:
   *
   * - `recorridoSentido-vigenciaDesde[ne]`:
   *
   * - `recorridoSentido-vigenciaDesde[lt]`:
   *
   * - `recorridoSentido-vigenciaDesde[le]`:
   *
   * - `recorridoSentido-vigenciaDesde[gt]`:
   *
   * - `recorridoSentido-vigenciaDesde[ge]`:
   *
   * - `recorridoSentido-vigenciaDesde`:
   *
   * - `recorridoSentido-sentido-id[ne]`:
   *
   * - `recorridoSentido-sentido-id[lt]`:
   *
   * - `recorridoSentido-sentido-id[le]`:
   *
   * - `recorridoSentido-sentido-id[gt]`:
   *
   * - `recorridoSentido-sentido-id[ge]`:
   *
   * - `recorridoSentido-sentido-id`:
   *
   * - `recorridoSentido-sentido-descripcion[ne]`:
   *
   * - `recorridoSentido-sentido-descripcion[like]`:
   *
   * - `recorridoSentido-sentido-descripcion`:
   *
   * - `recorridoSentido-sentido-activo`:
   *
   * - `recorridoSentido-sentido-abrev[ne]`:
   *
   * - `recorridoSentido-sentido-abrev[like]`:
   *
   * - `recorridoSentido-sentido-abrev`:
   *
   * - `recorridoSentido-recorrido-vinculacionCaminera[ne]`:
   *
   * - `recorridoSentido-recorrido-vinculacionCaminera[like]`:
   *
   * - `recorridoSentido-recorrido-vinculacionCaminera`:
   *
   * - `recorridoSentido-recorrido-vigenciaHasta[ne]`:
   *
   * - `recorridoSentido-recorrido-vigenciaHasta[lt]`:
   *
   * - `recorridoSentido-recorrido-vigenciaHasta[le]`:
   *
   * - `recorridoSentido-recorrido-vigenciaHasta[gt]`:
   *
   * - `recorridoSentido-recorrido-vigenciaHasta[ge]`:
   *
   * - `recorridoSentido-recorrido-vigenciaHasta`:
   *
   * - `recorridoSentido-recorrido-vigenciaDesde[ne]`:
   *
   * - `recorridoSentido-recorrido-vigenciaDesde[lt]`:
   *
   * - `recorridoSentido-recorrido-vigenciaDesde[le]`:
   *
   * - `recorridoSentido-recorrido-vigenciaDesde[gt]`:
   *
   * - `recorridoSentido-recorrido-vigenciaDesde[ge]`:
   *
   * - `recorridoSentido-recorrido-vigenciaDesde`:
   *
   * - `recorridoSentido-recorrido-transito[ne]`:
   *
   * - `recorridoSentido-recorrido-transito[like]`:
   *
   * - `recorridoSentido-recorrido-transito`:
   *
   * - `recorridoSentido-recorrido-sitioOrigen[ne]`:
   *
   * - `recorridoSentido-recorrido-sitioOrigen[like]`:
   *
   * - `recorridoSentido-recorrido-sitioOrigen`:
   *
   * - `recorridoSentido-recorrido-sitioDestino[ne]`:
   *
   * - `recorridoSentido-recorrido-sitioDestino[like]`:
   *
   * - `recorridoSentido-recorrido-sitioDestino`:
   *
   * - `recorridoSentido-recorrido-recorrido[ne]`:
   *
   * - `recorridoSentido-recorrido-recorrido[like]`:
   *
   * - `recorridoSentido-recorrido-recorrido`:
   *
   * - `recorridoSentido-recorrido-origen[ne]`:
   *
   * - `recorridoSentido-recorrido-origen[like]`:
   *
   * - `recorridoSentido-recorrido-origen`:
   *
   * - `recorridoSentido-recorrido-itinerarioVuelta[ne]`:
   *
   * - `recorridoSentido-recorrido-itinerarioVuelta[like]`:
   *
   * - `recorridoSentido-recorrido-itinerarioVuelta`:
   *
   * - `recorridoSentido-recorrido-itinerarioIda[ne]`:
   *
   * - `recorridoSentido-recorrido-itinerarioIda[like]`:
   *
   * - `recorridoSentido-recorrido-itinerarioIda`:
   *
   * - `recorridoSentido-recorrido-id[ne]`:
   *
   * - `recorridoSentido-recorrido-id[lt]`:
   *
   * - `recorridoSentido-recorrido-id[le]`:
   *
   * - `recorridoSentido-recorrido-id[gt]`:
   *
   * - `recorridoSentido-recorrido-id[ge]`:
   *
   * - `recorridoSentido-recorrido-id`:
   *
   * - `recorridoSentido-recorrido-destino[ne]`:
   *
   * - `recorridoSentido-recorrido-destino[like]`:
   *
   * - `recorridoSentido-recorrido-destino`:
   *
   * - `recorridoSentido-recorrido-aplicaGasoil`:
   *
   * - `recorridoSentido-origen[ne]`:
   *
   * - `recorridoSentido-origen[like]`:
   *
   * - `recorridoSentido-origen`:
   *
   * - `recorridoSentido-localidadOrigen-updatedAt[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-updatedAt[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-updatedAt[le]`:
   *
   * - `recorridoSentido-localidadOrigen-updatedAt[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-updatedAt[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-updatedAt`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-id[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-id[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-id[le]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-id[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-id[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-id`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-descripcion[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-descripcion[like]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-descripcion`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-codigo[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-codigo[like]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-codigo`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-activo`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-abrev[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-abrev[like]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-abrev`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-id[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-id[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-id[le]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-id[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-id[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-id`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-descripcion[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-descripcion[like]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-descripcion`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-codigo[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-codigo[like]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-codigoINDEC[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-codigoINDEC[like]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-codigoINDEC`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-codigo`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-activo`:
   *
   * - `recorridoSentido-localidadOrigen-parada`:
   *
   * - `recorridoSentido-localidadOrigen-lon[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-lon[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-lon[le]`:
   *
   * - `recorridoSentido-localidadOrigen-lon[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-lon[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-lon`:
   *
   * - `recorridoSentido-localidadOrigen-lat[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-lat[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-lat[le]`:
   *
   * - `recorridoSentido-localidadOrigen-lat[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-lat[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-lat`:
   *
   * - `recorridoSentido-localidadOrigen-id[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-id[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-id[le]`:
   *
   * - `recorridoSentido-localidadOrigen-id[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-id[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-id`:
   *
   * - `recorridoSentido-localidadOrigen-fuente[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-fuente[like]`:
   *
   * - `recorridoSentido-localidadOrigen-fuente`:
   *
   * - `recorridoSentido-localidadOrigen-descripcion[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-descripcion[like]`:
   *
   * - `recorridoSentido-localidadOrigen-descripcion`:
   *
   * - `recorridoSentido-localidadOrigen-createdAt[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-createdAt[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-createdAt[le]`:
   *
   * - `recorridoSentido-localidadOrigen-createdAt[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-createdAt[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-createdAt`:
   *
   * - `recorridoSentido-localidadOrigen-codigoINDEC[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-codigoINDEC[like]`:
   *
   * - `recorridoSentido-localidadOrigen-codigoINDEC`:
   *
   * - `recorridoSentido-localidadOrigen-activo`:
   *
   * - `recorridoSentido-localidadDestino-updatedAt[ne]`:
   *
   * - `recorridoSentido-localidadDestino-updatedAt[lt]`:
   *
   * - `recorridoSentido-localidadDestino-updatedAt[le]`:
   *
   * - `recorridoSentido-localidadDestino-updatedAt[gt]`:
   *
   * - `recorridoSentido-localidadDestino-updatedAt[ge]`:
   *
   * - `recorridoSentido-localidadDestino-updatedAt`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-id[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-id[lt]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-id[le]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-id[gt]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-id[ge]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-id`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-descripcion[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-descripcion[like]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-descripcion`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-codigo[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-codigo[like]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-codigo`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-activo`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-abrev[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-abrev[like]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-abrev`:
   *
   * - `recorridoSentido-localidadDestino-provincia-id[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-id[lt]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-id[le]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-id[gt]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-id[ge]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-id`:
   *
   * - `recorridoSentido-localidadDestino-provincia-descripcion[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-descripcion[like]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-descripcion`:
   *
   * - `recorridoSentido-localidadDestino-provincia-codigo[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-codigo[like]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-codigoINDEC[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-codigoINDEC[like]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-codigoINDEC`:
   *
   * - `recorridoSentido-localidadDestino-provincia-codigo`:
   *
   * - `recorridoSentido-localidadDestino-provincia-activo`:
   *
   * - `recorridoSentido-localidadDestino-parada`:
   *
   * - `recorridoSentido-localidadDestino-lon[ne]`:
   *
   * - `recorridoSentido-localidadDestino-lon[lt]`:
   *
   * - `recorridoSentido-localidadDestino-lon[le]`:
   *
   * - `recorridoSentido-localidadDestino-lon[gt]`:
   *
   * - `recorridoSentido-localidadDestino-lon[ge]`:
   *
   * - `recorridoSentido-localidadDestino-lon`:
   *
   * - `recorridoSentido-localidadDestino-lat[ne]`:
   *
   * - `recorridoSentido-localidadDestino-lat[lt]`:
   *
   * - `recorridoSentido-localidadDestino-lat[le]`:
   *
   * - `recorridoSentido-localidadDestino-lat[gt]`:
   *
   * - `recorridoSentido-localidadDestino-lat[ge]`:
   *
   * - `recorridoSentido-localidadDestino-lat`:
   *
   * - `recorridoSentido-localidadDestino-id[ne]`:
   *
   * - `recorridoSentido-localidadDestino-id[lt]`:
   *
   * - `recorridoSentido-localidadDestino-id[le]`:
   *
   * - `recorridoSentido-localidadDestino-id[gt]`:
   *
   * - `recorridoSentido-localidadDestino-id[ge]`:
   *
   * - `recorridoSentido-localidadDestino-id`:
   *
   * - `recorridoSentido-localidadDestino-fuente[ne]`:
   *
   * - `recorridoSentido-localidadDestino-fuente[like]`:
   *
   * - `recorridoSentido-localidadDestino-fuente`:
   *
   * - `recorridoSentido-localidadDestino-descripcion[ne]`:
   *
   * - `recorridoSentido-localidadDestino-descripcion[like]`:
   *
   * - `recorridoSentido-localidadDestino-descripcion`:
   *
   * - `recorridoSentido-localidadDestino-createdAt[ne]`:
   *
   * - `recorridoSentido-localidadDestino-createdAt[lt]`:
   *
   * - `recorridoSentido-localidadDestino-createdAt[le]`:
   *
   * - `recorridoSentido-localidadDestino-createdAt[gt]`:
   *
   * - `recorridoSentido-localidadDestino-createdAt[ge]`:
   *
   * - `recorridoSentido-localidadDestino-createdAt`:
   *
   * - `recorridoSentido-localidadDestino-codigoINDEC[ne]`:
   *
   * - `recorridoSentido-localidadDestino-codigoINDEC[like]`:
   *
   * - `recorridoSentido-localidadDestino-codigoINDEC`:
   *
   * - `recorridoSentido-localidadDestino-activo`:
   *
   * - `recorridoSentido-itinerario[ne]`:
   *
   * - `recorridoSentido-itinerario[like]`:
   *
   * - `recorridoSentido-itinerario`:
   *
   * - `recorridoSentido-id[ne]`:
   *
   * - `recorridoSentido-id[lt]`:
   *
   * - `recorridoSentido-id[le]`:
   *
   * - `recorridoSentido-id[gt]`:
   *
   * - `recorridoSentido-id[ge]`:
   *
   * - `recorridoSentido-id`:
   *
   * - `recorridoSentido-destino[ne]`:
   *
   * - `recorridoSentido-destino[like]`:
   *
   * - `recorridoSentido-destino`:
   *
   * - `recorrido-vinculacionCaminera[ne]`:
   *
   * - `recorrido-vinculacionCaminera[like]`:
   *
   * - `recorrido-vinculacionCaminera`:
   *
   * - `recorrido-vigenciaHasta[ne]`:
   *
   * - `recorrido-vigenciaHasta[lt]`:
   *
   * - `recorrido-vigenciaHasta[le]`:
   *
   * - `recorrido-vigenciaHasta[gt]`:
   *
   * - `recorrido-vigenciaHasta[ge]`:
   *
   * - `recorrido-vigenciaHasta`:
   *
   * - `recorrido-vigenciaDesde[ne]`:
   *
   * - `recorrido-vigenciaDesde[lt]`:
   *
   * - `recorrido-vigenciaDesde[le]`:
   *
   * - `recorrido-vigenciaDesde[gt]`:
   *
   * - `recorrido-vigenciaDesde[ge]`:
   *
   * - `recorrido-vigenciaDesde`:
   *
   * - `recorrido-transito[ne]`:
   *
   * - `recorrido-transito[like]`:
   *
   * - `recorrido-transito`:
   *
   * - `recorrido-sitioOrigen[ne]`:
   *
   * - `recorrido-sitioOrigen[like]`:
   *
   * - `recorrido-sitioOrigen`:
   *
   * - `recorrido-sitioDestino[ne]`:
   *
   * - `recorrido-sitioDestino[like]`:
   *
   * - `recorrido-sitioDestino`:
   *
   * - `recorrido-recorrido[ne]`:
   *
   * - `recorrido-recorrido[like]`:
   *
   * - `recorrido-recorrido`:
   *
   * - `recorrido-origen[ne]`:
   *
   * - `recorrido-origen[like]`:
   *
   * - `recorrido-origen`:
   *
   * - `recorrido-itinerarioVuelta[ne]`:
   *
   * - `recorrido-itinerarioVuelta[like]`:
   *
   * - `recorrido-itinerarioVuelta`:
   *
   * - `recorrido-itinerarioIda[ne]`:
   *
   * - `recorrido-itinerarioIda[like]`:
   *
   * - `recorrido-itinerarioIda`:
   *
   * - `recorrido-id[ne]`:
   *
   * - `recorrido-id[lt]`:
   *
   * - `recorrido-id[le]`:
   *
   * - `recorrido-id[gt]`:
   *
   * - `recorrido-id[ge]`:
   *
   * - `recorrido-id`:
   *
   * - `recorrido-destino[ne]`:
   *
   * - `recorrido-destino[like]`:
   *
   * - `recorrido-destino`:
   *
   * - `recorrido-aplicaGasoil`:
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `operador-vigenciaHasta[ne]`:
   *
   * - `operador-vigenciaHasta[lt]`:
   *
   * - `operador-vigenciaHasta[le]`:
   *
   * - `operador-vigenciaHasta[gt]`:
   *
   * - `operador-vigenciaHasta[ge]`:
   *
   * - `operador-vigenciaHasta`:
   *
   * - `operador-vigenciaDesde[ne]`:
   *
   * - `operador-vigenciaDesde[lt]`:
   *
   * - `operador-vigenciaDesde[le]`:
   *
   * - `operador-vigenciaDesde[gt]`:
   *
   * - `operador-vigenciaDesde[ge]`:
   *
   * - `operador-vigenciaDesde`:
   *
   * - `operador-localidadOrigen-updatedAt[ne]`:
   *
   * - `operador-localidadOrigen-updatedAt[lt]`:
   *
   * - `operador-localidadOrigen-updatedAt[le]`:
   *
   * - `operador-localidadOrigen-updatedAt[gt]`:
   *
   * - `operador-localidadOrigen-updatedAt[ge]`:
   *
   * - `operador-localidadOrigen-updatedAt`:
   *
   * - `operador-localidadOrigen-provincia-pais-id[ne]`:
   *
   * - `operador-localidadOrigen-provincia-pais-id[lt]`:
   *
   * - `operador-localidadOrigen-provincia-pais-id[le]`:
   *
   * - `operador-localidadOrigen-provincia-pais-id[gt]`:
   *
   * - `operador-localidadOrigen-provincia-pais-id[ge]`:
   *
   * - `operador-localidadOrigen-provincia-pais-id`:
   *
   * - `operador-localidadOrigen-provincia-pais-descripcion[ne]`:
   *
   * - `operador-localidadOrigen-provincia-pais-descripcion[like]`:
   *
   * - `operador-localidadOrigen-provincia-pais-descripcion`:
   *
   * - `operador-localidadOrigen-provincia-pais-codigo[ne]`:
   *
   * - `operador-localidadOrigen-provincia-pais-codigo[like]`:
   *
   * - `operador-localidadOrigen-provincia-pais-codigo`:
   *
   * - `operador-localidadOrigen-provincia-pais-activo`:
   *
   * - `operador-localidadOrigen-provincia-pais-abrev[ne]`:
   *
   * - `operador-localidadOrigen-provincia-pais-abrev[like]`:
   *
   * - `operador-localidadOrigen-provincia-pais-abrev`:
   *
   * - `operador-localidadOrigen-provincia-id[ne]`:
   *
   * - `operador-localidadOrigen-provincia-id[lt]`:
   *
   * - `operador-localidadOrigen-provincia-id[le]`:
   *
   * - `operador-localidadOrigen-provincia-id[gt]`:
   *
   * - `operador-localidadOrigen-provincia-id[ge]`:
   *
   * - `operador-localidadOrigen-provincia-id`:
   *
   * - `operador-localidadOrigen-provincia-descripcion[ne]`:
   *
   * - `operador-localidadOrigen-provincia-descripcion[like]`:
   *
   * - `operador-localidadOrigen-provincia-descripcion`:
   *
   * - `operador-localidadOrigen-provincia-codigo[ne]`:
   *
   * - `operador-localidadOrigen-provincia-codigo[like]`:
   *
   * - `operador-localidadOrigen-provincia-codigoINDEC[ne]`:
   *
   * - `operador-localidadOrigen-provincia-codigoINDEC[like]`:
   *
   * - `operador-localidadOrigen-provincia-codigoINDEC`:
   *
   * - `operador-localidadOrigen-provincia-codigo`:
   *
   * - `operador-localidadOrigen-provincia-activo`:
   *
   * - `operador-localidadOrigen-parada`:
   *
   * - `operador-localidadOrigen-lon[ne]`:
   *
   * - `operador-localidadOrigen-lon[lt]`:
   *
   * - `operador-localidadOrigen-lon[le]`:
   *
   * - `operador-localidadOrigen-lon[gt]`:
   *
   * - `operador-localidadOrigen-lon[ge]`:
   *
   * - `operador-localidadOrigen-lon`:
   *
   * - `operador-localidadOrigen-lat[ne]`:
   *
   * - `operador-localidadOrigen-lat[lt]`:
   *
   * - `operador-localidadOrigen-lat[le]`:
   *
   * - `operador-localidadOrigen-lat[gt]`:
   *
   * - `operador-localidadOrigen-lat[ge]`:
   *
   * - `operador-localidadOrigen-lat`:
   *
   * - `operador-localidadOrigen-id[ne]`:
   *
   * - `operador-localidadOrigen-id[lt]`:
   *
   * - `operador-localidadOrigen-id[le]`:
   *
   * - `operador-localidadOrigen-id[gt]`:
   *
   * - `operador-localidadOrigen-id[ge]`:
   *
   * - `operador-localidadOrigen-id`:
   *
   * - `operador-localidadOrigen-fuente[ne]`:
   *
   * - `operador-localidadOrigen-fuente[like]`:
   *
   * - `operador-localidadOrigen-fuente`:
   *
   * - `operador-localidadOrigen-descripcion[ne]`:
   *
   * - `operador-localidadOrigen-descripcion[like]`:
   *
   * - `operador-localidadOrigen-descripcion`:
   *
   * - `operador-localidadOrigen-createdAt[ne]`:
   *
   * - `operador-localidadOrigen-createdAt[lt]`:
   *
   * - `operador-localidadOrigen-createdAt[le]`:
   *
   * - `operador-localidadOrigen-createdAt[gt]`:
   *
   * - `operador-localidadOrigen-createdAt[ge]`:
   *
   * - `operador-localidadOrigen-createdAt`:
   *
   * - `operador-localidadOrigen-codigoINDEC[ne]`:
   *
   * - `operador-localidadOrigen-codigoINDEC[like]`:
   *
   * - `operador-localidadOrigen-codigoINDEC`:
   *
   * - `operador-localidadOrigen-activo`:
   *
   * - `operador-localidadDestino-updatedAt[ne]`:
   *
   * - `operador-localidadDestino-updatedAt[lt]`:
   *
   * - `operador-localidadDestino-updatedAt[le]`:
   *
   * - `operador-localidadDestino-updatedAt[gt]`:
   *
   * - `operador-localidadDestino-updatedAt[ge]`:
   *
   * - `operador-localidadDestino-updatedAt`:
   *
   * - `operador-localidadDestino-provincia-pais-id[ne]`:
   *
   * - `operador-localidadDestino-provincia-pais-id[lt]`:
   *
   * - `operador-localidadDestino-provincia-pais-id[le]`:
   *
   * - `operador-localidadDestino-provincia-pais-id[gt]`:
   *
   * - `operador-localidadDestino-provincia-pais-id[ge]`:
   *
   * - `operador-localidadDestino-provincia-pais-id`:
   *
   * - `operador-localidadDestino-provincia-pais-descripcion[ne]`:
   *
   * - `operador-localidadDestino-provincia-pais-descripcion[like]`:
   *
   * - `operador-localidadDestino-provincia-pais-descripcion`:
   *
   * - `operador-localidadDestino-provincia-pais-codigo[ne]`:
   *
   * - `operador-localidadDestino-provincia-pais-codigo[like]`:
   *
   * - `operador-localidadDestino-provincia-pais-codigo`:
   *
   * - `operador-localidadDestino-provincia-pais-activo`:
   *
   * - `operador-localidadDestino-provincia-pais-abrev[ne]`:
   *
   * - `operador-localidadDestino-provincia-pais-abrev[like]`:
   *
   * - `operador-localidadDestino-provincia-pais-abrev`:
   *
   * - `operador-localidadDestino-provincia-id[ne]`:
   *
   * - `operador-localidadDestino-provincia-id[lt]`:
   *
   * - `operador-localidadDestino-provincia-id[le]`:
   *
   * - `operador-localidadDestino-provincia-id[gt]`:
   *
   * - `operador-localidadDestino-provincia-id[ge]`:
   *
   * - `operador-localidadDestino-provincia-id`:
   *
   * - `operador-localidadDestino-provincia-descripcion[ne]`:
   *
   * - `operador-localidadDestino-provincia-descripcion[like]`:
   *
   * - `operador-localidadDestino-provincia-descripcion`:
   *
   * - `operador-localidadDestino-provincia-codigo[ne]`:
   *
   * - `operador-localidadDestino-provincia-codigo[like]`:
   *
   * - `operador-localidadDestino-provincia-codigoINDEC[ne]`:
   *
   * - `operador-localidadDestino-provincia-codigoINDEC[like]`:
   *
   * - `operador-localidadDestino-provincia-codigoINDEC`:
   *
   * - `operador-localidadDestino-provincia-codigo`:
   *
   * - `operador-localidadDestino-provincia-activo`:
   *
   * - `operador-localidadDestino-parada`:
   *
   * - `operador-localidadDestino-lon[ne]`:
   *
   * - `operador-localidadDestino-lon[lt]`:
   *
   * - `operador-localidadDestino-lon[le]`:
   *
   * - `operador-localidadDestino-lon[gt]`:
   *
   * - `operador-localidadDestino-lon[ge]`:
   *
   * - `operador-localidadDestino-lon`:
   *
   * - `operador-localidadDestino-lat[ne]`:
   *
   * - `operador-localidadDestino-lat[lt]`:
   *
   * - `operador-localidadDestino-lat[le]`:
   *
   * - `operador-localidadDestino-lat[gt]`:
   *
   * - `operador-localidadDestino-lat[ge]`:
   *
   * - `operador-localidadDestino-lat`:
   *
   * - `operador-localidadDestino-id[ne]`:
   *
   * - `operador-localidadDestino-id[lt]`:
   *
   * - `operador-localidadDestino-id[le]`:
   *
   * - `operador-localidadDestino-id[gt]`:
   *
   * - `operador-localidadDestino-id[ge]`:
   *
   * - `operador-localidadDestino-id`:
   *
   * - `operador-localidadDestino-fuente[ne]`:
   *
   * - `operador-localidadDestino-fuente[like]`:
   *
   * - `operador-localidadDestino-fuente`:
   *
   * - `operador-localidadDestino-descripcion[ne]`:
   *
   * - `operador-localidadDestino-descripcion[like]`:
   *
   * - `operador-localidadDestino-descripcion`:
   *
   * - `operador-localidadDestino-createdAt[ne]`:
   *
   * - `operador-localidadDestino-createdAt[lt]`:
   *
   * - `operador-localidadDestino-createdAt[le]`:
   *
   * - `operador-localidadDestino-createdAt[gt]`:
   *
   * - `operador-localidadDestino-createdAt[ge]`:
   *
   * - `operador-localidadDestino-createdAt`:
   *
   * - `operador-localidadDestino-codigoINDEC[ne]`:
   *
   * - `operador-localidadDestino-codigoINDEC[like]`:
   *
   * - `operador-localidadDestino-codigoINDEC`:
   *
   * - `operador-localidadDestino-activo`:
   *
   * - `operador-linea[ne]`:
   *
   * - `operador-linea[like]`:
   *
   * - `operador-linea`:
   *
   * - `operador-id[ne]`:
   *
   * - `operador-id[lt]`:
   *
   * - `operador-id[le]`:
   *
   * - `operador-id[gt]`:
   *
   * - `operador-id[ge]`:
   *
   * - `operador-id`:
   *
   * - `operador-empresaContratante[ne]`:
   *
   * - `operador-empresaContratante[like]`:
   *
   * - `operador-empresaContratanteCuit[ne]`:
   *
   * - `operador-empresaContratanteCuit[like]`:
   *
   * - `operador-empresaContratanteCuit`:
   *
   * - `operador-empresaContratante`:
   *
   * - `operador-empresa-vigenciaHasta[ne]`:
   *
   * - `operador-empresa-vigenciaHasta[lt]`:
   *
   * - `operador-empresa-vigenciaHasta[le]`:
   *
   * - `operador-empresa-vigenciaHasta[gt]`:
   *
   * - `operador-empresa-vigenciaHasta[ge]`:
   *
   * - `operador-empresa-vigenciaHasta`:
   *
   * - `operador-empresa-vigenciaDesde[ne]`:
   *
   * - `operador-empresa-vigenciaDesde[lt]`:
   *
   * - `operador-empresa-vigenciaDesde[le]`:
   *
   * - `operador-empresa-vigenciaDesde[gt]`:
   *
   * - `operador-empresa-vigenciaDesde[ge]`:
   *
   * - `operador-empresa-vigenciaDesde`:
   *
   * - `operador-empresa-tipoSociedad[ne]`:
   *
   * - `operador-empresa-tipoSociedad[lt]`:
   *
   * - `operador-empresa-tipoSociedad[le]`:
   *
   * - `operador-empresa-tipoSociedad[gt]`:
   *
   * - `operador-empresa-tipoSociedad[ge]`:
   *
   * - `operador-empresa-tipoSociedad`:
   *
   * - `operador-empresa-tipoDocumento-id[ne]`:
   *
   * - `operador-empresa-tipoDocumento-id[lt]`:
   *
   * - `operador-empresa-tipoDocumento-id[le]`:
   *
   * - `operador-empresa-tipoDocumento-id[gt]`:
   *
   * - `operador-empresa-tipoDocumento-id[ge]`:
   *
   * - `operador-empresa-tipoDocumento-id`:
   *
   * - `operador-empresa-tipoDocumento-descripcion[ne]`:
   *
   * - `operador-empresa-tipoDocumento-descripcion[like]`:
   *
   * - `operador-empresa-tipoDocumento-descripcion`:
   *
   * - `operador-empresa-tipoDocumento-abrev[ne]`:
   *
   * - `operador-empresa-tipoDocumento-abrev[like]`:
   *
   * - `operador-empresa-tipoDocumento-abrev`:
   *
   * - `operador-empresa-textoHash`:
   *
   * - `operador-empresa-razonSocial[ne]`:
   *
   * - `operador-empresa-razonSocial[like]`:
   *
   * - `operador-empresa-razonSocial`:
   *
   * - `operador-empresa-paut[ne]`:
   *
   * - `operador-empresa-paut[like]`:
   *
   * - `operador-empresa-paut`:
   *
   * - `operador-empresa-observacion[ne]`:
   *
   * - `operador-empresa-observacion[like]`:
   *
   * - `operador-empresa-observacion`:
   *
   * - `operador-empresa-nroDocumento[ne]`:
   *
   * - `operador-empresa-nroDocumento[like]`:
   *
   * - `operador-empresa-nroDocumento`:
   *
   * - `operador-empresa-nombreFantasia[ne]`:
   *
   * - `operador-empresa-nombreFantasia[like]`:
   *
   * - `operador-empresa-nombreFantasia`:
   *
   * - `operador-empresa-id[ne]`:
   *
   * - `operador-empresa-id[lt]`:
   *
   * - `operador-empresa-id[le]`:
   *
   * - `operador-empresa-id[gt]`:
   *
   * - `operador-empresa-id[ge]`:
   *
   * - `operador-empresa-id`:
   *
   * - `operador-empresa-estadoEmpresa[ne]`:
   *
   * - `operador-empresa-estadoEmpresa[lt]`:
   *
   * - `operador-empresa-estadoEmpresa[le]`:
   *
   * - `operador-empresa-estadoEmpresa[gt]`:
   *
   * - `operador-empresa-estadoEmpresa[ge]`:
   *
   * - `operador-empresa-estadoEmpresa`:
   *
   * - `operador-empresa-esPersonaFisica`:
   *
   * - `operador-empresa-email[ne]`:
   *
   * - `operador-empresa-email[like]`:
   *
   * - `operador-empresa-email`:
   *
   * - `operador-empresa-cuitEmpresaAnterior[ne]`:
   *
   * - `operador-empresa-cuitEmpresaAnterior[like]`:
   *
   * - `operador-empresa-cuitEmpresaAnterior`:
   *
   * - `operador-claseModalidad-id[ne]`:
   *
   * - `operador-claseModalidad-id[lt]`:
   *
   * - `operador-claseModalidad-id[le]`:
   *
   * - `operador-claseModalidad-id[gt]`:
   *
   * - `operador-claseModalidad-id[ge]`:
   *
   * - `operador-claseModalidad-id`:
   *
   * - `operador-claseModalidad-descripcion[ne]`:
   *
   * - `operador-claseModalidad-descripcion[like]`:
   *
   * - `operador-claseModalidad-descripcion`:
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `horaSalida[ne]`:
   *
   * - `horaSalida[lt]`:
   *
   * - `horaSalida[le]`:
   *
   * - `horaSalida[gt]`:
   *
   * - `horaSalida[ge]`:
   *
   * - `horaSalida`:
   *
   * - `horaLlegada[ne]`:
   *
   * - `horaLlegada[lt]`:
   *
   * - `horaLlegada[le]`:
   *
   * - `horaLlegada[gt]`:
   *
   * - `horaLlegada[ge]`:
   *
   * - `horaLlegada`:
   *
   * - `aprobado`:
   *
   * - `<atributo>[<criteria>]`: Lista de atributos a filtrar: ...&lastname[like]=Gonz%&city->state->code[eq]=AR
   *
   * @return Respuesta exitosa.
   */
  findAllServiciosResponse(params: ServicioService.FindAllServiciosParams): __Observable<__StrictHttpResponse<{result?: string, status?: number, data?: any, userMessage?: string, actions?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.vigente != null) __params = __params.set('vigente', params.vigente.toString());
    if (params.vigenciaHastaNe != null) __params = __params.set('vigenciaHasta[ne]', params.vigenciaHastaNe.toString());
    if (params.vigenciaHastaLt != null) __params = __params.set('vigenciaHasta[lt]', params.vigenciaHastaLt.toString());
    if (params.vigenciaHastaLe != null) __params = __params.set('vigenciaHasta[le]', params.vigenciaHastaLe.toString());
    if (params.vigenciaHastaGt != null) __params = __params.set('vigenciaHasta[gt]', params.vigenciaHastaGt.toString());
    if (params.vigenciaHastaGe != null) __params = __params.set('vigenciaHasta[ge]', params.vigenciaHastaGe.toString());
    if (params.vigenciaHasta != null) __params = __params.set('vigenciaHasta', params.vigenciaHasta.toString());
    if (params.vigenciaDesdeNe != null) __params = __params.set('vigenciaDesde[ne]', params.vigenciaDesdeNe.toString());
    if (params.vigenciaDesdeLt != null) __params = __params.set('vigenciaDesde[lt]', params.vigenciaDesdeLt.toString());
    if (params.vigenciaDesdeLe != null) __params = __params.set('vigenciaDesde[le]', params.vigenciaDesdeLe.toString());
    if (params.vigenciaDesdeGt != null) __params = __params.set('vigenciaDesde[gt]', params.vigenciaDesdeGt.toString());
    if (params.vigenciaDesdeGe != null) __params = __params.set('vigenciaDesde[ge]', params.vigenciaDesdeGe.toString());
    if (params.vigenciaDesde != null) __params = __params.set('vigenciaDesde', params.vigenciaDesde.toString());
    if (params.velocidadPromedioTotalNe != null) __params = __params.set('velocidadPromedioTotal[ne]', params.velocidadPromedioTotalNe.toString());
    if (params.velocidadPromedioTotalLt != null) __params = __params.set('velocidadPromedioTotal[lt]', params.velocidadPromedioTotalLt.toString());
    if (params.velocidadPromedioTotalLe != null) __params = __params.set('velocidadPromedioTotal[le]', params.velocidadPromedioTotalLe.toString());
    if (params.velocidadPromedioTotalGt != null) __params = __params.set('velocidadPromedioTotal[gt]', params.velocidadPromedioTotalGt.toString());
    if (params.velocidadPromedioTotalGe != null) __params = __params.set('velocidadPromedioTotal[ge]', params.velocidadPromedioTotalGe.toString());
    if (params.velocidadPromedioTotal != null) __params = __params.set('velocidadPromedioTotal', params.velocidadPromedioTotal.toString());
    if (params.tramiteUsuarioNe != null) __params = __params.set('tramite-usuario[ne]', params.tramiteUsuarioNe.toString());
    if (params.tramiteUsuarioLike != null) __params = __params.set('tramite-usuario[like]', params.tramiteUsuarioLike.toString());
    if (params.tramiteUsuario != null) __params = __params.set('tramite-usuario', params.tramiteUsuario.toString());
    if (params.tramiteTipoTramiteIdNe != null) __params = __params.set('tramite-tipoTramite-id[ne]', params.tramiteTipoTramiteIdNe.toString());
    if (params.tramiteTipoTramiteIdLt != null) __params = __params.set('tramite-tipoTramite-id[lt]', params.tramiteTipoTramiteIdLt.toString());
    if (params.tramiteTipoTramiteIdLe != null) __params = __params.set('tramite-tipoTramite-id[le]', params.tramiteTipoTramiteIdLe.toString());
    if (params.tramiteTipoTramiteIdGt != null) __params = __params.set('tramite-tipoTramite-id[gt]', params.tramiteTipoTramiteIdGt.toString());
    if (params.tramiteTipoTramiteIdGe != null) __params = __params.set('tramite-tipoTramite-id[ge]', params.tramiteTipoTramiteIdGe.toString());
    if (params.tramiteTipoTramiteId != null) __params = __params.set('tramite-tipoTramite-id', params.tramiteTipoTramiteId.toString());
    if (params.tramiteTipoTramiteDescripcionNe != null) __params = __params.set('tramite-tipoTramite-descripcion[ne]', params.tramiteTipoTramiteDescripcionNe.toString());
    if (params.tramiteTipoTramiteDescripcionLike != null) __params = __params.set('tramite-tipoTramite-descripcion[like]', params.tramiteTipoTramiteDescripcionLike.toString());
    if (params.tramiteTipoTramiteDescripcion != null) __params = __params.set('tramite-tipoTramite-descripcion', params.tramiteTipoTramiteDescripcion.toString());
    if (params.tramiteTipoTramiteActivo != null) __params = __params.set('tramite-tipoTramite-activo', params.tramiteTipoTramiteActivo.toString());
    if (params.tramiteSubtipoTramiteIdNe != null) __params = __params.set('tramite-subtipoTramite-id[ne]', params.tramiteSubtipoTramiteIdNe.toString());
    if (params.tramiteSubtipoTramiteIdLt != null) __params = __params.set('tramite-subtipoTramite-id[lt]', params.tramiteSubtipoTramiteIdLt.toString());
    if (params.tramiteSubtipoTramiteIdLe != null) __params = __params.set('tramite-subtipoTramite-id[le]', params.tramiteSubtipoTramiteIdLe.toString());
    if (params.tramiteSubtipoTramiteIdGt != null) __params = __params.set('tramite-subtipoTramite-id[gt]', params.tramiteSubtipoTramiteIdGt.toString());
    if (params.tramiteSubtipoTramiteIdGe != null) __params = __params.set('tramite-subtipoTramite-id[ge]', params.tramiteSubtipoTramiteIdGe.toString());
    if (params.tramiteSubtipoTramiteId != null) __params = __params.set('tramite-subtipoTramite-id', params.tramiteSubtipoTramiteId.toString());
    if (params.tramiteSubtipoTramiteDescripcionNe != null) __params = __params.set('tramite-subtipoTramite-descripcion[ne]', params.tramiteSubtipoTramiteDescripcionNe.toString());
    if (params.tramiteSubtipoTramiteDescripcionLike != null) __params = __params.set('tramite-subtipoTramite-descripcion[like]', params.tramiteSubtipoTramiteDescripcionLike.toString());
    if (params.tramiteSubtipoTramiteDescripcion != null) __params = __params.set('tramite-subtipoTramite-descripcion', params.tramiteSubtipoTramiteDescripcion.toString());
    if (params.tramiteSubtipoTramiteActivo != null) __params = __params.set('tramite-subtipoTramite-activo', params.tramiteSubtipoTramiteActivo.toString());
    if (params.tramiteNroBoletaPagoNe != null) __params = __params.set('tramite-nroBoletaPago[ne]', params.tramiteNroBoletaPagoNe.toString());
    if (params.tramiteNroBoletaPagoLt != null) __params = __params.set('tramite-nroBoletaPago[lt]', params.tramiteNroBoletaPagoLt.toString());
    if (params.tramiteNroBoletaPagoLe != null) __params = __params.set('tramite-nroBoletaPago[le]', params.tramiteNroBoletaPagoLe.toString());
    if (params.tramiteNroBoletaPagoGt != null) __params = __params.set('tramite-nroBoletaPago[gt]', params.tramiteNroBoletaPagoGt.toString());
    if (params.tramiteNroBoletaPagoGe != null) __params = __params.set('tramite-nroBoletaPago[ge]', params.tramiteNroBoletaPagoGe.toString());
    if (params.tramiteNroBoletaPago != null) __params = __params.set('tramite-nroBoletaPago', params.tramiteNroBoletaPago.toString());
    if (params.tramiteIdNe != null) __params = __params.set('tramite-id[ne]', params.tramiteIdNe.toString());
    if (params.tramiteIdLt != null) __params = __params.set('tramite-id[lt]', params.tramiteIdLt.toString());
    if (params.tramiteIdLe != null) __params = __params.set('tramite-id[le]', params.tramiteIdLe.toString());
    if (params.tramiteIdGt != null) __params = __params.set('tramite-id[gt]', params.tramiteIdGt.toString());
    if (params.tramiteIdGe != null) __params = __params.set('tramite-id[ge]', params.tramiteIdGe.toString());
    if (params.tramiteId != null) __params = __params.set('tramite-id', params.tramiteId.toString());
    if (params.tramiteEstadoTramiteIdNe != null) __params = __params.set('tramite-estadoTramite-id[ne]', params.tramiteEstadoTramiteIdNe.toString());
    if (params.tramiteEstadoTramiteIdLt != null) __params = __params.set('tramite-estadoTramite-id[lt]', params.tramiteEstadoTramiteIdLt.toString());
    if (params.tramiteEstadoTramiteIdLe != null) __params = __params.set('tramite-estadoTramite-id[le]', params.tramiteEstadoTramiteIdLe.toString());
    if (params.tramiteEstadoTramiteIdGt != null) __params = __params.set('tramite-estadoTramite-id[gt]', params.tramiteEstadoTramiteIdGt.toString());
    if (params.tramiteEstadoTramiteIdGe != null) __params = __params.set('tramite-estadoTramite-id[ge]', params.tramiteEstadoTramiteIdGe.toString());
    if (params.tramiteEstadoTramiteId != null) __params = __params.set('tramite-estadoTramite-id', params.tramiteEstadoTramiteId.toString());
    if (params.tramiteEstadoTramiteDescripcionNe != null) __params = __params.set('tramite-estadoTramite-descripcion[ne]', params.tramiteEstadoTramiteDescripcionNe.toString());
    if (params.tramiteEstadoTramiteDescripcionLike != null) __params = __params.set('tramite-estadoTramite-descripcion[like]', params.tramiteEstadoTramiteDescripcionLike.toString());
    if (params.tramiteEstadoTramiteDescripcion != null) __params = __params.set('tramite-estadoTramite-descripcion', params.tramiteEstadoTramiteDescripcion.toString());
    if (params.tramiteEstadoTramiteActivo != null) __params = __params.set('tramite-estadoTramite-activo', params.tramiteEstadoTramiteActivo.toString());
    if (params.tramiteEstadoTramiteAbrevNe != null) __params = __params.set('tramite-estadoTramite-abrev[ne]', params.tramiteEstadoTramiteAbrevNe.toString());
    if (params.tramiteEstadoTramiteAbrevLike != null) __params = __params.set('tramite-estadoTramite-abrev[like]', params.tramiteEstadoTramiteAbrevLike.toString());
    if (params.tramiteEstadoTramiteAbrev != null) __params = __params.set('tramite-estadoTramite-abrev', params.tramiteEstadoTramiteAbrev.toString());
    if (params.tramiteEmpresaVigenciaHastaNe != null) __params = __params.set('tramite-empresa-vigenciaHasta[ne]', params.tramiteEmpresaVigenciaHastaNe.toString());
    if (params.tramiteEmpresaVigenciaHastaLt != null) __params = __params.set('tramite-empresa-vigenciaHasta[lt]', params.tramiteEmpresaVigenciaHastaLt.toString());
    if (params.tramiteEmpresaVigenciaHastaLe != null) __params = __params.set('tramite-empresa-vigenciaHasta[le]', params.tramiteEmpresaVigenciaHastaLe.toString());
    if (params.tramiteEmpresaVigenciaHastaGt != null) __params = __params.set('tramite-empresa-vigenciaHasta[gt]', params.tramiteEmpresaVigenciaHastaGt.toString());
    if (params.tramiteEmpresaVigenciaHastaGe != null) __params = __params.set('tramite-empresa-vigenciaHasta[ge]', params.tramiteEmpresaVigenciaHastaGe.toString());
    if (params.tramiteEmpresaVigenciaHasta != null) __params = __params.set('tramite-empresa-vigenciaHasta', params.tramiteEmpresaVigenciaHasta.toString());
    if (params.tramiteEmpresaVigenciaDesdeNe != null) __params = __params.set('tramite-empresa-vigenciaDesde[ne]', params.tramiteEmpresaVigenciaDesdeNe.toString());
    if (params.tramiteEmpresaVigenciaDesdeLt != null) __params = __params.set('tramite-empresa-vigenciaDesde[lt]', params.tramiteEmpresaVigenciaDesdeLt.toString());
    if (params.tramiteEmpresaVigenciaDesdeLe != null) __params = __params.set('tramite-empresa-vigenciaDesde[le]', params.tramiteEmpresaVigenciaDesdeLe.toString());
    if (params.tramiteEmpresaVigenciaDesdeGt != null) __params = __params.set('tramite-empresa-vigenciaDesde[gt]', params.tramiteEmpresaVigenciaDesdeGt.toString());
    if (params.tramiteEmpresaVigenciaDesdeGe != null) __params = __params.set('tramite-empresa-vigenciaDesde[ge]', params.tramiteEmpresaVigenciaDesdeGe.toString());
    if (params.tramiteEmpresaVigenciaDesde != null) __params = __params.set('tramite-empresa-vigenciaDesde', params.tramiteEmpresaVigenciaDesde.toString());
    if (params.tramiteEmpresaTipoSociedadNe != null) __params = __params.set('tramite-empresa-tipoSociedad[ne]', params.tramiteEmpresaTipoSociedadNe.toString());
    if (params.tramiteEmpresaTipoSociedadLt != null) __params = __params.set('tramite-empresa-tipoSociedad[lt]', params.tramiteEmpresaTipoSociedadLt.toString());
    if (params.tramiteEmpresaTipoSociedadLe != null) __params = __params.set('tramite-empresa-tipoSociedad[le]', params.tramiteEmpresaTipoSociedadLe.toString());
    if (params.tramiteEmpresaTipoSociedadGt != null) __params = __params.set('tramite-empresa-tipoSociedad[gt]', params.tramiteEmpresaTipoSociedadGt.toString());
    if (params.tramiteEmpresaTipoSociedadGe != null) __params = __params.set('tramite-empresa-tipoSociedad[ge]', params.tramiteEmpresaTipoSociedadGe.toString());
    if (params.tramiteEmpresaTipoSociedad != null) __params = __params.set('tramite-empresa-tipoSociedad', params.tramiteEmpresaTipoSociedad.toString());
    if (params.tramiteEmpresaTipoDocumentoIdNe != null) __params = __params.set('tramite-empresa-tipoDocumento-id[ne]', params.tramiteEmpresaTipoDocumentoIdNe.toString());
    if (params.tramiteEmpresaTipoDocumentoIdLt != null) __params = __params.set('tramite-empresa-tipoDocumento-id[lt]', params.tramiteEmpresaTipoDocumentoIdLt.toString());
    if (params.tramiteEmpresaTipoDocumentoIdLe != null) __params = __params.set('tramite-empresa-tipoDocumento-id[le]', params.tramiteEmpresaTipoDocumentoIdLe.toString());
    if (params.tramiteEmpresaTipoDocumentoIdGt != null) __params = __params.set('tramite-empresa-tipoDocumento-id[gt]', params.tramiteEmpresaTipoDocumentoIdGt.toString());
    if (params.tramiteEmpresaTipoDocumentoIdGe != null) __params = __params.set('tramite-empresa-tipoDocumento-id[ge]', params.tramiteEmpresaTipoDocumentoIdGe.toString());
    if (params.tramiteEmpresaTipoDocumentoId != null) __params = __params.set('tramite-empresa-tipoDocumento-id', params.tramiteEmpresaTipoDocumentoId.toString());
    if (params.tramiteEmpresaTipoDocumentoDescripcionNe != null) __params = __params.set('tramite-empresa-tipoDocumento-descripcion[ne]', params.tramiteEmpresaTipoDocumentoDescripcionNe.toString());
    if (params.tramiteEmpresaTipoDocumentoDescripcionLike != null) __params = __params.set('tramite-empresa-tipoDocumento-descripcion[like]', params.tramiteEmpresaTipoDocumentoDescripcionLike.toString());
    if (params.tramiteEmpresaTipoDocumentoDescripcion != null) __params = __params.set('tramite-empresa-tipoDocumento-descripcion', params.tramiteEmpresaTipoDocumentoDescripcion.toString());
    if (params.tramiteEmpresaTipoDocumentoAbrevNe != null) __params = __params.set('tramite-empresa-tipoDocumento-abrev[ne]', params.tramiteEmpresaTipoDocumentoAbrevNe.toString());
    if (params.tramiteEmpresaTipoDocumentoAbrevLike != null) __params = __params.set('tramite-empresa-tipoDocumento-abrev[like]', params.tramiteEmpresaTipoDocumentoAbrevLike.toString());
    if (params.tramiteEmpresaTipoDocumentoAbrev != null) __params = __params.set('tramite-empresa-tipoDocumento-abrev', params.tramiteEmpresaTipoDocumentoAbrev.toString());
    if (params.tramiteEmpresaTextoHash != null) __params = __params.set('tramite-empresa-textoHash', params.tramiteEmpresaTextoHash.toString());
    if (params.tramiteEmpresaRazonSocialNe != null) __params = __params.set('tramite-empresa-razonSocial[ne]', params.tramiteEmpresaRazonSocialNe.toString());
    if (params.tramiteEmpresaRazonSocialLike != null) __params = __params.set('tramite-empresa-razonSocial[like]', params.tramiteEmpresaRazonSocialLike.toString());
    if (params.tramiteEmpresaRazonSocial != null) __params = __params.set('tramite-empresa-razonSocial', params.tramiteEmpresaRazonSocial.toString());
    if (params.tramiteEmpresaPautNe != null) __params = __params.set('tramite-empresa-paut[ne]', params.tramiteEmpresaPautNe.toString());
    if (params.tramiteEmpresaPautLike != null) __params = __params.set('tramite-empresa-paut[like]', params.tramiteEmpresaPautLike.toString());
    if (params.tramiteEmpresaPaut != null) __params = __params.set('tramite-empresa-paut', params.tramiteEmpresaPaut.toString());
    if (params.tramiteEmpresaObservacionNe != null) __params = __params.set('tramite-empresa-observacion[ne]', params.tramiteEmpresaObservacionNe.toString());
    if (params.tramiteEmpresaObservacionLike != null) __params = __params.set('tramite-empresa-observacion[like]', params.tramiteEmpresaObservacionLike.toString());
    if (params.tramiteEmpresaObservacion != null) __params = __params.set('tramite-empresa-observacion', params.tramiteEmpresaObservacion.toString());
    if (params.tramiteEmpresaNroDocumentoNe != null) __params = __params.set('tramite-empresa-nroDocumento[ne]', params.tramiteEmpresaNroDocumentoNe.toString());
    if (params.tramiteEmpresaNroDocumentoLike != null) __params = __params.set('tramite-empresa-nroDocumento[like]', params.tramiteEmpresaNroDocumentoLike.toString());
    if (params.tramiteEmpresaNroDocumento != null) __params = __params.set('tramite-empresa-nroDocumento', params.tramiteEmpresaNroDocumento.toString());
    if (params.tramiteEmpresaNombreFantasiaNe != null) __params = __params.set('tramite-empresa-nombreFantasia[ne]', params.tramiteEmpresaNombreFantasiaNe.toString());
    if (params.tramiteEmpresaNombreFantasiaLike != null) __params = __params.set('tramite-empresa-nombreFantasia[like]', params.tramiteEmpresaNombreFantasiaLike.toString());
    if (params.tramiteEmpresaNombreFantasia != null) __params = __params.set('tramite-empresa-nombreFantasia', params.tramiteEmpresaNombreFantasia.toString());
    if (params.tramiteEmpresaIdNe != null) __params = __params.set('tramite-empresa-id[ne]', params.tramiteEmpresaIdNe.toString());
    if (params.tramiteEmpresaIdLt != null) __params = __params.set('tramite-empresa-id[lt]', params.tramiteEmpresaIdLt.toString());
    if (params.tramiteEmpresaIdLe != null) __params = __params.set('tramite-empresa-id[le]', params.tramiteEmpresaIdLe.toString());
    if (params.tramiteEmpresaIdGt != null) __params = __params.set('tramite-empresa-id[gt]', params.tramiteEmpresaIdGt.toString());
    if (params.tramiteEmpresaIdGe != null) __params = __params.set('tramite-empresa-id[ge]', params.tramiteEmpresaIdGe.toString());
    if (params.tramiteEmpresaId != null) __params = __params.set('tramite-empresa-id', params.tramiteEmpresaId.toString());
    if (params.tramiteEmpresaEstadoEmpresaNe != null) __params = __params.set('tramite-empresa-estadoEmpresa[ne]', params.tramiteEmpresaEstadoEmpresaNe.toString());
    if (params.tramiteEmpresaEstadoEmpresaLt != null) __params = __params.set('tramite-empresa-estadoEmpresa[lt]', params.tramiteEmpresaEstadoEmpresaLt.toString());
    if (params.tramiteEmpresaEstadoEmpresaLe != null) __params = __params.set('tramite-empresa-estadoEmpresa[le]', params.tramiteEmpresaEstadoEmpresaLe.toString());
    if (params.tramiteEmpresaEstadoEmpresaGt != null) __params = __params.set('tramite-empresa-estadoEmpresa[gt]', params.tramiteEmpresaEstadoEmpresaGt.toString());
    if (params.tramiteEmpresaEstadoEmpresaGe != null) __params = __params.set('tramite-empresa-estadoEmpresa[ge]', params.tramiteEmpresaEstadoEmpresaGe.toString());
    if (params.tramiteEmpresaEstadoEmpresa != null) __params = __params.set('tramite-empresa-estadoEmpresa', params.tramiteEmpresaEstadoEmpresa.toString());
    if (params.tramiteEmpresaEsPersonaFisica != null) __params = __params.set('tramite-empresa-esPersonaFisica', params.tramiteEmpresaEsPersonaFisica.toString());
    if (params.tramiteEmpresaEmailNe != null) __params = __params.set('tramite-empresa-email[ne]', params.tramiteEmpresaEmailNe.toString());
    if (params.tramiteEmpresaEmailLike != null) __params = __params.set('tramite-empresa-email[like]', params.tramiteEmpresaEmailLike.toString());
    if (params.tramiteEmpresaEmail != null) __params = __params.set('tramite-empresa-email', params.tramiteEmpresaEmail.toString());
    if (params.tramiteEmpresaCuitEmpresaAnteriorNe != null) __params = __params.set('tramite-empresa-cuitEmpresaAnterior[ne]', params.tramiteEmpresaCuitEmpresaAnteriorNe.toString());
    if (params.tramiteEmpresaCuitEmpresaAnteriorLike != null) __params = __params.set('tramite-empresa-cuitEmpresaAnterior[like]', params.tramiteEmpresaCuitEmpresaAnteriorLike.toString());
    if (params.tramiteEmpresaCuitEmpresaAnterior != null) __params = __params.set('tramite-empresa-cuitEmpresaAnterior', params.tramiteEmpresaCuitEmpresaAnterior.toString());
    if (params.sort != null) __params = __params.set('sort', params.sort.toString());
    if (params.recorridoSentidoVinculacionCamineraNe != null) __params = __params.set('recorridoSentido-vinculacionCaminera[ne]', params.recorridoSentidoVinculacionCamineraNe.toString());
    if (params.recorridoSentidoVinculacionCamineraLike != null) __params = __params.set('recorridoSentido-vinculacionCaminera[like]', params.recorridoSentidoVinculacionCamineraLike.toString());
    if (params.recorridoSentidoVinculacionCaminera != null) __params = __params.set('recorridoSentido-vinculacionCaminera', params.recorridoSentidoVinculacionCaminera.toString());
    if (params.recorridoSentidoVigenciaHastaNe != null) __params = __params.set('recorridoSentido-vigenciaHasta[ne]', params.recorridoSentidoVigenciaHastaNe.toString());
    if (params.recorridoSentidoVigenciaHastaLt != null) __params = __params.set('recorridoSentido-vigenciaHasta[lt]', params.recorridoSentidoVigenciaHastaLt.toString());
    if (params.recorridoSentidoVigenciaHastaLe != null) __params = __params.set('recorridoSentido-vigenciaHasta[le]', params.recorridoSentidoVigenciaHastaLe.toString());
    if (params.recorridoSentidoVigenciaHastaGt != null) __params = __params.set('recorridoSentido-vigenciaHasta[gt]', params.recorridoSentidoVigenciaHastaGt.toString());
    if (params.recorridoSentidoVigenciaHastaGe != null) __params = __params.set('recorridoSentido-vigenciaHasta[ge]', params.recorridoSentidoVigenciaHastaGe.toString());
    if (params.recorridoSentidoVigenciaHasta != null) __params = __params.set('recorridoSentido-vigenciaHasta', params.recorridoSentidoVigenciaHasta.toString());
    if (params.recorridoSentidoVigenciaDesdeNe != null) __params = __params.set('recorridoSentido-vigenciaDesde[ne]', params.recorridoSentidoVigenciaDesdeNe.toString());
    if (params.recorridoSentidoVigenciaDesdeLt != null) __params = __params.set('recorridoSentido-vigenciaDesde[lt]', params.recorridoSentidoVigenciaDesdeLt.toString());
    if (params.recorridoSentidoVigenciaDesdeLe != null) __params = __params.set('recorridoSentido-vigenciaDesde[le]', params.recorridoSentidoVigenciaDesdeLe.toString());
    if (params.recorridoSentidoVigenciaDesdeGt != null) __params = __params.set('recorridoSentido-vigenciaDesde[gt]', params.recorridoSentidoVigenciaDesdeGt.toString());
    if (params.recorridoSentidoVigenciaDesdeGe != null) __params = __params.set('recorridoSentido-vigenciaDesde[ge]', params.recorridoSentidoVigenciaDesdeGe.toString());
    if (params.recorridoSentidoVigenciaDesde != null) __params = __params.set('recorridoSentido-vigenciaDesde', params.recorridoSentidoVigenciaDesde.toString());
    if (params.recorridoSentidoSentidoIdNe != null) __params = __params.set('recorridoSentido-sentido-id[ne]', params.recorridoSentidoSentidoIdNe.toString());
    if (params.recorridoSentidoSentidoIdLt != null) __params = __params.set('recorridoSentido-sentido-id[lt]', params.recorridoSentidoSentidoIdLt.toString());
    if (params.recorridoSentidoSentidoIdLe != null) __params = __params.set('recorridoSentido-sentido-id[le]', params.recorridoSentidoSentidoIdLe.toString());
    if (params.recorridoSentidoSentidoIdGt != null) __params = __params.set('recorridoSentido-sentido-id[gt]', params.recorridoSentidoSentidoIdGt.toString());
    if (params.recorridoSentidoSentidoIdGe != null) __params = __params.set('recorridoSentido-sentido-id[ge]', params.recorridoSentidoSentidoIdGe.toString());
    if (params.recorridoSentidoSentidoId != null) __params = __params.set('recorridoSentido-sentido-id', params.recorridoSentidoSentidoId.toString());
    if (params.recorridoSentidoSentidoDescripcionNe != null) __params = __params.set('recorridoSentido-sentido-descripcion[ne]', params.recorridoSentidoSentidoDescripcionNe.toString());
    if (params.recorridoSentidoSentidoDescripcionLike != null) __params = __params.set('recorridoSentido-sentido-descripcion[like]', params.recorridoSentidoSentidoDescripcionLike.toString());
    if (params.recorridoSentidoSentidoDescripcion != null) __params = __params.set('recorridoSentido-sentido-descripcion', params.recorridoSentidoSentidoDescripcion.toString());
    if (params.recorridoSentidoSentidoActivo != null) __params = __params.set('recorridoSentido-sentido-activo', params.recorridoSentidoSentidoActivo.toString());
    if (params.recorridoSentidoSentidoAbrevNe != null) __params = __params.set('recorridoSentido-sentido-abrev[ne]', params.recorridoSentidoSentidoAbrevNe.toString());
    if (params.recorridoSentidoSentidoAbrevLike != null) __params = __params.set('recorridoSentido-sentido-abrev[like]', params.recorridoSentidoSentidoAbrevLike.toString());
    if (params.recorridoSentidoSentidoAbrev != null) __params = __params.set('recorridoSentido-sentido-abrev', params.recorridoSentidoSentidoAbrev.toString());
    if (params.recorridoSentidoRecorridoVinculacionCamineraNe != null) __params = __params.set('recorridoSentido-recorrido-vinculacionCaminera[ne]', params.recorridoSentidoRecorridoVinculacionCamineraNe.toString());
    if (params.recorridoSentidoRecorridoVinculacionCamineraLike != null) __params = __params.set('recorridoSentido-recorrido-vinculacionCaminera[like]', params.recorridoSentidoRecorridoVinculacionCamineraLike.toString());
    if (params.recorridoSentidoRecorridoVinculacionCaminera != null) __params = __params.set('recorridoSentido-recorrido-vinculacionCaminera', params.recorridoSentidoRecorridoVinculacionCaminera.toString());
    if (params.recorridoSentidoRecorridoVigenciaHastaNe != null) __params = __params.set('recorridoSentido-recorrido-vigenciaHasta[ne]', params.recorridoSentidoRecorridoVigenciaHastaNe.toString());
    if (params.recorridoSentidoRecorridoVigenciaHastaLt != null) __params = __params.set('recorridoSentido-recorrido-vigenciaHasta[lt]', params.recorridoSentidoRecorridoVigenciaHastaLt.toString());
    if (params.recorridoSentidoRecorridoVigenciaHastaLe != null) __params = __params.set('recorridoSentido-recorrido-vigenciaHasta[le]', params.recorridoSentidoRecorridoVigenciaHastaLe.toString());
    if (params.recorridoSentidoRecorridoVigenciaHastaGt != null) __params = __params.set('recorridoSentido-recorrido-vigenciaHasta[gt]', params.recorridoSentidoRecorridoVigenciaHastaGt.toString());
    if (params.recorridoSentidoRecorridoVigenciaHastaGe != null) __params = __params.set('recorridoSentido-recorrido-vigenciaHasta[ge]', params.recorridoSentidoRecorridoVigenciaHastaGe.toString());
    if (params.recorridoSentidoRecorridoVigenciaHasta != null) __params = __params.set('recorridoSentido-recorrido-vigenciaHasta', params.recorridoSentidoRecorridoVigenciaHasta.toString());
    if (params.recorridoSentidoRecorridoVigenciaDesdeNe != null) __params = __params.set('recorridoSentido-recorrido-vigenciaDesde[ne]', params.recorridoSentidoRecorridoVigenciaDesdeNe.toString());
    if (params.recorridoSentidoRecorridoVigenciaDesdeLt != null) __params = __params.set('recorridoSentido-recorrido-vigenciaDesde[lt]', params.recorridoSentidoRecorridoVigenciaDesdeLt.toString());
    if (params.recorridoSentidoRecorridoVigenciaDesdeLe != null) __params = __params.set('recorridoSentido-recorrido-vigenciaDesde[le]', params.recorridoSentidoRecorridoVigenciaDesdeLe.toString());
    if (params.recorridoSentidoRecorridoVigenciaDesdeGt != null) __params = __params.set('recorridoSentido-recorrido-vigenciaDesde[gt]', params.recorridoSentidoRecorridoVigenciaDesdeGt.toString());
    if (params.recorridoSentidoRecorridoVigenciaDesdeGe != null) __params = __params.set('recorridoSentido-recorrido-vigenciaDesde[ge]', params.recorridoSentidoRecorridoVigenciaDesdeGe.toString());
    if (params.recorridoSentidoRecorridoVigenciaDesde != null) __params = __params.set('recorridoSentido-recorrido-vigenciaDesde', params.recorridoSentidoRecorridoVigenciaDesde.toString());
    if (params.recorridoSentidoRecorridoTransitoNe != null) __params = __params.set('recorridoSentido-recorrido-transito[ne]', params.recorridoSentidoRecorridoTransitoNe.toString());
    if (params.recorridoSentidoRecorridoTransitoLike != null) __params = __params.set('recorridoSentido-recorrido-transito[like]', params.recorridoSentidoRecorridoTransitoLike.toString());
    if (params.recorridoSentidoRecorridoTransito != null) __params = __params.set('recorridoSentido-recorrido-transito', params.recorridoSentidoRecorridoTransito.toString());
    if (params.recorridoSentidoRecorridoSitioOrigenNe != null) __params = __params.set('recorridoSentido-recorrido-sitioOrigen[ne]', params.recorridoSentidoRecorridoSitioOrigenNe.toString());
    if (params.recorridoSentidoRecorridoSitioOrigenLike != null) __params = __params.set('recorridoSentido-recorrido-sitioOrigen[like]', params.recorridoSentidoRecorridoSitioOrigenLike.toString());
    if (params.recorridoSentidoRecorridoSitioOrigen != null) __params = __params.set('recorridoSentido-recorrido-sitioOrigen', params.recorridoSentidoRecorridoSitioOrigen.toString());
    if (params.recorridoSentidoRecorridoSitioDestinoNe != null) __params = __params.set('recorridoSentido-recorrido-sitioDestino[ne]', params.recorridoSentidoRecorridoSitioDestinoNe.toString());
    if (params.recorridoSentidoRecorridoSitioDestinoLike != null) __params = __params.set('recorridoSentido-recorrido-sitioDestino[like]', params.recorridoSentidoRecorridoSitioDestinoLike.toString());
    if (params.recorridoSentidoRecorridoSitioDestino != null) __params = __params.set('recorridoSentido-recorrido-sitioDestino', params.recorridoSentidoRecorridoSitioDestino.toString());
    if (params.recorridoSentidoRecorridoRecorridoNe != null) __params = __params.set('recorridoSentido-recorrido-recorrido[ne]', params.recorridoSentidoRecorridoRecorridoNe.toString());
    if (params.recorridoSentidoRecorridoRecorridoLike != null) __params = __params.set('recorridoSentido-recorrido-recorrido[like]', params.recorridoSentidoRecorridoRecorridoLike.toString());
    if (params.recorridoSentidoRecorridoRecorrido != null) __params = __params.set('recorridoSentido-recorrido-recorrido', params.recorridoSentidoRecorridoRecorrido.toString());
    if (params.recorridoSentidoRecorridoOrigenNe != null) __params = __params.set('recorridoSentido-recorrido-origen[ne]', params.recorridoSentidoRecorridoOrigenNe.toString());
    if (params.recorridoSentidoRecorridoOrigenLike != null) __params = __params.set('recorridoSentido-recorrido-origen[like]', params.recorridoSentidoRecorridoOrigenLike.toString());
    if (params.recorridoSentidoRecorridoOrigen != null) __params = __params.set('recorridoSentido-recorrido-origen', params.recorridoSentidoRecorridoOrigen.toString());
    if (params.recorridoSentidoRecorridoItinerarioVueltaNe != null) __params = __params.set('recorridoSentido-recorrido-itinerarioVuelta[ne]', params.recorridoSentidoRecorridoItinerarioVueltaNe.toString());
    if (params.recorridoSentidoRecorridoItinerarioVueltaLike != null) __params = __params.set('recorridoSentido-recorrido-itinerarioVuelta[like]', params.recorridoSentidoRecorridoItinerarioVueltaLike.toString());
    if (params.recorridoSentidoRecorridoItinerarioVuelta != null) __params = __params.set('recorridoSentido-recorrido-itinerarioVuelta', params.recorridoSentidoRecorridoItinerarioVuelta.toString());
    if (params.recorridoSentidoRecorridoItinerarioIdaNe != null) __params = __params.set('recorridoSentido-recorrido-itinerarioIda[ne]', params.recorridoSentidoRecorridoItinerarioIdaNe.toString());
    if (params.recorridoSentidoRecorridoItinerarioIdaLike != null) __params = __params.set('recorridoSentido-recorrido-itinerarioIda[like]', params.recorridoSentidoRecorridoItinerarioIdaLike.toString());
    if (params.recorridoSentidoRecorridoItinerarioIda != null) __params = __params.set('recorridoSentido-recorrido-itinerarioIda', params.recorridoSentidoRecorridoItinerarioIda.toString());
    if (params.recorridoSentidoRecorridoIdNe != null) __params = __params.set('recorridoSentido-recorrido-id[ne]', params.recorridoSentidoRecorridoIdNe.toString());
    if (params.recorridoSentidoRecorridoIdLt != null) __params = __params.set('recorridoSentido-recorrido-id[lt]', params.recorridoSentidoRecorridoIdLt.toString());
    if (params.recorridoSentidoRecorridoIdLe != null) __params = __params.set('recorridoSentido-recorrido-id[le]', params.recorridoSentidoRecorridoIdLe.toString());
    if (params.recorridoSentidoRecorridoIdGt != null) __params = __params.set('recorridoSentido-recorrido-id[gt]', params.recorridoSentidoRecorridoIdGt.toString());
    if (params.recorridoSentidoRecorridoIdGe != null) __params = __params.set('recorridoSentido-recorrido-id[ge]', params.recorridoSentidoRecorridoIdGe.toString());
    if (params.recorridoSentidoRecorridoId != null) __params = __params.set('recorridoSentido-recorrido-id', params.recorridoSentidoRecorridoId.toString());
    if (params.recorridoSentidoRecorridoDestinoNe != null) __params = __params.set('recorridoSentido-recorrido-destino[ne]', params.recorridoSentidoRecorridoDestinoNe.toString());
    if (params.recorridoSentidoRecorridoDestinoLike != null) __params = __params.set('recorridoSentido-recorrido-destino[like]', params.recorridoSentidoRecorridoDestinoLike.toString());
    if (params.recorridoSentidoRecorridoDestino != null) __params = __params.set('recorridoSentido-recorrido-destino', params.recorridoSentidoRecorridoDestino.toString());
    if (params.recorridoSentidoRecorridoAplicaGasoil != null) __params = __params.set('recorridoSentido-recorrido-aplicaGasoil', params.recorridoSentidoRecorridoAplicaGasoil.toString());
    if (params.recorridoSentidoOrigenNe != null) __params = __params.set('recorridoSentido-origen[ne]', params.recorridoSentidoOrigenNe.toString());
    if (params.recorridoSentidoOrigenLike != null) __params = __params.set('recorridoSentido-origen[like]', params.recorridoSentidoOrigenLike.toString());
    if (params.recorridoSentidoOrigen != null) __params = __params.set('recorridoSentido-origen', params.recorridoSentidoOrigen.toString());
    if (params.recorridoSentidoLocalidadOrigenUpdatedAtNe != null) __params = __params.set('recorridoSentido-localidadOrigen-updatedAt[ne]', params.recorridoSentidoLocalidadOrigenUpdatedAtNe.toString());
    if (params.recorridoSentidoLocalidadOrigenUpdatedAtLt != null) __params = __params.set('recorridoSentido-localidadOrigen-updatedAt[lt]', params.recorridoSentidoLocalidadOrigenUpdatedAtLt.toString());
    if (params.recorridoSentidoLocalidadOrigenUpdatedAtLe != null) __params = __params.set('recorridoSentido-localidadOrigen-updatedAt[le]', params.recorridoSentidoLocalidadOrigenUpdatedAtLe.toString());
    if (params.recorridoSentidoLocalidadOrigenUpdatedAtGt != null) __params = __params.set('recorridoSentido-localidadOrigen-updatedAt[gt]', params.recorridoSentidoLocalidadOrigenUpdatedAtGt.toString());
    if (params.recorridoSentidoLocalidadOrigenUpdatedAtGe != null) __params = __params.set('recorridoSentido-localidadOrigen-updatedAt[ge]', params.recorridoSentidoLocalidadOrigenUpdatedAtGe.toString());
    if (params.recorridoSentidoLocalidadOrigenUpdatedAt != null) __params = __params.set('recorridoSentido-localidadOrigen-updatedAt', params.recorridoSentidoLocalidadOrigenUpdatedAt.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisIdNe != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-id[ne]', params.recorridoSentidoLocalidadOrigenProvinciaPaisIdNe.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisIdLt != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-id[lt]', params.recorridoSentidoLocalidadOrigenProvinciaPaisIdLt.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisIdLe != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-id[le]', params.recorridoSentidoLocalidadOrigenProvinciaPaisIdLe.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisIdGt != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-id[gt]', params.recorridoSentidoLocalidadOrigenProvinciaPaisIdGt.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisIdGe != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-id[ge]', params.recorridoSentidoLocalidadOrigenProvinciaPaisIdGe.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisId != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-id', params.recorridoSentidoLocalidadOrigenProvinciaPaisId.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisDescripcionNe != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-descripcion[ne]', params.recorridoSentidoLocalidadOrigenProvinciaPaisDescripcionNe.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisDescripcionLike != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-descripcion[like]', params.recorridoSentidoLocalidadOrigenProvinciaPaisDescripcionLike.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisDescripcion != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-descripcion', params.recorridoSentidoLocalidadOrigenProvinciaPaisDescripcion.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisCodigoNe != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-codigo[ne]', params.recorridoSentidoLocalidadOrigenProvinciaPaisCodigoNe.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisCodigoLike != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-codigo[like]', params.recorridoSentidoLocalidadOrigenProvinciaPaisCodigoLike.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisCodigo != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-codigo', params.recorridoSentidoLocalidadOrigenProvinciaPaisCodigo.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisActivo != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-activo', params.recorridoSentidoLocalidadOrigenProvinciaPaisActivo.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisAbrevNe != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-abrev[ne]', params.recorridoSentidoLocalidadOrigenProvinciaPaisAbrevNe.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisAbrevLike != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-abrev[like]', params.recorridoSentidoLocalidadOrigenProvinciaPaisAbrevLike.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaPaisAbrev != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-pais-abrev', params.recorridoSentidoLocalidadOrigenProvinciaPaisAbrev.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaIdNe != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-id[ne]', params.recorridoSentidoLocalidadOrigenProvinciaIdNe.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaIdLt != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-id[lt]', params.recorridoSentidoLocalidadOrigenProvinciaIdLt.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaIdLe != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-id[le]', params.recorridoSentidoLocalidadOrigenProvinciaIdLe.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaIdGt != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-id[gt]', params.recorridoSentidoLocalidadOrigenProvinciaIdGt.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaIdGe != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-id[ge]', params.recorridoSentidoLocalidadOrigenProvinciaIdGe.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaId != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-id', params.recorridoSentidoLocalidadOrigenProvinciaId.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaDescripcionNe != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-descripcion[ne]', params.recorridoSentidoLocalidadOrigenProvinciaDescripcionNe.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaDescripcionLike != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-descripcion[like]', params.recorridoSentidoLocalidadOrigenProvinciaDescripcionLike.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaDescripcion != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-descripcion', params.recorridoSentidoLocalidadOrigenProvinciaDescripcion.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaCodigoNe != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-codigo[ne]', params.recorridoSentidoLocalidadOrigenProvinciaCodigoNe.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaCodigoLike != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-codigo[like]', params.recorridoSentidoLocalidadOrigenProvinciaCodigoLike.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaCodigoINDECNe != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-codigoINDEC[ne]', params.recorridoSentidoLocalidadOrigenProvinciaCodigoINDECNe.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaCodigoINDECLike != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-codigoINDEC[like]', params.recorridoSentidoLocalidadOrigenProvinciaCodigoINDECLike.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaCodigoINDEC != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-codigoINDEC', params.recorridoSentidoLocalidadOrigenProvinciaCodigoINDEC.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaCodigo != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-codigo', params.recorridoSentidoLocalidadOrigenProvinciaCodigo.toString());
    if (params.recorridoSentidoLocalidadOrigenProvinciaActivo != null) __params = __params.set('recorridoSentido-localidadOrigen-provincia-activo', params.recorridoSentidoLocalidadOrigenProvinciaActivo.toString());
    if (params.recorridoSentidoLocalidadOrigenParada != null) __params = __params.set('recorridoSentido-localidadOrigen-parada', params.recorridoSentidoLocalidadOrigenParada.toString());
    if (params.recorridoSentidoLocalidadOrigenLonNe != null) __params = __params.set('recorridoSentido-localidadOrigen-lon[ne]', params.recorridoSentidoLocalidadOrigenLonNe.toString());
    if (params.recorridoSentidoLocalidadOrigenLonLt != null) __params = __params.set('recorridoSentido-localidadOrigen-lon[lt]', params.recorridoSentidoLocalidadOrigenLonLt.toString());
    if (params.recorridoSentidoLocalidadOrigenLonLe != null) __params = __params.set('recorridoSentido-localidadOrigen-lon[le]', params.recorridoSentidoLocalidadOrigenLonLe.toString());
    if (params.recorridoSentidoLocalidadOrigenLonGt != null) __params = __params.set('recorridoSentido-localidadOrigen-lon[gt]', params.recorridoSentidoLocalidadOrigenLonGt.toString());
    if (params.recorridoSentidoLocalidadOrigenLonGe != null) __params = __params.set('recorridoSentido-localidadOrigen-lon[ge]', params.recorridoSentidoLocalidadOrigenLonGe.toString());
    if (params.recorridoSentidoLocalidadOrigenLon != null) __params = __params.set('recorridoSentido-localidadOrigen-lon', params.recorridoSentidoLocalidadOrigenLon.toString());
    if (params.recorridoSentidoLocalidadOrigenLatNe != null) __params = __params.set('recorridoSentido-localidadOrigen-lat[ne]', params.recorridoSentidoLocalidadOrigenLatNe.toString());
    if (params.recorridoSentidoLocalidadOrigenLatLt != null) __params = __params.set('recorridoSentido-localidadOrigen-lat[lt]', params.recorridoSentidoLocalidadOrigenLatLt.toString());
    if (params.recorridoSentidoLocalidadOrigenLatLe != null) __params = __params.set('recorridoSentido-localidadOrigen-lat[le]', params.recorridoSentidoLocalidadOrigenLatLe.toString());
    if (params.recorridoSentidoLocalidadOrigenLatGt != null) __params = __params.set('recorridoSentido-localidadOrigen-lat[gt]', params.recorridoSentidoLocalidadOrigenLatGt.toString());
    if (params.recorridoSentidoLocalidadOrigenLatGe != null) __params = __params.set('recorridoSentido-localidadOrigen-lat[ge]', params.recorridoSentidoLocalidadOrigenLatGe.toString());
    if (params.recorridoSentidoLocalidadOrigenLat != null) __params = __params.set('recorridoSentido-localidadOrigen-lat', params.recorridoSentidoLocalidadOrigenLat.toString());
    if (params.recorridoSentidoLocalidadOrigenIdNe != null) __params = __params.set('recorridoSentido-localidadOrigen-id[ne]', params.recorridoSentidoLocalidadOrigenIdNe.toString());
    if (params.recorridoSentidoLocalidadOrigenIdLt != null) __params = __params.set('recorridoSentido-localidadOrigen-id[lt]', params.recorridoSentidoLocalidadOrigenIdLt.toString());
    if (params.recorridoSentidoLocalidadOrigenIdLe != null) __params = __params.set('recorridoSentido-localidadOrigen-id[le]', params.recorridoSentidoLocalidadOrigenIdLe.toString());
    if (params.recorridoSentidoLocalidadOrigenIdGt != null) __params = __params.set('recorridoSentido-localidadOrigen-id[gt]', params.recorridoSentidoLocalidadOrigenIdGt.toString());
    if (params.recorridoSentidoLocalidadOrigenIdGe != null) __params = __params.set('recorridoSentido-localidadOrigen-id[ge]', params.recorridoSentidoLocalidadOrigenIdGe.toString());
    if (params.recorridoSentidoLocalidadOrigenId != null) __params = __params.set('recorridoSentido-localidadOrigen-id', params.recorridoSentidoLocalidadOrigenId.toString());
    if (params.recorridoSentidoLocalidadOrigenFuenteNe != null) __params = __params.set('recorridoSentido-localidadOrigen-fuente[ne]', params.recorridoSentidoLocalidadOrigenFuenteNe.toString());
    if (params.recorridoSentidoLocalidadOrigenFuenteLike != null) __params = __params.set('recorridoSentido-localidadOrigen-fuente[like]', params.recorridoSentidoLocalidadOrigenFuenteLike.toString());
    if (params.recorridoSentidoLocalidadOrigenFuente != null) __params = __params.set('recorridoSentido-localidadOrigen-fuente', params.recorridoSentidoLocalidadOrigenFuente.toString());
    if (params.recorridoSentidoLocalidadOrigenDescripcionNe != null) __params = __params.set('recorridoSentido-localidadOrigen-descripcion[ne]', params.recorridoSentidoLocalidadOrigenDescripcionNe.toString());
    if (params.recorridoSentidoLocalidadOrigenDescripcionLike != null) __params = __params.set('recorridoSentido-localidadOrigen-descripcion[like]', params.recorridoSentidoLocalidadOrigenDescripcionLike.toString());
    if (params.recorridoSentidoLocalidadOrigenDescripcion != null) __params = __params.set('recorridoSentido-localidadOrigen-descripcion', params.recorridoSentidoLocalidadOrigenDescripcion.toString());
    if (params.recorridoSentidoLocalidadOrigenCreatedAtNe != null) __params = __params.set('recorridoSentido-localidadOrigen-createdAt[ne]', params.recorridoSentidoLocalidadOrigenCreatedAtNe.toString());
    if (params.recorridoSentidoLocalidadOrigenCreatedAtLt != null) __params = __params.set('recorridoSentido-localidadOrigen-createdAt[lt]', params.recorridoSentidoLocalidadOrigenCreatedAtLt.toString());
    if (params.recorridoSentidoLocalidadOrigenCreatedAtLe != null) __params = __params.set('recorridoSentido-localidadOrigen-createdAt[le]', params.recorridoSentidoLocalidadOrigenCreatedAtLe.toString());
    if (params.recorridoSentidoLocalidadOrigenCreatedAtGt != null) __params = __params.set('recorridoSentido-localidadOrigen-createdAt[gt]', params.recorridoSentidoLocalidadOrigenCreatedAtGt.toString());
    if (params.recorridoSentidoLocalidadOrigenCreatedAtGe != null) __params = __params.set('recorridoSentido-localidadOrigen-createdAt[ge]', params.recorridoSentidoLocalidadOrigenCreatedAtGe.toString());
    if (params.recorridoSentidoLocalidadOrigenCreatedAt != null) __params = __params.set('recorridoSentido-localidadOrigen-createdAt', params.recorridoSentidoLocalidadOrigenCreatedAt.toString());
    if (params.recorridoSentidoLocalidadOrigenCodigoINDECNe != null) __params = __params.set('recorridoSentido-localidadOrigen-codigoINDEC[ne]', params.recorridoSentidoLocalidadOrigenCodigoINDECNe.toString());
    if (params.recorridoSentidoLocalidadOrigenCodigoINDECLike != null) __params = __params.set('recorridoSentido-localidadOrigen-codigoINDEC[like]', params.recorridoSentidoLocalidadOrigenCodigoINDECLike.toString());
    if (params.recorridoSentidoLocalidadOrigenCodigoINDEC != null) __params = __params.set('recorridoSentido-localidadOrigen-codigoINDEC', params.recorridoSentidoLocalidadOrigenCodigoINDEC.toString());
    if (params.recorridoSentidoLocalidadOrigenActivo != null) __params = __params.set('recorridoSentido-localidadOrigen-activo', params.recorridoSentidoLocalidadOrigenActivo.toString());
    if (params.recorridoSentidoLocalidadDestinoUpdatedAtNe != null) __params = __params.set('recorridoSentido-localidadDestino-updatedAt[ne]', params.recorridoSentidoLocalidadDestinoUpdatedAtNe.toString());
    if (params.recorridoSentidoLocalidadDestinoUpdatedAtLt != null) __params = __params.set('recorridoSentido-localidadDestino-updatedAt[lt]', params.recorridoSentidoLocalidadDestinoUpdatedAtLt.toString());
    if (params.recorridoSentidoLocalidadDestinoUpdatedAtLe != null) __params = __params.set('recorridoSentido-localidadDestino-updatedAt[le]', params.recorridoSentidoLocalidadDestinoUpdatedAtLe.toString());
    if (params.recorridoSentidoLocalidadDestinoUpdatedAtGt != null) __params = __params.set('recorridoSentido-localidadDestino-updatedAt[gt]', params.recorridoSentidoLocalidadDestinoUpdatedAtGt.toString());
    if (params.recorridoSentidoLocalidadDestinoUpdatedAtGe != null) __params = __params.set('recorridoSentido-localidadDestino-updatedAt[ge]', params.recorridoSentidoLocalidadDestinoUpdatedAtGe.toString());
    if (params.recorridoSentidoLocalidadDestinoUpdatedAt != null) __params = __params.set('recorridoSentido-localidadDestino-updatedAt', params.recorridoSentidoLocalidadDestinoUpdatedAt.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisIdNe != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-id[ne]', params.recorridoSentidoLocalidadDestinoProvinciaPaisIdNe.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisIdLt != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-id[lt]', params.recorridoSentidoLocalidadDestinoProvinciaPaisIdLt.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisIdLe != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-id[le]', params.recorridoSentidoLocalidadDestinoProvinciaPaisIdLe.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisIdGt != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-id[gt]', params.recorridoSentidoLocalidadDestinoProvinciaPaisIdGt.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisIdGe != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-id[ge]', params.recorridoSentidoLocalidadDestinoProvinciaPaisIdGe.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisId != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-id', params.recorridoSentidoLocalidadDestinoProvinciaPaisId.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisDescripcionNe != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-descripcion[ne]', params.recorridoSentidoLocalidadDestinoProvinciaPaisDescripcionNe.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisDescripcionLike != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-descripcion[like]', params.recorridoSentidoLocalidadDestinoProvinciaPaisDescripcionLike.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisDescripcion != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-descripcion', params.recorridoSentidoLocalidadDestinoProvinciaPaisDescripcion.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisCodigoNe != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-codigo[ne]', params.recorridoSentidoLocalidadDestinoProvinciaPaisCodigoNe.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisCodigoLike != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-codigo[like]', params.recorridoSentidoLocalidadDestinoProvinciaPaisCodigoLike.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisCodigo != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-codigo', params.recorridoSentidoLocalidadDestinoProvinciaPaisCodigo.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisActivo != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-activo', params.recorridoSentidoLocalidadDestinoProvinciaPaisActivo.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisAbrevNe != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-abrev[ne]', params.recorridoSentidoLocalidadDestinoProvinciaPaisAbrevNe.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisAbrevLike != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-abrev[like]', params.recorridoSentidoLocalidadDestinoProvinciaPaisAbrevLike.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaPaisAbrev != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-pais-abrev', params.recorridoSentidoLocalidadDestinoProvinciaPaisAbrev.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaIdNe != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-id[ne]', params.recorridoSentidoLocalidadDestinoProvinciaIdNe.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaIdLt != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-id[lt]', params.recorridoSentidoLocalidadDestinoProvinciaIdLt.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaIdLe != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-id[le]', params.recorridoSentidoLocalidadDestinoProvinciaIdLe.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaIdGt != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-id[gt]', params.recorridoSentidoLocalidadDestinoProvinciaIdGt.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaIdGe != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-id[ge]', params.recorridoSentidoLocalidadDestinoProvinciaIdGe.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaId != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-id', params.recorridoSentidoLocalidadDestinoProvinciaId.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaDescripcionNe != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-descripcion[ne]', params.recorridoSentidoLocalidadDestinoProvinciaDescripcionNe.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaDescripcionLike != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-descripcion[like]', params.recorridoSentidoLocalidadDestinoProvinciaDescripcionLike.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaDescripcion != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-descripcion', params.recorridoSentidoLocalidadDestinoProvinciaDescripcion.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaCodigoNe != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-codigo[ne]', params.recorridoSentidoLocalidadDestinoProvinciaCodigoNe.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaCodigoLike != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-codigo[like]', params.recorridoSentidoLocalidadDestinoProvinciaCodigoLike.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaCodigoINDECNe != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-codigoINDEC[ne]', params.recorridoSentidoLocalidadDestinoProvinciaCodigoINDECNe.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaCodigoINDECLike != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-codigoINDEC[like]', params.recorridoSentidoLocalidadDestinoProvinciaCodigoINDECLike.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaCodigoINDEC != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-codigoINDEC', params.recorridoSentidoLocalidadDestinoProvinciaCodigoINDEC.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaCodigo != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-codigo', params.recorridoSentidoLocalidadDestinoProvinciaCodigo.toString());
    if (params.recorridoSentidoLocalidadDestinoProvinciaActivo != null) __params = __params.set('recorridoSentido-localidadDestino-provincia-activo', params.recorridoSentidoLocalidadDestinoProvinciaActivo.toString());
    if (params.recorridoSentidoLocalidadDestinoParada != null) __params = __params.set('recorridoSentido-localidadDestino-parada', params.recorridoSentidoLocalidadDestinoParada.toString());
    if (params.recorridoSentidoLocalidadDestinoLonNe != null) __params = __params.set('recorridoSentido-localidadDestino-lon[ne]', params.recorridoSentidoLocalidadDestinoLonNe.toString());
    if (params.recorridoSentidoLocalidadDestinoLonLt != null) __params = __params.set('recorridoSentido-localidadDestino-lon[lt]', params.recorridoSentidoLocalidadDestinoLonLt.toString());
    if (params.recorridoSentidoLocalidadDestinoLonLe != null) __params = __params.set('recorridoSentido-localidadDestino-lon[le]', params.recorridoSentidoLocalidadDestinoLonLe.toString());
    if (params.recorridoSentidoLocalidadDestinoLonGt != null) __params = __params.set('recorridoSentido-localidadDestino-lon[gt]', params.recorridoSentidoLocalidadDestinoLonGt.toString());
    if (params.recorridoSentidoLocalidadDestinoLonGe != null) __params = __params.set('recorridoSentido-localidadDestino-lon[ge]', params.recorridoSentidoLocalidadDestinoLonGe.toString());
    if (params.recorridoSentidoLocalidadDestinoLon != null) __params = __params.set('recorridoSentido-localidadDestino-lon', params.recorridoSentidoLocalidadDestinoLon.toString());
    if (params.recorridoSentidoLocalidadDestinoLatNe != null) __params = __params.set('recorridoSentido-localidadDestino-lat[ne]', params.recorridoSentidoLocalidadDestinoLatNe.toString());
    if (params.recorridoSentidoLocalidadDestinoLatLt != null) __params = __params.set('recorridoSentido-localidadDestino-lat[lt]', params.recorridoSentidoLocalidadDestinoLatLt.toString());
    if (params.recorridoSentidoLocalidadDestinoLatLe != null) __params = __params.set('recorridoSentido-localidadDestino-lat[le]', params.recorridoSentidoLocalidadDestinoLatLe.toString());
    if (params.recorridoSentidoLocalidadDestinoLatGt != null) __params = __params.set('recorridoSentido-localidadDestino-lat[gt]', params.recorridoSentidoLocalidadDestinoLatGt.toString());
    if (params.recorridoSentidoLocalidadDestinoLatGe != null) __params = __params.set('recorridoSentido-localidadDestino-lat[ge]', params.recorridoSentidoLocalidadDestinoLatGe.toString());
    if (params.recorridoSentidoLocalidadDestinoLat != null) __params = __params.set('recorridoSentido-localidadDestino-lat', params.recorridoSentidoLocalidadDestinoLat.toString());
    if (params.recorridoSentidoLocalidadDestinoIdNe != null) __params = __params.set('recorridoSentido-localidadDestino-id[ne]', params.recorridoSentidoLocalidadDestinoIdNe.toString());
    if (params.recorridoSentidoLocalidadDestinoIdLt != null) __params = __params.set('recorridoSentido-localidadDestino-id[lt]', params.recorridoSentidoLocalidadDestinoIdLt.toString());
    if (params.recorridoSentidoLocalidadDestinoIdLe != null) __params = __params.set('recorridoSentido-localidadDestino-id[le]', params.recorridoSentidoLocalidadDestinoIdLe.toString());
    if (params.recorridoSentidoLocalidadDestinoIdGt != null) __params = __params.set('recorridoSentido-localidadDestino-id[gt]', params.recorridoSentidoLocalidadDestinoIdGt.toString());
    if (params.recorridoSentidoLocalidadDestinoIdGe != null) __params = __params.set('recorridoSentido-localidadDestino-id[ge]', params.recorridoSentidoLocalidadDestinoIdGe.toString());
    if (params.recorridoSentidoLocalidadDestinoId != null) __params = __params.set('recorridoSentido-localidadDestino-id', params.recorridoSentidoLocalidadDestinoId.toString());
    if (params.recorridoSentidoLocalidadDestinoFuenteNe != null) __params = __params.set('recorridoSentido-localidadDestino-fuente[ne]', params.recorridoSentidoLocalidadDestinoFuenteNe.toString());
    if (params.recorridoSentidoLocalidadDestinoFuenteLike != null) __params = __params.set('recorridoSentido-localidadDestino-fuente[like]', params.recorridoSentidoLocalidadDestinoFuenteLike.toString());
    if (params.recorridoSentidoLocalidadDestinoFuente != null) __params = __params.set('recorridoSentido-localidadDestino-fuente', params.recorridoSentidoLocalidadDestinoFuente.toString());
    if (params.recorridoSentidoLocalidadDestinoDescripcionNe != null) __params = __params.set('recorridoSentido-localidadDestino-descripcion[ne]', params.recorridoSentidoLocalidadDestinoDescripcionNe.toString());
    if (params.recorridoSentidoLocalidadDestinoDescripcionLike != null) __params = __params.set('recorridoSentido-localidadDestino-descripcion[like]', params.recorridoSentidoLocalidadDestinoDescripcionLike.toString());
    if (params.recorridoSentidoLocalidadDestinoDescripcion != null) __params = __params.set('recorridoSentido-localidadDestino-descripcion', params.recorridoSentidoLocalidadDestinoDescripcion.toString());
    if (params.recorridoSentidoLocalidadDestinoCreatedAtNe != null) __params = __params.set('recorridoSentido-localidadDestino-createdAt[ne]', params.recorridoSentidoLocalidadDestinoCreatedAtNe.toString());
    if (params.recorridoSentidoLocalidadDestinoCreatedAtLt != null) __params = __params.set('recorridoSentido-localidadDestino-createdAt[lt]', params.recorridoSentidoLocalidadDestinoCreatedAtLt.toString());
    if (params.recorridoSentidoLocalidadDestinoCreatedAtLe != null) __params = __params.set('recorridoSentido-localidadDestino-createdAt[le]', params.recorridoSentidoLocalidadDestinoCreatedAtLe.toString());
    if (params.recorridoSentidoLocalidadDestinoCreatedAtGt != null) __params = __params.set('recorridoSentido-localidadDestino-createdAt[gt]', params.recorridoSentidoLocalidadDestinoCreatedAtGt.toString());
    if (params.recorridoSentidoLocalidadDestinoCreatedAtGe != null) __params = __params.set('recorridoSentido-localidadDestino-createdAt[ge]', params.recorridoSentidoLocalidadDestinoCreatedAtGe.toString());
    if (params.recorridoSentidoLocalidadDestinoCreatedAt != null) __params = __params.set('recorridoSentido-localidadDestino-createdAt', params.recorridoSentidoLocalidadDestinoCreatedAt.toString());
    if (params.recorridoSentidoLocalidadDestinoCodigoINDECNe != null) __params = __params.set('recorridoSentido-localidadDestino-codigoINDEC[ne]', params.recorridoSentidoLocalidadDestinoCodigoINDECNe.toString());
    if (params.recorridoSentidoLocalidadDestinoCodigoINDECLike != null) __params = __params.set('recorridoSentido-localidadDestino-codigoINDEC[like]', params.recorridoSentidoLocalidadDestinoCodigoINDECLike.toString());
    if (params.recorridoSentidoLocalidadDestinoCodigoINDEC != null) __params = __params.set('recorridoSentido-localidadDestino-codigoINDEC', params.recorridoSentidoLocalidadDestinoCodigoINDEC.toString());
    if (params.recorridoSentidoLocalidadDestinoActivo != null) __params = __params.set('recorridoSentido-localidadDestino-activo', params.recorridoSentidoLocalidadDestinoActivo.toString());
    if (params.recorridoSentidoItinerarioNe != null) __params = __params.set('recorridoSentido-itinerario[ne]', params.recorridoSentidoItinerarioNe.toString());
    if (params.recorridoSentidoItinerarioLike != null) __params = __params.set('recorridoSentido-itinerario[like]', params.recorridoSentidoItinerarioLike.toString());
    if (params.recorridoSentidoItinerario != null) __params = __params.set('recorridoSentido-itinerario', params.recorridoSentidoItinerario.toString());
    if (params.recorridoSentidoIdNe != null) __params = __params.set('recorridoSentido-id[ne]', params.recorridoSentidoIdNe.toString());
    if (params.recorridoSentidoIdLt != null) __params = __params.set('recorridoSentido-id[lt]', params.recorridoSentidoIdLt.toString());
    if (params.recorridoSentidoIdLe != null) __params = __params.set('recorridoSentido-id[le]', params.recorridoSentidoIdLe.toString());
    if (params.recorridoSentidoIdGt != null) __params = __params.set('recorridoSentido-id[gt]', params.recorridoSentidoIdGt.toString());
    if (params.recorridoSentidoIdGe != null) __params = __params.set('recorridoSentido-id[ge]', params.recorridoSentidoIdGe.toString());
    if (params.recorridoSentidoId != null) __params = __params.set('recorridoSentido-id', params.recorridoSentidoId.toString());
    if (params.recorridoSentidoDestinoNe != null) __params = __params.set('recorridoSentido-destino[ne]', params.recorridoSentidoDestinoNe.toString());
    if (params.recorridoSentidoDestinoLike != null) __params = __params.set('recorridoSentido-destino[like]', params.recorridoSentidoDestinoLike.toString());
    if (params.recorridoSentidoDestino != null) __params = __params.set('recorridoSentido-destino', params.recorridoSentidoDestino.toString());
    if (params.recorridoVinculacionCamineraNe != null) __params = __params.set('recorrido-vinculacionCaminera[ne]', params.recorridoVinculacionCamineraNe.toString());
    if (params.recorridoVinculacionCamineraLike != null) __params = __params.set('recorrido-vinculacionCaminera[like]', params.recorridoVinculacionCamineraLike.toString());
    if (params.recorridoVinculacionCaminera != null) __params = __params.set('recorrido-vinculacionCaminera', params.recorridoVinculacionCaminera.toString());
    if (params.recorridoVigenciaHastaNe != null) __params = __params.set('recorrido-vigenciaHasta[ne]', params.recorridoVigenciaHastaNe.toString());
    if (params.recorridoVigenciaHastaLt != null) __params = __params.set('recorrido-vigenciaHasta[lt]', params.recorridoVigenciaHastaLt.toString());
    if (params.recorridoVigenciaHastaLe != null) __params = __params.set('recorrido-vigenciaHasta[le]', params.recorridoVigenciaHastaLe.toString());
    if (params.recorridoVigenciaHastaGt != null) __params = __params.set('recorrido-vigenciaHasta[gt]', params.recorridoVigenciaHastaGt.toString());
    if (params.recorridoVigenciaHastaGe != null) __params = __params.set('recorrido-vigenciaHasta[ge]', params.recorridoVigenciaHastaGe.toString());
    if (params.recorridoVigenciaHasta != null) __params = __params.set('recorrido-vigenciaHasta', params.recorridoVigenciaHasta.toString());
    if (params.recorridoVigenciaDesdeNe != null) __params = __params.set('recorrido-vigenciaDesde[ne]', params.recorridoVigenciaDesdeNe.toString());
    if (params.recorridoVigenciaDesdeLt != null) __params = __params.set('recorrido-vigenciaDesde[lt]', params.recorridoVigenciaDesdeLt.toString());
    if (params.recorridoVigenciaDesdeLe != null) __params = __params.set('recorrido-vigenciaDesde[le]', params.recorridoVigenciaDesdeLe.toString());
    if (params.recorridoVigenciaDesdeGt != null) __params = __params.set('recorrido-vigenciaDesde[gt]', params.recorridoVigenciaDesdeGt.toString());
    if (params.recorridoVigenciaDesdeGe != null) __params = __params.set('recorrido-vigenciaDesde[ge]', params.recorridoVigenciaDesdeGe.toString());
    if (params.recorridoVigenciaDesde != null) __params = __params.set('recorrido-vigenciaDesde', params.recorridoVigenciaDesde.toString());
    if (params.recorridoTransitoNe != null) __params = __params.set('recorrido-transito[ne]', params.recorridoTransitoNe.toString());
    if (params.recorridoTransitoLike != null) __params = __params.set('recorrido-transito[like]', params.recorridoTransitoLike.toString());
    if (params.recorridoTransito != null) __params = __params.set('recorrido-transito', params.recorridoTransito.toString());
    if (params.recorridoSitioOrigenNe != null) __params = __params.set('recorrido-sitioOrigen[ne]', params.recorridoSitioOrigenNe.toString());
    if (params.recorridoSitioOrigenLike != null) __params = __params.set('recorrido-sitioOrigen[like]', params.recorridoSitioOrigenLike.toString());
    if (params.recorridoSitioOrigen != null) __params = __params.set('recorrido-sitioOrigen', params.recorridoSitioOrigen.toString());
    if (params.recorridoSitioDestinoNe != null) __params = __params.set('recorrido-sitioDestino[ne]', params.recorridoSitioDestinoNe.toString());
    if (params.recorridoSitioDestinoLike != null) __params = __params.set('recorrido-sitioDestino[like]', params.recorridoSitioDestinoLike.toString());
    if (params.recorridoSitioDestino != null) __params = __params.set('recorrido-sitioDestino', params.recorridoSitioDestino.toString());
    if (params.recorridoRecorridoNe != null) __params = __params.set('recorrido-recorrido[ne]', params.recorridoRecorridoNe.toString());
    if (params.recorridoRecorridoLike != null) __params = __params.set('recorrido-recorrido[like]', params.recorridoRecorridoLike.toString());
    if (params.recorridoRecorrido != null) __params = __params.set('recorrido-recorrido', params.recorridoRecorrido.toString());
    if (params.recorridoOrigenNe != null) __params = __params.set('recorrido-origen[ne]', params.recorridoOrigenNe.toString());
    if (params.recorridoOrigenLike != null) __params = __params.set('recorrido-origen[like]', params.recorridoOrigenLike.toString());
    if (params.recorridoOrigen != null) __params = __params.set('recorrido-origen', params.recorridoOrigen.toString());
    if (params.recorridoItinerarioVueltaNe != null) __params = __params.set('recorrido-itinerarioVuelta[ne]', params.recorridoItinerarioVueltaNe.toString());
    if (params.recorridoItinerarioVueltaLike != null) __params = __params.set('recorrido-itinerarioVuelta[like]', params.recorridoItinerarioVueltaLike.toString());
    if (params.recorridoItinerarioVuelta != null) __params = __params.set('recorrido-itinerarioVuelta', params.recorridoItinerarioVuelta.toString());
    if (params.recorridoItinerarioIdaNe != null) __params = __params.set('recorrido-itinerarioIda[ne]', params.recorridoItinerarioIdaNe.toString());
    if (params.recorridoItinerarioIdaLike != null) __params = __params.set('recorrido-itinerarioIda[like]', params.recorridoItinerarioIdaLike.toString());
    if (params.recorridoItinerarioIda != null) __params = __params.set('recorrido-itinerarioIda', params.recorridoItinerarioIda.toString());
    if (params.recorridoIdNe != null) __params = __params.set('recorrido-id[ne]', params.recorridoIdNe.toString());
    if (params.recorridoIdLt != null) __params = __params.set('recorrido-id[lt]', params.recorridoIdLt.toString());
    if (params.recorridoIdLe != null) __params = __params.set('recorrido-id[le]', params.recorridoIdLe.toString());
    if (params.recorridoIdGt != null) __params = __params.set('recorrido-id[gt]', params.recorridoIdGt.toString());
    if (params.recorridoIdGe != null) __params = __params.set('recorrido-id[ge]', params.recorridoIdGe.toString());
    if (params.recorridoId != null) __params = __params.set('recorrido-id', params.recorridoId.toString());
    if (params.recorridoDestinoNe != null) __params = __params.set('recorrido-destino[ne]', params.recorridoDestinoNe.toString());
    if (params.recorridoDestinoLike != null) __params = __params.set('recorrido-destino[like]', params.recorridoDestinoLike.toString());
    if (params.recorridoDestino != null) __params = __params.set('recorrido-destino', params.recorridoDestino.toString());
    if (params.recorridoAplicaGasoil != null) __params = __params.set('recorrido-aplicaGasoil', params.recorridoAplicaGasoil.toString());
    if (params.order != null) __params = __params.set('order', params.order.toString());
    if (params.operadorVigenciaHastaNe != null) __params = __params.set('operador-vigenciaHasta[ne]', params.operadorVigenciaHastaNe.toString());
    if (params.operadorVigenciaHastaLt != null) __params = __params.set('operador-vigenciaHasta[lt]', params.operadorVigenciaHastaLt.toString());
    if (params.operadorVigenciaHastaLe != null) __params = __params.set('operador-vigenciaHasta[le]', params.operadorVigenciaHastaLe.toString());
    if (params.operadorVigenciaHastaGt != null) __params = __params.set('operador-vigenciaHasta[gt]', params.operadorVigenciaHastaGt.toString());
    if (params.operadorVigenciaHastaGe != null) __params = __params.set('operador-vigenciaHasta[ge]', params.operadorVigenciaHastaGe.toString());
    if (params.operadorVigenciaHasta != null) __params = __params.set('operador-vigenciaHasta', params.operadorVigenciaHasta.toString());
    if (params.operadorVigenciaDesdeNe != null) __params = __params.set('operador-vigenciaDesde[ne]', params.operadorVigenciaDesdeNe.toString());
    if (params.operadorVigenciaDesdeLt != null) __params = __params.set('operador-vigenciaDesde[lt]', params.operadorVigenciaDesdeLt.toString());
    if (params.operadorVigenciaDesdeLe != null) __params = __params.set('operador-vigenciaDesde[le]', params.operadorVigenciaDesdeLe.toString());
    if (params.operadorVigenciaDesdeGt != null) __params = __params.set('operador-vigenciaDesde[gt]', params.operadorVigenciaDesdeGt.toString());
    if (params.operadorVigenciaDesdeGe != null) __params = __params.set('operador-vigenciaDesde[ge]', params.operadorVigenciaDesdeGe.toString());
    if (params.operadorVigenciaDesde != null) __params = __params.set('operador-vigenciaDesde', params.operadorVigenciaDesde.toString());
    if (params.operadorLocalidadOrigenUpdatedAtNe != null) __params = __params.set('operador-localidadOrigen-updatedAt[ne]', params.operadorLocalidadOrigenUpdatedAtNe.toString());
    if (params.operadorLocalidadOrigenUpdatedAtLt != null) __params = __params.set('operador-localidadOrigen-updatedAt[lt]', params.operadorLocalidadOrigenUpdatedAtLt.toString());
    if (params.operadorLocalidadOrigenUpdatedAtLe != null) __params = __params.set('operador-localidadOrigen-updatedAt[le]', params.operadorLocalidadOrigenUpdatedAtLe.toString());
    if (params.operadorLocalidadOrigenUpdatedAtGt != null) __params = __params.set('operador-localidadOrigen-updatedAt[gt]', params.operadorLocalidadOrigenUpdatedAtGt.toString());
    if (params.operadorLocalidadOrigenUpdatedAtGe != null) __params = __params.set('operador-localidadOrigen-updatedAt[ge]', params.operadorLocalidadOrigenUpdatedAtGe.toString());
    if (params.operadorLocalidadOrigenUpdatedAt != null) __params = __params.set('operador-localidadOrigen-updatedAt', params.operadorLocalidadOrigenUpdatedAt.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisIdNe != null) __params = __params.set('operador-localidadOrigen-provincia-pais-id[ne]', params.operadorLocalidadOrigenProvinciaPaisIdNe.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisIdLt != null) __params = __params.set('operador-localidadOrigen-provincia-pais-id[lt]', params.operadorLocalidadOrigenProvinciaPaisIdLt.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisIdLe != null) __params = __params.set('operador-localidadOrigen-provincia-pais-id[le]', params.operadorLocalidadOrigenProvinciaPaisIdLe.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisIdGt != null) __params = __params.set('operador-localidadOrigen-provincia-pais-id[gt]', params.operadorLocalidadOrigenProvinciaPaisIdGt.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisIdGe != null) __params = __params.set('operador-localidadOrigen-provincia-pais-id[ge]', params.operadorLocalidadOrigenProvinciaPaisIdGe.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisId != null) __params = __params.set('operador-localidadOrigen-provincia-pais-id', params.operadorLocalidadOrigenProvinciaPaisId.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisDescripcionNe != null) __params = __params.set('operador-localidadOrigen-provincia-pais-descripcion[ne]', params.operadorLocalidadOrigenProvinciaPaisDescripcionNe.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisDescripcionLike != null) __params = __params.set('operador-localidadOrigen-provincia-pais-descripcion[like]', params.operadorLocalidadOrigenProvinciaPaisDescripcionLike.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisDescripcion != null) __params = __params.set('operador-localidadOrigen-provincia-pais-descripcion', params.operadorLocalidadOrigenProvinciaPaisDescripcion.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisCodigoNe != null) __params = __params.set('operador-localidadOrigen-provincia-pais-codigo[ne]', params.operadorLocalidadOrigenProvinciaPaisCodigoNe.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisCodigoLike != null) __params = __params.set('operador-localidadOrigen-provincia-pais-codigo[like]', params.operadorLocalidadOrigenProvinciaPaisCodigoLike.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisCodigo != null) __params = __params.set('operador-localidadOrigen-provincia-pais-codigo', params.operadorLocalidadOrigenProvinciaPaisCodigo.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisActivo != null) __params = __params.set('operador-localidadOrigen-provincia-pais-activo', params.operadorLocalidadOrigenProvinciaPaisActivo.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisAbrevNe != null) __params = __params.set('operador-localidadOrigen-provincia-pais-abrev[ne]', params.operadorLocalidadOrigenProvinciaPaisAbrevNe.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisAbrevLike != null) __params = __params.set('operador-localidadOrigen-provincia-pais-abrev[like]', params.operadorLocalidadOrigenProvinciaPaisAbrevLike.toString());
    if (params.operadorLocalidadOrigenProvinciaPaisAbrev != null) __params = __params.set('operador-localidadOrigen-provincia-pais-abrev', params.operadorLocalidadOrigenProvinciaPaisAbrev.toString());
    if (params.operadorLocalidadOrigenProvinciaIdNe != null) __params = __params.set('operador-localidadOrigen-provincia-id[ne]', params.operadorLocalidadOrigenProvinciaIdNe.toString());
    if (params.operadorLocalidadOrigenProvinciaIdLt != null) __params = __params.set('operador-localidadOrigen-provincia-id[lt]', params.operadorLocalidadOrigenProvinciaIdLt.toString());
    if (params.operadorLocalidadOrigenProvinciaIdLe != null) __params = __params.set('operador-localidadOrigen-provincia-id[le]', params.operadorLocalidadOrigenProvinciaIdLe.toString());
    if (params.operadorLocalidadOrigenProvinciaIdGt != null) __params = __params.set('operador-localidadOrigen-provincia-id[gt]', params.operadorLocalidadOrigenProvinciaIdGt.toString());
    if (params.operadorLocalidadOrigenProvinciaIdGe != null) __params = __params.set('operador-localidadOrigen-provincia-id[ge]', params.operadorLocalidadOrigenProvinciaIdGe.toString());
    if (params.operadorLocalidadOrigenProvinciaId != null) __params = __params.set('operador-localidadOrigen-provincia-id', params.operadorLocalidadOrigenProvinciaId.toString());
    if (params.operadorLocalidadOrigenProvinciaDescripcionNe != null) __params = __params.set('operador-localidadOrigen-provincia-descripcion[ne]', params.operadorLocalidadOrigenProvinciaDescripcionNe.toString());
    if (params.operadorLocalidadOrigenProvinciaDescripcionLike != null) __params = __params.set('operador-localidadOrigen-provincia-descripcion[like]', params.operadorLocalidadOrigenProvinciaDescripcionLike.toString());
    if (params.operadorLocalidadOrigenProvinciaDescripcion != null) __params = __params.set('operador-localidadOrigen-provincia-descripcion', params.operadorLocalidadOrigenProvinciaDescripcion.toString());
    if (params.operadorLocalidadOrigenProvinciaCodigoNe != null) __params = __params.set('operador-localidadOrigen-provincia-codigo[ne]', params.operadorLocalidadOrigenProvinciaCodigoNe.toString());
    if (params.operadorLocalidadOrigenProvinciaCodigoLike != null) __params = __params.set('operador-localidadOrigen-provincia-codigo[like]', params.operadorLocalidadOrigenProvinciaCodigoLike.toString());
    if (params.operadorLocalidadOrigenProvinciaCodigoINDECNe != null) __params = __params.set('operador-localidadOrigen-provincia-codigoINDEC[ne]', params.operadorLocalidadOrigenProvinciaCodigoINDECNe.toString());
    if (params.operadorLocalidadOrigenProvinciaCodigoINDECLike != null) __params = __params.set('operador-localidadOrigen-provincia-codigoINDEC[like]', params.operadorLocalidadOrigenProvinciaCodigoINDECLike.toString());
    if (params.operadorLocalidadOrigenProvinciaCodigoINDEC != null) __params = __params.set('operador-localidadOrigen-provincia-codigoINDEC', params.operadorLocalidadOrigenProvinciaCodigoINDEC.toString());
    if (params.operadorLocalidadOrigenProvinciaCodigo != null) __params = __params.set('operador-localidadOrigen-provincia-codigo', params.operadorLocalidadOrigenProvinciaCodigo.toString());
    if (params.operadorLocalidadOrigenProvinciaActivo != null) __params = __params.set('operador-localidadOrigen-provincia-activo', params.operadorLocalidadOrigenProvinciaActivo.toString());
    if (params.operadorLocalidadOrigenParada != null) __params = __params.set('operador-localidadOrigen-parada', params.operadorLocalidadOrigenParada.toString());
    if (params.operadorLocalidadOrigenLonNe != null) __params = __params.set('operador-localidadOrigen-lon[ne]', params.operadorLocalidadOrigenLonNe.toString());
    if (params.operadorLocalidadOrigenLonLt != null) __params = __params.set('operador-localidadOrigen-lon[lt]', params.operadorLocalidadOrigenLonLt.toString());
    if (params.operadorLocalidadOrigenLonLe != null) __params = __params.set('operador-localidadOrigen-lon[le]', params.operadorLocalidadOrigenLonLe.toString());
    if (params.operadorLocalidadOrigenLonGt != null) __params = __params.set('operador-localidadOrigen-lon[gt]', params.operadorLocalidadOrigenLonGt.toString());
    if (params.operadorLocalidadOrigenLonGe != null) __params = __params.set('operador-localidadOrigen-lon[ge]', params.operadorLocalidadOrigenLonGe.toString());
    if (params.operadorLocalidadOrigenLon != null) __params = __params.set('operador-localidadOrigen-lon', params.operadorLocalidadOrigenLon.toString());
    if (params.operadorLocalidadOrigenLatNe != null) __params = __params.set('operador-localidadOrigen-lat[ne]', params.operadorLocalidadOrigenLatNe.toString());
    if (params.operadorLocalidadOrigenLatLt != null) __params = __params.set('operador-localidadOrigen-lat[lt]', params.operadorLocalidadOrigenLatLt.toString());
    if (params.operadorLocalidadOrigenLatLe != null) __params = __params.set('operador-localidadOrigen-lat[le]', params.operadorLocalidadOrigenLatLe.toString());
    if (params.operadorLocalidadOrigenLatGt != null) __params = __params.set('operador-localidadOrigen-lat[gt]', params.operadorLocalidadOrigenLatGt.toString());
    if (params.operadorLocalidadOrigenLatGe != null) __params = __params.set('operador-localidadOrigen-lat[ge]', params.operadorLocalidadOrigenLatGe.toString());
    if (params.operadorLocalidadOrigenLat != null) __params = __params.set('operador-localidadOrigen-lat', params.operadorLocalidadOrigenLat.toString());
    if (params.operadorLocalidadOrigenIdNe != null) __params = __params.set('operador-localidadOrigen-id[ne]', params.operadorLocalidadOrigenIdNe.toString());
    if (params.operadorLocalidadOrigenIdLt != null) __params = __params.set('operador-localidadOrigen-id[lt]', params.operadorLocalidadOrigenIdLt.toString());
    if (params.operadorLocalidadOrigenIdLe != null) __params = __params.set('operador-localidadOrigen-id[le]', params.operadorLocalidadOrigenIdLe.toString());
    if (params.operadorLocalidadOrigenIdGt != null) __params = __params.set('operador-localidadOrigen-id[gt]', params.operadorLocalidadOrigenIdGt.toString());
    if (params.operadorLocalidadOrigenIdGe != null) __params = __params.set('operador-localidadOrigen-id[ge]', params.operadorLocalidadOrigenIdGe.toString());
    if (params.operadorLocalidadOrigenId != null) __params = __params.set('operador-localidadOrigen-id', params.operadorLocalidadOrigenId.toString());
    if (params.operadorLocalidadOrigenFuenteNe != null) __params = __params.set('operador-localidadOrigen-fuente[ne]', params.operadorLocalidadOrigenFuenteNe.toString());
    if (params.operadorLocalidadOrigenFuenteLike != null) __params = __params.set('operador-localidadOrigen-fuente[like]', params.operadorLocalidadOrigenFuenteLike.toString());
    if (params.operadorLocalidadOrigenFuente != null) __params = __params.set('operador-localidadOrigen-fuente', params.operadorLocalidadOrigenFuente.toString());
    if (params.operadorLocalidadOrigenDescripcionNe != null) __params = __params.set('operador-localidadOrigen-descripcion[ne]', params.operadorLocalidadOrigenDescripcionNe.toString());
    if (params.operadorLocalidadOrigenDescripcionLike != null) __params = __params.set('operador-localidadOrigen-descripcion[like]', params.operadorLocalidadOrigenDescripcionLike.toString());
    if (params.operadorLocalidadOrigenDescripcion != null) __params = __params.set('operador-localidadOrigen-descripcion', params.operadorLocalidadOrigenDescripcion.toString());
    if (params.operadorLocalidadOrigenCreatedAtNe != null) __params = __params.set('operador-localidadOrigen-createdAt[ne]', params.operadorLocalidadOrigenCreatedAtNe.toString());
    if (params.operadorLocalidadOrigenCreatedAtLt != null) __params = __params.set('operador-localidadOrigen-createdAt[lt]', params.operadorLocalidadOrigenCreatedAtLt.toString());
    if (params.operadorLocalidadOrigenCreatedAtLe != null) __params = __params.set('operador-localidadOrigen-createdAt[le]', params.operadorLocalidadOrigenCreatedAtLe.toString());
    if (params.operadorLocalidadOrigenCreatedAtGt != null) __params = __params.set('operador-localidadOrigen-createdAt[gt]', params.operadorLocalidadOrigenCreatedAtGt.toString());
    if (params.operadorLocalidadOrigenCreatedAtGe != null) __params = __params.set('operador-localidadOrigen-createdAt[ge]', params.operadorLocalidadOrigenCreatedAtGe.toString());
    if (params.operadorLocalidadOrigenCreatedAt != null) __params = __params.set('operador-localidadOrigen-createdAt', params.operadorLocalidadOrigenCreatedAt.toString());
    if (params.operadorLocalidadOrigenCodigoINDECNe != null) __params = __params.set('operador-localidadOrigen-codigoINDEC[ne]', params.operadorLocalidadOrigenCodigoINDECNe.toString());
    if (params.operadorLocalidadOrigenCodigoINDECLike != null) __params = __params.set('operador-localidadOrigen-codigoINDEC[like]', params.operadorLocalidadOrigenCodigoINDECLike.toString());
    if (params.operadorLocalidadOrigenCodigoINDEC != null) __params = __params.set('operador-localidadOrigen-codigoINDEC', params.operadorLocalidadOrigenCodigoINDEC.toString());
    if (params.operadorLocalidadOrigenActivo != null) __params = __params.set('operador-localidadOrigen-activo', params.operadorLocalidadOrigenActivo.toString());
    if (params.operadorLocalidadDestinoUpdatedAtNe != null) __params = __params.set('operador-localidadDestino-updatedAt[ne]', params.operadorLocalidadDestinoUpdatedAtNe.toString());
    if (params.operadorLocalidadDestinoUpdatedAtLt != null) __params = __params.set('operador-localidadDestino-updatedAt[lt]', params.operadorLocalidadDestinoUpdatedAtLt.toString());
    if (params.operadorLocalidadDestinoUpdatedAtLe != null) __params = __params.set('operador-localidadDestino-updatedAt[le]', params.operadorLocalidadDestinoUpdatedAtLe.toString());
    if (params.operadorLocalidadDestinoUpdatedAtGt != null) __params = __params.set('operador-localidadDestino-updatedAt[gt]', params.operadorLocalidadDestinoUpdatedAtGt.toString());
    if (params.operadorLocalidadDestinoUpdatedAtGe != null) __params = __params.set('operador-localidadDestino-updatedAt[ge]', params.operadorLocalidadDestinoUpdatedAtGe.toString());
    if (params.operadorLocalidadDestinoUpdatedAt != null) __params = __params.set('operador-localidadDestino-updatedAt', params.operadorLocalidadDestinoUpdatedAt.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisIdNe != null) __params = __params.set('operador-localidadDestino-provincia-pais-id[ne]', params.operadorLocalidadDestinoProvinciaPaisIdNe.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisIdLt != null) __params = __params.set('operador-localidadDestino-provincia-pais-id[lt]', params.operadorLocalidadDestinoProvinciaPaisIdLt.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisIdLe != null) __params = __params.set('operador-localidadDestino-provincia-pais-id[le]', params.operadorLocalidadDestinoProvinciaPaisIdLe.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisIdGt != null) __params = __params.set('operador-localidadDestino-provincia-pais-id[gt]', params.operadorLocalidadDestinoProvinciaPaisIdGt.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisIdGe != null) __params = __params.set('operador-localidadDestino-provincia-pais-id[ge]', params.operadorLocalidadDestinoProvinciaPaisIdGe.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisId != null) __params = __params.set('operador-localidadDestino-provincia-pais-id', params.operadorLocalidadDestinoProvinciaPaisId.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisDescripcionNe != null) __params = __params.set('operador-localidadDestino-provincia-pais-descripcion[ne]', params.operadorLocalidadDestinoProvinciaPaisDescripcionNe.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisDescripcionLike != null) __params = __params.set('operador-localidadDestino-provincia-pais-descripcion[like]', params.operadorLocalidadDestinoProvinciaPaisDescripcionLike.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisDescripcion != null) __params = __params.set('operador-localidadDestino-provincia-pais-descripcion', params.operadorLocalidadDestinoProvinciaPaisDescripcion.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisCodigoNe != null) __params = __params.set('operador-localidadDestino-provincia-pais-codigo[ne]', params.operadorLocalidadDestinoProvinciaPaisCodigoNe.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisCodigoLike != null) __params = __params.set('operador-localidadDestino-provincia-pais-codigo[like]', params.operadorLocalidadDestinoProvinciaPaisCodigoLike.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisCodigo != null) __params = __params.set('operador-localidadDestino-provincia-pais-codigo', params.operadorLocalidadDestinoProvinciaPaisCodigo.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisActivo != null) __params = __params.set('operador-localidadDestino-provincia-pais-activo', params.operadorLocalidadDestinoProvinciaPaisActivo.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisAbrevNe != null) __params = __params.set('operador-localidadDestino-provincia-pais-abrev[ne]', params.operadorLocalidadDestinoProvinciaPaisAbrevNe.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisAbrevLike != null) __params = __params.set('operador-localidadDestino-provincia-pais-abrev[like]', params.operadorLocalidadDestinoProvinciaPaisAbrevLike.toString());
    if (params.operadorLocalidadDestinoProvinciaPaisAbrev != null) __params = __params.set('operador-localidadDestino-provincia-pais-abrev', params.operadorLocalidadDestinoProvinciaPaisAbrev.toString());
    if (params.operadorLocalidadDestinoProvinciaIdNe != null) __params = __params.set('operador-localidadDestino-provincia-id[ne]', params.operadorLocalidadDestinoProvinciaIdNe.toString());
    if (params.operadorLocalidadDestinoProvinciaIdLt != null) __params = __params.set('operador-localidadDestino-provincia-id[lt]', params.operadorLocalidadDestinoProvinciaIdLt.toString());
    if (params.operadorLocalidadDestinoProvinciaIdLe != null) __params = __params.set('operador-localidadDestino-provincia-id[le]', params.operadorLocalidadDestinoProvinciaIdLe.toString());
    if (params.operadorLocalidadDestinoProvinciaIdGt != null) __params = __params.set('operador-localidadDestino-provincia-id[gt]', params.operadorLocalidadDestinoProvinciaIdGt.toString());
    if (params.operadorLocalidadDestinoProvinciaIdGe != null) __params = __params.set('operador-localidadDestino-provincia-id[ge]', params.operadorLocalidadDestinoProvinciaIdGe.toString());
    if (params.operadorLocalidadDestinoProvinciaId != null) __params = __params.set('operador-localidadDestino-provincia-id', params.operadorLocalidadDestinoProvinciaId.toString());
    if (params.operadorLocalidadDestinoProvinciaDescripcionNe != null) __params = __params.set('operador-localidadDestino-provincia-descripcion[ne]', params.operadorLocalidadDestinoProvinciaDescripcionNe.toString());
    if (params.operadorLocalidadDestinoProvinciaDescripcionLike != null) __params = __params.set('operador-localidadDestino-provincia-descripcion[like]', params.operadorLocalidadDestinoProvinciaDescripcionLike.toString());
    if (params.operadorLocalidadDestinoProvinciaDescripcion != null) __params = __params.set('operador-localidadDestino-provincia-descripcion', params.operadorLocalidadDestinoProvinciaDescripcion.toString());
    if (params.operadorLocalidadDestinoProvinciaCodigoNe != null) __params = __params.set('operador-localidadDestino-provincia-codigo[ne]', params.operadorLocalidadDestinoProvinciaCodigoNe.toString());
    if (params.operadorLocalidadDestinoProvinciaCodigoLike != null) __params = __params.set('operador-localidadDestino-provincia-codigo[like]', params.operadorLocalidadDestinoProvinciaCodigoLike.toString());
    if (params.operadorLocalidadDestinoProvinciaCodigoINDECNe != null) __params = __params.set('operador-localidadDestino-provincia-codigoINDEC[ne]', params.operadorLocalidadDestinoProvinciaCodigoINDECNe.toString());
    if (params.operadorLocalidadDestinoProvinciaCodigoINDECLike != null) __params = __params.set('operador-localidadDestino-provincia-codigoINDEC[like]', params.operadorLocalidadDestinoProvinciaCodigoINDECLike.toString());
    if (params.operadorLocalidadDestinoProvinciaCodigoINDEC != null) __params = __params.set('operador-localidadDestino-provincia-codigoINDEC', params.operadorLocalidadDestinoProvinciaCodigoINDEC.toString());
    if (params.operadorLocalidadDestinoProvinciaCodigo != null) __params = __params.set('operador-localidadDestino-provincia-codigo', params.operadorLocalidadDestinoProvinciaCodigo.toString());
    if (params.operadorLocalidadDestinoProvinciaActivo != null) __params = __params.set('operador-localidadDestino-provincia-activo', params.operadorLocalidadDestinoProvinciaActivo.toString());
    if (params.operadorLocalidadDestinoParada != null) __params = __params.set('operador-localidadDestino-parada', params.operadorLocalidadDestinoParada.toString());
    if (params.operadorLocalidadDestinoLonNe != null) __params = __params.set('operador-localidadDestino-lon[ne]', params.operadorLocalidadDestinoLonNe.toString());
    if (params.operadorLocalidadDestinoLonLt != null) __params = __params.set('operador-localidadDestino-lon[lt]', params.operadorLocalidadDestinoLonLt.toString());
    if (params.operadorLocalidadDestinoLonLe != null) __params = __params.set('operador-localidadDestino-lon[le]', params.operadorLocalidadDestinoLonLe.toString());
    if (params.operadorLocalidadDestinoLonGt != null) __params = __params.set('operador-localidadDestino-lon[gt]', params.operadorLocalidadDestinoLonGt.toString());
    if (params.operadorLocalidadDestinoLonGe != null) __params = __params.set('operador-localidadDestino-lon[ge]', params.operadorLocalidadDestinoLonGe.toString());
    if (params.operadorLocalidadDestinoLon != null) __params = __params.set('operador-localidadDestino-lon', params.operadorLocalidadDestinoLon.toString());
    if (params.operadorLocalidadDestinoLatNe != null) __params = __params.set('operador-localidadDestino-lat[ne]', params.operadorLocalidadDestinoLatNe.toString());
    if (params.operadorLocalidadDestinoLatLt != null) __params = __params.set('operador-localidadDestino-lat[lt]', params.operadorLocalidadDestinoLatLt.toString());
    if (params.operadorLocalidadDestinoLatLe != null) __params = __params.set('operador-localidadDestino-lat[le]', params.operadorLocalidadDestinoLatLe.toString());
    if (params.operadorLocalidadDestinoLatGt != null) __params = __params.set('operador-localidadDestino-lat[gt]', params.operadorLocalidadDestinoLatGt.toString());
    if (params.operadorLocalidadDestinoLatGe != null) __params = __params.set('operador-localidadDestino-lat[ge]', params.operadorLocalidadDestinoLatGe.toString());
    if (params.operadorLocalidadDestinoLat != null) __params = __params.set('operador-localidadDestino-lat', params.operadorLocalidadDestinoLat.toString());
    if (params.operadorLocalidadDestinoIdNe != null) __params = __params.set('operador-localidadDestino-id[ne]', params.operadorLocalidadDestinoIdNe.toString());
    if (params.operadorLocalidadDestinoIdLt != null) __params = __params.set('operador-localidadDestino-id[lt]', params.operadorLocalidadDestinoIdLt.toString());
    if (params.operadorLocalidadDestinoIdLe != null) __params = __params.set('operador-localidadDestino-id[le]', params.operadorLocalidadDestinoIdLe.toString());
    if (params.operadorLocalidadDestinoIdGt != null) __params = __params.set('operador-localidadDestino-id[gt]', params.operadorLocalidadDestinoIdGt.toString());
    if (params.operadorLocalidadDestinoIdGe != null) __params = __params.set('operador-localidadDestino-id[ge]', params.operadorLocalidadDestinoIdGe.toString());
    if (params.operadorLocalidadDestinoId != null) __params = __params.set('operador-localidadDestino-id', params.operadorLocalidadDestinoId.toString());
    if (params.operadorLocalidadDestinoFuenteNe != null) __params = __params.set('operador-localidadDestino-fuente[ne]', params.operadorLocalidadDestinoFuenteNe.toString());
    if (params.operadorLocalidadDestinoFuenteLike != null) __params = __params.set('operador-localidadDestino-fuente[like]', params.operadorLocalidadDestinoFuenteLike.toString());
    if (params.operadorLocalidadDestinoFuente != null) __params = __params.set('operador-localidadDestino-fuente', params.operadorLocalidadDestinoFuente.toString());
    if (params.operadorLocalidadDestinoDescripcionNe != null) __params = __params.set('operador-localidadDestino-descripcion[ne]', params.operadorLocalidadDestinoDescripcionNe.toString());
    if (params.operadorLocalidadDestinoDescripcionLike != null) __params = __params.set('operador-localidadDestino-descripcion[like]', params.operadorLocalidadDestinoDescripcionLike.toString());
    if (params.operadorLocalidadDestinoDescripcion != null) __params = __params.set('operador-localidadDestino-descripcion', params.operadorLocalidadDestinoDescripcion.toString());
    if (params.operadorLocalidadDestinoCreatedAtNe != null) __params = __params.set('operador-localidadDestino-createdAt[ne]', params.operadorLocalidadDestinoCreatedAtNe.toString());
    if (params.operadorLocalidadDestinoCreatedAtLt != null) __params = __params.set('operador-localidadDestino-createdAt[lt]', params.operadorLocalidadDestinoCreatedAtLt.toString());
    if (params.operadorLocalidadDestinoCreatedAtLe != null) __params = __params.set('operador-localidadDestino-createdAt[le]', params.operadorLocalidadDestinoCreatedAtLe.toString());
    if (params.operadorLocalidadDestinoCreatedAtGt != null) __params = __params.set('operador-localidadDestino-createdAt[gt]', params.operadorLocalidadDestinoCreatedAtGt.toString());
    if (params.operadorLocalidadDestinoCreatedAtGe != null) __params = __params.set('operador-localidadDestino-createdAt[ge]', params.operadorLocalidadDestinoCreatedAtGe.toString());
    if (params.operadorLocalidadDestinoCreatedAt != null) __params = __params.set('operador-localidadDestino-createdAt', params.operadorLocalidadDestinoCreatedAt.toString());
    if (params.operadorLocalidadDestinoCodigoINDECNe != null) __params = __params.set('operador-localidadDestino-codigoINDEC[ne]', params.operadorLocalidadDestinoCodigoINDECNe.toString());
    if (params.operadorLocalidadDestinoCodigoINDECLike != null) __params = __params.set('operador-localidadDestino-codigoINDEC[like]', params.operadorLocalidadDestinoCodigoINDECLike.toString());
    if (params.operadorLocalidadDestinoCodigoINDEC != null) __params = __params.set('operador-localidadDestino-codigoINDEC', params.operadorLocalidadDestinoCodigoINDEC.toString());
    if (params.operadorLocalidadDestinoActivo != null) __params = __params.set('operador-localidadDestino-activo', params.operadorLocalidadDestinoActivo.toString());
    if (params.operadorLineaNe != null) __params = __params.set('operador-linea[ne]', params.operadorLineaNe.toString());
    if (params.operadorLineaLike != null) __params = __params.set('operador-linea[like]', params.operadorLineaLike.toString());
    if (params.operadorLinea != null) __params = __params.set('operador-linea', params.operadorLinea.toString());
    if (params.operadorIdNe != null) __params = __params.set('operador-id[ne]', params.operadorIdNe.toString());
    if (params.operadorIdLt != null) __params = __params.set('operador-id[lt]', params.operadorIdLt.toString());
    if (params.operadorIdLe != null) __params = __params.set('operador-id[le]', params.operadorIdLe.toString());
    if (params.operadorIdGt != null) __params = __params.set('operador-id[gt]', params.operadorIdGt.toString());
    if (params.operadorIdGe != null) __params = __params.set('operador-id[ge]', params.operadorIdGe.toString());
    if (params.operadorId != null) __params = __params.set('operador-id', params.operadorId.toString());
    if (params.operadorEmpresaContratanteNe != null) __params = __params.set('operador-empresaContratante[ne]', params.operadorEmpresaContratanteNe.toString());
    if (params.operadorEmpresaContratanteLike != null) __params = __params.set('operador-empresaContratante[like]', params.operadorEmpresaContratanteLike.toString());
    if (params.operadorEmpresaContratanteCuitNe != null) __params = __params.set('operador-empresaContratanteCuit[ne]', params.operadorEmpresaContratanteCuitNe.toString());
    if (params.operadorEmpresaContratanteCuitLike != null) __params = __params.set('operador-empresaContratanteCuit[like]', params.operadorEmpresaContratanteCuitLike.toString());
    if (params.operadorEmpresaContratanteCuit != null) __params = __params.set('operador-empresaContratanteCuit', params.operadorEmpresaContratanteCuit.toString());
    if (params.operadorEmpresaContratante != null) __params = __params.set('operador-empresaContratante', params.operadorEmpresaContratante.toString());
    if (params.operadorEmpresaVigenciaHastaNe != null) __params = __params.set('operador-empresa-vigenciaHasta[ne]', params.operadorEmpresaVigenciaHastaNe.toString());
    if (params.operadorEmpresaVigenciaHastaLt != null) __params = __params.set('operador-empresa-vigenciaHasta[lt]', params.operadorEmpresaVigenciaHastaLt.toString());
    if (params.operadorEmpresaVigenciaHastaLe != null) __params = __params.set('operador-empresa-vigenciaHasta[le]', params.operadorEmpresaVigenciaHastaLe.toString());
    if (params.operadorEmpresaVigenciaHastaGt != null) __params = __params.set('operador-empresa-vigenciaHasta[gt]', params.operadorEmpresaVigenciaHastaGt.toString());
    if (params.operadorEmpresaVigenciaHastaGe != null) __params = __params.set('operador-empresa-vigenciaHasta[ge]', params.operadorEmpresaVigenciaHastaGe.toString());
    if (params.operadorEmpresaVigenciaHasta != null) __params = __params.set('operador-empresa-vigenciaHasta', params.operadorEmpresaVigenciaHasta.toString());
    if (params.operadorEmpresaVigenciaDesdeNe != null) __params = __params.set('operador-empresa-vigenciaDesde[ne]', params.operadorEmpresaVigenciaDesdeNe.toString());
    if (params.operadorEmpresaVigenciaDesdeLt != null) __params = __params.set('operador-empresa-vigenciaDesde[lt]', params.operadorEmpresaVigenciaDesdeLt.toString());
    if (params.operadorEmpresaVigenciaDesdeLe != null) __params = __params.set('operador-empresa-vigenciaDesde[le]', params.operadorEmpresaVigenciaDesdeLe.toString());
    if (params.operadorEmpresaVigenciaDesdeGt != null) __params = __params.set('operador-empresa-vigenciaDesde[gt]', params.operadorEmpresaVigenciaDesdeGt.toString());
    if (params.operadorEmpresaVigenciaDesdeGe != null) __params = __params.set('operador-empresa-vigenciaDesde[ge]', params.operadorEmpresaVigenciaDesdeGe.toString());
    if (params.operadorEmpresaVigenciaDesde != null) __params = __params.set('operador-empresa-vigenciaDesde', params.operadorEmpresaVigenciaDesde.toString());
    if (params.operadorEmpresaTipoSociedadNe != null) __params = __params.set('operador-empresa-tipoSociedad[ne]', params.operadorEmpresaTipoSociedadNe.toString());
    if (params.operadorEmpresaTipoSociedadLt != null) __params = __params.set('operador-empresa-tipoSociedad[lt]', params.operadorEmpresaTipoSociedadLt.toString());
    if (params.operadorEmpresaTipoSociedadLe != null) __params = __params.set('operador-empresa-tipoSociedad[le]', params.operadorEmpresaTipoSociedadLe.toString());
    if (params.operadorEmpresaTipoSociedadGt != null) __params = __params.set('operador-empresa-tipoSociedad[gt]', params.operadorEmpresaTipoSociedadGt.toString());
    if (params.operadorEmpresaTipoSociedadGe != null) __params = __params.set('operador-empresa-tipoSociedad[ge]', params.operadorEmpresaTipoSociedadGe.toString());
    if (params.operadorEmpresaTipoSociedad != null) __params = __params.set('operador-empresa-tipoSociedad', params.operadorEmpresaTipoSociedad.toString());
    if (params.operadorEmpresaTipoDocumentoIdNe != null) __params = __params.set('operador-empresa-tipoDocumento-id[ne]', params.operadorEmpresaTipoDocumentoIdNe.toString());
    if (params.operadorEmpresaTipoDocumentoIdLt != null) __params = __params.set('operador-empresa-tipoDocumento-id[lt]', params.operadorEmpresaTipoDocumentoIdLt.toString());
    if (params.operadorEmpresaTipoDocumentoIdLe != null) __params = __params.set('operador-empresa-tipoDocumento-id[le]', params.operadorEmpresaTipoDocumentoIdLe.toString());
    if (params.operadorEmpresaTipoDocumentoIdGt != null) __params = __params.set('operador-empresa-tipoDocumento-id[gt]', params.operadorEmpresaTipoDocumentoIdGt.toString());
    if (params.operadorEmpresaTipoDocumentoIdGe != null) __params = __params.set('operador-empresa-tipoDocumento-id[ge]', params.operadorEmpresaTipoDocumentoIdGe.toString());
    if (params.operadorEmpresaTipoDocumentoId != null) __params = __params.set('operador-empresa-tipoDocumento-id', params.operadorEmpresaTipoDocumentoId.toString());
    if (params.operadorEmpresaTipoDocumentoDescripcionNe != null) __params = __params.set('operador-empresa-tipoDocumento-descripcion[ne]', params.operadorEmpresaTipoDocumentoDescripcionNe.toString());
    if (params.operadorEmpresaTipoDocumentoDescripcionLike != null) __params = __params.set('operador-empresa-tipoDocumento-descripcion[like]', params.operadorEmpresaTipoDocumentoDescripcionLike.toString());
    if (params.operadorEmpresaTipoDocumentoDescripcion != null) __params = __params.set('operador-empresa-tipoDocumento-descripcion', params.operadorEmpresaTipoDocumentoDescripcion.toString());
    if (params.operadorEmpresaTipoDocumentoAbrevNe != null) __params = __params.set('operador-empresa-tipoDocumento-abrev[ne]', params.operadorEmpresaTipoDocumentoAbrevNe.toString());
    if (params.operadorEmpresaTipoDocumentoAbrevLike != null) __params = __params.set('operador-empresa-tipoDocumento-abrev[like]', params.operadorEmpresaTipoDocumentoAbrevLike.toString());
    if (params.operadorEmpresaTipoDocumentoAbrev != null) __params = __params.set('operador-empresa-tipoDocumento-abrev', params.operadorEmpresaTipoDocumentoAbrev.toString());
    if (params.operadorEmpresaTextoHash != null) __params = __params.set('operador-empresa-textoHash', params.operadorEmpresaTextoHash.toString());
    if (params.operadorEmpresaRazonSocialNe != null) __params = __params.set('operador-empresa-razonSocial[ne]', params.operadorEmpresaRazonSocialNe.toString());
    if (params.operadorEmpresaRazonSocialLike != null) __params = __params.set('operador-empresa-razonSocial[like]', params.operadorEmpresaRazonSocialLike.toString());
    if (params.operadorEmpresaRazonSocial != null) __params = __params.set('operador-empresa-razonSocial', params.operadorEmpresaRazonSocial.toString());
    if (params.operadorEmpresaPautNe != null) __params = __params.set('operador-empresa-paut[ne]', params.operadorEmpresaPautNe.toString());
    if (params.operadorEmpresaPautLike != null) __params = __params.set('operador-empresa-paut[like]', params.operadorEmpresaPautLike.toString());
    if (params.operadorEmpresaPaut != null) __params = __params.set('operador-empresa-paut', params.operadorEmpresaPaut.toString());
    if (params.operadorEmpresaObservacionNe != null) __params = __params.set('operador-empresa-observacion[ne]', params.operadorEmpresaObservacionNe.toString());
    if (params.operadorEmpresaObservacionLike != null) __params = __params.set('operador-empresa-observacion[like]', params.operadorEmpresaObservacionLike.toString());
    if (params.operadorEmpresaObservacion != null) __params = __params.set('operador-empresa-observacion', params.operadorEmpresaObservacion.toString());
    if (params.operadorEmpresaNroDocumentoNe != null) __params = __params.set('operador-empresa-nroDocumento[ne]', params.operadorEmpresaNroDocumentoNe.toString());
    if (params.operadorEmpresaNroDocumentoLike != null) __params = __params.set('operador-empresa-nroDocumento[like]', params.operadorEmpresaNroDocumentoLike.toString());
    if (params.operadorEmpresaNroDocumento != null) __params = __params.set('operador-empresa-nroDocumento', params.operadorEmpresaNroDocumento.toString());
    if (params.operadorEmpresaNombreFantasiaNe != null) __params = __params.set('operador-empresa-nombreFantasia[ne]', params.operadorEmpresaNombreFantasiaNe.toString());
    if (params.operadorEmpresaNombreFantasiaLike != null) __params = __params.set('operador-empresa-nombreFantasia[like]', params.operadorEmpresaNombreFantasiaLike.toString());
    if (params.operadorEmpresaNombreFantasia != null) __params = __params.set('operador-empresa-nombreFantasia', params.operadorEmpresaNombreFantasia.toString());
    if (params.operadorEmpresaIdNe != null) __params = __params.set('operador-empresa-id[ne]', params.operadorEmpresaIdNe.toString());
    if (params.operadorEmpresaIdLt != null) __params = __params.set('operador-empresa-id[lt]', params.operadorEmpresaIdLt.toString());
    if (params.operadorEmpresaIdLe != null) __params = __params.set('operador-empresa-id[le]', params.operadorEmpresaIdLe.toString());
    if (params.operadorEmpresaIdGt != null) __params = __params.set('operador-empresa-id[gt]', params.operadorEmpresaIdGt.toString());
    if (params.operadorEmpresaIdGe != null) __params = __params.set('operador-empresa-id[ge]', params.operadorEmpresaIdGe.toString());
    if (params.operadorEmpresaId != null) __params = __params.set('operador-empresa-id', params.operadorEmpresaId.toString());
    if (params.operadorEmpresaEstadoEmpresaNe != null) __params = __params.set('operador-empresa-estadoEmpresa[ne]', params.operadorEmpresaEstadoEmpresaNe.toString());
    if (params.operadorEmpresaEstadoEmpresaLt != null) __params = __params.set('operador-empresa-estadoEmpresa[lt]', params.operadorEmpresaEstadoEmpresaLt.toString());
    if (params.operadorEmpresaEstadoEmpresaLe != null) __params = __params.set('operador-empresa-estadoEmpresa[le]', params.operadorEmpresaEstadoEmpresaLe.toString());
    if (params.operadorEmpresaEstadoEmpresaGt != null) __params = __params.set('operador-empresa-estadoEmpresa[gt]', params.operadorEmpresaEstadoEmpresaGt.toString());
    if (params.operadorEmpresaEstadoEmpresaGe != null) __params = __params.set('operador-empresa-estadoEmpresa[ge]', params.operadorEmpresaEstadoEmpresaGe.toString());
    if (params.operadorEmpresaEstadoEmpresa != null) __params = __params.set('operador-empresa-estadoEmpresa', params.operadorEmpresaEstadoEmpresa.toString());
    if (params.operadorEmpresaEsPersonaFisica != null) __params = __params.set('operador-empresa-esPersonaFisica', params.operadorEmpresaEsPersonaFisica.toString());
    if (params.operadorEmpresaEmailNe != null) __params = __params.set('operador-empresa-email[ne]', params.operadorEmpresaEmailNe.toString());
    if (params.operadorEmpresaEmailLike != null) __params = __params.set('operador-empresa-email[like]', params.operadorEmpresaEmailLike.toString());
    if (params.operadorEmpresaEmail != null) __params = __params.set('operador-empresa-email', params.operadorEmpresaEmail.toString());
    if (params.operadorEmpresaCuitEmpresaAnteriorNe != null) __params = __params.set('operador-empresa-cuitEmpresaAnterior[ne]', params.operadorEmpresaCuitEmpresaAnteriorNe.toString());
    if (params.operadorEmpresaCuitEmpresaAnteriorLike != null) __params = __params.set('operador-empresa-cuitEmpresaAnterior[like]', params.operadorEmpresaCuitEmpresaAnteriorLike.toString());
    if (params.operadorEmpresaCuitEmpresaAnterior != null) __params = __params.set('operador-empresa-cuitEmpresaAnterior', params.operadorEmpresaCuitEmpresaAnterior.toString());
    if (params.operadorClaseModalidadIdNe != null) __params = __params.set('operador-claseModalidad-id[ne]', params.operadorClaseModalidadIdNe.toString());
    if (params.operadorClaseModalidadIdLt != null) __params = __params.set('operador-claseModalidad-id[lt]', params.operadorClaseModalidadIdLt.toString());
    if (params.operadorClaseModalidadIdLe != null) __params = __params.set('operador-claseModalidad-id[le]', params.operadorClaseModalidadIdLe.toString());
    if (params.operadorClaseModalidadIdGt != null) __params = __params.set('operador-claseModalidad-id[gt]', params.operadorClaseModalidadIdGt.toString());
    if (params.operadorClaseModalidadIdGe != null) __params = __params.set('operador-claseModalidad-id[ge]', params.operadorClaseModalidadIdGe.toString());
    if (params.operadorClaseModalidadId != null) __params = __params.set('operador-claseModalidad-id', params.operadorClaseModalidadId.toString());
    if (params.operadorClaseModalidadDescripcionNe != null) __params = __params.set('operador-claseModalidad-descripcion[ne]', params.operadorClaseModalidadDescripcionNe.toString());
    if (params.operadorClaseModalidadDescripcionLike != null) __params = __params.set('operador-claseModalidad-descripcion[like]', params.operadorClaseModalidadDescripcionLike.toString());
    if (params.operadorClaseModalidadDescripcion != null) __params = __params.set('operador-claseModalidad-descripcion', params.operadorClaseModalidadDescripcion.toString());
    if (params.offset != null) __params = __params.set('offset', params.offset.toString());
    if (params.limit != null) __params = __params.set('limit', params.limit.toString());
    if (params.idNe != null) __params = __params.set('id[ne]', params.idNe.toString());
    if (params.idLt != null) __params = __params.set('id[lt]', params.idLt.toString());
    if (params.idLe != null) __params = __params.set('id[le]', params.idLe.toString());
    if (params.idGt != null) __params = __params.set('id[gt]', params.idGt.toString());
    if (params.idGe != null) __params = __params.set('id[ge]', params.idGe.toString());
    if (params.id != null) __params = __params.set('id', params.id.toString());
    if (params.horaSalidaNe != null) __params = __params.set('horaSalida[ne]', params.horaSalidaNe.toString());
    if (params.horaSalidaLt != null) __params = __params.set('horaSalida[lt]', params.horaSalidaLt.toString());
    if (params.horaSalidaLe != null) __params = __params.set('horaSalida[le]', params.horaSalidaLe.toString());
    if (params.horaSalidaGt != null) __params = __params.set('horaSalida[gt]', params.horaSalidaGt.toString());
    if (params.horaSalidaGe != null) __params = __params.set('horaSalida[ge]', params.horaSalidaGe.toString());
    if (params.horaSalida != null) __params = __params.set('horaSalida', params.horaSalida.toString());
    if (params.horaLlegadaNe != null) __params = __params.set('horaLlegada[ne]', params.horaLlegadaNe.toString());
    if (params.horaLlegadaLt != null) __params = __params.set('horaLlegada[lt]', params.horaLlegadaLt.toString());
    if (params.horaLlegadaLe != null) __params = __params.set('horaLlegada[le]', params.horaLlegadaLe.toString());
    if (params.horaLlegadaGt != null) __params = __params.set('horaLlegada[gt]', params.horaLlegadaGt.toString());
    if (params.horaLlegadaGe != null) __params = __params.set('horaLlegada[ge]', params.horaLlegadaGe.toString());
    if (params.horaLlegada != null) __params = __params.set('horaLlegada', params.horaLlegada.toString());
    if (params.aprobado != null) __params = __params.set('aprobado', params.aprobado.toString());
    if (params.AtributoCriteria != null) __params = __params.set('&lt;atributo&gt;[&lt;criteria&gt;]', params.AtributoCriteria.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/servicios`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{result?: string, status?: number, data?: any, userMessage?: string, actions?: string}>;
      })
    );
  }
  /**
   * Obtiene una lista de cuadros horarios paginados
   * @param params The `ServicioService.FindAllServiciosParams` containing the following parameters:
   *
   * - `vigente`: Filtra los cuadros horarios con vigenciaHasta mayor o menor a la fecha de hoy: vigente=true (devuelve los cuadros horarios con vigenciaHasta mayor a la fecha de hoy o con vigenciaHasta = null // vigente=false (devuelve los servicios con vigenciaHasta menor o igual a la fecha de hoy - es decir, cuadros horarios que perdieron su vigencia - ). Este filtro se puede usar junto con los atributos de criteriaBuilder. ejemplo: ...&id[eq]=23%&vigente=true
   *
   * - `vigenciaHasta[ne]`:
   *
   * - `vigenciaHasta[lt]`:
   *
   * - `vigenciaHasta[le]`:
   *
   * - `vigenciaHasta[gt]`:
   *
   * - `vigenciaHasta[ge]`:
   *
   * - `vigenciaHasta`:
   *
   * - `vigenciaDesde[ne]`:
   *
   * - `vigenciaDesde[lt]`:
   *
   * - `vigenciaDesde[le]`:
   *
   * - `vigenciaDesde[gt]`:
   *
   * - `vigenciaDesde[ge]`:
   *
   * - `vigenciaDesde`:
   *
   * - `velocidadPromedioTotal[ne]`:
   *
   * - `velocidadPromedioTotal[lt]`:
   *
   * - `velocidadPromedioTotal[le]`:
   *
   * - `velocidadPromedioTotal[gt]`:
   *
   * - `velocidadPromedioTotal[ge]`:
   *
   * - `velocidadPromedioTotal`:
   *
   * - `tramite-usuario[ne]`:
   *
   * - `tramite-usuario[like]`:
   *
   * - `tramite-usuario`:
   *
   * - `tramite-tipoTramite-id[ne]`:
   *
   * - `tramite-tipoTramite-id[lt]`:
   *
   * - `tramite-tipoTramite-id[le]`:
   *
   * - `tramite-tipoTramite-id[gt]`:
   *
   * - `tramite-tipoTramite-id[ge]`:
   *
   * - `tramite-tipoTramite-id`:
   *
   * - `tramite-tipoTramite-descripcion[ne]`:
   *
   * - `tramite-tipoTramite-descripcion[like]`:
   *
   * - `tramite-tipoTramite-descripcion`:
   *
   * - `tramite-tipoTramite-activo`:
   *
   * - `tramite-subtipoTramite-id[ne]`:
   *
   * - `tramite-subtipoTramite-id[lt]`:
   *
   * - `tramite-subtipoTramite-id[le]`:
   *
   * - `tramite-subtipoTramite-id[gt]`:
   *
   * - `tramite-subtipoTramite-id[ge]`:
   *
   * - `tramite-subtipoTramite-id`:
   *
   * - `tramite-subtipoTramite-descripcion[ne]`:
   *
   * - `tramite-subtipoTramite-descripcion[like]`:
   *
   * - `tramite-subtipoTramite-descripcion`:
   *
   * - `tramite-subtipoTramite-activo`:
   *
   * - `tramite-nroBoletaPago[ne]`:
   *
   * - `tramite-nroBoletaPago[lt]`:
   *
   * - `tramite-nroBoletaPago[le]`:
   *
   * - `tramite-nroBoletaPago[gt]`:
   *
   * - `tramite-nroBoletaPago[ge]`:
   *
   * - `tramite-nroBoletaPago`:
   *
   * - `tramite-id[ne]`:
   *
   * - `tramite-id[lt]`:
   *
   * - `tramite-id[le]`:
   *
   * - `tramite-id[gt]`:
   *
   * - `tramite-id[ge]`:
   *
   * - `tramite-id`:
   *
   * - `tramite-estadoTramite-id[ne]`:
   *
   * - `tramite-estadoTramite-id[lt]`:
   *
   * - `tramite-estadoTramite-id[le]`:
   *
   * - `tramite-estadoTramite-id[gt]`:
   *
   * - `tramite-estadoTramite-id[ge]`:
   *
   * - `tramite-estadoTramite-id`:
   *
   * - `tramite-estadoTramite-descripcion[ne]`:
   *
   * - `tramite-estadoTramite-descripcion[like]`:
   *
   * - `tramite-estadoTramite-descripcion`:
   *
   * - `tramite-estadoTramite-activo`:
   *
   * - `tramite-estadoTramite-abrev[ne]`:
   *
   * - `tramite-estadoTramite-abrev[like]`:
   *
   * - `tramite-estadoTramite-abrev`:
   *
   * - `tramite-empresa-vigenciaHasta[ne]`:
   *
   * - `tramite-empresa-vigenciaHasta[lt]`:
   *
   * - `tramite-empresa-vigenciaHasta[le]`:
   *
   * - `tramite-empresa-vigenciaHasta[gt]`:
   *
   * - `tramite-empresa-vigenciaHasta[ge]`:
   *
   * - `tramite-empresa-vigenciaHasta`:
   *
   * - `tramite-empresa-vigenciaDesde[ne]`:
   *
   * - `tramite-empresa-vigenciaDesde[lt]`:
   *
   * - `tramite-empresa-vigenciaDesde[le]`:
   *
   * - `tramite-empresa-vigenciaDesde[gt]`:
   *
   * - `tramite-empresa-vigenciaDesde[ge]`:
   *
   * - `tramite-empresa-vigenciaDesde`:
   *
   * - `tramite-empresa-tipoSociedad[ne]`:
   *
   * - `tramite-empresa-tipoSociedad[lt]`:
   *
   * - `tramite-empresa-tipoSociedad[le]`:
   *
   * - `tramite-empresa-tipoSociedad[gt]`:
   *
   * - `tramite-empresa-tipoSociedad[ge]`:
   *
   * - `tramite-empresa-tipoSociedad`:
   *
   * - `tramite-empresa-tipoDocumento-id[ne]`:
   *
   * - `tramite-empresa-tipoDocumento-id[lt]`:
   *
   * - `tramite-empresa-tipoDocumento-id[le]`:
   *
   * - `tramite-empresa-tipoDocumento-id[gt]`:
   *
   * - `tramite-empresa-tipoDocumento-id[ge]`:
   *
   * - `tramite-empresa-tipoDocumento-id`:
   *
   * - `tramite-empresa-tipoDocumento-descripcion[ne]`:
   *
   * - `tramite-empresa-tipoDocumento-descripcion[like]`:
   *
   * - `tramite-empresa-tipoDocumento-descripcion`:
   *
   * - `tramite-empresa-tipoDocumento-abrev[ne]`:
   *
   * - `tramite-empresa-tipoDocumento-abrev[like]`:
   *
   * - `tramite-empresa-tipoDocumento-abrev`:
   *
   * - `tramite-empresa-textoHash`:
   *
   * - `tramite-empresa-razonSocial[ne]`:
   *
   * - `tramite-empresa-razonSocial[like]`:
   *
   * - `tramite-empresa-razonSocial`:
   *
   * - `tramite-empresa-paut[ne]`:
   *
   * - `tramite-empresa-paut[like]`:
   *
   * - `tramite-empresa-paut`:
   *
   * - `tramite-empresa-observacion[ne]`:
   *
   * - `tramite-empresa-observacion[like]`:
   *
   * - `tramite-empresa-observacion`:
   *
   * - `tramite-empresa-nroDocumento[ne]`:
   *
   * - `tramite-empresa-nroDocumento[like]`:
   *
   * - `tramite-empresa-nroDocumento`:
   *
   * - `tramite-empresa-nombreFantasia[ne]`:
   *
   * - `tramite-empresa-nombreFantasia[like]`:
   *
   * - `tramite-empresa-nombreFantasia`:
   *
   * - `tramite-empresa-id[ne]`:
   *
   * - `tramite-empresa-id[lt]`:
   *
   * - `tramite-empresa-id[le]`:
   *
   * - `tramite-empresa-id[gt]`:
   *
   * - `tramite-empresa-id[ge]`:
   *
   * - `tramite-empresa-id`:
   *
   * - `tramite-empresa-estadoEmpresa[ne]`:
   *
   * - `tramite-empresa-estadoEmpresa[lt]`:
   *
   * - `tramite-empresa-estadoEmpresa[le]`:
   *
   * - `tramite-empresa-estadoEmpresa[gt]`:
   *
   * - `tramite-empresa-estadoEmpresa[ge]`:
   *
   * - `tramite-empresa-estadoEmpresa`:
   *
   * - `tramite-empresa-esPersonaFisica`:
   *
   * - `tramite-empresa-email[ne]`:
   *
   * - `tramite-empresa-email[like]`:
   *
   * - `tramite-empresa-email`:
   *
   * - `tramite-empresa-cuitEmpresaAnterior[ne]`:
   *
   * - `tramite-empresa-cuitEmpresaAnterior[like]`:
   *
   * - `tramite-empresa-cuitEmpresaAnterior`:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `recorridoSentido-vinculacionCaminera[ne]`:
   *
   * - `recorridoSentido-vinculacionCaminera[like]`:
   *
   * - `recorridoSentido-vinculacionCaminera`:
   *
   * - `recorridoSentido-vigenciaHasta[ne]`:
   *
   * - `recorridoSentido-vigenciaHasta[lt]`:
   *
   * - `recorridoSentido-vigenciaHasta[le]`:
   *
   * - `recorridoSentido-vigenciaHasta[gt]`:
   *
   * - `recorridoSentido-vigenciaHasta[ge]`:
   *
   * - `recorridoSentido-vigenciaHasta`:
   *
   * - `recorridoSentido-vigenciaDesde[ne]`:
   *
   * - `recorridoSentido-vigenciaDesde[lt]`:
   *
   * - `recorridoSentido-vigenciaDesde[le]`:
   *
   * - `recorridoSentido-vigenciaDesde[gt]`:
   *
   * - `recorridoSentido-vigenciaDesde[ge]`:
   *
   * - `recorridoSentido-vigenciaDesde`:
   *
   * - `recorridoSentido-sentido-id[ne]`:
   *
   * - `recorridoSentido-sentido-id[lt]`:
   *
   * - `recorridoSentido-sentido-id[le]`:
   *
   * - `recorridoSentido-sentido-id[gt]`:
   *
   * - `recorridoSentido-sentido-id[ge]`:
   *
   * - `recorridoSentido-sentido-id`:
   *
   * - `recorridoSentido-sentido-descripcion[ne]`:
   *
   * - `recorridoSentido-sentido-descripcion[like]`:
   *
   * - `recorridoSentido-sentido-descripcion`:
   *
   * - `recorridoSentido-sentido-activo`:
   *
   * - `recorridoSentido-sentido-abrev[ne]`:
   *
   * - `recorridoSentido-sentido-abrev[like]`:
   *
   * - `recorridoSentido-sentido-abrev`:
   *
   * - `recorridoSentido-recorrido-vinculacionCaminera[ne]`:
   *
   * - `recorridoSentido-recorrido-vinculacionCaminera[like]`:
   *
   * - `recorridoSentido-recorrido-vinculacionCaminera`:
   *
   * - `recorridoSentido-recorrido-vigenciaHasta[ne]`:
   *
   * - `recorridoSentido-recorrido-vigenciaHasta[lt]`:
   *
   * - `recorridoSentido-recorrido-vigenciaHasta[le]`:
   *
   * - `recorridoSentido-recorrido-vigenciaHasta[gt]`:
   *
   * - `recorridoSentido-recorrido-vigenciaHasta[ge]`:
   *
   * - `recorridoSentido-recorrido-vigenciaHasta`:
   *
   * - `recorridoSentido-recorrido-vigenciaDesde[ne]`:
   *
   * - `recorridoSentido-recorrido-vigenciaDesde[lt]`:
   *
   * - `recorridoSentido-recorrido-vigenciaDesde[le]`:
   *
   * - `recorridoSentido-recorrido-vigenciaDesde[gt]`:
   *
   * - `recorridoSentido-recorrido-vigenciaDesde[ge]`:
   *
   * - `recorridoSentido-recorrido-vigenciaDesde`:
   *
   * - `recorridoSentido-recorrido-transito[ne]`:
   *
   * - `recorridoSentido-recorrido-transito[like]`:
   *
   * - `recorridoSentido-recorrido-transito`:
   *
   * - `recorridoSentido-recorrido-sitioOrigen[ne]`:
   *
   * - `recorridoSentido-recorrido-sitioOrigen[like]`:
   *
   * - `recorridoSentido-recorrido-sitioOrigen`:
   *
   * - `recorridoSentido-recorrido-sitioDestino[ne]`:
   *
   * - `recorridoSentido-recorrido-sitioDestino[like]`:
   *
   * - `recorridoSentido-recorrido-sitioDestino`:
   *
   * - `recorridoSentido-recorrido-recorrido[ne]`:
   *
   * - `recorridoSentido-recorrido-recorrido[like]`:
   *
   * - `recorridoSentido-recorrido-recorrido`:
   *
   * - `recorridoSentido-recorrido-origen[ne]`:
   *
   * - `recorridoSentido-recorrido-origen[like]`:
   *
   * - `recorridoSentido-recorrido-origen`:
   *
   * - `recorridoSentido-recorrido-itinerarioVuelta[ne]`:
   *
   * - `recorridoSentido-recorrido-itinerarioVuelta[like]`:
   *
   * - `recorridoSentido-recorrido-itinerarioVuelta`:
   *
   * - `recorridoSentido-recorrido-itinerarioIda[ne]`:
   *
   * - `recorridoSentido-recorrido-itinerarioIda[like]`:
   *
   * - `recorridoSentido-recorrido-itinerarioIda`:
   *
   * - `recorridoSentido-recorrido-id[ne]`:
   *
   * - `recorridoSentido-recorrido-id[lt]`:
   *
   * - `recorridoSentido-recorrido-id[le]`:
   *
   * - `recorridoSentido-recorrido-id[gt]`:
   *
   * - `recorridoSentido-recorrido-id[ge]`:
   *
   * - `recorridoSentido-recorrido-id`:
   *
   * - `recorridoSentido-recorrido-destino[ne]`:
   *
   * - `recorridoSentido-recorrido-destino[like]`:
   *
   * - `recorridoSentido-recorrido-destino`:
   *
   * - `recorridoSentido-recorrido-aplicaGasoil`:
   *
   * - `recorridoSentido-origen[ne]`:
   *
   * - `recorridoSentido-origen[like]`:
   *
   * - `recorridoSentido-origen`:
   *
   * - `recorridoSentido-localidadOrigen-updatedAt[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-updatedAt[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-updatedAt[le]`:
   *
   * - `recorridoSentido-localidadOrigen-updatedAt[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-updatedAt[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-updatedAt`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-id[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-id[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-id[le]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-id[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-id[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-id`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-descripcion[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-descripcion[like]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-descripcion`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-codigo[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-codigo[like]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-codigo`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-activo`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-abrev[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-abrev[like]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-pais-abrev`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-id[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-id[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-id[le]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-id[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-id[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-id`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-descripcion[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-descripcion[like]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-descripcion`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-codigo[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-codigo[like]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-codigoINDEC[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-codigoINDEC[like]`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-codigoINDEC`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-codigo`:
   *
   * - `recorridoSentido-localidadOrigen-provincia-activo`:
   *
   * - `recorridoSentido-localidadOrigen-parada`:
   *
   * - `recorridoSentido-localidadOrigen-lon[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-lon[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-lon[le]`:
   *
   * - `recorridoSentido-localidadOrigen-lon[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-lon[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-lon`:
   *
   * - `recorridoSentido-localidadOrigen-lat[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-lat[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-lat[le]`:
   *
   * - `recorridoSentido-localidadOrigen-lat[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-lat[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-lat`:
   *
   * - `recorridoSentido-localidadOrigen-id[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-id[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-id[le]`:
   *
   * - `recorridoSentido-localidadOrigen-id[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-id[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-id`:
   *
   * - `recorridoSentido-localidadOrigen-fuente[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-fuente[like]`:
   *
   * - `recorridoSentido-localidadOrigen-fuente`:
   *
   * - `recorridoSentido-localidadOrigen-descripcion[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-descripcion[like]`:
   *
   * - `recorridoSentido-localidadOrigen-descripcion`:
   *
   * - `recorridoSentido-localidadOrigen-createdAt[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-createdAt[lt]`:
   *
   * - `recorridoSentido-localidadOrigen-createdAt[le]`:
   *
   * - `recorridoSentido-localidadOrigen-createdAt[gt]`:
   *
   * - `recorridoSentido-localidadOrigen-createdAt[ge]`:
   *
   * - `recorridoSentido-localidadOrigen-createdAt`:
   *
   * - `recorridoSentido-localidadOrigen-codigoINDEC[ne]`:
   *
   * - `recorridoSentido-localidadOrigen-codigoINDEC[like]`:
   *
   * - `recorridoSentido-localidadOrigen-codigoINDEC`:
   *
   * - `recorridoSentido-localidadOrigen-activo`:
   *
   * - `recorridoSentido-localidadDestino-updatedAt[ne]`:
   *
   * - `recorridoSentido-localidadDestino-updatedAt[lt]`:
   *
   * - `recorridoSentido-localidadDestino-updatedAt[le]`:
   *
   * - `recorridoSentido-localidadDestino-updatedAt[gt]`:
   *
   * - `recorridoSentido-localidadDestino-updatedAt[ge]`:
   *
   * - `recorridoSentido-localidadDestino-updatedAt`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-id[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-id[lt]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-id[le]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-id[gt]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-id[ge]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-id`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-descripcion[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-descripcion[like]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-descripcion`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-codigo[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-codigo[like]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-codigo`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-activo`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-abrev[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-abrev[like]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-pais-abrev`:
   *
   * - `recorridoSentido-localidadDestino-provincia-id[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-id[lt]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-id[le]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-id[gt]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-id[ge]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-id`:
   *
   * - `recorridoSentido-localidadDestino-provincia-descripcion[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-descripcion[like]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-descripcion`:
   *
   * - `recorridoSentido-localidadDestino-provincia-codigo[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-codigo[like]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-codigoINDEC[ne]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-codigoINDEC[like]`:
   *
   * - `recorridoSentido-localidadDestino-provincia-codigoINDEC`:
   *
   * - `recorridoSentido-localidadDestino-provincia-codigo`:
   *
   * - `recorridoSentido-localidadDestino-provincia-activo`:
   *
   * - `recorridoSentido-localidadDestino-parada`:
   *
   * - `recorridoSentido-localidadDestino-lon[ne]`:
   *
   * - `recorridoSentido-localidadDestino-lon[lt]`:
   *
   * - `recorridoSentido-localidadDestino-lon[le]`:
   *
   * - `recorridoSentido-localidadDestino-lon[gt]`:
   *
   * - `recorridoSentido-localidadDestino-lon[ge]`:
   *
   * - `recorridoSentido-localidadDestino-lon`:
   *
   * - `recorridoSentido-localidadDestino-lat[ne]`:
   *
   * - `recorridoSentido-localidadDestino-lat[lt]`:
   *
   * - `recorridoSentido-localidadDestino-lat[le]`:
   *
   * - `recorridoSentido-localidadDestino-lat[gt]`:
   *
   * - `recorridoSentido-localidadDestino-lat[ge]`:
   *
   * - `recorridoSentido-localidadDestino-lat`:
   *
   * - `recorridoSentido-localidadDestino-id[ne]`:
   *
   * - `recorridoSentido-localidadDestino-id[lt]`:
   *
   * - `recorridoSentido-localidadDestino-id[le]`:
   *
   * - `recorridoSentido-localidadDestino-id[gt]`:
   *
   * - `recorridoSentido-localidadDestino-id[ge]`:
   *
   * - `recorridoSentido-localidadDestino-id`:
   *
   * - `recorridoSentido-localidadDestino-fuente[ne]`:
   *
   * - `recorridoSentido-localidadDestino-fuente[like]`:
   *
   * - `recorridoSentido-localidadDestino-fuente`:
   *
   * - `recorridoSentido-localidadDestino-descripcion[ne]`:
   *
   * - `recorridoSentido-localidadDestino-descripcion[like]`:
   *
   * - `recorridoSentido-localidadDestino-descripcion`:
   *
   * - `recorridoSentido-localidadDestino-createdAt[ne]`:
   *
   * - `recorridoSentido-localidadDestino-createdAt[lt]`:
   *
   * - `recorridoSentido-localidadDestino-createdAt[le]`:
   *
   * - `recorridoSentido-localidadDestino-createdAt[gt]`:
   *
   * - `recorridoSentido-localidadDestino-createdAt[ge]`:
   *
   * - `recorridoSentido-localidadDestino-createdAt`:
   *
   * - `recorridoSentido-localidadDestino-codigoINDEC[ne]`:
   *
   * - `recorridoSentido-localidadDestino-codigoINDEC[like]`:
   *
   * - `recorridoSentido-localidadDestino-codigoINDEC`:
   *
   * - `recorridoSentido-localidadDestino-activo`:
   *
   * - `recorridoSentido-itinerario[ne]`:
   *
   * - `recorridoSentido-itinerario[like]`:
   *
   * - `recorridoSentido-itinerario`:
   *
   * - `recorridoSentido-id[ne]`:
   *
   * - `recorridoSentido-id[lt]`:
   *
   * - `recorridoSentido-id[le]`:
   *
   * - `recorridoSentido-id[gt]`:
   *
   * - `recorridoSentido-id[ge]`:
   *
   * - `recorridoSentido-id`:
   *
   * - `recorridoSentido-destino[ne]`:
   *
   * - `recorridoSentido-destino[like]`:
   *
   * - `recorridoSentido-destino`:
   *
   * - `recorrido-vinculacionCaminera[ne]`:
   *
   * - `recorrido-vinculacionCaminera[like]`:
   *
   * - `recorrido-vinculacionCaminera`:
   *
   * - `recorrido-vigenciaHasta[ne]`:
   *
   * - `recorrido-vigenciaHasta[lt]`:
   *
   * - `recorrido-vigenciaHasta[le]`:
   *
   * - `recorrido-vigenciaHasta[gt]`:
   *
   * - `recorrido-vigenciaHasta[ge]`:
   *
   * - `recorrido-vigenciaHasta`:
   *
   * - `recorrido-vigenciaDesde[ne]`:
   *
   * - `recorrido-vigenciaDesde[lt]`:
   *
   * - `recorrido-vigenciaDesde[le]`:
   *
   * - `recorrido-vigenciaDesde[gt]`:
   *
   * - `recorrido-vigenciaDesde[ge]`:
   *
   * - `recorrido-vigenciaDesde`:
   *
   * - `recorrido-transito[ne]`:
   *
   * - `recorrido-transito[like]`:
   *
   * - `recorrido-transito`:
   *
   * - `recorrido-sitioOrigen[ne]`:
   *
   * - `recorrido-sitioOrigen[like]`:
   *
   * - `recorrido-sitioOrigen`:
   *
   * - `recorrido-sitioDestino[ne]`:
   *
   * - `recorrido-sitioDestino[like]`:
   *
   * - `recorrido-sitioDestino`:
   *
   * - `recorrido-recorrido[ne]`:
   *
   * - `recorrido-recorrido[like]`:
   *
   * - `recorrido-recorrido`:
   *
   * - `recorrido-origen[ne]`:
   *
   * - `recorrido-origen[like]`:
   *
   * - `recorrido-origen`:
   *
   * - `recorrido-itinerarioVuelta[ne]`:
   *
   * - `recorrido-itinerarioVuelta[like]`:
   *
   * - `recorrido-itinerarioVuelta`:
   *
   * - `recorrido-itinerarioIda[ne]`:
   *
   * - `recorrido-itinerarioIda[like]`:
   *
   * - `recorrido-itinerarioIda`:
   *
   * - `recorrido-id[ne]`:
   *
   * - `recorrido-id[lt]`:
   *
   * - `recorrido-id[le]`:
   *
   * - `recorrido-id[gt]`:
   *
   * - `recorrido-id[ge]`:
   *
   * - `recorrido-id`:
   *
   * - `recorrido-destino[ne]`:
   *
   * - `recorrido-destino[like]`:
   *
   * - `recorrido-destino`:
   *
   * - `recorrido-aplicaGasoil`:
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `operador-vigenciaHasta[ne]`:
   *
   * - `operador-vigenciaHasta[lt]`:
   *
   * - `operador-vigenciaHasta[le]`:
   *
   * - `operador-vigenciaHasta[gt]`:
   *
   * - `operador-vigenciaHasta[ge]`:
   *
   * - `operador-vigenciaHasta`:
   *
   * - `operador-vigenciaDesde[ne]`:
   *
   * - `operador-vigenciaDesde[lt]`:
   *
   * - `operador-vigenciaDesde[le]`:
   *
   * - `operador-vigenciaDesde[gt]`:
   *
   * - `operador-vigenciaDesde[ge]`:
   *
   * - `operador-vigenciaDesde`:
   *
   * - `operador-localidadOrigen-updatedAt[ne]`:
   *
   * - `operador-localidadOrigen-updatedAt[lt]`:
   *
   * - `operador-localidadOrigen-updatedAt[le]`:
   *
   * - `operador-localidadOrigen-updatedAt[gt]`:
   *
   * - `operador-localidadOrigen-updatedAt[ge]`:
   *
   * - `operador-localidadOrigen-updatedAt`:
   *
   * - `operador-localidadOrigen-provincia-pais-id[ne]`:
   *
   * - `operador-localidadOrigen-provincia-pais-id[lt]`:
   *
   * - `operador-localidadOrigen-provincia-pais-id[le]`:
   *
   * - `operador-localidadOrigen-provincia-pais-id[gt]`:
   *
   * - `operador-localidadOrigen-provincia-pais-id[ge]`:
   *
   * - `operador-localidadOrigen-provincia-pais-id`:
   *
   * - `operador-localidadOrigen-provincia-pais-descripcion[ne]`:
   *
   * - `operador-localidadOrigen-provincia-pais-descripcion[like]`:
   *
   * - `operador-localidadOrigen-provincia-pais-descripcion`:
   *
   * - `operador-localidadOrigen-provincia-pais-codigo[ne]`:
   *
   * - `operador-localidadOrigen-provincia-pais-codigo[like]`:
   *
   * - `operador-localidadOrigen-provincia-pais-codigo`:
   *
   * - `operador-localidadOrigen-provincia-pais-activo`:
   *
   * - `operador-localidadOrigen-provincia-pais-abrev[ne]`:
   *
   * - `operador-localidadOrigen-provincia-pais-abrev[like]`:
   *
   * - `operador-localidadOrigen-provincia-pais-abrev`:
   *
   * - `operador-localidadOrigen-provincia-id[ne]`:
   *
   * - `operador-localidadOrigen-provincia-id[lt]`:
   *
   * - `operador-localidadOrigen-provincia-id[le]`:
   *
   * - `operador-localidadOrigen-provincia-id[gt]`:
   *
   * - `operador-localidadOrigen-provincia-id[ge]`:
   *
   * - `operador-localidadOrigen-provincia-id`:
   *
   * - `operador-localidadOrigen-provincia-descripcion[ne]`:
   *
   * - `operador-localidadOrigen-provincia-descripcion[like]`:
   *
   * - `operador-localidadOrigen-provincia-descripcion`:
   *
   * - `operador-localidadOrigen-provincia-codigo[ne]`:
   *
   * - `operador-localidadOrigen-provincia-codigo[like]`:
   *
   * - `operador-localidadOrigen-provincia-codigoINDEC[ne]`:
   *
   * - `operador-localidadOrigen-provincia-codigoINDEC[like]`:
   *
   * - `operador-localidadOrigen-provincia-codigoINDEC`:
   *
   * - `operador-localidadOrigen-provincia-codigo`:
   *
   * - `operador-localidadOrigen-provincia-activo`:
   *
   * - `operador-localidadOrigen-parada`:
   *
   * - `operador-localidadOrigen-lon[ne]`:
   *
   * - `operador-localidadOrigen-lon[lt]`:
   *
   * - `operador-localidadOrigen-lon[le]`:
   *
   * - `operador-localidadOrigen-lon[gt]`:
   *
   * - `operador-localidadOrigen-lon[ge]`:
   *
   * - `operador-localidadOrigen-lon`:
   *
   * - `operador-localidadOrigen-lat[ne]`:
   *
   * - `operador-localidadOrigen-lat[lt]`:
   *
   * - `operador-localidadOrigen-lat[le]`:
   *
   * - `operador-localidadOrigen-lat[gt]`:
   *
   * - `operador-localidadOrigen-lat[ge]`:
   *
   * - `operador-localidadOrigen-lat`:
   *
   * - `operador-localidadOrigen-id[ne]`:
   *
   * - `operador-localidadOrigen-id[lt]`:
   *
   * - `operador-localidadOrigen-id[le]`:
   *
   * - `operador-localidadOrigen-id[gt]`:
   *
   * - `operador-localidadOrigen-id[ge]`:
   *
   * - `operador-localidadOrigen-id`:
   *
   * - `operador-localidadOrigen-fuente[ne]`:
   *
   * - `operador-localidadOrigen-fuente[like]`:
   *
   * - `operador-localidadOrigen-fuente`:
   *
   * - `operador-localidadOrigen-descripcion[ne]`:
   *
   * - `operador-localidadOrigen-descripcion[like]`:
   *
   * - `operador-localidadOrigen-descripcion`:
   *
   * - `operador-localidadOrigen-createdAt[ne]`:
   *
   * - `operador-localidadOrigen-createdAt[lt]`:
   *
   * - `operador-localidadOrigen-createdAt[le]`:
   *
   * - `operador-localidadOrigen-createdAt[gt]`:
   *
   * - `operador-localidadOrigen-createdAt[ge]`:
   *
   * - `operador-localidadOrigen-createdAt`:
   *
   * - `operador-localidadOrigen-codigoINDEC[ne]`:
   *
   * - `operador-localidadOrigen-codigoINDEC[like]`:
   *
   * - `operador-localidadOrigen-codigoINDEC`:
   *
   * - `operador-localidadOrigen-activo`:
   *
   * - `operador-localidadDestino-updatedAt[ne]`:
   *
   * - `operador-localidadDestino-updatedAt[lt]`:
   *
   * - `operador-localidadDestino-updatedAt[le]`:
   *
   * - `operador-localidadDestino-updatedAt[gt]`:
   *
   * - `operador-localidadDestino-updatedAt[ge]`:
   *
   * - `operador-localidadDestino-updatedAt`:
   *
   * - `operador-localidadDestino-provincia-pais-id[ne]`:
   *
   * - `operador-localidadDestino-provincia-pais-id[lt]`:
   *
   * - `operador-localidadDestino-provincia-pais-id[le]`:
   *
   * - `operador-localidadDestino-provincia-pais-id[gt]`:
   *
   * - `operador-localidadDestino-provincia-pais-id[ge]`:
   *
   * - `operador-localidadDestino-provincia-pais-id`:
   *
   * - `operador-localidadDestino-provincia-pais-descripcion[ne]`:
   *
   * - `operador-localidadDestino-provincia-pais-descripcion[like]`:
   *
   * - `operador-localidadDestino-provincia-pais-descripcion`:
   *
   * - `operador-localidadDestino-provincia-pais-codigo[ne]`:
   *
   * - `operador-localidadDestino-provincia-pais-codigo[like]`:
   *
   * - `operador-localidadDestino-provincia-pais-codigo`:
   *
   * - `operador-localidadDestino-provincia-pais-activo`:
   *
   * - `operador-localidadDestino-provincia-pais-abrev[ne]`:
   *
   * - `operador-localidadDestino-provincia-pais-abrev[like]`:
   *
   * - `operador-localidadDestino-provincia-pais-abrev`:
   *
   * - `operador-localidadDestino-provincia-id[ne]`:
   *
   * - `operador-localidadDestino-provincia-id[lt]`:
   *
   * - `operador-localidadDestino-provincia-id[le]`:
   *
   * - `operador-localidadDestino-provincia-id[gt]`:
   *
   * - `operador-localidadDestino-provincia-id[ge]`:
   *
   * - `operador-localidadDestino-provincia-id`:
   *
   * - `operador-localidadDestino-provincia-descripcion[ne]`:
   *
   * - `operador-localidadDestino-provincia-descripcion[like]`:
   *
   * - `operador-localidadDestino-provincia-descripcion`:
   *
   * - `operador-localidadDestino-provincia-codigo[ne]`:
   *
   * - `operador-localidadDestino-provincia-codigo[like]`:
   *
   * - `operador-localidadDestino-provincia-codigoINDEC[ne]`:
   *
   * - `operador-localidadDestino-provincia-codigoINDEC[like]`:
   *
   * - `operador-localidadDestino-provincia-codigoINDEC`:
   *
   * - `operador-localidadDestino-provincia-codigo`:
   *
   * - `operador-localidadDestino-provincia-activo`:
   *
   * - `operador-localidadDestino-parada`:
   *
   * - `operador-localidadDestino-lon[ne]`:
   *
   * - `operador-localidadDestino-lon[lt]`:
   *
   * - `operador-localidadDestino-lon[le]`:
   *
   * - `operador-localidadDestino-lon[gt]`:
   *
   * - `operador-localidadDestino-lon[ge]`:
   *
   * - `operador-localidadDestino-lon`:
   *
   * - `operador-localidadDestino-lat[ne]`:
   *
   * - `operador-localidadDestino-lat[lt]`:
   *
   * - `operador-localidadDestino-lat[le]`:
   *
   * - `operador-localidadDestino-lat[gt]`:
   *
   * - `operador-localidadDestino-lat[ge]`:
   *
   * - `operador-localidadDestino-lat`:
   *
   * - `operador-localidadDestino-id[ne]`:
   *
   * - `operador-localidadDestino-id[lt]`:
   *
   * - `operador-localidadDestino-id[le]`:
   *
   * - `operador-localidadDestino-id[gt]`:
   *
   * - `operador-localidadDestino-id[ge]`:
   *
   * - `operador-localidadDestino-id`:
   *
   * - `operador-localidadDestino-fuente[ne]`:
   *
   * - `operador-localidadDestino-fuente[like]`:
   *
   * - `operador-localidadDestino-fuente`:
   *
   * - `operador-localidadDestino-descripcion[ne]`:
   *
   * - `operador-localidadDestino-descripcion[like]`:
   *
   * - `operador-localidadDestino-descripcion`:
   *
   * - `operador-localidadDestino-createdAt[ne]`:
   *
   * - `operador-localidadDestino-createdAt[lt]`:
   *
   * - `operador-localidadDestino-createdAt[le]`:
   *
   * - `operador-localidadDestino-createdAt[gt]`:
   *
   * - `operador-localidadDestino-createdAt[ge]`:
   *
   * - `operador-localidadDestino-createdAt`:
   *
   * - `operador-localidadDestino-codigoINDEC[ne]`:
   *
   * - `operador-localidadDestino-codigoINDEC[like]`:
   *
   * - `operador-localidadDestino-codigoINDEC`:
   *
   * - `operador-localidadDestino-activo`:
   *
   * - `operador-linea[ne]`:
   *
   * - `operador-linea[like]`:
   *
   * - `operador-linea`:
   *
   * - `operador-id[ne]`:
   *
   * - `operador-id[lt]`:
   *
   * - `operador-id[le]`:
   *
   * - `operador-id[gt]`:
   *
   * - `operador-id[ge]`:
   *
   * - `operador-id`:
   *
   * - `operador-empresaContratante[ne]`:
   *
   * - `operador-empresaContratante[like]`:
   *
   * - `operador-empresaContratanteCuit[ne]`:
   *
   * - `operador-empresaContratanteCuit[like]`:
   *
   * - `operador-empresaContratanteCuit`:
   *
   * - `operador-empresaContratante`:
   *
   * - `operador-empresa-vigenciaHasta[ne]`:
   *
   * - `operador-empresa-vigenciaHasta[lt]`:
   *
   * - `operador-empresa-vigenciaHasta[le]`:
   *
   * - `operador-empresa-vigenciaHasta[gt]`:
   *
   * - `operador-empresa-vigenciaHasta[ge]`:
   *
   * - `operador-empresa-vigenciaHasta`:
   *
   * - `operador-empresa-vigenciaDesde[ne]`:
   *
   * - `operador-empresa-vigenciaDesde[lt]`:
   *
   * - `operador-empresa-vigenciaDesde[le]`:
   *
   * - `operador-empresa-vigenciaDesde[gt]`:
   *
   * - `operador-empresa-vigenciaDesde[ge]`:
   *
   * - `operador-empresa-vigenciaDesde`:
   *
   * - `operador-empresa-tipoSociedad[ne]`:
   *
   * - `operador-empresa-tipoSociedad[lt]`:
   *
   * - `operador-empresa-tipoSociedad[le]`:
   *
   * - `operador-empresa-tipoSociedad[gt]`:
   *
   * - `operador-empresa-tipoSociedad[ge]`:
   *
   * - `operador-empresa-tipoSociedad`:
   *
   * - `operador-empresa-tipoDocumento-id[ne]`:
   *
   * - `operador-empresa-tipoDocumento-id[lt]`:
   *
   * - `operador-empresa-tipoDocumento-id[le]`:
   *
   * - `operador-empresa-tipoDocumento-id[gt]`:
   *
   * - `operador-empresa-tipoDocumento-id[ge]`:
   *
   * - `operador-empresa-tipoDocumento-id`:
   *
   * - `operador-empresa-tipoDocumento-descripcion[ne]`:
   *
   * - `operador-empresa-tipoDocumento-descripcion[like]`:
   *
   * - `operador-empresa-tipoDocumento-descripcion`:
   *
   * - `operador-empresa-tipoDocumento-abrev[ne]`:
   *
   * - `operador-empresa-tipoDocumento-abrev[like]`:
   *
   * - `operador-empresa-tipoDocumento-abrev`:
   *
   * - `operador-empresa-textoHash`:
   *
   * - `operador-empresa-razonSocial[ne]`:
   *
   * - `operador-empresa-razonSocial[like]`:
   *
   * - `operador-empresa-razonSocial`:
   *
   * - `operador-empresa-paut[ne]`:
   *
   * - `operador-empresa-paut[like]`:
   *
   * - `operador-empresa-paut`:
   *
   * - `operador-empresa-observacion[ne]`:
   *
   * - `operador-empresa-observacion[like]`:
   *
   * - `operador-empresa-observacion`:
   *
   * - `operador-empresa-nroDocumento[ne]`:
   *
   * - `operador-empresa-nroDocumento[like]`:
   *
   * - `operador-empresa-nroDocumento`:
   *
   * - `operador-empresa-nombreFantasia[ne]`:
   *
   * - `operador-empresa-nombreFantasia[like]`:
   *
   * - `operador-empresa-nombreFantasia`:
   *
   * - `operador-empresa-id[ne]`:
   *
   * - `operador-empresa-id[lt]`:
   *
   * - `operador-empresa-id[le]`:
   *
   * - `operador-empresa-id[gt]`:
   *
   * - `operador-empresa-id[ge]`:
   *
   * - `operador-empresa-id`:
   *
   * - `operador-empresa-estadoEmpresa[ne]`:
   *
   * - `operador-empresa-estadoEmpresa[lt]`:
   *
   * - `operador-empresa-estadoEmpresa[le]`:
   *
   * - `operador-empresa-estadoEmpresa[gt]`:
   *
   * - `operador-empresa-estadoEmpresa[ge]`:
   *
   * - `operador-empresa-estadoEmpresa`:
   *
   * - `operador-empresa-esPersonaFisica`:
   *
   * - `operador-empresa-email[ne]`:
   *
   * - `operador-empresa-email[like]`:
   *
   * - `operador-empresa-email`:
   *
   * - `operador-empresa-cuitEmpresaAnterior[ne]`:
   *
   * - `operador-empresa-cuitEmpresaAnterior[like]`:
   *
   * - `operador-empresa-cuitEmpresaAnterior`:
   *
   * - `operador-claseModalidad-id[ne]`:
   *
   * - `operador-claseModalidad-id[lt]`:
   *
   * - `operador-claseModalidad-id[le]`:
   *
   * - `operador-claseModalidad-id[gt]`:
   *
   * - `operador-claseModalidad-id[ge]`:
   *
   * - `operador-claseModalidad-id`:
   *
   * - `operador-claseModalidad-descripcion[ne]`:
   *
   * - `operador-claseModalidad-descripcion[like]`:
   *
   * - `operador-claseModalidad-descripcion`:
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `horaSalida[ne]`:
   *
   * - `horaSalida[lt]`:
   *
   * - `horaSalida[le]`:
   *
   * - `horaSalida[gt]`:
   *
   * - `horaSalida[ge]`:
   *
   * - `horaSalida`:
   *
   * - `horaLlegada[ne]`:
   *
   * - `horaLlegada[lt]`:
   *
   * - `horaLlegada[le]`:
   *
   * - `horaLlegada[gt]`:
   *
   * - `horaLlegada[ge]`:
   *
   * - `horaLlegada`:
   *
   * - `aprobado`:
   *
   * - `<atributo>[<criteria>]`: Lista de atributos a filtrar: ...&lastname[like]=Gonz%&city->state->code[eq]=AR
   *
   * @return Respuesta exitosa.
   */
  findAllServicios(params: ServicioService.FindAllServiciosParams): __Observable<{result?: string, status?: number, data?: any, userMessage?: string, actions?: string}> {
    return this.findAllServiciosResponse(params).pipe(
      __map(_r => _r.body as {result?: string, status?: number, data?: any, userMessage?: string, actions?: string})
    );
  }

  /**
   * Registra un cuadro horario nuevo
   * @param data Datos del cuadro horario a registrar
   * @return Cuadro horario creado correctamente.
   */
  saveServicioResponse(data: ServicioAPISaveRequest): __Observable<__StrictHttpResponse<ServicioAPIResponseData>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = data;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v2/servicios`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ServicioAPIResponseData>;
      })
    );
  }
  /**
   * Registra un cuadro horario nuevo
   * @param data Datos del cuadro horario a registrar
   * @return Cuadro horario creado correctamente.
   */
  saveServicio(data: ServicioAPISaveRequest): __Observable<ServicioAPIResponseData> {
    return this.saveServicioResponse(data).pipe(
      __map(_r => _r.body as ServicioAPIResponseData)
    );
  }

  /**
   * Obtiene un cuadro horario por ID
   * @param id Id de cuadro horario a buscar
   * @return Respuesta exitosa.
   */
  findServicioResponse(id: number): __Observable<__StrictHttpResponse<ServicioAPIResponseData>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/servicios/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ServicioAPIResponseData>;
      })
    );
  }
  /**
   * Obtiene un cuadro horario por ID
   * @param id Id de cuadro horario a buscar
   * @return Respuesta exitosa.
   */
  findServicio(id: number): __Observable<ServicioAPIResponseData> {
    return this.findServicioResponse(id).pipe(
      __map(_r => _r.body as ServicioAPIResponseData)
    );
  }

  /**
   * Actualiza un cuadro horario por ID
   * @param params The `ServicioService.PutApiV2ServiciosIdParams` containing the following parameters:
   *
   * - `id`: Id del cuadro horario a actualizar
   *
   * - `data`: Datos del cuadro horario a actualizar
   *
   * @return Cuadro horario guardado correctamente.
   */
  putApiV2ServiciosIdResponse(params: ServicioService.PutApiV2ServiciosIdParams): __Observable<__StrictHttpResponse<ServicioAPIResponseData>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.data;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v2/servicios/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ServicioAPIResponseData>;
      })
    );
  }
  /**
   * Actualiza un cuadro horario por ID
   * @param params The `ServicioService.PutApiV2ServiciosIdParams` containing the following parameters:
   *
   * - `id`: Id del cuadro horario a actualizar
   *
   * - `data`: Datos del cuadro horario a actualizar
   *
   * @return Cuadro horario guardado correctamente.
   */
  putApiV2ServiciosId(params: ServicioService.PutApiV2ServiciosIdParams): __Observable<ServicioAPIResponseData> {
    return this.putApiV2ServiciosIdResponse(params).pipe(
      __map(_r => _r.body as ServicioAPIResponseData)
    );
  }

  /**
   * Elimina un cuadro horario que se encuentra en estado borrador
   * @param id Id del cuadro horario a anular
   * @return Cuadro horario anulado correctamente.
   */
  deleteApiV2ServiciosIdResponse(id: number): __Observable<__StrictHttpResponse<ServicioAPIResponseData>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v2/servicios/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ServicioAPIResponseData>;
      })
    );
  }
  /**
   * Elimina un cuadro horario que se encuentra en estado borrador
   * @param id Id del cuadro horario a anular
   * @return Cuadro horario anulado correctamente.
   */
  deleteApiV2ServiciosId(id: number): __Observable<ServicioAPIResponseData> {
    return this.deleteApiV2ServiciosIdResponse(id).pipe(
      __map(_r => _r.body as ServicioAPIResponseData)
    );
  }

  /**
   * Obtiene los horarios de un servicio por ID
   * @param id Id de cuadro horario a buscar
   * @return Respuesta exitosa.
   */
  findAllHorariosByServicioResponse(id: number): __Observable<__StrictHttpResponse<ServicioHorarioAPIResponseLista>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/servicios/${encodeURIComponent(id)}/horarios`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ServicioHorarioAPIResponseLista>;
      })
    );
  }
  /**
   * Obtiene los horarios de un servicio por ID
   * @param id Id de cuadro horario a buscar
   * @return Respuesta exitosa.
   */
  findAllHorariosByServicio(id: number): __Observable<ServicioHorarioAPIResponseLista> {
    return this.findAllHorariosByServicioResponse(id).pipe(
      __map(_r => _r.body as ServicioHorarioAPIResponseLista)
    );
  }

  /**
   * Permite agregar una lista de horarios al cuadro horario
   * @param params The `ServicioService.PostApiV2ServiciosIdHorariosParams` containing the following parameters:
   *
   * - `id`: Id del cuadro horario a actualizar
   *
   * - `data`: Listado de horarios a agregar
   *
   * @return Horarios agregados correctamente.
   */
  postApiV2ServiciosIdHorariosResponse(params: ServicioService.PostApiV2ServiciosIdHorariosParams): __Observable<__StrictHttpResponse<ServicioHorarioAPIResponseLista>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.data;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v2/servicios/${encodeURIComponent(params.id)}/horarios`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ServicioHorarioAPIResponseLista>;
      })
    );
  }
  /**
   * Permite agregar una lista de horarios al cuadro horario
   * @param params The `ServicioService.PostApiV2ServiciosIdHorariosParams` containing the following parameters:
   *
   * - `id`: Id del cuadro horario a actualizar
   *
   * - `data`: Listado de horarios a agregar
   *
   * @return Horarios agregados correctamente.
   */
  postApiV2ServiciosIdHorarios(params: ServicioService.PostApiV2ServiciosIdHorariosParams): __Observable<ServicioHorarioAPIResponseLista> {
    return this.postApiV2ServiciosIdHorariosResponse(params).pipe(
      __map(_r => _r.body as ServicioHorarioAPIResponseLista)
    );
  }

  /**
   * Obtiene los días de un cuadro horario por ID
   * @param id ID de cuadro horario a buscar
   * @return Respuesta exitosa.
   */
  findAllDiasByServicioResponse(id: number): __Observable<__StrictHttpResponse<DiaServicioAPIResponseLista>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/servicios/${encodeURIComponent(id)}/dias`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<DiaServicioAPIResponseLista>;
      })
    );
  }
  /**
   * Obtiene los días de un cuadro horario por ID
   * @param id ID de cuadro horario a buscar
   * @return Respuesta exitosa.
   */
  findAllDiasByServicio(id: number): __Observable<DiaServicioAPIResponseLista> {
    return this.findAllDiasByServicioResponse(id).pipe(
      __map(_r => _r.body as DiaServicioAPIResponseLista)
    );
  }

  /**
   * Permite agregar una lista de días al cuadro horario
   * @param params The `ServicioService.PostApiV2ServiciosIdDiasParams` containing the following parameters:
   *
   * - `id`: Id del cuadro horario a actualizar
   *
   * - `data`: Listado de días a agregar
   *
   * @return Días agregados correctamente.
   */
  postApiV2ServiciosIdDiasResponse(params: ServicioService.PostApiV2ServiciosIdDiasParams): __Observable<__StrictHttpResponse<DiaServicioAPIResponseLista>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.data;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v2/servicios/${encodeURIComponent(params.id)}/dias`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<DiaServicioAPIResponseLista>;
      })
    );
  }
  /**
   * Permite agregar una lista de días al cuadro horario
   * @param params The `ServicioService.PostApiV2ServiciosIdDiasParams` containing the following parameters:
   *
   * - `id`: Id del cuadro horario a actualizar
   *
   * - `data`: Listado de días a agregar
   *
   * @return Días agregados correctamente.
   */
  postApiV2ServiciosIdDias(params: ServicioService.PostApiV2ServiciosIdDiasParams): __Observable<DiaServicioAPIResponseLista> {
    return this.postApiV2ServiciosIdDiasResponse(params).pipe(
      __map(_r => _r.body as DiaServicioAPIResponseLista)
    );
  }

  /**
   * Obtiene los cuadros horarios con origen y destino buscados
   * @param params The `ServicioService.GetApiV2ServiciosIdOrigenIdDestinoOrigenDestinoParams` containing the following parameters:
   *
   * - `idOrigen`: Id de la Localidad Origen
   *
   * - `idDestino`: Id de la Localidad Destino
   *
   * @return Respuesta exitosa.
   */
  getApiV2ServiciosIdOrigenIdDestinoOrigenDestinoResponse(params: ServicioService.GetApiV2ServiciosIdOrigenIdDestinoOrigenDestinoParams): __Observable<__StrictHttpResponse<ServicioAPIResponseLista>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/servicios/${encodeURIComponent(params.idOrigen)}-${encodeURIComponent(params.idDestino)}/origenDestino`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ServicioAPIResponseLista>;
      })
    );
  }
  /**
   * Obtiene los cuadros horarios con origen y destino buscados
   * @param params The `ServicioService.GetApiV2ServiciosIdOrigenIdDestinoOrigenDestinoParams` containing the following parameters:
   *
   * - `idOrigen`: Id de la Localidad Origen
   *
   * - `idDestino`: Id de la Localidad Destino
   *
   * @return Respuesta exitosa.
   */
  getApiV2ServiciosIdOrigenIdDestinoOrigenDestino(params: ServicioService.GetApiV2ServiciosIdOrigenIdDestinoOrigenDestinoParams): __Observable<ServicioAPIResponseLista> {
    return this.getApiV2ServiciosIdOrigenIdDestinoOrigenDestinoResponse(params).pipe(
      __map(_r => _r.body as ServicioAPIResponseLista)
    );
  }

  /**
   * Aprueba un cuadro horario por ID. Adicionalmente reemplaza un cuadro horario aprobado previamente para el mismo recorrido sentido (si existe)
   * @param id Id de cuadro horario a aprobar
   * @return Cuadro horario aprobado exitosamente.
   */
  putApiV2ServiciosIdAprobarResponse(id: number): __Observable<__StrictHttpResponse<any>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v2/servicios/${encodeURIComponent(id)}/aprobar`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<any>;
      })
    );
  }
  /**
   * Aprueba un cuadro horario por ID. Adicionalmente reemplaza un cuadro horario aprobado previamente para el mismo recorrido sentido (si existe)
   * @param id Id de cuadro horario a aprobar
   * @return Cuadro horario aprobado exitosamente.
   */
  putApiV2ServiciosIdAprobar(id: number): __Observable<any> {
    return this.putApiV2ServiciosIdAprobarResponse(id).pipe(
      __map(_r => _r.body as any)
    );
  }

  /**
   * Rechaza un cuadro horario por ID
   * @param params The `ServicioService.PutApiV2ServiciosIdRechazarParams` containing the following parameters:
   *
   * - `id`: Id de cuadro horario a rechazar
   *
   * - `data`: Datos del rechazo del cuadro horario
   *
   * @return Cuadro horario rechazado exitosamente.
   */
  putApiV2ServiciosIdRechazarResponse(params: ServicioService.PutApiV2ServiciosIdRechazarParams): __Observable<__StrictHttpResponse<any>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.data;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v2/servicios/${encodeURIComponent(params.id)}/rechazar`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<any>;
      })
    );
  }
  /**
   * Rechaza un cuadro horario por ID
   * @param params The `ServicioService.PutApiV2ServiciosIdRechazarParams` containing the following parameters:
   *
   * - `id`: Id de cuadro horario a rechazar
   *
   * - `data`: Datos del rechazo del cuadro horario
   *
   * @return Cuadro horario rechazado exitosamente.
   */
  putApiV2ServiciosIdRechazar(params: ServicioService.PutApiV2ServiciosIdRechazarParams): __Observable<any> {
    return this.putApiV2ServiciosIdRechazarResponse(params).pipe(
      __map(_r => _r.body as any)
    );
  }

  /**
   * Envía a CNRT para revisión un cuadro horario por ID
   * @param id Id de cuadro horario a enviar a CNRT para revisión
   * @return Cuadro horario enviado a CNRT para su revisión exitosamente.
   */
  putApiV2ServiciosIdEnviaracnrtResponse(id: number): __Observable<__StrictHttpResponse<any>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v2/servicios/${encodeURIComponent(id)}/enviaracnrt`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<any>;
      })
    );
  }
  /**
   * Envía a CNRT para revisión un cuadro horario por ID
   * @param id Id de cuadro horario a enviar a CNRT para revisión
   * @return Cuadro horario enviado a CNRT para su revisión exitosamente.
   */
  putApiV2ServiciosIdEnviaracnrt(id: number): __Observable<any> {
    return this.putApiV2ServiciosIdEnviaracnrtResponse(id).pipe(
      __map(_r => _r.body as any)
    );
  }

  /**
   * Pone en estado de EN EVALUACIÓN un cuadro horario por ID
   * @param id Id de cuadro horario que será evaluado por CNRT
   * @return Estado de cuadro horario cambiado a EN EVALUACIÓN exitosamente.
   */
  putApiV2ServiciosIdEnevaluacionResponse(id: number): __Observable<__StrictHttpResponse<any>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v2/servicios/${encodeURIComponent(id)}/enevaluacion`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<any>;
      })
    );
  }
  /**
   * Pone en estado de EN EVALUACIÓN un cuadro horario por ID
   * @param id Id de cuadro horario que será evaluado por CNRT
   * @return Estado de cuadro horario cambiado a EN EVALUACIÓN exitosamente.
   */
  putApiV2ServiciosIdEnevaluacion(id: number): __Observable<any> {
    return this.putApiV2ServiciosIdEnevaluacionResponse(id).pipe(
      __map(_r => _r.body as any)
    );
  }

  /**
   * Valida un VEP para la creación de un cuadro horario
   * @param params The `ServicioService.GetApiV2ServiciosValidacionVepParams` containing the following parameters:
   *
   * - `nroBoletaPago`: Número de boleta de pago (VEP) a validar.
   *
   * - `empresaId`: Id de Empresa para la cual se quiere realizar el trámite.
   *
   * @return El VEP ha sido validado exitosamente.
   */
  getApiV2ServiciosValidacionVepResponse(params: ServicioService.GetApiV2ServiciosValidacionVepParams): __Observable<__StrictHttpResponse<any>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.nroBoletaPago != null) __params = __params.set('nroBoletaPago', params.nroBoletaPago.toString());
    if (params.empresaId != null) __params = __params.set('empresaId', params.empresaId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/servicios/validacionVep`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<any>;
      })
    );
  }
  /**
   * Valida un VEP para la creación de un cuadro horario
   * @param params The `ServicioService.GetApiV2ServiciosValidacionVepParams` containing the following parameters:
   *
   * - `nroBoletaPago`: Número de boleta de pago (VEP) a validar.
   *
   * - `empresaId`: Id de Empresa para la cual se quiere realizar el trámite.
   *
   * @return El VEP ha sido validado exitosamente.
   */
  getApiV2ServiciosValidacionVep(params: ServicioService.GetApiV2ServiciosValidacionVepParams): __Observable<any> {
    return this.getApiV2ServiciosValidacionVepResponse(params).pipe(
      __map(_r => _r.body as any)
    );
  }

  /**
   * Da de baja un cuadro horario por ID
   * @param params The `ServicioService.PutApiV2ServiciosIdBajaParams` containing the following parameters:
   *
   * - `id`: Id de cuadro horario a dar de baja
   *
   * - `data`: Datos de la baja del cuadro horario
   *
   * @return Cuadro horario dado de baja exitosamente.
   */
  putApiV2ServiciosIdBajaResponse(params: ServicioService.PutApiV2ServiciosIdBajaParams): __Observable<__StrictHttpResponse<any>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.data;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v2/servicios/${encodeURIComponent(params.id)}/baja`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<any>;
      })
    );
  }
  /**
   * Da de baja un cuadro horario por ID
   * @param params The `ServicioService.PutApiV2ServiciosIdBajaParams` containing the following parameters:
   *
   * - `id`: Id de cuadro horario a dar de baja
   *
   * - `data`: Datos de la baja del cuadro horario
   *
   * @return Cuadro horario dado de baja exitosamente.
   */
  putApiV2ServiciosIdBaja(params: ServicioService.PutApiV2ServiciosIdBajaParams): __Observable<any> {
    return this.putApiV2ServiciosIdBajaResponse(params).pipe(
      __map(_r => _r.body as any)
    );
  }
}

module ServicioService {

  /**
   * Parameters for findAllServicios
   */
  export interface FindAllServiciosParams {

    /**
     * Filtra los cuadros horarios con vigenciaHasta mayor o menor a la fecha de hoy: vigente=true (devuelve los cuadros horarios con vigenciaHasta mayor a la fecha de hoy o con vigenciaHasta = null // vigente=false (devuelve los servicios con vigenciaHasta menor o igual a la fecha de hoy - es decir, cuadros horarios que perdieron su vigencia - ). Este filtro se puede usar junto con los atributos de criteriaBuilder. ejemplo: ...&id[eq]=23%&vigente=true
     */
    vigente?: string;
    vigenciaHastaNe?: any;
    vigenciaHastaLt?: any;
    vigenciaHastaLe?: any;
    vigenciaHastaGt?: any;
    vigenciaHastaGe?: any;
    vigenciaHasta?: any;
    vigenciaDesdeNe?: any;
    vigenciaDesdeLt?: any;
    vigenciaDesdeLe?: any;
    vigenciaDesdeGt?: any;
    vigenciaDesdeGe?: any;
    vigenciaDesde?: any;
    velocidadPromedioTotalNe?: number;
    velocidadPromedioTotalLt?: number;
    velocidadPromedioTotalLe?: number;
    velocidadPromedioTotalGt?: number;
    velocidadPromedioTotalGe?: number;
    velocidadPromedioTotal?: number;
    tramiteUsuarioNe?: string;
    tramiteUsuarioLike?: string;
    tramiteUsuario?: string;
    tramiteTipoTramiteIdNe?: number;
    tramiteTipoTramiteIdLt?: number;
    tramiteTipoTramiteIdLe?: number;
    tramiteTipoTramiteIdGt?: number;
    tramiteTipoTramiteIdGe?: number;
    tramiteTipoTramiteId?: number;
    tramiteTipoTramiteDescripcionNe?: string;
    tramiteTipoTramiteDescripcionLike?: string;
    tramiteTipoTramiteDescripcion?: string;
    tramiteTipoTramiteActivo?: boolean;
    tramiteSubtipoTramiteIdNe?: number;
    tramiteSubtipoTramiteIdLt?: number;
    tramiteSubtipoTramiteIdLe?: number;
    tramiteSubtipoTramiteIdGt?: number;
    tramiteSubtipoTramiteIdGe?: number;
    tramiteSubtipoTramiteId?: number;
    tramiteSubtipoTramiteDescripcionNe?: string;
    tramiteSubtipoTramiteDescripcionLike?: string;
    tramiteSubtipoTramiteDescripcion?: string;
    tramiteSubtipoTramiteActivo?: boolean;
    tramiteNroBoletaPagoNe?: number;
    tramiteNroBoletaPagoLt?: number;
    tramiteNroBoletaPagoLe?: number;
    tramiteNroBoletaPagoGt?: number;
    tramiteNroBoletaPagoGe?: number;
    tramiteNroBoletaPago?: number;
    tramiteIdNe?: number;
    tramiteIdLt?: number;
    tramiteIdLe?: number;
    tramiteIdGt?: number;
    tramiteIdGe?: number;
    tramiteId?: number;
    tramiteEstadoTramiteIdNe?: any;
    tramiteEstadoTramiteIdLt?: any;
    tramiteEstadoTramiteIdLe?: any;
    tramiteEstadoTramiteIdGt?: any;
    tramiteEstadoTramiteIdGe?: any;
    tramiteEstadoTramiteId?: any;
    tramiteEstadoTramiteDescripcionNe?: string;
    tramiteEstadoTramiteDescripcionLike?: string;
    tramiteEstadoTramiteDescripcion?: string;
    tramiteEstadoTramiteActivo?: boolean;
    tramiteEstadoTramiteAbrevNe?: string;
    tramiteEstadoTramiteAbrevLike?: string;
    tramiteEstadoTramiteAbrev?: string;
    tramiteEmpresaVigenciaHastaNe?: any;
    tramiteEmpresaVigenciaHastaLt?: any;
    tramiteEmpresaVigenciaHastaLe?: any;
    tramiteEmpresaVigenciaHastaGt?: any;
    tramiteEmpresaVigenciaHastaGe?: any;
    tramiteEmpresaVigenciaHasta?: any;
    tramiteEmpresaVigenciaDesdeNe?: any;
    tramiteEmpresaVigenciaDesdeLt?: any;
    tramiteEmpresaVigenciaDesdeLe?: any;
    tramiteEmpresaVigenciaDesdeGt?: any;
    tramiteEmpresaVigenciaDesdeGe?: any;
    tramiteEmpresaVigenciaDesde?: any;
    tramiteEmpresaTipoSociedadNe?: number;
    tramiteEmpresaTipoSociedadLt?: number;
    tramiteEmpresaTipoSociedadLe?: number;
    tramiteEmpresaTipoSociedadGt?: number;
    tramiteEmpresaTipoSociedadGe?: number;
    tramiteEmpresaTipoSociedad?: number;
    tramiteEmpresaTipoDocumentoIdNe?: number;
    tramiteEmpresaTipoDocumentoIdLt?: number;
    tramiteEmpresaTipoDocumentoIdLe?: number;
    tramiteEmpresaTipoDocumentoIdGt?: number;
    tramiteEmpresaTipoDocumentoIdGe?: number;
    tramiteEmpresaTipoDocumentoId?: number;
    tramiteEmpresaTipoDocumentoDescripcionNe?: string;
    tramiteEmpresaTipoDocumentoDescripcionLike?: string;
    tramiteEmpresaTipoDocumentoDescripcion?: string;
    tramiteEmpresaTipoDocumentoAbrevNe?: any;
    tramiteEmpresaTipoDocumentoAbrevLike?: any;
    tramiteEmpresaTipoDocumentoAbrev?: any;
    tramiteEmpresaTextoHash?: string;
    tramiteEmpresaRazonSocialNe?: string;
    tramiteEmpresaRazonSocialLike?: string;
    tramiteEmpresaRazonSocial?: string;
    tramiteEmpresaPautNe?: string;
    tramiteEmpresaPautLike?: string;
    tramiteEmpresaPaut?: string;
    tramiteEmpresaObservacionNe?: string;
    tramiteEmpresaObservacionLike?: string;
    tramiteEmpresaObservacion?: string;
    tramiteEmpresaNroDocumentoNe?: string;
    tramiteEmpresaNroDocumentoLike?: string;
    tramiteEmpresaNroDocumento?: string;
    tramiteEmpresaNombreFantasiaNe?: string;
    tramiteEmpresaNombreFantasiaLike?: string;
    tramiteEmpresaNombreFantasia?: string;
    tramiteEmpresaIdNe?: number;
    tramiteEmpresaIdLt?: number;
    tramiteEmpresaIdLe?: number;
    tramiteEmpresaIdGt?: number;
    tramiteEmpresaIdGe?: number;
    tramiteEmpresaId?: number;
    tramiteEmpresaEstadoEmpresaNe?: number;
    tramiteEmpresaEstadoEmpresaLt?: number;
    tramiteEmpresaEstadoEmpresaLe?: number;
    tramiteEmpresaEstadoEmpresaGt?: number;
    tramiteEmpresaEstadoEmpresaGe?: number;
    tramiteEmpresaEstadoEmpresa?: number;
    tramiteEmpresaEsPersonaFisica?: boolean;
    tramiteEmpresaEmailNe?: string;
    tramiteEmpresaEmailLike?: string;
    tramiteEmpresaEmail?: string;
    tramiteEmpresaCuitEmpresaAnteriorNe?: string;
    tramiteEmpresaCuitEmpresaAnteriorLike?: string;
    tramiteEmpresaCuitEmpresaAnterior?: string;

    /**
     * Campo por el cual se ordena.
     */
    sort?: string;
    recorridoSentidoVinculacionCamineraNe?: string;
    recorridoSentidoVinculacionCamineraLike?: string;
    recorridoSentidoVinculacionCaminera?: string;
    recorridoSentidoVigenciaHastaNe?: any;
    recorridoSentidoVigenciaHastaLt?: any;
    recorridoSentidoVigenciaHastaLe?: any;
    recorridoSentidoVigenciaHastaGt?: any;
    recorridoSentidoVigenciaHastaGe?: any;
    recorridoSentidoVigenciaHasta?: any;
    recorridoSentidoVigenciaDesdeNe?: any;
    recorridoSentidoVigenciaDesdeLt?: any;
    recorridoSentidoVigenciaDesdeLe?: any;
    recorridoSentidoVigenciaDesdeGt?: any;
    recorridoSentidoVigenciaDesdeGe?: any;
    recorridoSentidoVigenciaDesde?: any;
    recorridoSentidoSentidoIdNe?: number;
    recorridoSentidoSentidoIdLt?: number;
    recorridoSentidoSentidoIdLe?: number;
    recorridoSentidoSentidoIdGt?: number;
    recorridoSentidoSentidoIdGe?: number;
    recorridoSentidoSentidoId?: number;
    recorridoSentidoSentidoDescripcionNe?: string;
    recorridoSentidoSentidoDescripcionLike?: string;
    recorridoSentidoSentidoDescripcion?: string;
    recorridoSentidoSentidoActivo?: boolean;
    recorridoSentidoSentidoAbrevNe?: string;
    recorridoSentidoSentidoAbrevLike?: string;
    recorridoSentidoSentidoAbrev?: string;
    recorridoSentidoRecorridoVinculacionCamineraNe?: string;
    recorridoSentidoRecorridoVinculacionCamineraLike?: string;
    recorridoSentidoRecorridoVinculacionCaminera?: string;
    recorridoSentidoRecorridoVigenciaHastaNe?: any;
    recorridoSentidoRecorridoVigenciaHastaLt?: any;
    recorridoSentidoRecorridoVigenciaHastaLe?: any;
    recorridoSentidoRecorridoVigenciaHastaGt?: any;
    recorridoSentidoRecorridoVigenciaHastaGe?: any;
    recorridoSentidoRecorridoVigenciaHasta?: any;
    recorridoSentidoRecorridoVigenciaDesdeNe?: any;
    recorridoSentidoRecorridoVigenciaDesdeLt?: any;
    recorridoSentidoRecorridoVigenciaDesdeLe?: any;
    recorridoSentidoRecorridoVigenciaDesdeGt?: any;
    recorridoSentidoRecorridoVigenciaDesdeGe?: any;
    recorridoSentidoRecorridoVigenciaDesde?: any;
    recorridoSentidoRecorridoTransitoNe?: string;
    recorridoSentidoRecorridoTransitoLike?: string;
    recorridoSentidoRecorridoTransito?: string;
    recorridoSentidoRecorridoSitioOrigenNe?: string;
    recorridoSentidoRecorridoSitioOrigenLike?: string;
    recorridoSentidoRecorridoSitioOrigen?: string;
    recorridoSentidoRecorridoSitioDestinoNe?: string;
    recorridoSentidoRecorridoSitioDestinoLike?: string;
    recorridoSentidoRecorridoSitioDestino?: string;
    recorridoSentidoRecorridoRecorridoNe?: string;
    recorridoSentidoRecorridoRecorridoLike?: string;
    recorridoSentidoRecorridoRecorrido?: string;
    recorridoSentidoRecorridoOrigenNe?: string;
    recorridoSentidoRecorridoOrigenLike?: string;
    recorridoSentidoRecorridoOrigen?: string;
    recorridoSentidoRecorridoItinerarioVueltaNe?: string;
    recorridoSentidoRecorridoItinerarioVueltaLike?: string;
    recorridoSentidoRecorridoItinerarioVuelta?: string;
    recorridoSentidoRecorridoItinerarioIdaNe?: string;
    recorridoSentidoRecorridoItinerarioIdaLike?: string;
    recorridoSentidoRecorridoItinerarioIda?: string;
    recorridoSentidoRecorridoIdNe?: number;
    recorridoSentidoRecorridoIdLt?: number;
    recorridoSentidoRecorridoIdLe?: number;
    recorridoSentidoRecorridoIdGt?: number;
    recorridoSentidoRecorridoIdGe?: number;
    recorridoSentidoRecorridoId?: number;
    recorridoSentidoRecorridoDestinoNe?: string;
    recorridoSentidoRecorridoDestinoLike?: string;
    recorridoSentidoRecorridoDestino?: string;
    recorridoSentidoRecorridoAplicaGasoil?: boolean;
    recorridoSentidoOrigenNe?: string;
    recorridoSentidoOrigenLike?: string;
    recorridoSentidoOrigen?: string;
    recorridoSentidoLocalidadOrigenUpdatedAtNe?: any;
    recorridoSentidoLocalidadOrigenUpdatedAtLt?: any;
    recorridoSentidoLocalidadOrigenUpdatedAtLe?: any;
    recorridoSentidoLocalidadOrigenUpdatedAtGt?: any;
    recorridoSentidoLocalidadOrigenUpdatedAtGe?: any;
    recorridoSentidoLocalidadOrigenUpdatedAt?: any;
    recorridoSentidoLocalidadOrigenProvinciaPaisIdNe?: number;
    recorridoSentidoLocalidadOrigenProvinciaPaisIdLt?: number;
    recorridoSentidoLocalidadOrigenProvinciaPaisIdLe?: number;
    recorridoSentidoLocalidadOrigenProvinciaPaisIdGt?: number;
    recorridoSentidoLocalidadOrigenProvinciaPaisIdGe?: number;
    recorridoSentidoLocalidadOrigenProvinciaPaisId?: number;
    recorridoSentidoLocalidadOrigenProvinciaPaisDescripcionNe?: string;
    recorridoSentidoLocalidadOrigenProvinciaPaisDescripcionLike?: string;
    recorridoSentidoLocalidadOrigenProvinciaPaisDescripcion?: string;
    recorridoSentidoLocalidadOrigenProvinciaPaisCodigoNe?: string;
    recorridoSentidoLocalidadOrigenProvinciaPaisCodigoLike?: string;
    recorridoSentidoLocalidadOrigenProvinciaPaisCodigo?: string;
    recorridoSentidoLocalidadOrigenProvinciaPaisActivo?: boolean;
    recorridoSentidoLocalidadOrigenProvinciaPaisAbrevNe?: any;
    recorridoSentidoLocalidadOrigenProvinciaPaisAbrevLike?: any;
    recorridoSentidoLocalidadOrigenProvinciaPaisAbrev?: any;
    recorridoSentidoLocalidadOrigenProvinciaIdNe?: number;
    recorridoSentidoLocalidadOrigenProvinciaIdLt?: number;
    recorridoSentidoLocalidadOrigenProvinciaIdLe?: number;
    recorridoSentidoLocalidadOrigenProvinciaIdGt?: number;
    recorridoSentidoLocalidadOrigenProvinciaIdGe?: number;
    recorridoSentidoLocalidadOrigenProvinciaId?: number;
    recorridoSentidoLocalidadOrigenProvinciaDescripcionNe?: string;
    recorridoSentidoLocalidadOrigenProvinciaDescripcionLike?: string;
    recorridoSentidoLocalidadOrigenProvinciaDescripcion?: string;
    recorridoSentidoLocalidadOrigenProvinciaCodigoNe?: string;
    recorridoSentidoLocalidadOrigenProvinciaCodigoLike?: string;
    recorridoSentidoLocalidadOrigenProvinciaCodigoINDECNe?: string;
    recorridoSentidoLocalidadOrigenProvinciaCodigoINDECLike?: string;
    recorridoSentidoLocalidadOrigenProvinciaCodigoINDEC?: string;
    recorridoSentidoLocalidadOrigenProvinciaCodigo?: string;
    recorridoSentidoLocalidadOrigenProvinciaActivo?: boolean;
    recorridoSentidoLocalidadOrigenParada?: any;
    recorridoSentidoLocalidadOrigenLonNe?: number;
    recorridoSentidoLocalidadOrigenLonLt?: number;
    recorridoSentidoLocalidadOrigenLonLe?: number;
    recorridoSentidoLocalidadOrigenLonGt?: number;
    recorridoSentidoLocalidadOrigenLonGe?: number;
    recorridoSentidoLocalidadOrigenLon?: number;
    recorridoSentidoLocalidadOrigenLatNe?: number;
    recorridoSentidoLocalidadOrigenLatLt?: number;
    recorridoSentidoLocalidadOrigenLatLe?: number;
    recorridoSentidoLocalidadOrigenLatGt?: number;
    recorridoSentidoLocalidadOrigenLatGe?: number;
    recorridoSentidoLocalidadOrigenLat?: number;
    recorridoSentidoLocalidadOrigenIdNe?: number;
    recorridoSentidoLocalidadOrigenIdLt?: number;
    recorridoSentidoLocalidadOrigenIdLe?: number;
    recorridoSentidoLocalidadOrigenIdGt?: number;
    recorridoSentidoLocalidadOrigenIdGe?: number;
    recorridoSentidoLocalidadOrigenId?: number;
    recorridoSentidoLocalidadOrigenFuenteNe?: string;
    recorridoSentidoLocalidadOrigenFuenteLike?: string;
    recorridoSentidoLocalidadOrigenFuente?: string;
    recorridoSentidoLocalidadOrigenDescripcionNe?: string;
    recorridoSentidoLocalidadOrigenDescripcionLike?: string;
    recorridoSentidoLocalidadOrigenDescripcion?: string;
    recorridoSentidoLocalidadOrigenCreatedAtNe?: any;
    recorridoSentidoLocalidadOrigenCreatedAtLt?: any;
    recorridoSentidoLocalidadOrigenCreatedAtLe?: any;
    recorridoSentidoLocalidadOrigenCreatedAtGt?: any;
    recorridoSentidoLocalidadOrigenCreatedAtGe?: any;
    recorridoSentidoLocalidadOrigenCreatedAt?: any;
    recorridoSentidoLocalidadOrigenCodigoINDECNe?: string;
    recorridoSentidoLocalidadOrigenCodigoINDECLike?: string;
    recorridoSentidoLocalidadOrigenCodigoINDEC?: string;
    recorridoSentidoLocalidadOrigenActivo?: boolean;
    recorridoSentidoLocalidadDestinoUpdatedAtNe?: any;
    recorridoSentidoLocalidadDestinoUpdatedAtLt?: any;
    recorridoSentidoLocalidadDestinoUpdatedAtLe?: any;
    recorridoSentidoLocalidadDestinoUpdatedAtGt?: any;
    recorridoSentidoLocalidadDestinoUpdatedAtGe?: any;
    recorridoSentidoLocalidadDestinoUpdatedAt?: any;
    recorridoSentidoLocalidadDestinoProvinciaPaisIdNe?: number;
    recorridoSentidoLocalidadDestinoProvinciaPaisIdLt?: number;
    recorridoSentidoLocalidadDestinoProvinciaPaisIdLe?: number;
    recorridoSentidoLocalidadDestinoProvinciaPaisIdGt?: number;
    recorridoSentidoLocalidadDestinoProvinciaPaisIdGe?: number;
    recorridoSentidoLocalidadDestinoProvinciaPaisId?: number;
    recorridoSentidoLocalidadDestinoProvinciaPaisDescripcionNe?: string;
    recorridoSentidoLocalidadDestinoProvinciaPaisDescripcionLike?: string;
    recorridoSentidoLocalidadDestinoProvinciaPaisDescripcion?: string;
    recorridoSentidoLocalidadDestinoProvinciaPaisCodigoNe?: string;
    recorridoSentidoLocalidadDestinoProvinciaPaisCodigoLike?: string;
    recorridoSentidoLocalidadDestinoProvinciaPaisCodigo?: string;
    recorridoSentidoLocalidadDestinoProvinciaPaisActivo?: boolean;
    recorridoSentidoLocalidadDestinoProvinciaPaisAbrevNe?: any;
    recorridoSentidoLocalidadDestinoProvinciaPaisAbrevLike?: any;
    recorridoSentidoLocalidadDestinoProvinciaPaisAbrev?: any;
    recorridoSentidoLocalidadDestinoProvinciaIdNe?: number;
    recorridoSentidoLocalidadDestinoProvinciaIdLt?: number;
    recorridoSentidoLocalidadDestinoProvinciaIdLe?: number;
    recorridoSentidoLocalidadDestinoProvinciaIdGt?: number;
    recorridoSentidoLocalidadDestinoProvinciaIdGe?: number;
    recorridoSentidoLocalidadDestinoProvinciaId?: number;
    recorridoSentidoLocalidadDestinoProvinciaDescripcionNe?: string;
    recorridoSentidoLocalidadDestinoProvinciaDescripcionLike?: string;
    recorridoSentidoLocalidadDestinoProvinciaDescripcion?: string;
    recorridoSentidoLocalidadDestinoProvinciaCodigoNe?: string;
    recorridoSentidoLocalidadDestinoProvinciaCodigoLike?: string;
    recorridoSentidoLocalidadDestinoProvinciaCodigoINDECNe?: string;
    recorridoSentidoLocalidadDestinoProvinciaCodigoINDECLike?: string;
    recorridoSentidoLocalidadDestinoProvinciaCodigoINDEC?: string;
    recorridoSentidoLocalidadDestinoProvinciaCodigo?: string;
    recorridoSentidoLocalidadDestinoProvinciaActivo?: boolean;
    recorridoSentidoLocalidadDestinoParada?: any;
    recorridoSentidoLocalidadDestinoLonNe?: number;
    recorridoSentidoLocalidadDestinoLonLt?: number;
    recorridoSentidoLocalidadDestinoLonLe?: number;
    recorridoSentidoLocalidadDestinoLonGt?: number;
    recorridoSentidoLocalidadDestinoLonGe?: number;
    recorridoSentidoLocalidadDestinoLon?: number;
    recorridoSentidoLocalidadDestinoLatNe?: number;
    recorridoSentidoLocalidadDestinoLatLt?: number;
    recorridoSentidoLocalidadDestinoLatLe?: number;
    recorridoSentidoLocalidadDestinoLatGt?: number;
    recorridoSentidoLocalidadDestinoLatGe?: number;
    recorridoSentidoLocalidadDestinoLat?: number;
    recorridoSentidoLocalidadDestinoIdNe?: number;
    recorridoSentidoLocalidadDestinoIdLt?: number;
    recorridoSentidoLocalidadDestinoIdLe?: number;
    recorridoSentidoLocalidadDestinoIdGt?: number;
    recorridoSentidoLocalidadDestinoIdGe?: number;
    recorridoSentidoLocalidadDestinoId?: number;
    recorridoSentidoLocalidadDestinoFuenteNe?: string;
    recorridoSentidoLocalidadDestinoFuenteLike?: string;
    recorridoSentidoLocalidadDestinoFuente?: string;
    recorridoSentidoLocalidadDestinoDescripcionNe?: string;
    recorridoSentidoLocalidadDestinoDescripcionLike?: string;
    recorridoSentidoLocalidadDestinoDescripcion?: string;
    recorridoSentidoLocalidadDestinoCreatedAtNe?: any;
    recorridoSentidoLocalidadDestinoCreatedAtLt?: any;
    recorridoSentidoLocalidadDestinoCreatedAtLe?: any;
    recorridoSentidoLocalidadDestinoCreatedAtGt?: any;
    recorridoSentidoLocalidadDestinoCreatedAtGe?: any;
    recorridoSentidoLocalidadDestinoCreatedAt?: any;
    recorridoSentidoLocalidadDestinoCodigoINDECNe?: string;
    recorridoSentidoLocalidadDestinoCodigoINDECLike?: string;
    recorridoSentidoLocalidadDestinoCodigoINDEC?: string;
    recorridoSentidoLocalidadDestinoActivo?: boolean;
    recorridoSentidoItinerarioNe?: string;
    recorridoSentidoItinerarioLike?: string;
    recorridoSentidoItinerario?: string;
    recorridoSentidoIdNe?: number;
    recorridoSentidoIdLt?: number;
    recorridoSentidoIdLe?: number;
    recorridoSentidoIdGt?: number;
    recorridoSentidoIdGe?: number;
    recorridoSentidoId?: number;
    recorridoSentidoDestinoNe?: string;
    recorridoSentidoDestinoLike?: string;
    recorridoSentidoDestino?: string;
    recorridoVinculacionCamineraNe?: string;
    recorridoVinculacionCamineraLike?: string;
    recorridoVinculacionCaminera?: string;
    recorridoVigenciaHastaNe?: any;
    recorridoVigenciaHastaLt?: any;
    recorridoVigenciaHastaLe?: any;
    recorridoVigenciaHastaGt?: any;
    recorridoVigenciaHastaGe?: any;
    recorridoVigenciaHasta?: any;
    recorridoVigenciaDesdeNe?: any;
    recorridoVigenciaDesdeLt?: any;
    recorridoVigenciaDesdeLe?: any;
    recorridoVigenciaDesdeGt?: any;
    recorridoVigenciaDesdeGe?: any;
    recorridoVigenciaDesde?: any;
    recorridoTransitoNe?: string;
    recorridoTransitoLike?: string;
    recorridoTransito?: string;
    recorridoSitioOrigenNe?: string;
    recorridoSitioOrigenLike?: string;
    recorridoSitioOrigen?: string;
    recorridoSitioDestinoNe?: string;
    recorridoSitioDestinoLike?: string;
    recorridoSitioDestino?: string;
    recorridoRecorridoNe?: string;
    recorridoRecorridoLike?: string;
    recorridoRecorrido?: string;
    recorridoOrigenNe?: string;
    recorridoOrigenLike?: string;
    recorridoOrigen?: string;
    recorridoItinerarioVueltaNe?: string;
    recorridoItinerarioVueltaLike?: string;
    recorridoItinerarioVuelta?: string;
    recorridoItinerarioIdaNe?: string;
    recorridoItinerarioIdaLike?: string;
    recorridoItinerarioIda?: string;
    recorridoIdNe?: number;
    recorridoIdLt?: number;
    recorridoIdLe?: number;
    recorridoIdGt?: number;
    recorridoIdGe?: number;
    recorridoId?: number;
    recorridoDestinoNe?: string;
    recorridoDestinoLike?: string;
    recorridoDestino?: string;
    recorridoAplicaGasoil?: boolean;

    /**
     * Método de ordenación, ascendente (ASC) o descendente (DESC).
     */
    order?: 'ASC' | 'DESC';
    operadorVigenciaHastaNe?: any;
    operadorVigenciaHastaLt?: any;
    operadorVigenciaHastaLe?: any;
    operadorVigenciaHastaGt?: any;
    operadorVigenciaHastaGe?: any;
    operadorVigenciaHasta?: any;
    operadorVigenciaDesdeNe?: any;
    operadorVigenciaDesdeLt?: any;
    operadorVigenciaDesdeLe?: any;
    operadorVigenciaDesdeGt?: any;
    operadorVigenciaDesdeGe?: any;
    operadorVigenciaDesde?: any;
    operadorLocalidadOrigenUpdatedAtNe?: any;
    operadorLocalidadOrigenUpdatedAtLt?: any;
    operadorLocalidadOrigenUpdatedAtLe?: any;
    operadorLocalidadOrigenUpdatedAtGt?: any;
    operadorLocalidadOrigenUpdatedAtGe?: any;
    operadorLocalidadOrigenUpdatedAt?: any;
    operadorLocalidadOrigenProvinciaPaisIdNe?: number;
    operadorLocalidadOrigenProvinciaPaisIdLt?: number;
    operadorLocalidadOrigenProvinciaPaisIdLe?: number;
    operadorLocalidadOrigenProvinciaPaisIdGt?: number;
    operadorLocalidadOrigenProvinciaPaisIdGe?: number;
    operadorLocalidadOrigenProvinciaPaisId?: number;
    operadorLocalidadOrigenProvinciaPaisDescripcionNe?: string;
    operadorLocalidadOrigenProvinciaPaisDescripcionLike?: string;
    operadorLocalidadOrigenProvinciaPaisDescripcion?: string;
    operadorLocalidadOrigenProvinciaPaisCodigoNe?: string;
    operadorLocalidadOrigenProvinciaPaisCodigoLike?: string;
    operadorLocalidadOrigenProvinciaPaisCodigo?: string;
    operadorLocalidadOrigenProvinciaPaisActivo?: boolean;
    operadorLocalidadOrigenProvinciaPaisAbrevNe?: any;
    operadorLocalidadOrigenProvinciaPaisAbrevLike?: any;
    operadorLocalidadOrigenProvinciaPaisAbrev?: any;
    operadorLocalidadOrigenProvinciaIdNe?: number;
    operadorLocalidadOrigenProvinciaIdLt?: number;
    operadorLocalidadOrigenProvinciaIdLe?: number;
    operadorLocalidadOrigenProvinciaIdGt?: number;
    operadorLocalidadOrigenProvinciaIdGe?: number;
    operadorLocalidadOrigenProvinciaId?: number;
    operadorLocalidadOrigenProvinciaDescripcionNe?: string;
    operadorLocalidadOrigenProvinciaDescripcionLike?: string;
    operadorLocalidadOrigenProvinciaDescripcion?: string;
    operadorLocalidadOrigenProvinciaCodigoNe?: string;
    operadorLocalidadOrigenProvinciaCodigoLike?: string;
    operadorLocalidadOrigenProvinciaCodigoINDECNe?: string;
    operadorLocalidadOrigenProvinciaCodigoINDECLike?: string;
    operadorLocalidadOrigenProvinciaCodigoINDEC?: string;
    operadorLocalidadOrigenProvinciaCodigo?: string;
    operadorLocalidadOrigenProvinciaActivo?: boolean;
    operadorLocalidadOrigenParada?: any;
    operadorLocalidadOrigenLonNe?: number;
    operadorLocalidadOrigenLonLt?: number;
    operadorLocalidadOrigenLonLe?: number;
    operadorLocalidadOrigenLonGt?: number;
    operadorLocalidadOrigenLonGe?: number;
    operadorLocalidadOrigenLon?: number;
    operadorLocalidadOrigenLatNe?: number;
    operadorLocalidadOrigenLatLt?: number;
    operadorLocalidadOrigenLatLe?: number;
    operadorLocalidadOrigenLatGt?: number;
    operadorLocalidadOrigenLatGe?: number;
    operadorLocalidadOrigenLat?: number;
    operadorLocalidadOrigenIdNe?: number;
    operadorLocalidadOrigenIdLt?: number;
    operadorLocalidadOrigenIdLe?: number;
    operadorLocalidadOrigenIdGt?: number;
    operadorLocalidadOrigenIdGe?: number;
    operadorLocalidadOrigenId?: number;
    operadorLocalidadOrigenFuenteNe?: string;
    operadorLocalidadOrigenFuenteLike?: string;
    operadorLocalidadOrigenFuente?: string;
    operadorLocalidadOrigenDescripcionNe?: string;
    operadorLocalidadOrigenDescripcionLike?: string;
    operadorLocalidadOrigenDescripcion?: string;
    operadorLocalidadOrigenCreatedAtNe?: any;
    operadorLocalidadOrigenCreatedAtLt?: any;
    operadorLocalidadOrigenCreatedAtLe?: any;
    operadorLocalidadOrigenCreatedAtGt?: any;
    operadorLocalidadOrigenCreatedAtGe?: any;
    operadorLocalidadOrigenCreatedAt?: any;
    operadorLocalidadOrigenCodigoINDECNe?: string;
    operadorLocalidadOrigenCodigoINDECLike?: string;
    operadorLocalidadOrigenCodigoINDEC?: string;
    operadorLocalidadOrigenActivo?: boolean;
    operadorLocalidadDestinoUpdatedAtNe?: any;
    operadorLocalidadDestinoUpdatedAtLt?: any;
    operadorLocalidadDestinoUpdatedAtLe?: any;
    operadorLocalidadDestinoUpdatedAtGt?: any;
    operadorLocalidadDestinoUpdatedAtGe?: any;
    operadorLocalidadDestinoUpdatedAt?: any;
    operadorLocalidadDestinoProvinciaPaisIdNe?: number;
    operadorLocalidadDestinoProvinciaPaisIdLt?: number;
    operadorLocalidadDestinoProvinciaPaisIdLe?: number;
    operadorLocalidadDestinoProvinciaPaisIdGt?: number;
    operadorLocalidadDestinoProvinciaPaisIdGe?: number;
    operadorLocalidadDestinoProvinciaPaisId?: number;
    operadorLocalidadDestinoProvinciaPaisDescripcionNe?: string;
    operadorLocalidadDestinoProvinciaPaisDescripcionLike?: string;
    operadorLocalidadDestinoProvinciaPaisDescripcion?: string;
    operadorLocalidadDestinoProvinciaPaisCodigoNe?: string;
    operadorLocalidadDestinoProvinciaPaisCodigoLike?: string;
    operadorLocalidadDestinoProvinciaPaisCodigo?: string;
    operadorLocalidadDestinoProvinciaPaisActivo?: boolean;
    operadorLocalidadDestinoProvinciaPaisAbrevNe?: any;
    operadorLocalidadDestinoProvinciaPaisAbrevLike?: any;
    operadorLocalidadDestinoProvinciaPaisAbrev?: any;
    operadorLocalidadDestinoProvinciaIdNe?: number;
    operadorLocalidadDestinoProvinciaIdLt?: number;
    operadorLocalidadDestinoProvinciaIdLe?: number;
    operadorLocalidadDestinoProvinciaIdGt?: number;
    operadorLocalidadDestinoProvinciaIdGe?: number;
    operadorLocalidadDestinoProvinciaId?: number;
    operadorLocalidadDestinoProvinciaDescripcionNe?: string;
    operadorLocalidadDestinoProvinciaDescripcionLike?: string;
    operadorLocalidadDestinoProvinciaDescripcion?: string;
    operadorLocalidadDestinoProvinciaCodigoNe?: string;
    operadorLocalidadDestinoProvinciaCodigoLike?: string;
    operadorLocalidadDestinoProvinciaCodigoINDECNe?: string;
    operadorLocalidadDestinoProvinciaCodigoINDECLike?: string;
    operadorLocalidadDestinoProvinciaCodigoINDEC?: string;
    operadorLocalidadDestinoProvinciaCodigo?: string;
    operadorLocalidadDestinoProvinciaActivo?: boolean;
    operadorLocalidadDestinoParada?: any;
    operadorLocalidadDestinoLonNe?: number;
    operadorLocalidadDestinoLonLt?: number;
    operadorLocalidadDestinoLonLe?: number;
    operadorLocalidadDestinoLonGt?: number;
    operadorLocalidadDestinoLonGe?: number;
    operadorLocalidadDestinoLon?: number;
    operadorLocalidadDestinoLatNe?: number;
    operadorLocalidadDestinoLatLt?: number;
    operadorLocalidadDestinoLatLe?: number;
    operadorLocalidadDestinoLatGt?: number;
    operadorLocalidadDestinoLatGe?: number;
    operadorLocalidadDestinoLat?: number;
    operadorLocalidadDestinoIdNe?: number;
    operadorLocalidadDestinoIdLt?: number;
    operadorLocalidadDestinoIdLe?: number;
    operadorLocalidadDestinoIdGt?: number;
    operadorLocalidadDestinoIdGe?: number;
    operadorLocalidadDestinoId?: number;
    operadorLocalidadDestinoFuenteNe?: string;
    operadorLocalidadDestinoFuenteLike?: string;
    operadorLocalidadDestinoFuente?: string;
    operadorLocalidadDestinoDescripcionNe?: string;
    operadorLocalidadDestinoDescripcionLike?: string;
    operadorLocalidadDestinoDescripcion?: string;
    operadorLocalidadDestinoCreatedAtNe?: any;
    operadorLocalidadDestinoCreatedAtLt?: any;
    operadorLocalidadDestinoCreatedAtLe?: any;
    operadorLocalidadDestinoCreatedAtGt?: any;
    operadorLocalidadDestinoCreatedAtGe?: any;
    operadorLocalidadDestinoCreatedAt?: any;
    operadorLocalidadDestinoCodigoINDECNe?: string;
    operadorLocalidadDestinoCodigoINDECLike?: string;
    operadorLocalidadDestinoCodigoINDEC?: string;
    operadorLocalidadDestinoActivo?: boolean;
    operadorLineaNe?: string;
    operadorLineaLike?: string;
    operadorLinea?: string;
    operadorIdNe?: number;
    operadorIdLt?: number;
    operadorIdLe?: number;
    operadorIdGt?: number;
    operadorIdGe?: number;
    operadorId?: number;
    operadorEmpresaContratanteNe?: string;
    operadorEmpresaContratanteLike?: string;
    operadorEmpresaContratanteCuitNe?: string;
    operadorEmpresaContratanteCuitLike?: string;
    operadorEmpresaContratanteCuit?: string;
    operadorEmpresaContratante?: string;
    operadorEmpresaVigenciaHastaNe?: any;
    operadorEmpresaVigenciaHastaLt?: any;
    operadorEmpresaVigenciaHastaLe?: any;
    operadorEmpresaVigenciaHastaGt?: any;
    operadorEmpresaVigenciaHastaGe?: any;
    operadorEmpresaVigenciaHasta?: any;
    operadorEmpresaVigenciaDesdeNe?: any;
    operadorEmpresaVigenciaDesdeLt?: any;
    operadorEmpresaVigenciaDesdeLe?: any;
    operadorEmpresaVigenciaDesdeGt?: any;
    operadorEmpresaVigenciaDesdeGe?: any;
    operadorEmpresaVigenciaDesde?: any;
    operadorEmpresaTipoSociedadNe?: number;
    operadorEmpresaTipoSociedadLt?: number;
    operadorEmpresaTipoSociedadLe?: number;
    operadorEmpresaTipoSociedadGt?: number;
    operadorEmpresaTipoSociedadGe?: number;
    operadorEmpresaTipoSociedad?: number;
    operadorEmpresaTipoDocumentoIdNe?: number;
    operadorEmpresaTipoDocumentoIdLt?: number;
    operadorEmpresaTipoDocumentoIdLe?: number;
    operadorEmpresaTipoDocumentoIdGt?: number;
    operadorEmpresaTipoDocumentoIdGe?: number;
    operadorEmpresaTipoDocumentoId?: number;
    operadorEmpresaTipoDocumentoDescripcionNe?: string;
    operadorEmpresaTipoDocumentoDescripcionLike?: string;
    operadorEmpresaTipoDocumentoDescripcion?: string;
    operadorEmpresaTipoDocumentoAbrevNe?: any;
    operadorEmpresaTipoDocumentoAbrevLike?: any;
    operadorEmpresaTipoDocumentoAbrev?: any;
    operadorEmpresaTextoHash?: string;
    operadorEmpresaRazonSocialNe?: string;
    operadorEmpresaRazonSocialLike?: string;
    operadorEmpresaRazonSocial?: string;
    operadorEmpresaPautNe?: string;
    operadorEmpresaPautLike?: string;
    operadorEmpresaPaut?: string;
    operadorEmpresaObservacionNe?: string;
    operadorEmpresaObservacionLike?: string;
    operadorEmpresaObservacion?: string;
    operadorEmpresaNroDocumentoNe?: string;
    operadorEmpresaNroDocumentoLike?: string;
    operadorEmpresaNroDocumento?: string;
    operadorEmpresaNombreFantasiaNe?: string;
    operadorEmpresaNombreFantasiaLike?: string;
    operadorEmpresaNombreFantasia?: string;
    operadorEmpresaIdNe?: number;
    operadorEmpresaIdLt?: number;
    operadorEmpresaIdLe?: number;
    operadorEmpresaIdGt?: number;
    operadorEmpresaIdGe?: number;
    operadorEmpresaId?: number;
    operadorEmpresaEstadoEmpresaNe?: number;
    operadorEmpresaEstadoEmpresaLt?: number;
    operadorEmpresaEstadoEmpresaLe?: number;
    operadorEmpresaEstadoEmpresaGt?: number;
    operadorEmpresaEstadoEmpresaGe?: number;
    operadorEmpresaEstadoEmpresa?: number;
    operadorEmpresaEsPersonaFisica?: boolean;
    operadorEmpresaEmailNe?: string;
    operadorEmpresaEmailLike?: string;
    operadorEmpresaEmail?: string;
    operadorEmpresaCuitEmpresaAnteriorNe?: string;
    operadorEmpresaCuitEmpresaAnteriorLike?: string;
    operadorEmpresaCuitEmpresaAnterior?: string;
    operadorClaseModalidadIdNe?: number;
    operadorClaseModalidadIdLt?: number;
    operadorClaseModalidadIdLe?: number;
    operadorClaseModalidadIdGt?: number;
    operadorClaseModalidadIdGe?: number;
    operadorClaseModalidadId?: number;
    operadorClaseModalidadDescripcionNe?: string;
    operadorClaseModalidadDescripcionLike?: string;
    operadorClaseModalidadDescripcion?: string;

    /**
     * Cantidad de registros que deben omitirse en la consulta.
     */
    offset?: number;

    /**
     * Cantidad máxima de registros que debe regresar la consulta.
     */
    limit?: number;
    idNe?: number;
    idLt?: number;
    idLe?: number;
    idGt?: number;
    idGe?: number;
    id?: number;
    horaSalidaNe?: any;
    horaSalidaLt?: any;
    horaSalidaLe?: any;
    horaSalidaGt?: any;
    horaSalidaGe?: any;
    horaSalida?: any;
    horaLlegadaNe?: any;
    horaLlegadaLt?: any;
    horaLlegadaLe?: any;
    horaLlegadaGt?: any;
    horaLlegadaGe?: any;
    horaLlegada?: any;
    aprobado?: boolean;

    /**
     * Lista de atributos a filtrar: ...&lastname[like]=Gonz%&city->state->code[eq]=AR
     */
    AtributoCriteria?: string;
  }

  /**
   * Parameters for putApiV2ServiciosId
   */
  export interface PutApiV2ServiciosIdParams {

    /**
     * Id del cuadro horario a actualizar
     */
    id: number;

    /**
     * Datos del cuadro horario a actualizar
     */
    data: ServicioAPIUpdateRequest;
  }

  /**
   * Parameters for postApiV2ServiciosIdHorarios
   */
  export interface PostApiV2ServiciosIdHorariosParams {

    /**
     * Id del cuadro horario a actualizar
     */
    id: number;

    /**
     * Listado de horarios a agregar
     */
    data: Array<{nroOrden?: number, parada?: number, horaLlegada?: string, tiempoEspera?: string, horaSalida?: string, diasViajeExtra?: number, posta?: boolean, pasoFronterizo?: boolean, cantidadChoferes?: number, km?: number, tiempoViaje?: string}>;
  }

  /**
   * Parameters for postApiV2ServiciosIdDias
   */
  export interface PostApiV2ServiciosIdDiasParams {

    /**
     * Id del cuadro horario a actualizar
     */
    id: number;

    /**
     * Listado de días a agregar
     */
    data: Array<{periodoFrecuencia?: number, categoriaServicioPlantaBaja?: number, categoriaServicioPrimerPiso?: number, domingo?: boolean, lunes?: boolean, martes?: boolean, miercoles?: boolean, jueves?: boolean, viernes?: boolean, sabado?: boolean}>;
  }

  /**
   * Parameters for getApiV2ServiciosIdOrigenIdDestinoOrigenDestino
   */
  export interface GetApiV2ServiciosIdOrigenIdDestinoOrigenDestinoParams {

    /**
     * Id de la Localidad Origen
     */
    idOrigen: number;

    /**
     * Id de la Localidad Destino
     */
    idDestino: number;
  }

  /**
   * Parameters for putApiV2ServiciosIdRechazar
   */
  export interface PutApiV2ServiciosIdRechazarParams {

    /**
     * Id de cuadro horario a rechazar
     */
    id: number;

    /**
     * Datos del rechazo del cuadro horario
     */
    data: any;
  }

  /**
   * Parameters for getApiV2ServiciosValidacionVep
   */
  export interface GetApiV2ServiciosValidacionVepParams {

    /**
     * Número de boleta de pago (VEP) a validar.
     */
    nroBoletaPago: number;

    /**
     * Id de Empresa para la cual se quiere realizar el trámite.
     */
    empresaId: number;
  }

  /**
   * Parameters for putApiV2ServiciosIdBaja
   */
  export interface PutApiV2ServiciosIdBajaParams {

    /**
     * Id de cuadro horario a dar de baja
     */
    id: number;

    /**
     * Datos de la baja del cuadro horario
     */
    data: any;
  }
}

export { ServicioService }
