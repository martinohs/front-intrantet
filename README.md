# angular-base

## Proyecto de Angular base.

### Se inicia con la versión 9 de Angular y con los componentes base (header, menu, auth menu, home) desarrollados en GEOP.

Se adjunta en el proyecto un módulo de ejemplo que se carga con el modo lazy loading para optimizar la carga al iniciar la app.

### Recomendación:

-   En los proyectos de Portal organismo, Geop y Dut comenzamos a implementar el patrón de diseño de "Facade", además usamos un store y servicios por módulo. La idea de esta implementación es que los componentes que renderizan vistas se comuniquen sólo con el Facade y que el Facade consuma los servicios (generados en el módulo y los servicios generados con swagger para conectar con las APIs) para realizar las peticiones de los componentes.

-   El Facade sería un "intermediario", que agrupa las funcionalidades de los diferentes servicios, para que el componente sólo tenga que buscar ahi lo que necesita.

-   En el store usamos behavior subjects (de la librería RxJS) para generar observables que se exportan con getters en el facade para que los componentes puedan subscribirse. La idea de implementar un store es que la data esté centralizada para evitar inconsistencias o problemas por el pasaje de data entre los componentes que renderizan vistas. La data en el store la tiene que setear un servicio o un facade y NO LOS COMPONENTES QUE RENDERIZAN VISTAS (el componente que renderiza una vista puede llamar a una función del facade que se encargará de actualizar la data del store ya sea dentro del facade o dentro de un servicio que consuma el facade).

-   Para generar formularios, estamos usando los formularios reactivos (Ver implementación en GEOP, PORTAL ORGANISMOS y DUT / PORTAL EMPRESAS). Una buena práctica es generar un servicio que arme el formulario reactivo ( FormGroup ) y luego generar un getter en el facade para que el formulario esté disponible para los componentes que rendericen la vista (ver ejemplo en PORTAL ORGANISMOS y en la sección de modalidad de tráfico en GEOP y DUT - branch de Cuadro horario - )

### Otra recomendación:

-   Si se necesitan componentes como ser: tablas, tabs, cards, formularios con stepper, etc, usar Angular Material (ver implementaciones en GEOP y Portal Organismo).

-   Este proyecto tiene configurado Angular Material y la versión 4 de Bootstrap. Las clases de css custom para definir paleta de colores etc se tomaron de GEOP (ver implementación ahi).

## Environment

Crear archivo `environment.ts` tomando como base el archivo `environment.local.ts`

```
cp src/environments/environment.local.ts src/environments/environment.ts
```

## Development

### Node

Utilizar la versión correcta de Node según `.nvmrc`

```
nvm use
```

### Local Server

```
npm run start
```

### Generate API Client from Swagger

En este proyecto base se agrega la configuración, scripts y carpeta en src/core/ para generar los archivos de la versión 2 de DUT API que está documentada con Swagger. Es un modelo de la implentación de Swagger.

Para conectar una nueva api, hay que crear una carpeta con los archivos de configuración dentro de la carpeta `src/swagger`, configurar el script en el package.json (seguir el ejemplo del script "generareDut"), y luego correr el command (npm run nombreDelScript). Una vez se generen los archivos de swagger en la carpeta de la api dentro de `src/core`, configurar el seteo de la url base (tomando la url del environment) en el archivo app.component.ts (seguir el ejemplo de la configuración de DUT API, y agregar el seteo a la función ya definida ahi que se llama "setApisConfig").

-   La API de seop tiene problemas para tomar el swagger.json directo de la url al correr el command de ng-swagger-gen. Se aconseja ver el fix que se hizo en GEOP, DUT/PORTAL EMPRESAS y en PORTAL ORGANISMOS, donde se utiliza Axios como dependencia de desarrollo (ejecutar npm i axios --save-dev) y luego se corre un script que tomar el swagger.json de seop api y lo guarda en un archivo .json local, para luego usarlo en la generación de archivos con ng-swagger-gen.

Ejecutar el siguiente comando para generar el cliente según la configuración en `src/swagger/api-dut/api-dut.config.json`:

```
npm run generateDut
```

Los archivos auto-generados por Swagger se guardarán en la carpeta seteada en "output" en la configuración

## Build

```
npm run build
```

Para hacer un build en el entorno de DEV, correr:

```
npm run build:dev
```

Para hacer un build en el entorno de PRE, correr:

```
npm run build:pre
```

## Deploy

## Contributing

Comprobar Build y formater código con [Prettier](https://prettier.io/) según `.prettierrc` antes de hacer commit:

```
npm run build
npm run prettier
```


##

INSTALAR:
https://www.npmjs.com/package/@kolkov/angular-editor
