/* tslint:disable */
import { ParadaAPI } from './parada-api';
export interface ServicioHorarioAPI {
  cantidadChoferes?: number;

  /**
   * Cantidad de días de viaje desde que salió el servicio
   */
  diasViajeExtra?: number;
  horaLlegada?: any;
  horaSalida?: string;
  id?: number;
  km?: number;
  nroOrden?: number;
  parada?: ParadaAPI;
  pasoFronterizo?: boolean;
  posta?: boolean;
  servicio?: number;
  tiempoEspera?: string;
  tiempoViaje?: string;
  velocidadPromedio?: number;
}
