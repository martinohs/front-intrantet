/* tslint:disable */
import { CategoriaServicioAPI } from './categoria-servicio-api';
import { DiaAPI } from './dia-api';
import { PeriodoFrecuenciaAPI } from './periodo-frecuencia-api';
export interface DiaServicioAPI {
  activo?: boolean;
  categoriaServicioPlantaBaja?: CategoriaServicioAPI;
  categoriaServicioPrimerPiso?: CategoriaServicioAPI;
  dia?: DiaAPI;
  id?: number;
  periodoFrecuencia?: PeriodoFrecuenciaAPI;
  servicio?: number;
}
