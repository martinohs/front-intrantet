/* tslint:disable */
export interface DetalleModalidadTraficoAPIUpdateRequest {
  id?: number;
  ofreceServicio?: boolean;
  paisDestino?: number;
  paisOrigen?: number;
  paradaDestino?: number;
  paradaOrigen?: number;
  provinciaDestino?: number;
  provinciaOrigen?: number;
  updatedLastBy?: string;
}
