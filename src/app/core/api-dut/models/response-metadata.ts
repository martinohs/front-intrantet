/* tslint:disable */

/**
 * Metadata de las consultas paginadas
 */
export interface ResponseMetadata {
  resultset?: {offset?: number, limit?: number, count?: number};
}
