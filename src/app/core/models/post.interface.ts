import { from } from 'rxjs';
import { MediaContent } from './mediaContent.interface';

export interface Post{
  id?: number;
  title: string;
  isImportant?: boolean;
  likes: number;
  content: string;
  media_img_id?: MediaContent; //VER BIEN
  from_area?: string;
  uploadDate?: uDate;
  creator: string;
}

export interface uDate {
  date: Date;
  timezone: string;
  timezone_type: number;
}
