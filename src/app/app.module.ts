// Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { HttpClientModule } from '@angular/common/http';

// APP
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// GoogleAnalytics
import { GoogleAnalyticsService } from '@core/services';


import { SharedModule } from './shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from '@core/core.module';
import { FooterComponent, HomeComponent, HeaderComponent, MenuComponent } from './index';
import { NoticiasComponent } from './modules/modulo-noticias/noticias/noticias.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PosteoComponent } from './modules/posteo/posteo.component';
import { PostCreadorComponent } from './modules/post-creador/post-creador.component';



@NgModule({
	declarations: [AppComponent, HeaderComponent, FooterComponent, HomeComponent, MenuComponent, NoticiasComponent, PosteoComponent, PostCreadorComponent, ],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		HttpClientModule,
		SharedModule,
		BrowserAnimationsModule,
    CoreModule.forRoot(),
    NgbModule,
    AngularEditorModule,
	],
	providers: [GoogleAnalyticsService],
	bootstrap: [AppComponent],
})
export class AppModule {
	constructor(protected googleAnalyticsService: GoogleAnalyticsService) {} // <-- We inject the service here to keep it alive whole time





}

