/* tslint:disable */
export interface TramiteAPISaveRequest {
  empresa?: number;
  id?: number;
  nroBoletaPago?: number;
  subtipoTramite?: number;
  tipoTramite?: number;
  usuario?: string;
}
