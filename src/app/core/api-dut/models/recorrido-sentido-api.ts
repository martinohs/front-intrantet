/* tslint:disable */
import { LocalidadSeopAPI } from './localidad-seop-api';
import { RecorridoAPI } from './recorrido-api';
import { SentidoAPI } from './sentido-api';
export interface RecorridoSentidoAPI {
  destino?: string;
  id?: number;
  itinerario?: string;
  localidadDestino?: LocalidadSeopAPI;
  localidadOrigen?: LocalidadSeopAPI;
  origen?: string;
  recorrido?: RecorridoAPI;
  sentido?: SentidoAPI;
  vigenciaDesde?: any;
  vigenciaHasta?: any;
  vinculacionCaminera?: string;
}
