import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuloEjemploComponent, EmpresaComponent, EmpresaDetalleComponent } from './index';

const routes: Routes = [
	{
		path: '',
		component: ModuloEjemploComponent,
	},
	{
		path: 'empresas',
		component: EmpresaComponent,
	},
	{
		path: 'empresas/:id',
		component: EmpresaDetalleComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ModuloEjemploRoutingModule {}
