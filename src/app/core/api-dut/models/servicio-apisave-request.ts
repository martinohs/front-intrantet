/* tslint:disable */
import { DiaServicioAPISaveRequest } from './dia-servicio-apisave-request';
import { ServicioHorarioAPISaveRequest } from './servicio-horario-apisave-request';
import { TramiteAPISaveRequest } from './tramite-apisave-request';
export interface ServicioAPISaveRequest {
  dias?: Array<DiaServicioAPISaveRequest>;
  diasDuracion?: number;
  horaLlegada?: string;
  horaSalida?: string;
  horarios?: Array<ServicioHorarioAPISaveRequest>;
  operador?: number;
  recorrido?: number;
  recorridoSentido?: number;
  servicioReemplazado?: number;
  tramite?: TramiteAPISaveRequest;
  velocidadPromedioTotal?: number;
  vigenciaDesde?: string;
  vigenciaHasta?: string;
}
