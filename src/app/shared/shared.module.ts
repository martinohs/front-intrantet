import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

//material
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatMenuModule } from '@angular/material/menu';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';

@NgModule({
	imports: [
		MatProgressSpinnerModule,
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		MatToolbarModule,
		MatSidenavModule,
		MatMenuModule,
		MatButtonModule,
		MatDividerModule,
		MatCardModule,
		MatListModule,
		MatProgressBarModule,
	],
	exports: [
		MatProgressSpinnerModule,
		CommonModule,
		ReactiveFormsModule,
		FormsModule,
		RouterModule,
		MatToolbarModule,
		MatSidenavModule,
		MatMenuModule,
		MatButtonModule,
		MatDividerModule,
		MatCardModule,
		MatListModule,
		MatProgressBarModule,
	],
})
export class SharedModule {}
