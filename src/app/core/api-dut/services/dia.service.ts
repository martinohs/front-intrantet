/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiDutConfiguration as __Configuration } from '../api-dut-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { DiaAPIResponsePaginacion } from '../models/dia-apiresponse-paginacion';
import { DiaAPIResponseData } from '../models/dia-apiresponse-data';
@Injectable({
  providedIn: 'root',
})
class DiaService extends __BaseService {
  static readonly getApiV2DiasPath = '/api/v2/dias';
  static readonly findDiaPath = '/api/v2/dias/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Obtiene una lista de Días
   * @param params The `DiaService.GetApiV2DiasParams` containing the following parameters:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `descripcion[ne]`:
   *
   * - `descripcion[like]`:
   *
   * - `descripcion`:
   *
   * @return Respuesta exitosa.
   */
  getApiV2DiasResponse(params: DiaService.GetApiV2DiasParams): __Observable<__StrictHttpResponse<DiaAPIResponsePaginacion>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.sort != null) __params = __params.set('sort', params.sort.toString());
    if (params.order != null) __params = __params.set('order', params.order.toString());
    if (params.offset != null) __params = __params.set('offset', params.offset.toString());
    if (params.limit != null) __params = __params.set('limit', params.limit.toString());
    if (params.idNe != null) __params = __params.set('id[ne]', params.idNe.toString());
    if (params.idLt != null) __params = __params.set('id[lt]', params.idLt.toString());
    if (params.idLe != null) __params = __params.set('id[le]', params.idLe.toString());
    if (params.idGt != null) __params = __params.set('id[gt]', params.idGt.toString());
    if (params.idGe != null) __params = __params.set('id[ge]', params.idGe.toString());
    if (params.id != null) __params = __params.set('id', params.id.toString());
    if (params.descripcionNe != null) __params = __params.set('descripcion[ne]', params.descripcionNe.toString());
    if (params.descripcionLike != null) __params = __params.set('descripcion[like]', params.descripcionLike.toString());
    if (params.descripcion != null) __params = __params.set('descripcion', params.descripcion.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/dias`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<DiaAPIResponsePaginacion>;
      })
    );
  }
  /**
   * Obtiene una lista de Días
   * @param params The `DiaService.GetApiV2DiasParams` containing the following parameters:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `descripcion[ne]`:
   *
   * - `descripcion[like]`:
   *
   * - `descripcion`:
   *
   * @return Respuesta exitosa.
   */
  getApiV2Dias(params: DiaService.GetApiV2DiasParams): __Observable<DiaAPIResponsePaginacion> {
    return this.getApiV2DiasResponse(params).pipe(
      __map(_r => _r.body as DiaAPIResponsePaginacion)
    );
  }

  /**
   * Obtiene un día por ID
   * @param id Id de Día a buscar
   * @return Respuesta exitosa.
   */
  findDiaResponse(id: number): __Observable<__StrictHttpResponse<DiaAPIResponseData>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/dias/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<DiaAPIResponseData>;
      })
    );
  }
  /**
   * Obtiene un día por ID
   * @param id Id de Día a buscar
   * @return Respuesta exitosa.
   */
  findDia(id: number): __Observable<DiaAPIResponseData> {
    return this.findDiaResponse(id).pipe(
      __map(_r => _r.body as DiaAPIResponseData)
    );
  }
}

module DiaService {

  /**
   * Parameters for getApiV2Dias
   */
  export interface GetApiV2DiasParams {

    /**
     * Campo por el cual se ordena.
     */
    sort?: string;

    /**
     * Método de ordenación, ascendente (ASC) o descendente (DESC).
     */
    order?: 'ASC' | 'DESC';

    /**
     * Cantidad de registros que deben omitirse en la consulta.
     */
    offset?: number;

    /**
     * Cantidad máxima de registros que debe regresar la consulta.
     */
    limit?: number;
    idNe?: number;
    idLt?: number;
    idLe?: number;
    idGt?: number;
    idGe?: number;
    id?: number;
    descripcionNe?: string;
    descripcionLike?: string;
    descripcion?: string;
  }
}

export { DiaService }
