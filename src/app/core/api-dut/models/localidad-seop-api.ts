/* tslint:disable */
import { ProvinciaAPI } from './provincia-api';
export interface LocalidadSeopAPI {
  activo?: boolean;
  descripcion?: string;
  id?: number;
  provincia?: ProvinciaAPI;
}
