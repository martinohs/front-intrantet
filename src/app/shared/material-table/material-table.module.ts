import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';

import { MatPaginatorModule, MatPaginatorIntl } from '@angular/material/paginator';
import { MatPaginatorIntlCNRT } from './mat-paginator.config';
import { MatTableModule } from '@angular/material/table';

@NgModule({
	declarations: [],
	imports: [MatPaginatorModule, MatTableModule],
	exports: [MatPaginatorModule, MatTableModule],
})
export class MaterialTableModule {
	constructor(@Optional() @SkipSelf() parentModule: MaterialTableModule) {
		if (parentModule) {
			throw new Error('MaterialTableModule ya cargado');
		}
	}
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: MaterialTableModule,
			providers: [{ provide: MatPaginatorIntl, useClass: MatPaginatorIntlCNRT }],
		};
	}
}
