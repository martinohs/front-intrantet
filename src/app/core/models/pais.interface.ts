export interface Pais {
	codigo: string;
	abrev: string;
	descripcion: string;
	iso3166a3: string;
	iso3166numerico: number;
	activo: boolean;
	id: number;
}
