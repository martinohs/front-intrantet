/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiDutConfiguration as __Configuration } from '../api-dut-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ParadaAPIResponsePaginacion } from '../models/parada-apiresponse-paginacion';
import { ParadaAPIResponseData } from '../models/parada-apiresponse-data';
import { ParadaAPISaveRequest } from '../models/parada-apisave-request';
import { ParadaAPIUpdateRequest } from '../models/parada-apiupdate-request';
@Injectable({
  providedIn: 'root',
})
class ParadaService extends __BaseService {
  static readonly getApiV2ParadasPath = '/api/v2/paradas';
  static readonly postApiV2ParadasPath = '/api/v2/paradas';
  static readonly getApiV2ParadasIdPath = '/api/v2/paradas/{id}';
  static readonly putApiV2ParadasIdPath = '/api/v2/paradas/{id}';
  static readonly deleteApiV2ParadasIdPath = '/api/v2/paradas/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Obtiene una lista de paradas paginadas
   * @param params The `ParadaService.GetApiV2ParadasParams` containing the following parameters:
   *
   * - `state[ne]`:
   *
   * - `state[like]`:
   *
   * - `stateDistrict[ne]`:
   *
   * - `stateDistrict[like]`:
   *
   * - `stateDistrict`:
   *
   * - `state`:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `nombre[ne]`:
   *
   * - `nombre[like]`:
   *
   * - `nombreCompleto[ne]`:
   *
   * - `nombreCompleto[like]`:
   *
   * - `nombreCompleto`:
   *
   * - `nombre`:
   *
   * - `longitud[ne]`:
   *
   * - `longitud[lt]`:
   *
   * - `longitud[le]`:
   *
   * - `longitud[gt]`:
   *
   * - `longitud[ge]`:
   *
   * - `longitud`:
   *
   * - `localidad-updatedAt[ne]`:
   *
   * - `localidad-updatedAt[lt]`:
   *
   * - `localidad-updatedAt[le]`:
   *
   * - `localidad-updatedAt[gt]`:
   *
   * - `localidad-updatedAt[ge]`:
   *
   * - `localidad-updatedAt`:
   *
   * - `localidad-provincia-pais-id[ne]`:
   *
   * - `localidad-provincia-pais-id[lt]`:
   *
   * - `localidad-provincia-pais-id[le]`:
   *
   * - `localidad-provincia-pais-id[gt]`:
   *
   * - `localidad-provincia-pais-id[ge]`:
   *
   * - `localidad-provincia-pais-id`:
   *
   * - `localidad-provincia-pais-descripcion[ne]`:
   *
   * - `localidad-provincia-pais-descripcion[like]`:
   *
   * - `localidad-provincia-pais-descripcion`:
   *
   * - `localidad-provincia-pais-codigo[ne]`:
   *
   * - `localidad-provincia-pais-codigo[like]`:
   *
   * - `localidad-provincia-pais-codigo`:
   *
   * - `localidad-provincia-pais-activo`:
   *
   * - `localidad-provincia-pais-abrev[ne]`:
   *
   * - `localidad-provincia-pais-abrev[like]`:
   *
   * - `localidad-provincia-pais-abrev`:
   *
   * - `localidad-provincia-id[ne]`:
   *
   * - `localidad-provincia-id[lt]`:
   *
   * - `localidad-provincia-id[le]`:
   *
   * - `localidad-provincia-id[gt]`:
   *
   * - `localidad-provincia-id[ge]`:
   *
   * - `localidad-provincia-id`:
   *
   * - `localidad-provincia-descripcion[ne]`:
   *
   * - `localidad-provincia-descripcion[like]`:
   *
   * - `localidad-provincia-descripcion`:
   *
   * - `localidad-provincia-codigo[ne]`:
   *
   * - `localidad-provincia-codigo[like]`:
   *
   * - `localidad-provincia-codigoINDEC[ne]`:
   *
   * - `localidad-provincia-codigoINDEC[like]`:
   *
   * - `localidad-provincia-codigoINDEC`:
   *
   * - `localidad-provincia-codigo`:
   *
   * - `localidad-provincia-activo`:
   *
   * - `localidad-parada`:
   *
   * - `localidad-lon[ne]`:
   *
   * - `localidad-lon[lt]`:
   *
   * - `localidad-lon[le]`:
   *
   * - `localidad-lon[gt]`:
   *
   * - `localidad-lon[ge]`:
   *
   * - `localidad-lon`:
   *
   * - `localidad-lat[ne]`:
   *
   * - `localidad-lat[lt]`:
   *
   * - `localidad-lat[le]`:
   *
   * - `localidad-lat[gt]`:
   *
   * - `localidad-lat[ge]`:
   *
   * - `localidad-lat`:
   *
   * - `localidad-id[ne]`:
   *
   * - `localidad-id[lt]`:
   *
   * - `localidad-id[le]`:
   *
   * - `localidad-id[gt]`:
   *
   * - `localidad-id[ge]`:
   *
   * - `localidad-id`:
   *
   * - `localidad-fuente[ne]`:
   *
   * - `localidad-fuente[like]`:
   *
   * - `localidad-fuente`:
   *
   * - `localidad-descripcion[ne]`:
   *
   * - `localidad-descripcion[like]`:
   *
   * - `localidad-descripcion`:
   *
   * - `localidad-createdAt[ne]`:
   *
   * - `localidad-createdAt[lt]`:
   *
   * - `localidad-createdAt[le]`:
   *
   * - `localidad-createdAt[gt]`:
   *
   * - `localidad-createdAt[ge]`:
   *
   * - `localidad-createdAt`:
   *
   * - `localidad-codigoINDEC[ne]`:
   *
   * - `localidad-codigoINDEC[like]`:
   *
   * - `localidad-codigoINDEC`:
   *
   * - `localidad-activo`:
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `latitud[ne]`:
   *
   * - `latitud[lt]`:
   *
   * - `latitud[le]`:
   *
   * - `latitud[gt]`:
   *
   * - `latitud[ge]`:
   *
   * - `latitud`:
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `empresa-vigenciaHasta[ne]`:
   *
   * - `empresa-vigenciaHasta[lt]`:
   *
   * - `empresa-vigenciaHasta[le]`:
   *
   * - `empresa-vigenciaHasta[gt]`:
   *
   * - `empresa-vigenciaHasta[ge]`:
   *
   * - `empresa-vigenciaHasta`:
   *
   * - `empresa-vigenciaDesde[ne]`:
   *
   * - `empresa-vigenciaDesde[lt]`:
   *
   * - `empresa-vigenciaDesde[le]`:
   *
   * - `empresa-vigenciaDesde[gt]`:
   *
   * - `empresa-vigenciaDesde[ge]`:
   *
   * - `empresa-vigenciaDesde`:
   *
   * - `empresa-tipoSociedad[ne]`:
   *
   * - `empresa-tipoSociedad[lt]`:
   *
   * - `empresa-tipoSociedad[le]`:
   *
   * - `empresa-tipoSociedad[gt]`:
   *
   * - `empresa-tipoSociedad[ge]`:
   *
   * - `empresa-tipoSociedad`:
   *
   * - `empresa-tipoDocumento-id[ne]`:
   *
   * - `empresa-tipoDocumento-id[lt]`:
   *
   * - `empresa-tipoDocumento-id[le]`:
   *
   * - `empresa-tipoDocumento-id[gt]`:
   *
   * - `empresa-tipoDocumento-id[ge]`:
   *
   * - `empresa-tipoDocumento-id`:
   *
   * - `empresa-tipoDocumento-descripcion[ne]`:
   *
   * - `empresa-tipoDocumento-descripcion[like]`:
   *
   * - `empresa-tipoDocumento-descripcion`:
   *
   * - `empresa-tipoDocumento-abrev[ne]`:
   *
   * - `empresa-tipoDocumento-abrev[like]`:
   *
   * - `empresa-tipoDocumento-abrev`:
   *
   * - `empresa-textoHash`:
   *
   * - `empresa-razonSocial[ne]`:
   *
   * - `empresa-razonSocial[like]`:
   *
   * - `empresa-razonSocial`:
   *
   * - `empresa-paut[ne]`:
   *
   * - `empresa-paut[like]`:
   *
   * - `empresa-paut`:
   *
   * - `empresa-observacion[ne]`:
   *
   * - `empresa-observacion[like]`:
   *
   * - `empresa-observacion`:
   *
   * - `empresa-nroDocumento[ne]`:
   *
   * - `empresa-nroDocumento[like]`:
   *
   * - `empresa-nroDocumento`:
   *
   * - `empresa-nombreFantasia[ne]`:
   *
   * - `empresa-nombreFantasia[like]`:
   *
   * - `empresa-nombreFantasia`:
   *
   * - `empresa-id[ne]`:
   *
   * - `empresa-id[lt]`:
   *
   * - `empresa-id[le]`:
   *
   * - `empresa-id[gt]`:
   *
   * - `empresa-id[ge]`:
   *
   * - `empresa-id`:
   *
   * - `empresa-estadoEmpresa[ne]`:
   *
   * - `empresa-estadoEmpresa[lt]`:
   *
   * - `empresa-estadoEmpresa[le]`:
   *
   * - `empresa-estadoEmpresa[gt]`:
   *
   * - `empresa-estadoEmpresa[ge]`:
   *
   * - `empresa-estadoEmpresa`:
   *
   * - `empresa-esPersonaFisica`:
   *
   * - `empresa-email[ne]`:
   *
   * - `empresa-email[like]`:
   *
   * - `empresa-email`:
   *
   * - `empresa-cuitEmpresaAnterior[ne]`:
   *
   * - `empresa-cuitEmpresaAnterior[like]`:
   *
   * - `empresa-cuitEmpresaAnterior`:
   *
   * - `displayName[ne]`:
   *
   * - `displayName[like]`:
   *
   * - `displayName`:
   *
   * - `county[ne]`:
   *
   * - `county[like]`:
   *
   * - `county`:
   *
   * - `cityTown[ne]`:
   *
   * - `cityTown[like]`:
   *
   * - `cityTown`:
   *
   * - `aprobada`:
   *
   * - `activa`:
   *
   * @return Respuesta exitosa.
   */
  getApiV2ParadasResponse(params: ParadaService.GetApiV2ParadasParams): __Observable<__StrictHttpResponse<ParadaAPIResponsePaginacion>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.stateNe != null) __params = __params.set('state[ne]', params.stateNe.toString());
    if (params.stateLike != null) __params = __params.set('state[like]', params.stateLike.toString());
    if (params.stateDistrictNe != null) __params = __params.set('stateDistrict[ne]', params.stateDistrictNe.toString());
    if (params.stateDistrictLike != null) __params = __params.set('stateDistrict[like]', params.stateDistrictLike.toString());
    if (params.stateDistrict != null) __params = __params.set('stateDistrict', params.stateDistrict.toString());
    if (params.state != null) __params = __params.set('state', params.state.toString());
    if (params.sort != null) __params = __params.set('sort', params.sort.toString());
    if (params.order != null) __params = __params.set('order', params.order.toString());
    if (params.offset != null) __params = __params.set('offset', params.offset.toString());
    if (params.nombreNe != null) __params = __params.set('nombre[ne]', params.nombreNe.toString());
    if (params.nombreLike != null) __params = __params.set('nombre[like]', params.nombreLike.toString());
    if (params.nombreCompletoNe != null) __params = __params.set('nombreCompleto[ne]', params.nombreCompletoNe.toString());
    if (params.nombreCompletoLike != null) __params = __params.set('nombreCompleto[like]', params.nombreCompletoLike.toString());
    if (params.nombreCompleto != null) __params = __params.set('nombreCompleto', params.nombreCompleto.toString());
    if (params.nombre != null) __params = __params.set('nombre', params.nombre.toString());
    if (params.longitudNe != null) __params = __params.set('longitud[ne]', params.longitudNe.toString());
    if (params.longitudLt != null) __params = __params.set('longitud[lt]', params.longitudLt.toString());
    if (params.longitudLe != null) __params = __params.set('longitud[le]', params.longitudLe.toString());
    if (params.longitudGt != null) __params = __params.set('longitud[gt]', params.longitudGt.toString());
    if (params.longitudGe != null) __params = __params.set('longitud[ge]', params.longitudGe.toString());
    if (params.longitud != null) __params = __params.set('longitud', params.longitud.toString());
    if (params.localidadUpdatedAtNe != null) __params = __params.set('localidad-updatedAt[ne]', params.localidadUpdatedAtNe.toString());
    if (params.localidadUpdatedAtLt != null) __params = __params.set('localidad-updatedAt[lt]', params.localidadUpdatedAtLt.toString());
    if (params.localidadUpdatedAtLe != null) __params = __params.set('localidad-updatedAt[le]', params.localidadUpdatedAtLe.toString());
    if (params.localidadUpdatedAtGt != null) __params = __params.set('localidad-updatedAt[gt]', params.localidadUpdatedAtGt.toString());
    if (params.localidadUpdatedAtGe != null) __params = __params.set('localidad-updatedAt[ge]', params.localidadUpdatedAtGe.toString());
    if (params.localidadUpdatedAt != null) __params = __params.set('localidad-updatedAt', params.localidadUpdatedAt.toString());
    if (params.localidadProvinciaPaisIdNe != null) __params = __params.set('localidad-provincia-pais-id[ne]', params.localidadProvinciaPaisIdNe.toString());
    if (params.localidadProvinciaPaisIdLt != null) __params = __params.set('localidad-provincia-pais-id[lt]', params.localidadProvinciaPaisIdLt.toString());
    if (params.localidadProvinciaPaisIdLe != null) __params = __params.set('localidad-provincia-pais-id[le]', params.localidadProvinciaPaisIdLe.toString());
    if (params.localidadProvinciaPaisIdGt != null) __params = __params.set('localidad-provincia-pais-id[gt]', params.localidadProvinciaPaisIdGt.toString());
    if (params.localidadProvinciaPaisIdGe != null) __params = __params.set('localidad-provincia-pais-id[ge]', params.localidadProvinciaPaisIdGe.toString());
    if (params.localidadProvinciaPaisId != null) __params = __params.set('localidad-provincia-pais-id', params.localidadProvinciaPaisId.toString());
    if (params.localidadProvinciaPaisDescripcionNe != null) __params = __params.set('localidad-provincia-pais-descripcion[ne]', params.localidadProvinciaPaisDescripcionNe.toString());
    if (params.localidadProvinciaPaisDescripcionLike != null) __params = __params.set('localidad-provincia-pais-descripcion[like]', params.localidadProvinciaPaisDescripcionLike.toString());
    if (params.localidadProvinciaPaisDescripcion != null) __params = __params.set('localidad-provincia-pais-descripcion', params.localidadProvinciaPaisDescripcion.toString());
    if (params.localidadProvinciaPaisCodigoNe != null) __params = __params.set('localidad-provincia-pais-codigo[ne]', params.localidadProvinciaPaisCodigoNe.toString());
    if (params.localidadProvinciaPaisCodigoLike != null) __params = __params.set('localidad-provincia-pais-codigo[like]', params.localidadProvinciaPaisCodigoLike.toString());
    if (params.localidadProvinciaPaisCodigo != null) __params = __params.set('localidad-provincia-pais-codigo', params.localidadProvinciaPaisCodigo.toString());
    if (params.localidadProvinciaPaisActivo != null) __params = __params.set('localidad-provincia-pais-activo', params.localidadProvinciaPaisActivo.toString());
    if (params.localidadProvinciaPaisAbrevNe != null) __params = __params.set('localidad-provincia-pais-abrev[ne]', params.localidadProvinciaPaisAbrevNe.toString());
    if (params.localidadProvinciaPaisAbrevLike != null) __params = __params.set('localidad-provincia-pais-abrev[like]', params.localidadProvinciaPaisAbrevLike.toString());
    if (params.localidadProvinciaPaisAbrev != null) __params = __params.set('localidad-provincia-pais-abrev', params.localidadProvinciaPaisAbrev.toString());
    if (params.localidadProvinciaIdNe != null) __params = __params.set('localidad-provincia-id[ne]', params.localidadProvinciaIdNe.toString());
    if (params.localidadProvinciaIdLt != null) __params = __params.set('localidad-provincia-id[lt]', params.localidadProvinciaIdLt.toString());
    if (params.localidadProvinciaIdLe != null) __params = __params.set('localidad-provincia-id[le]', params.localidadProvinciaIdLe.toString());
    if (params.localidadProvinciaIdGt != null) __params = __params.set('localidad-provincia-id[gt]', params.localidadProvinciaIdGt.toString());
    if (params.localidadProvinciaIdGe != null) __params = __params.set('localidad-provincia-id[ge]', params.localidadProvinciaIdGe.toString());
    if (params.localidadProvinciaId != null) __params = __params.set('localidad-provincia-id', params.localidadProvinciaId.toString());
    if (params.localidadProvinciaDescripcionNe != null) __params = __params.set('localidad-provincia-descripcion[ne]', params.localidadProvinciaDescripcionNe.toString());
    if (params.localidadProvinciaDescripcionLike != null) __params = __params.set('localidad-provincia-descripcion[like]', params.localidadProvinciaDescripcionLike.toString());
    if (params.localidadProvinciaDescripcion != null) __params = __params.set('localidad-provincia-descripcion', params.localidadProvinciaDescripcion.toString());
    if (params.localidadProvinciaCodigoNe != null) __params = __params.set('localidad-provincia-codigo[ne]', params.localidadProvinciaCodigoNe.toString());
    if (params.localidadProvinciaCodigoLike != null) __params = __params.set('localidad-provincia-codigo[like]', params.localidadProvinciaCodigoLike.toString());
    if (params.localidadProvinciaCodigoINDECNe != null) __params = __params.set('localidad-provincia-codigoINDEC[ne]', params.localidadProvinciaCodigoINDECNe.toString());
    if (params.localidadProvinciaCodigoINDECLike != null) __params = __params.set('localidad-provincia-codigoINDEC[like]', params.localidadProvinciaCodigoINDECLike.toString());
    if (params.localidadProvinciaCodigoINDEC != null) __params = __params.set('localidad-provincia-codigoINDEC', params.localidadProvinciaCodigoINDEC.toString());
    if (params.localidadProvinciaCodigo != null) __params = __params.set('localidad-provincia-codigo', params.localidadProvinciaCodigo.toString());
    if (params.localidadProvinciaActivo != null) __params = __params.set('localidad-provincia-activo', params.localidadProvinciaActivo.toString());
    if (params.localidadParada != null) __params = __params.set('localidad-parada', params.localidadParada.toString());
    if (params.localidadLonNe != null) __params = __params.set('localidad-lon[ne]', params.localidadLonNe.toString());
    if (params.localidadLonLt != null) __params = __params.set('localidad-lon[lt]', params.localidadLonLt.toString());
    if (params.localidadLonLe != null) __params = __params.set('localidad-lon[le]', params.localidadLonLe.toString());
    if (params.localidadLonGt != null) __params = __params.set('localidad-lon[gt]', params.localidadLonGt.toString());
    if (params.localidadLonGe != null) __params = __params.set('localidad-lon[ge]', params.localidadLonGe.toString());
    if (params.localidadLon != null) __params = __params.set('localidad-lon', params.localidadLon.toString());
    if (params.localidadLatNe != null) __params = __params.set('localidad-lat[ne]', params.localidadLatNe.toString());
    if (params.localidadLatLt != null) __params = __params.set('localidad-lat[lt]', params.localidadLatLt.toString());
    if (params.localidadLatLe != null) __params = __params.set('localidad-lat[le]', params.localidadLatLe.toString());
    if (params.localidadLatGt != null) __params = __params.set('localidad-lat[gt]', params.localidadLatGt.toString());
    if (params.localidadLatGe != null) __params = __params.set('localidad-lat[ge]', params.localidadLatGe.toString());
    if (params.localidadLat != null) __params = __params.set('localidad-lat', params.localidadLat.toString());
    if (params.localidadIdNe != null) __params = __params.set('localidad-id[ne]', params.localidadIdNe.toString());
    if (params.localidadIdLt != null) __params = __params.set('localidad-id[lt]', params.localidadIdLt.toString());
    if (params.localidadIdLe != null) __params = __params.set('localidad-id[le]', params.localidadIdLe.toString());
    if (params.localidadIdGt != null) __params = __params.set('localidad-id[gt]', params.localidadIdGt.toString());
    if (params.localidadIdGe != null) __params = __params.set('localidad-id[ge]', params.localidadIdGe.toString());
    if (params.localidadId != null) __params = __params.set('localidad-id', params.localidadId.toString());
    if (params.localidadFuenteNe != null) __params = __params.set('localidad-fuente[ne]', params.localidadFuenteNe.toString());
    if (params.localidadFuenteLike != null) __params = __params.set('localidad-fuente[like]', params.localidadFuenteLike.toString());
    if (params.localidadFuente != null) __params = __params.set('localidad-fuente', params.localidadFuente.toString());
    if (params.localidadDescripcionNe != null) __params = __params.set('localidad-descripcion[ne]', params.localidadDescripcionNe.toString());
    if (params.localidadDescripcionLike != null) __params = __params.set('localidad-descripcion[like]', params.localidadDescripcionLike.toString());
    if (params.localidadDescripcion != null) __params = __params.set('localidad-descripcion', params.localidadDescripcion.toString());
    if (params.localidadCreatedAtNe != null) __params = __params.set('localidad-createdAt[ne]', params.localidadCreatedAtNe.toString());
    if (params.localidadCreatedAtLt != null) __params = __params.set('localidad-createdAt[lt]', params.localidadCreatedAtLt.toString());
    if (params.localidadCreatedAtLe != null) __params = __params.set('localidad-createdAt[le]', params.localidadCreatedAtLe.toString());
    if (params.localidadCreatedAtGt != null) __params = __params.set('localidad-createdAt[gt]', params.localidadCreatedAtGt.toString());
    if (params.localidadCreatedAtGe != null) __params = __params.set('localidad-createdAt[ge]', params.localidadCreatedAtGe.toString());
    if (params.localidadCreatedAt != null) __params = __params.set('localidad-createdAt', params.localidadCreatedAt.toString());
    if (params.localidadCodigoINDECNe != null) __params = __params.set('localidad-codigoINDEC[ne]', params.localidadCodigoINDECNe.toString());
    if (params.localidadCodigoINDECLike != null) __params = __params.set('localidad-codigoINDEC[like]', params.localidadCodigoINDECLike.toString());
    if (params.localidadCodigoINDEC != null) __params = __params.set('localidad-codigoINDEC', params.localidadCodigoINDEC.toString());
    if (params.localidadActivo != null) __params = __params.set('localidad-activo', params.localidadActivo.toString());
    if (params.limit != null) __params = __params.set('limit', params.limit.toString());
    if (params.latitudNe != null) __params = __params.set('latitud[ne]', params.latitudNe.toString());
    if (params.latitudLt != null) __params = __params.set('latitud[lt]', params.latitudLt.toString());
    if (params.latitudLe != null) __params = __params.set('latitud[le]', params.latitudLe.toString());
    if (params.latitudGt != null) __params = __params.set('latitud[gt]', params.latitudGt.toString());
    if (params.latitudGe != null) __params = __params.set('latitud[ge]', params.latitudGe.toString());
    if (params.latitud != null) __params = __params.set('latitud', params.latitud.toString());
    if (params.idNe != null) __params = __params.set('id[ne]', params.idNe.toString());
    if (params.idLt != null) __params = __params.set('id[lt]', params.idLt.toString());
    if (params.idLe != null) __params = __params.set('id[le]', params.idLe.toString());
    if (params.idGt != null) __params = __params.set('id[gt]', params.idGt.toString());
    if (params.idGe != null) __params = __params.set('id[ge]', params.idGe.toString());
    if (params.id != null) __params = __params.set('id', params.id.toString());
    if (params.empresaVigenciaHastaNe != null) __params = __params.set('empresa-vigenciaHasta[ne]', params.empresaVigenciaHastaNe.toString());
    if (params.empresaVigenciaHastaLt != null) __params = __params.set('empresa-vigenciaHasta[lt]', params.empresaVigenciaHastaLt.toString());
    if (params.empresaVigenciaHastaLe != null) __params = __params.set('empresa-vigenciaHasta[le]', params.empresaVigenciaHastaLe.toString());
    if (params.empresaVigenciaHastaGt != null) __params = __params.set('empresa-vigenciaHasta[gt]', params.empresaVigenciaHastaGt.toString());
    if (params.empresaVigenciaHastaGe != null) __params = __params.set('empresa-vigenciaHasta[ge]', params.empresaVigenciaHastaGe.toString());
    if (params.empresaVigenciaHasta != null) __params = __params.set('empresa-vigenciaHasta', params.empresaVigenciaHasta.toString());
    if (params.empresaVigenciaDesdeNe != null) __params = __params.set('empresa-vigenciaDesde[ne]', params.empresaVigenciaDesdeNe.toString());
    if (params.empresaVigenciaDesdeLt != null) __params = __params.set('empresa-vigenciaDesde[lt]', params.empresaVigenciaDesdeLt.toString());
    if (params.empresaVigenciaDesdeLe != null) __params = __params.set('empresa-vigenciaDesde[le]', params.empresaVigenciaDesdeLe.toString());
    if (params.empresaVigenciaDesdeGt != null) __params = __params.set('empresa-vigenciaDesde[gt]', params.empresaVigenciaDesdeGt.toString());
    if (params.empresaVigenciaDesdeGe != null) __params = __params.set('empresa-vigenciaDesde[ge]', params.empresaVigenciaDesdeGe.toString());
    if (params.empresaVigenciaDesde != null) __params = __params.set('empresa-vigenciaDesde', params.empresaVigenciaDesde.toString());
    if (params.empresaTipoSociedadNe != null) __params = __params.set('empresa-tipoSociedad[ne]', params.empresaTipoSociedadNe.toString());
    if (params.empresaTipoSociedadLt != null) __params = __params.set('empresa-tipoSociedad[lt]', params.empresaTipoSociedadLt.toString());
    if (params.empresaTipoSociedadLe != null) __params = __params.set('empresa-tipoSociedad[le]', params.empresaTipoSociedadLe.toString());
    if (params.empresaTipoSociedadGt != null) __params = __params.set('empresa-tipoSociedad[gt]', params.empresaTipoSociedadGt.toString());
    if (params.empresaTipoSociedadGe != null) __params = __params.set('empresa-tipoSociedad[ge]', params.empresaTipoSociedadGe.toString());
    if (params.empresaTipoSociedad != null) __params = __params.set('empresa-tipoSociedad', params.empresaTipoSociedad.toString());
    if (params.empresaTipoDocumentoIdNe != null) __params = __params.set('empresa-tipoDocumento-id[ne]', params.empresaTipoDocumentoIdNe.toString());
    if (params.empresaTipoDocumentoIdLt != null) __params = __params.set('empresa-tipoDocumento-id[lt]', params.empresaTipoDocumentoIdLt.toString());
    if (params.empresaTipoDocumentoIdLe != null) __params = __params.set('empresa-tipoDocumento-id[le]', params.empresaTipoDocumentoIdLe.toString());
    if (params.empresaTipoDocumentoIdGt != null) __params = __params.set('empresa-tipoDocumento-id[gt]', params.empresaTipoDocumentoIdGt.toString());
    if (params.empresaTipoDocumentoIdGe != null) __params = __params.set('empresa-tipoDocumento-id[ge]', params.empresaTipoDocumentoIdGe.toString());
    if (params.empresaTipoDocumentoId != null) __params = __params.set('empresa-tipoDocumento-id', params.empresaTipoDocumentoId.toString());
    if (params.empresaTipoDocumentoDescripcionNe != null) __params = __params.set('empresa-tipoDocumento-descripcion[ne]', params.empresaTipoDocumentoDescripcionNe.toString());
    if (params.empresaTipoDocumentoDescripcionLike != null) __params = __params.set('empresa-tipoDocumento-descripcion[like]', params.empresaTipoDocumentoDescripcionLike.toString());
    if (params.empresaTipoDocumentoDescripcion != null) __params = __params.set('empresa-tipoDocumento-descripcion', params.empresaTipoDocumentoDescripcion.toString());
    if (params.empresaTipoDocumentoAbrevNe != null) __params = __params.set('empresa-tipoDocumento-abrev[ne]', params.empresaTipoDocumentoAbrevNe.toString());
    if (params.empresaTipoDocumentoAbrevLike != null) __params = __params.set('empresa-tipoDocumento-abrev[like]', params.empresaTipoDocumentoAbrevLike.toString());
    if (params.empresaTipoDocumentoAbrev != null) __params = __params.set('empresa-tipoDocumento-abrev', params.empresaTipoDocumentoAbrev.toString());
    if (params.empresaTextoHash != null) __params = __params.set('empresa-textoHash', params.empresaTextoHash.toString());
    if (params.empresaRazonSocialNe != null) __params = __params.set('empresa-razonSocial[ne]', params.empresaRazonSocialNe.toString());
    if (params.empresaRazonSocialLike != null) __params = __params.set('empresa-razonSocial[like]', params.empresaRazonSocialLike.toString());
    if (params.empresaRazonSocial != null) __params = __params.set('empresa-razonSocial', params.empresaRazonSocial.toString());
    if (params.empresaPautNe != null) __params = __params.set('empresa-paut[ne]', params.empresaPautNe.toString());
    if (params.empresaPautLike != null) __params = __params.set('empresa-paut[like]', params.empresaPautLike.toString());
    if (params.empresaPaut != null) __params = __params.set('empresa-paut', params.empresaPaut.toString());
    if (params.empresaObservacionNe != null) __params = __params.set('empresa-observacion[ne]', params.empresaObservacionNe.toString());
    if (params.empresaObservacionLike != null) __params = __params.set('empresa-observacion[like]', params.empresaObservacionLike.toString());
    if (params.empresaObservacion != null) __params = __params.set('empresa-observacion', params.empresaObservacion.toString());
    if (params.empresaNroDocumentoNe != null) __params = __params.set('empresa-nroDocumento[ne]', params.empresaNroDocumentoNe.toString());
    if (params.empresaNroDocumentoLike != null) __params = __params.set('empresa-nroDocumento[like]', params.empresaNroDocumentoLike.toString());
    if (params.empresaNroDocumento != null) __params = __params.set('empresa-nroDocumento', params.empresaNroDocumento.toString());
    if (params.empresaNombreFantasiaNe != null) __params = __params.set('empresa-nombreFantasia[ne]', params.empresaNombreFantasiaNe.toString());
    if (params.empresaNombreFantasiaLike != null) __params = __params.set('empresa-nombreFantasia[like]', params.empresaNombreFantasiaLike.toString());
    if (params.empresaNombreFantasia != null) __params = __params.set('empresa-nombreFantasia', params.empresaNombreFantasia.toString());
    if (params.empresaIdNe != null) __params = __params.set('empresa-id[ne]', params.empresaIdNe.toString());
    if (params.empresaIdLt != null) __params = __params.set('empresa-id[lt]', params.empresaIdLt.toString());
    if (params.empresaIdLe != null) __params = __params.set('empresa-id[le]', params.empresaIdLe.toString());
    if (params.empresaIdGt != null) __params = __params.set('empresa-id[gt]', params.empresaIdGt.toString());
    if (params.empresaIdGe != null) __params = __params.set('empresa-id[ge]', params.empresaIdGe.toString());
    if (params.empresaId != null) __params = __params.set('empresa-id', params.empresaId.toString());
    if (params.empresaEstadoEmpresaNe != null) __params = __params.set('empresa-estadoEmpresa[ne]', params.empresaEstadoEmpresaNe.toString());
    if (params.empresaEstadoEmpresaLt != null) __params = __params.set('empresa-estadoEmpresa[lt]', params.empresaEstadoEmpresaLt.toString());
    if (params.empresaEstadoEmpresaLe != null) __params = __params.set('empresa-estadoEmpresa[le]', params.empresaEstadoEmpresaLe.toString());
    if (params.empresaEstadoEmpresaGt != null) __params = __params.set('empresa-estadoEmpresa[gt]', params.empresaEstadoEmpresaGt.toString());
    if (params.empresaEstadoEmpresaGe != null) __params = __params.set('empresa-estadoEmpresa[ge]', params.empresaEstadoEmpresaGe.toString());
    if (params.empresaEstadoEmpresa != null) __params = __params.set('empresa-estadoEmpresa', params.empresaEstadoEmpresa.toString());
    if (params.empresaEsPersonaFisica != null) __params = __params.set('empresa-esPersonaFisica', params.empresaEsPersonaFisica.toString());
    if (params.empresaEmailNe != null) __params = __params.set('empresa-email[ne]', params.empresaEmailNe.toString());
    if (params.empresaEmailLike != null) __params = __params.set('empresa-email[like]', params.empresaEmailLike.toString());
    if (params.empresaEmail != null) __params = __params.set('empresa-email', params.empresaEmail.toString());
    if (params.empresaCuitEmpresaAnteriorNe != null) __params = __params.set('empresa-cuitEmpresaAnterior[ne]', params.empresaCuitEmpresaAnteriorNe.toString());
    if (params.empresaCuitEmpresaAnteriorLike != null) __params = __params.set('empresa-cuitEmpresaAnterior[like]', params.empresaCuitEmpresaAnteriorLike.toString());
    if (params.empresaCuitEmpresaAnterior != null) __params = __params.set('empresa-cuitEmpresaAnterior', params.empresaCuitEmpresaAnterior.toString());
    if (params.displayNameNe != null) __params = __params.set('displayName[ne]', params.displayNameNe.toString());
    if (params.displayNameLike != null) __params = __params.set('displayName[like]', params.displayNameLike.toString());
    if (params.displayName != null) __params = __params.set('displayName', params.displayName.toString());
    if (params.countyNe != null) __params = __params.set('county[ne]', params.countyNe.toString());
    if (params.countyLike != null) __params = __params.set('county[like]', params.countyLike.toString());
    if (params.county != null) __params = __params.set('county', params.county.toString());
    if (params.cityTownNe != null) __params = __params.set('cityTown[ne]', params.cityTownNe.toString());
    if (params.cityTownLike != null) __params = __params.set('cityTown[like]', params.cityTownLike.toString());
    if (params.cityTown != null) __params = __params.set('cityTown', params.cityTown.toString());
    if (params.aprobada != null) __params = __params.set('aprobada', params.aprobada.toString());
    if (params.activa != null) __params = __params.set('activa', params.activa.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/paradas`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ParadaAPIResponsePaginacion>;
      })
    );
  }
  /**
   * Obtiene una lista de paradas paginadas
   * @param params The `ParadaService.GetApiV2ParadasParams` containing the following parameters:
   *
   * - `state[ne]`:
   *
   * - `state[like]`:
   *
   * - `stateDistrict[ne]`:
   *
   * - `stateDistrict[like]`:
   *
   * - `stateDistrict`:
   *
   * - `state`:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `nombre[ne]`:
   *
   * - `nombre[like]`:
   *
   * - `nombreCompleto[ne]`:
   *
   * - `nombreCompleto[like]`:
   *
   * - `nombreCompleto`:
   *
   * - `nombre`:
   *
   * - `longitud[ne]`:
   *
   * - `longitud[lt]`:
   *
   * - `longitud[le]`:
   *
   * - `longitud[gt]`:
   *
   * - `longitud[ge]`:
   *
   * - `longitud`:
   *
   * - `localidad-updatedAt[ne]`:
   *
   * - `localidad-updatedAt[lt]`:
   *
   * - `localidad-updatedAt[le]`:
   *
   * - `localidad-updatedAt[gt]`:
   *
   * - `localidad-updatedAt[ge]`:
   *
   * - `localidad-updatedAt`:
   *
   * - `localidad-provincia-pais-id[ne]`:
   *
   * - `localidad-provincia-pais-id[lt]`:
   *
   * - `localidad-provincia-pais-id[le]`:
   *
   * - `localidad-provincia-pais-id[gt]`:
   *
   * - `localidad-provincia-pais-id[ge]`:
   *
   * - `localidad-provincia-pais-id`:
   *
   * - `localidad-provincia-pais-descripcion[ne]`:
   *
   * - `localidad-provincia-pais-descripcion[like]`:
   *
   * - `localidad-provincia-pais-descripcion`:
   *
   * - `localidad-provincia-pais-codigo[ne]`:
   *
   * - `localidad-provincia-pais-codigo[like]`:
   *
   * - `localidad-provincia-pais-codigo`:
   *
   * - `localidad-provincia-pais-activo`:
   *
   * - `localidad-provincia-pais-abrev[ne]`:
   *
   * - `localidad-provincia-pais-abrev[like]`:
   *
   * - `localidad-provincia-pais-abrev`:
   *
   * - `localidad-provincia-id[ne]`:
   *
   * - `localidad-provincia-id[lt]`:
   *
   * - `localidad-provincia-id[le]`:
   *
   * - `localidad-provincia-id[gt]`:
   *
   * - `localidad-provincia-id[ge]`:
   *
   * - `localidad-provincia-id`:
   *
   * - `localidad-provincia-descripcion[ne]`:
   *
   * - `localidad-provincia-descripcion[like]`:
   *
   * - `localidad-provincia-descripcion`:
   *
   * - `localidad-provincia-codigo[ne]`:
   *
   * - `localidad-provincia-codigo[like]`:
   *
   * - `localidad-provincia-codigoINDEC[ne]`:
   *
   * - `localidad-provincia-codigoINDEC[like]`:
   *
   * - `localidad-provincia-codigoINDEC`:
   *
   * - `localidad-provincia-codigo`:
   *
   * - `localidad-provincia-activo`:
   *
   * - `localidad-parada`:
   *
   * - `localidad-lon[ne]`:
   *
   * - `localidad-lon[lt]`:
   *
   * - `localidad-lon[le]`:
   *
   * - `localidad-lon[gt]`:
   *
   * - `localidad-lon[ge]`:
   *
   * - `localidad-lon`:
   *
   * - `localidad-lat[ne]`:
   *
   * - `localidad-lat[lt]`:
   *
   * - `localidad-lat[le]`:
   *
   * - `localidad-lat[gt]`:
   *
   * - `localidad-lat[ge]`:
   *
   * - `localidad-lat`:
   *
   * - `localidad-id[ne]`:
   *
   * - `localidad-id[lt]`:
   *
   * - `localidad-id[le]`:
   *
   * - `localidad-id[gt]`:
   *
   * - `localidad-id[ge]`:
   *
   * - `localidad-id`:
   *
   * - `localidad-fuente[ne]`:
   *
   * - `localidad-fuente[like]`:
   *
   * - `localidad-fuente`:
   *
   * - `localidad-descripcion[ne]`:
   *
   * - `localidad-descripcion[like]`:
   *
   * - `localidad-descripcion`:
   *
   * - `localidad-createdAt[ne]`:
   *
   * - `localidad-createdAt[lt]`:
   *
   * - `localidad-createdAt[le]`:
   *
   * - `localidad-createdAt[gt]`:
   *
   * - `localidad-createdAt[ge]`:
   *
   * - `localidad-createdAt`:
   *
   * - `localidad-codigoINDEC[ne]`:
   *
   * - `localidad-codigoINDEC[like]`:
   *
   * - `localidad-codigoINDEC`:
   *
   * - `localidad-activo`:
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `latitud[ne]`:
   *
   * - `latitud[lt]`:
   *
   * - `latitud[le]`:
   *
   * - `latitud[gt]`:
   *
   * - `latitud[ge]`:
   *
   * - `latitud`:
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `empresa-vigenciaHasta[ne]`:
   *
   * - `empresa-vigenciaHasta[lt]`:
   *
   * - `empresa-vigenciaHasta[le]`:
   *
   * - `empresa-vigenciaHasta[gt]`:
   *
   * - `empresa-vigenciaHasta[ge]`:
   *
   * - `empresa-vigenciaHasta`:
   *
   * - `empresa-vigenciaDesde[ne]`:
   *
   * - `empresa-vigenciaDesde[lt]`:
   *
   * - `empresa-vigenciaDesde[le]`:
   *
   * - `empresa-vigenciaDesde[gt]`:
   *
   * - `empresa-vigenciaDesde[ge]`:
   *
   * - `empresa-vigenciaDesde`:
   *
   * - `empresa-tipoSociedad[ne]`:
   *
   * - `empresa-tipoSociedad[lt]`:
   *
   * - `empresa-tipoSociedad[le]`:
   *
   * - `empresa-tipoSociedad[gt]`:
   *
   * - `empresa-tipoSociedad[ge]`:
   *
   * - `empresa-tipoSociedad`:
   *
   * - `empresa-tipoDocumento-id[ne]`:
   *
   * - `empresa-tipoDocumento-id[lt]`:
   *
   * - `empresa-tipoDocumento-id[le]`:
   *
   * - `empresa-tipoDocumento-id[gt]`:
   *
   * - `empresa-tipoDocumento-id[ge]`:
   *
   * - `empresa-tipoDocumento-id`:
   *
   * - `empresa-tipoDocumento-descripcion[ne]`:
   *
   * - `empresa-tipoDocumento-descripcion[like]`:
   *
   * - `empresa-tipoDocumento-descripcion`:
   *
   * - `empresa-tipoDocumento-abrev[ne]`:
   *
   * - `empresa-tipoDocumento-abrev[like]`:
   *
   * - `empresa-tipoDocumento-abrev`:
   *
   * - `empresa-textoHash`:
   *
   * - `empresa-razonSocial[ne]`:
   *
   * - `empresa-razonSocial[like]`:
   *
   * - `empresa-razonSocial`:
   *
   * - `empresa-paut[ne]`:
   *
   * - `empresa-paut[like]`:
   *
   * - `empresa-paut`:
   *
   * - `empresa-observacion[ne]`:
   *
   * - `empresa-observacion[like]`:
   *
   * - `empresa-observacion`:
   *
   * - `empresa-nroDocumento[ne]`:
   *
   * - `empresa-nroDocumento[like]`:
   *
   * - `empresa-nroDocumento`:
   *
   * - `empresa-nombreFantasia[ne]`:
   *
   * - `empresa-nombreFantasia[like]`:
   *
   * - `empresa-nombreFantasia`:
   *
   * - `empresa-id[ne]`:
   *
   * - `empresa-id[lt]`:
   *
   * - `empresa-id[le]`:
   *
   * - `empresa-id[gt]`:
   *
   * - `empresa-id[ge]`:
   *
   * - `empresa-id`:
   *
   * - `empresa-estadoEmpresa[ne]`:
   *
   * - `empresa-estadoEmpresa[lt]`:
   *
   * - `empresa-estadoEmpresa[le]`:
   *
   * - `empresa-estadoEmpresa[gt]`:
   *
   * - `empresa-estadoEmpresa[ge]`:
   *
   * - `empresa-estadoEmpresa`:
   *
   * - `empresa-esPersonaFisica`:
   *
   * - `empresa-email[ne]`:
   *
   * - `empresa-email[like]`:
   *
   * - `empresa-email`:
   *
   * - `empresa-cuitEmpresaAnterior[ne]`:
   *
   * - `empresa-cuitEmpresaAnterior[like]`:
   *
   * - `empresa-cuitEmpresaAnterior`:
   *
   * - `displayName[ne]`:
   *
   * - `displayName[like]`:
   *
   * - `displayName`:
   *
   * - `county[ne]`:
   *
   * - `county[like]`:
   *
   * - `county`:
   *
   * - `cityTown[ne]`:
   *
   * - `cityTown[like]`:
   *
   * - `cityTown`:
   *
   * - `aprobada`:
   *
   * - `activa`:
   *
   * @return Respuesta exitosa.
   */
  getApiV2Paradas(params: ParadaService.GetApiV2ParadasParams): __Observable<ParadaAPIResponsePaginacion> {
    return this.getApiV2ParadasResponse(params).pipe(
      __map(_r => _r.body as ParadaAPIResponsePaginacion)
    );
  }

  /**
   * Registra una parada
   * @param data Datos de Parada a registrar
   * @return Parada creada correctamente.
   */
  postApiV2ParadasResponse(data: ParadaAPISaveRequest): __Observable<__StrictHttpResponse<ParadaAPIResponseData>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = data;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v2/paradas`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ParadaAPIResponseData>;
      })
    );
  }
  /**
   * Registra una parada
   * @param data Datos de Parada a registrar
   * @return Parada creada correctamente.
   */
  postApiV2Paradas(data: ParadaAPISaveRequest): __Observable<ParadaAPIResponseData> {
    return this.postApiV2ParadasResponse(data).pipe(
      __map(_r => _r.body as ParadaAPIResponseData)
    );
  }

  /**
   * Obtiene una parada por ID
   * @param id Id de parada a buscar
   * @return Respuesta exitosa.
   */
  getApiV2ParadasIdResponse(id: number): __Observable<__StrictHttpResponse<ParadaAPIResponseData>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/paradas/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ParadaAPIResponseData>;
      })
    );
  }
  /**
   * Obtiene una parada por ID
   * @param id Id de parada a buscar
   * @return Respuesta exitosa.
   */
  getApiV2ParadasId(id: number): __Observable<ParadaAPIResponseData> {
    return this.getApiV2ParadasIdResponse(id).pipe(
      __map(_r => _r.body as ParadaAPIResponseData)
    );
  }

  /**
   * Actualiza una parada por ID
   * @param params The `ParadaService.PutApiV2ParadasIdParams` containing the following parameters:
   *
   * - `id`: Id de Parada a actualizar
   *
   * - `data`: Datos de Parada a actualizar
   *
   * @return Parada actualizada correctamente.
   */
  putApiV2ParadasIdResponse(params: ParadaService.PutApiV2ParadasIdParams): __Observable<__StrictHttpResponse<ParadaAPIResponseData>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.data;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v2/paradas/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ParadaAPIResponseData>;
      })
    );
  }
  /**
   * Actualiza una parada por ID
   * @param params The `ParadaService.PutApiV2ParadasIdParams` containing the following parameters:
   *
   * - `id`: Id de Parada a actualizar
   *
   * - `data`: Datos de Parada a actualizar
   *
   * @return Parada actualizada correctamente.
   */
  putApiV2ParadasId(params: ParadaService.PutApiV2ParadasIdParams): __Observable<ParadaAPIResponseData> {
    return this.putApiV2ParadasIdResponse(params).pipe(
      __map(_r => _r.body as ParadaAPIResponseData)
    );
  }

  /**
   * Da de baja una parada que no es parte de un cuadro horario por ID
   * @param id Id de Parada a dar de baja
   * @return Parada dada de baja correctamente.
   */
  deleteApiV2ParadasIdResponse(id: number): __Observable<__StrictHttpResponse<ParadaAPIResponseData>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v2/paradas/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ParadaAPIResponseData>;
      })
    );
  }
  /**
   * Da de baja una parada que no es parte de un cuadro horario por ID
   * @param id Id de Parada a dar de baja
   * @return Parada dada de baja correctamente.
   */
  deleteApiV2ParadasId(id: number): __Observable<ParadaAPIResponseData> {
    return this.deleteApiV2ParadasIdResponse(id).pipe(
      __map(_r => _r.body as ParadaAPIResponseData)
    );
  }
}

module ParadaService {

  /**
   * Parameters for getApiV2Paradas
   */
  export interface GetApiV2ParadasParams {
    stateNe?: string;
    stateLike?: string;
    stateDistrictNe?: string;
    stateDistrictLike?: string;
    stateDistrict?: string;
    state?: string;

    /**
     * Campo por el cual se ordena.
     */
    sort?: string;

    /**
     * Método de ordenación, ascendente (ASC) o descendente (DESC).
     */
    order?: 'ASC' | 'DESC';

    /**
     * Cantidad de registros que deben omitirse en la consulta.
     */
    offset?: number;
    nombreNe?: string;
    nombreLike?: string;
    nombreCompletoNe?: string;
    nombreCompletoLike?: string;
    nombreCompleto?: string;
    nombre?: string;
    longitudNe?: number;
    longitudLt?: number;
    longitudLe?: number;
    longitudGt?: number;
    longitudGe?: number;
    longitud?: number;
    localidadUpdatedAtNe?: any;
    localidadUpdatedAtLt?: any;
    localidadUpdatedAtLe?: any;
    localidadUpdatedAtGt?: any;
    localidadUpdatedAtGe?: any;
    localidadUpdatedAt?: any;
    localidadProvinciaPaisIdNe?: any;
    localidadProvinciaPaisIdLt?: any;
    localidadProvinciaPaisIdLe?: any;
    localidadProvinciaPaisIdGt?: any;
    localidadProvinciaPaisIdGe?: any;
    localidadProvinciaPaisId?: any;
    localidadProvinciaPaisDescripcionNe?: any;
    localidadProvinciaPaisDescripcionLike?: any;
    localidadProvinciaPaisDescripcion?: any;
    localidadProvinciaPaisCodigoNe?: any;
    localidadProvinciaPaisCodigoLike?: any;
    localidadProvinciaPaisCodigo?: any;
    localidadProvinciaPaisActivo?: any;
    localidadProvinciaPaisAbrevNe?: any;
    localidadProvinciaPaisAbrevLike?: any;
    localidadProvinciaPaisAbrev?: any;
    localidadProvinciaIdNe?: number;
    localidadProvinciaIdLt?: number;
    localidadProvinciaIdLe?: number;
    localidadProvinciaIdGt?: number;
    localidadProvinciaIdGe?: number;
    localidadProvinciaId?: number;
    localidadProvinciaDescripcionNe?: string;
    localidadProvinciaDescripcionLike?: string;
    localidadProvinciaDescripcion?: string;
    localidadProvinciaCodigoNe?: string;
    localidadProvinciaCodigoLike?: string;
    localidadProvinciaCodigoINDECNe?: string;
    localidadProvinciaCodigoINDECLike?: string;
    localidadProvinciaCodigoINDEC?: string;
    localidadProvinciaCodigo?: string;
    localidadProvinciaActivo?: boolean;
    localidadParada?: any;
    localidadLonNe?: number;
    localidadLonLt?: number;
    localidadLonLe?: number;
    localidadLonGt?: number;
    localidadLonGe?: number;
    localidadLon?: number;
    localidadLatNe?: number;
    localidadLatLt?: number;
    localidadLatLe?: number;
    localidadLatGt?: number;
    localidadLatGe?: number;
    localidadLat?: number;
    localidadIdNe?: number;
    localidadIdLt?: number;
    localidadIdLe?: number;
    localidadIdGt?: number;
    localidadIdGe?: number;
    localidadId?: number;
    localidadFuenteNe?: string;
    localidadFuenteLike?: string;
    localidadFuente?: string;
    localidadDescripcionNe?: string;
    localidadDescripcionLike?: string;
    localidadDescripcion?: string;
    localidadCreatedAtNe?: any;
    localidadCreatedAtLt?: any;
    localidadCreatedAtLe?: any;
    localidadCreatedAtGt?: any;
    localidadCreatedAtGe?: any;
    localidadCreatedAt?: any;
    localidadCodigoINDECNe?: string;
    localidadCodigoINDECLike?: string;
    localidadCodigoINDEC?: string;
    localidadActivo?: boolean;

    /**
     * Cantidad máxima de registros que debe regresar la consulta.
     */
    limit?: number;
    latitudNe?: number;
    latitudLt?: number;
    latitudLe?: number;
    latitudGt?: number;
    latitudGe?: number;
    latitud?: number;
    idNe?: number;
    idLt?: number;
    idLe?: number;
    idGt?: number;
    idGe?: number;
    id?: number;
    empresaVigenciaHastaNe?: any;
    empresaVigenciaHastaLt?: any;
    empresaVigenciaHastaLe?: any;
    empresaVigenciaHastaGt?: any;
    empresaVigenciaHastaGe?: any;
    empresaVigenciaHasta?: any;
    empresaVigenciaDesdeNe?: any;
    empresaVigenciaDesdeLt?: any;
    empresaVigenciaDesdeLe?: any;
    empresaVigenciaDesdeGt?: any;
    empresaVigenciaDesdeGe?: any;
    empresaVigenciaDesde?: any;
    empresaTipoSociedadNe?: number;
    empresaTipoSociedadLt?: number;
    empresaTipoSociedadLe?: number;
    empresaTipoSociedadGt?: number;
    empresaTipoSociedadGe?: number;
    empresaTipoSociedad?: number;
    empresaTipoDocumentoIdNe?: number;
    empresaTipoDocumentoIdLt?: number;
    empresaTipoDocumentoIdLe?: number;
    empresaTipoDocumentoIdGt?: number;
    empresaTipoDocumentoIdGe?: number;
    empresaTipoDocumentoId?: number;
    empresaTipoDocumentoDescripcionNe?: string;
    empresaTipoDocumentoDescripcionLike?: string;
    empresaTipoDocumentoDescripcion?: string;
    empresaTipoDocumentoAbrevNe?: any;
    empresaTipoDocumentoAbrevLike?: any;
    empresaTipoDocumentoAbrev?: any;
    empresaTextoHash?: string;
    empresaRazonSocialNe?: string;
    empresaRazonSocialLike?: string;
    empresaRazonSocial?: string;
    empresaPautNe?: string;
    empresaPautLike?: string;
    empresaPaut?: string;
    empresaObservacionNe?: string;
    empresaObservacionLike?: string;
    empresaObservacion?: string;
    empresaNroDocumentoNe?: string;
    empresaNroDocumentoLike?: string;
    empresaNroDocumento?: string;
    empresaNombreFantasiaNe?: string;
    empresaNombreFantasiaLike?: string;
    empresaNombreFantasia?: string;
    empresaIdNe?: number;
    empresaIdLt?: number;
    empresaIdLe?: number;
    empresaIdGt?: number;
    empresaIdGe?: number;
    empresaId?: number;
    empresaEstadoEmpresaNe?: number;
    empresaEstadoEmpresaLt?: number;
    empresaEstadoEmpresaLe?: number;
    empresaEstadoEmpresaGt?: number;
    empresaEstadoEmpresaGe?: number;
    empresaEstadoEmpresa?: number;
    empresaEsPersonaFisica?: boolean;
    empresaEmailNe?: string;
    empresaEmailLike?: string;
    empresaEmail?: string;
    empresaCuitEmpresaAnteriorNe?: string;
    empresaCuitEmpresaAnteriorLike?: string;
    empresaCuitEmpresaAnterior?: string;
    displayNameNe?: string;
    displayNameLike?: string;
    displayName?: string;
    countyNe?: string;
    countyLike?: string;
    county?: string;
    cityTownNe?: string;
    cityTownLike?: string;
    cityTown?: string;
    aprobada?: boolean;
    activa?: boolean;
  }

  /**
   * Parameters for putApiV2ParadasId
   */
  export interface PutApiV2ParadasIdParams {

    /**
     * Id de Parada a actualizar
     */
    id: number;

    /**
     * Datos de Parada a actualizar
     */
    data: ParadaAPIUpdateRequest;
  }
}

export { ParadaService }
