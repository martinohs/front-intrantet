import { Pais } from './pais.interface';

export interface Provincia {
	codigoINDEC: string;
	pais: Pais;
	descripcion: string;
	codigo: string;
	descripcionPaisProvincia: string;
	activo: boolean;
}
