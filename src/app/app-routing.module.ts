import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

// Guards
import { AuthGuardService } from './auth/guard/auth.guard';

// Componentes
import { HomeComponent } from './components/home/home.component';
import { NoticiasComponent } from './modules/modulo-noticias/noticias/noticias.component';
import { PosteoComponent } from './modules/posteo/posteo.component';
import { PostCreadorComponent } from './modules/post-creador/post-creador.component';
import { PostResolver } from './modules/posteo/post.resolver';

const routes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		redirectTo: 'home',
	},
	{
		path: 'home',
		component: HomeComponent,
	},
	{
		path: 'modulo-ejemplo',
		loadChildren: () => import('./modules/modulo-ejemplo/modulo-ejemplo.module').then((m) => m.ModuloEjemploModule),
		canActivate: [],
	},
  {
    path: 'noticias/:page',
    component: NoticiasComponent,
  },
  {
    path: 'post/:id',
    component: PosteoComponent,
    resolve: {
      post: PostResolver
    }
  },
  { path: 'crearpost',
    component: PostCreadorComponent,
  },


  //   //REDIRECCIONA EN CASO DE PATH ERRONEO, SIEMPRE DEBE IR ULTIMO!!!!!!
  //  {
	//  	path: '**',
	// 	redirectTo: 'home',
  //  },


];

@NgModule({
	imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
	exports: [RouterModule],
	providers: [AuthGuardService],
})
export class AppRoutingModule {}
