/* tslint:disable */
import { ParadaAPI } from './parada-api';
export interface ParadaAPIResponseData {
  actions?: string;
  data?: ParadaAPI;
  result?: string;
  status?: number;
  userMessage?: string;
}
