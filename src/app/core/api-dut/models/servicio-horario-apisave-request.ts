/* tslint:disable */
export interface ServicioHorarioAPISaveRequest {
  cantidadChoferes?: number;
  diasViajeExtra?: number;
  horaLlegada?: string;
  horaSalida?: string;
  km?: number;
  nroOrden?: number;
  parada?: number;
  pasoFronterizo?: boolean;
  posta?: boolean;
  tiempoEspera?: string;
  tiempoViaje?: string;
  velocidadPromedio?: number;
}
