export const environment = {
	production: true,
	entorno: 'PRODUCTION',
	api_dut_url: 'https://api.cnrt.gob.ar/dut',
	api_seop_url: 'https://api.cnrt.gob.ar/seop/doc',
	georef_api_url: 'https://api.cnrt.gob.ar/georef/api/v1',
	log_level: 2,
	googleAnalyticsKey: false,
	redirect_uri: 'https://dut.cnrt.gob.ar',
	current_idp: 'hydra',
	authConfig: {
		issuer: 'https://api.cnrt.gob.ar/oidc/',
		clientId: 'dut',
		customUserinfoEndpoint: 'https://api.cnrt.gob.ar/sia/api/v1/usuarios/actual',
	},
};
