import { Component, OnInit } from '@angular/core';
import { environment } from '@env/environment';
import { AuthService } from '@auth/services/auth.service';
import { Observable, of } from 'rxjs';
import { version } from '../../package.json';
import { CoreFacade } from '@core/facades/core.facade.ts';
import { ApiDutConfiguration } from '@core/api-dut/api-dut-configuration';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
	noEsProduccion: Observable<boolean>;
	environment;
	version: string;

	constructor(
		private authService: AuthService,
		private coreFacade: CoreFacade,
		private apiDutConfig: ApiDutConfiguration
	) {}

	ngOnInit() {
		this.authService.initialize();
		this.appendGaTrackingCode();
		this.setEntorno();
		this.environment = environment;
		this.version = version;
    this.setApisConfig();
    this.coreFacade.obtenerTodasLasNoticias(); //Obtengo los posteos al inicio (sino la primera vez que se accede a noticias no los muestra)
	}

	private appendGaTrackingCode() {
		if (environment.googleAnalyticsKey) {
			try {
				const script = document.createElement('script');
				script.innerHTML =
					`
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
                  ga('create', '` +
					environment.googleAnalyticsKey +
					`', 'auto');
                `;
				document.head.appendChild(script);
			} catch (ex) {
				console.error('Error appending google analytics');
				console.error(ex);
			}
		}
	}

	get cargando$() {
		return this.coreFacade.cargando$;
	}

	setEntorno() {
		environment.entorno === 'PRODUCCION' ? (this.noEsProduccion = of(false)) : (this.noEsProduccion = of(true));
	}

	setApisConfig(): void {
		this.apiDutConfig.rootUrl = environment.api_dut_url;
  }


}
