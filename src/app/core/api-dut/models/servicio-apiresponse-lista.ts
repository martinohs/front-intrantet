/* tslint:disable */
import { ServicioAPI2 } from './servicio-api2';
export interface ServicioAPIResponseLista {
  actions?: string;
  data?: Array<ServicioAPI2>;
  result?: string;
  status?: number;
  userMessage?: string;
}
