/* tslint:disable */
import { ValidationError } from './validation-error';

/**
 * Descripción del error
 */
export interface ResponseError {
  developerMessage?: string;
  result?: string;
  status?: number;
  userMessage?: string;
  validationErrors?: Array<ValidationError>;
}
