/* tslint:disable */
export interface DetalleModalidadTraficoAPISaveRequest {
  createdBy?: string;
  ofreceServicio?: boolean;
  paisDestino?: number;
  paisOrigen?: number;
  paradaDestino?: number;
  paradaOrigen?: number;
  provinciaDestino?: number;
  provinciaOrigen?: number;
}
