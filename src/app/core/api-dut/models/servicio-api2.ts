/* tslint:disable */
import { ServicioModalidadTraficoAPI2 } from './servicio-modalidad-trafico-api2';
import { DiaServicioAPI } from './dia-servicio-api';
import { ServicioHorarioAPI } from './servicio-horario-api';
import { OperadorAPI } from './operador-api';
import { RecorridoAPI } from './recorrido-api';
import { RecorridoSentidoAPI } from './recorrido-sentido-api';
import { TramiteAPI } from './tramite-api';
export interface ServicioAPI2 {
  aprobado?: boolean;
  cuadroModalidadTrafico?: ServicioModalidadTraficoAPI2;
  dias?: Array<DiaServicioAPI>;
  horaLlegada?: any;
  horaSalida?: any;
  horarios?: Array<ServicioHorarioAPI>;
  id?: number;
  operador?: OperadorAPI;
  recorrido?: RecorridoAPI;
  recorridoSentido?: RecorridoSentidoAPI;
  servicioReemplazado?: number;
  tramite?: TramiteAPI;
  velocidadPromedioTotal?: number;
  vigenciaDesde?: any;
  vigenciaHasta?: any;
}
