/* tslint:disable */
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApiDutConfiguration, ApiDutConfigurationInterface } from './api-dut-configuration';

import { DiaService } from './services/dia.service';
import { DiaServicioService } from './services/dia-servicio.service';
import { ParadaService } from './services/parada.service';
import { ServicioService } from './services/servicio.service';
import { ServicioModalidadTraficoService } from './services/servicio-modalidad-trafico.service';
import { TramiteService } from './services/tramite.service';
import { ApiService } from './services/api.service';

/**
 * Provider for all ApiDut services, plus ApiDutConfiguration
 */
@NgModule({
  imports: [
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ApiDutConfiguration,
    DiaService,
    DiaServicioService,
    ParadaService,
    ServicioService,
    ServicioModalidadTraficoService,
    TramiteService,
    ApiService
  ],
})
export class ApiDutModule {
  static forRoot(customParams: ApiDutConfigurationInterface): ModuleWithProviders<ApiDutModule> {
    return {
      ngModule: ApiDutModule,
      providers: [
        {
          provide: ApiDutConfiguration,
          useValue: {rootUrl: customParams.rootUrl}
        }
      ]
    }
  }
}
