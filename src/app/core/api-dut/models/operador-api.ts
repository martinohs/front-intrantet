/* tslint:disable */
import { ClaseModalidadAPI } from './clase-modalidad-api';
import { EmpresaAPI } from './empresa-api';
import { LocalidadAPI } from './localidad-api';
import { ProvinciaAPI } from './provincia-api';
export interface OperadorAPI {
  aplicaGasoil?: boolean;
  aplicaSubsidioCorredorAereo?: boolean;
  aplicaSubsidioDiscapacitados?: boolean;
  claseModalidad?: ClaseModalidadAPI;

  /**
   * Destino del Recorrido
   */
  destino?: string;
  empresa?: EmpresaAPI;
  empresaContratante?: string;
  empresaContratanteCuit?: string;
  id?: number;
  linea?: string;
  localidadDestino?: LocalidadAPI;
  localidadOrigen?: LocalidadAPI;

  /**
   * Origen del Recorrido
   */
  origen?: string;
  provincia?: ProvinciaAPI;
  rul?: string;
  vigenciaDesde?: any;
  vigenciaHasta?: any;
  vigente?: boolean;
}
