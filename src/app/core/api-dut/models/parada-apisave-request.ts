/* tslint:disable */
export interface ParadaAPISaveRequest {
  cityTown?: string;
  county?: string;
  displayName?: string;
  empresa?: number;
  latitud?: number;
  localidad?: number;
  longitud?: number;
  nombre?: string;
  state?: string;
  stateDistrict?: string;
}
