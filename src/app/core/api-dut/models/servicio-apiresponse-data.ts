/* tslint:disable */
import { ServicioAPI2 } from './servicio-api2';
export interface ServicioAPIResponseData {
  actions?: string;
  data?: ServicioAPI2;
  result?: string;
  status?: number;
  userMessage?: string;
}
