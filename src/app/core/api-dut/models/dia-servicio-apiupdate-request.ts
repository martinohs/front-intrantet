/* tslint:disable */
export interface DiaServicioAPIUpdateRequest {
  activo?: boolean;
  categoriaServicioPlantaBaja?: number;
  categoriaServicioPrimerPiso?: number;
  dia?: number;
}
