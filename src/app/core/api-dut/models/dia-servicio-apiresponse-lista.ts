/* tslint:disable */
import { DiaServicioAPI } from './dia-servicio-api';
export interface DiaServicioAPIResponseLista {
  actions?: string;
  data?: Array<DiaServicioAPI>;
  result?: string;
  status?: number;
  userMessage?: string;
}
