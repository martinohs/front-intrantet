import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';

@Component({
	selector: 'app-volver-button',
	templateUrl: './volver-button.component.html',
	//styleUrls: ['./footer.component.scss']
})
export class VolverButtonComponent implements OnInit {
	@Input() public fontSize: string;

	constructor(private location: Location) {}

	ngOnInit(): void {}

	goBack() {
		this.location.back();
	}
}
