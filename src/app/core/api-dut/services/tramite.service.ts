/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiDutConfiguration as __Configuration } from '../api-dut-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { TramiteAPIResponsePaginacion } from '../models/tramite-apiresponse-paginacion';
import { TramiteAPIResponseData } from '../models/tramite-apiresponse-data';
import { TramiteAPISaveRequest } from '../models/tramite-apisave-request';
@Injectable({
  providedIn: 'root',
})
class TramiteService extends __BaseService {
  static readonly findAllTramitesPath = '/api/v2/tramites';
  static readonly saveTramitePath = '/api/v2/tramites';
  static readonly findTramitePath = '/api/v2/tramites/{id}';
  static readonly compareMovimientosByTramitePath = '/api/v2/tramites/{id}/movimientos/compare/{idOrigen}...{idDestino}';
  static readonly cambiarEstadoTramitePath = '/api/v2/tramites/{id}/estados/{codigoEstado}';
  static readonly getSubtipoTramitesPath = '/api/v2/tramites/subtipoTramite';
  static readonly getEstadosTramitePath = '/api/v2/tramites/estadosTramite';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Obtiene una lista de tramites paginados
   * @param params The `TramiteService.FindAllTramitesParams` containing the following parameters:
   *
   * - `usuario[ne]`:
   *
   * - `usuario[like]`:
   *
   * - `usuario`:
   *
   * - `tipoTramite-id[ne]`:
   *
   * - `tipoTramite-id[lt]`:
   *
   * - `tipoTramite-id[le]`:
   *
   * - `tipoTramite-id[gt]`:
   *
   * - `tipoTramite-id[ge]`:
   *
   * - `tipoTramite-id`:
   *
   * - `tipoTramite-descripcion[ne]`:
   *
   * - `tipoTramite-descripcion[like]`:
   *
   * - `tipoTramite-descripcion`:
   *
   * - `tipoTramite-activo`:
   *
   * - `subtipoTramite-id[ne]`:
   *
   * - `subtipoTramite-id[lt]`:
   *
   * - `subtipoTramite-id[le]`:
   *
   * - `subtipoTramite-id[gt]`:
   *
   * - `subtipoTramite-id[ge]`:
   *
   * - `subtipoTramite-id`:
   *
   * - `subtipoTramite-descripcion[ne]`:
   *
   * - `subtipoTramite-descripcion[like]`:
   *
   * - `subtipoTramite-descripcion`:
   *
   * - `subtipoTramite-activo`:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `nroBoletaPago[ne]`:
   *
   * - `nroBoletaPago[lt]`:
   *
   * - `nroBoletaPago[le]`:
   *
   * - `nroBoletaPago[gt]`:
   *
   * - `nroBoletaPago[ge]`:
   *
   * - `nroBoletaPago`:
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `estadoTramite-id[ne]`:
   *
   * - `estadoTramite-id[lt]`:
   *
   * - `estadoTramite-id[le]`:
   *
   * - `estadoTramite-id[gt]`:
   *
   * - `estadoTramite-id[ge]`:
   *
   * - `estadoTramite-id`:
   *
   * - `estadoTramite-descripcion[ne]`:
   *
   * - `estadoTramite-descripcion[like]`:
   *
   * - `estadoTramite-descripcion`:
   *
   * - `estadoTramite-activo`:
   *
   * - `estadoTramite-abrev[ne]`:
   *
   * - `estadoTramite-abrev[like]`:
   *
   * - `estadoTramite-abrev`:
   *
   * - `empresa-vigenciaHasta[ne]`:
   *
   * - `empresa-vigenciaHasta[lt]`:
   *
   * - `empresa-vigenciaHasta[le]`:
   *
   * - `empresa-vigenciaHasta[gt]`:
   *
   * - `empresa-vigenciaHasta[ge]`:
   *
   * - `empresa-vigenciaHasta`:
   *
   * - `empresa-vigenciaDesde[ne]`:
   *
   * - `empresa-vigenciaDesde[lt]`:
   *
   * - `empresa-vigenciaDesde[le]`:
   *
   * - `empresa-vigenciaDesde[gt]`:
   *
   * - `empresa-vigenciaDesde[ge]`:
   *
   * - `empresa-vigenciaDesde`:
   *
   * - `empresa-tipoSociedad[ne]`:
   *
   * - `empresa-tipoSociedad[lt]`:
   *
   * - `empresa-tipoSociedad[le]`:
   *
   * - `empresa-tipoSociedad[gt]`:
   *
   * - `empresa-tipoSociedad[ge]`:
   *
   * - `empresa-tipoSociedad`:
   *
   * - `empresa-tipoDocumento-id[ne]`:
   *
   * - `empresa-tipoDocumento-id[lt]`:
   *
   * - `empresa-tipoDocumento-id[le]`:
   *
   * - `empresa-tipoDocumento-id[gt]`:
   *
   * - `empresa-tipoDocumento-id[ge]`:
   *
   * - `empresa-tipoDocumento-id`:
   *
   * - `empresa-tipoDocumento-descripcion[ne]`:
   *
   * - `empresa-tipoDocumento-descripcion[like]`:
   *
   * - `empresa-tipoDocumento-descripcion`:
   *
   * - `empresa-tipoDocumento-abrev[ne]`:
   *
   * - `empresa-tipoDocumento-abrev[like]`:
   *
   * - `empresa-tipoDocumento-abrev`:
   *
   * - `empresa-textoHash`:
   *
   * - `empresa-razonSocial[ne]`:
   *
   * - `empresa-razonSocial[like]`:
   *
   * - `empresa-razonSocial`:
   *
   * - `empresa-paut[ne]`:
   *
   * - `empresa-paut[like]`:
   *
   * - `empresa-paut`:
   *
   * - `empresa-observacion[ne]`:
   *
   * - `empresa-observacion[like]`:
   *
   * - `empresa-observacion`:
   *
   * - `empresa-nroDocumento[ne]`:
   *
   * - `empresa-nroDocumento[like]`:
   *
   * - `empresa-nroDocumento`:
   *
   * - `empresa-nombreFantasia[ne]`:
   *
   * - `empresa-nombreFantasia[like]`:
   *
   * - `empresa-nombreFantasia`:
   *
   * - `empresa-id[ne]`:
   *
   * - `empresa-id[lt]`:
   *
   * - `empresa-id[le]`:
   *
   * - `empresa-id[gt]`:
   *
   * - `empresa-id[ge]`:
   *
   * - `empresa-id`:
   *
   * - `empresa-estadoEmpresa[ne]`:
   *
   * - `empresa-estadoEmpresa[lt]`:
   *
   * - `empresa-estadoEmpresa[le]`:
   *
   * - `empresa-estadoEmpresa[gt]`:
   *
   * - `empresa-estadoEmpresa[ge]`:
   *
   * - `empresa-estadoEmpresa`:
   *
   * - `empresa-esPersonaFisica`:
   *
   * - `empresa-email[ne]`:
   *
   * - `empresa-email[like]`:
   *
   * - `empresa-email`:
   *
   * - `empresa-cuitEmpresaAnterior[ne]`:
   *
   * - `empresa-cuitEmpresaAnterior[like]`:
   *
   * - `empresa-cuitEmpresaAnterior`:
   *
   * @return Respuesta exitosa.
   */
  findAllTramitesResponse(params: TramiteService.FindAllTramitesParams): __Observable<__StrictHttpResponse<TramiteAPIResponsePaginacion>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.usuarioNe != null) __params = __params.set('usuario[ne]', params.usuarioNe.toString());
    if (params.usuarioLike != null) __params = __params.set('usuario[like]', params.usuarioLike.toString());
    if (params.usuario != null) __params = __params.set('usuario', params.usuario.toString());
    if (params.tipoTramiteIdNe != null) __params = __params.set('tipoTramite-id[ne]', params.tipoTramiteIdNe.toString());
    if (params.tipoTramiteIdLt != null) __params = __params.set('tipoTramite-id[lt]', params.tipoTramiteIdLt.toString());
    if (params.tipoTramiteIdLe != null) __params = __params.set('tipoTramite-id[le]', params.tipoTramiteIdLe.toString());
    if (params.tipoTramiteIdGt != null) __params = __params.set('tipoTramite-id[gt]', params.tipoTramiteIdGt.toString());
    if (params.tipoTramiteIdGe != null) __params = __params.set('tipoTramite-id[ge]', params.tipoTramiteIdGe.toString());
    if (params.tipoTramiteId != null) __params = __params.set('tipoTramite-id', params.tipoTramiteId.toString());
    if (params.tipoTramiteDescripcionNe != null) __params = __params.set('tipoTramite-descripcion[ne]', params.tipoTramiteDescripcionNe.toString());
    if (params.tipoTramiteDescripcionLike != null) __params = __params.set('tipoTramite-descripcion[like]', params.tipoTramiteDescripcionLike.toString());
    if (params.tipoTramiteDescripcion != null) __params = __params.set('tipoTramite-descripcion', params.tipoTramiteDescripcion.toString());
    if (params.tipoTramiteActivo != null) __params = __params.set('tipoTramite-activo', params.tipoTramiteActivo.toString());
    if (params.subtipoTramiteIdNe != null) __params = __params.set('subtipoTramite-id[ne]', params.subtipoTramiteIdNe.toString());
    if (params.subtipoTramiteIdLt != null) __params = __params.set('subtipoTramite-id[lt]', params.subtipoTramiteIdLt.toString());
    if (params.subtipoTramiteIdLe != null) __params = __params.set('subtipoTramite-id[le]', params.subtipoTramiteIdLe.toString());
    if (params.subtipoTramiteIdGt != null) __params = __params.set('subtipoTramite-id[gt]', params.subtipoTramiteIdGt.toString());
    if (params.subtipoTramiteIdGe != null) __params = __params.set('subtipoTramite-id[ge]', params.subtipoTramiteIdGe.toString());
    if (params.subtipoTramiteId != null) __params = __params.set('subtipoTramite-id', params.subtipoTramiteId.toString());
    if (params.subtipoTramiteDescripcionNe != null) __params = __params.set('subtipoTramite-descripcion[ne]', params.subtipoTramiteDescripcionNe.toString());
    if (params.subtipoTramiteDescripcionLike != null) __params = __params.set('subtipoTramite-descripcion[like]', params.subtipoTramiteDescripcionLike.toString());
    if (params.subtipoTramiteDescripcion != null) __params = __params.set('subtipoTramite-descripcion', params.subtipoTramiteDescripcion.toString());
    if (params.subtipoTramiteActivo != null) __params = __params.set('subtipoTramite-activo', params.subtipoTramiteActivo.toString());
    if (params.sort != null) __params = __params.set('sort', params.sort.toString());
    if (params.order != null) __params = __params.set('order', params.order.toString());
    if (params.offset != null) __params = __params.set('offset', params.offset.toString());
    if (params.nroBoletaPagoNe != null) __params = __params.set('nroBoletaPago[ne]', params.nroBoletaPagoNe.toString());
    if (params.nroBoletaPagoLt != null) __params = __params.set('nroBoletaPago[lt]', params.nroBoletaPagoLt.toString());
    if (params.nroBoletaPagoLe != null) __params = __params.set('nroBoletaPago[le]', params.nroBoletaPagoLe.toString());
    if (params.nroBoletaPagoGt != null) __params = __params.set('nroBoletaPago[gt]', params.nroBoletaPagoGt.toString());
    if (params.nroBoletaPagoGe != null) __params = __params.set('nroBoletaPago[ge]', params.nroBoletaPagoGe.toString());
    if (params.nroBoletaPago != null) __params = __params.set('nroBoletaPago', params.nroBoletaPago.toString());
    if (params.limit != null) __params = __params.set('limit', params.limit.toString());
    if (params.idNe != null) __params = __params.set('id[ne]', params.idNe.toString());
    if (params.idLt != null) __params = __params.set('id[lt]', params.idLt.toString());
    if (params.idLe != null) __params = __params.set('id[le]', params.idLe.toString());
    if (params.idGt != null) __params = __params.set('id[gt]', params.idGt.toString());
    if (params.idGe != null) __params = __params.set('id[ge]', params.idGe.toString());
    if (params.id != null) __params = __params.set('id', params.id.toString());
    if (params.estadoTramiteIdNe != null) __params = __params.set('estadoTramite-id[ne]', params.estadoTramiteIdNe.toString());
    if (params.estadoTramiteIdLt != null) __params = __params.set('estadoTramite-id[lt]', params.estadoTramiteIdLt.toString());
    if (params.estadoTramiteIdLe != null) __params = __params.set('estadoTramite-id[le]', params.estadoTramiteIdLe.toString());
    if (params.estadoTramiteIdGt != null) __params = __params.set('estadoTramite-id[gt]', params.estadoTramiteIdGt.toString());
    if (params.estadoTramiteIdGe != null) __params = __params.set('estadoTramite-id[ge]', params.estadoTramiteIdGe.toString());
    if (params.estadoTramiteId != null) __params = __params.set('estadoTramite-id', params.estadoTramiteId.toString());
    if (params.estadoTramiteDescripcionNe != null) __params = __params.set('estadoTramite-descripcion[ne]', params.estadoTramiteDescripcionNe.toString());
    if (params.estadoTramiteDescripcionLike != null) __params = __params.set('estadoTramite-descripcion[like]', params.estadoTramiteDescripcionLike.toString());
    if (params.estadoTramiteDescripcion != null) __params = __params.set('estadoTramite-descripcion', params.estadoTramiteDescripcion.toString());
    if (params.estadoTramiteActivo != null) __params = __params.set('estadoTramite-activo', params.estadoTramiteActivo.toString());
    if (params.estadoTramiteAbrevNe != null) __params = __params.set('estadoTramite-abrev[ne]', params.estadoTramiteAbrevNe.toString());
    if (params.estadoTramiteAbrevLike != null) __params = __params.set('estadoTramite-abrev[like]', params.estadoTramiteAbrevLike.toString());
    if (params.estadoTramiteAbrev != null) __params = __params.set('estadoTramite-abrev', params.estadoTramiteAbrev.toString());
    if (params.empresaVigenciaHastaNe != null) __params = __params.set('empresa-vigenciaHasta[ne]', params.empresaVigenciaHastaNe.toString());
    if (params.empresaVigenciaHastaLt != null) __params = __params.set('empresa-vigenciaHasta[lt]', params.empresaVigenciaHastaLt.toString());
    if (params.empresaVigenciaHastaLe != null) __params = __params.set('empresa-vigenciaHasta[le]', params.empresaVigenciaHastaLe.toString());
    if (params.empresaVigenciaHastaGt != null) __params = __params.set('empresa-vigenciaHasta[gt]', params.empresaVigenciaHastaGt.toString());
    if (params.empresaVigenciaHastaGe != null) __params = __params.set('empresa-vigenciaHasta[ge]', params.empresaVigenciaHastaGe.toString());
    if (params.empresaVigenciaHasta != null) __params = __params.set('empresa-vigenciaHasta', params.empresaVigenciaHasta.toString());
    if (params.empresaVigenciaDesdeNe != null) __params = __params.set('empresa-vigenciaDesde[ne]', params.empresaVigenciaDesdeNe.toString());
    if (params.empresaVigenciaDesdeLt != null) __params = __params.set('empresa-vigenciaDesde[lt]', params.empresaVigenciaDesdeLt.toString());
    if (params.empresaVigenciaDesdeLe != null) __params = __params.set('empresa-vigenciaDesde[le]', params.empresaVigenciaDesdeLe.toString());
    if (params.empresaVigenciaDesdeGt != null) __params = __params.set('empresa-vigenciaDesde[gt]', params.empresaVigenciaDesdeGt.toString());
    if (params.empresaVigenciaDesdeGe != null) __params = __params.set('empresa-vigenciaDesde[ge]', params.empresaVigenciaDesdeGe.toString());
    if (params.empresaVigenciaDesde != null) __params = __params.set('empresa-vigenciaDesde', params.empresaVigenciaDesde.toString());
    if (params.empresaTipoSociedadNe != null) __params = __params.set('empresa-tipoSociedad[ne]', params.empresaTipoSociedadNe.toString());
    if (params.empresaTipoSociedadLt != null) __params = __params.set('empresa-tipoSociedad[lt]', params.empresaTipoSociedadLt.toString());
    if (params.empresaTipoSociedadLe != null) __params = __params.set('empresa-tipoSociedad[le]', params.empresaTipoSociedadLe.toString());
    if (params.empresaTipoSociedadGt != null) __params = __params.set('empresa-tipoSociedad[gt]', params.empresaTipoSociedadGt.toString());
    if (params.empresaTipoSociedadGe != null) __params = __params.set('empresa-tipoSociedad[ge]', params.empresaTipoSociedadGe.toString());
    if (params.empresaTipoSociedad != null) __params = __params.set('empresa-tipoSociedad', params.empresaTipoSociedad.toString());
    if (params.empresaTipoDocumentoIdNe != null) __params = __params.set('empresa-tipoDocumento-id[ne]', params.empresaTipoDocumentoIdNe.toString());
    if (params.empresaTipoDocumentoIdLt != null) __params = __params.set('empresa-tipoDocumento-id[lt]', params.empresaTipoDocumentoIdLt.toString());
    if (params.empresaTipoDocumentoIdLe != null) __params = __params.set('empresa-tipoDocumento-id[le]', params.empresaTipoDocumentoIdLe.toString());
    if (params.empresaTipoDocumentoIdGt != null) __params = __params.set('empresa-tipoDocumento-id[gt]', params.empresaTipoDocumentoIdGt.toString());
    if (params.empresaTipoDocumentoIdGe != null) __params = __params.set('empresa-tipoDocumento-id[ge]', params.empresaTipoDocumentoIdGe.toString());
    if (params.empresaTipoDocumentoId != null) __params = __params.set('empresa-tipoDocumento-id', params.empresaTipoDocumentoId.toString());
    if (params.empresaTipoDocumentoDescripcionNe != null) __params = __params.set('empresa-tipoDocumento-descripcion[ne]', params.empresaTipoDocumentoDescripcionNe.toString());
    if (params.empresaTipoDocumentoDescripcionLike != null) __params = __params.set('empresa-tipoDocumento-descripcion[like]', params.empresaTipoDocumentoDescripcionLike.toString());
    if (params.empresaTipoDocumentoDescripcion != null) __params = __params.set('empresa-tipoDocumento-descripcion', params.empresaTipoDocumentoDescripcion.toString());
    if (params.empresaTipoDocumentoAbrevNe != null) __params = __params.set('empresa-tipoDocumento-abrev[ne]', params.empresaTipoDocumentoAbrevNe.toString());
    if (params.empresaTipoDocumentoAbrevLike != null) __params = __params.set('empresa-tipoDocumento-abrev[like]', params.empresaTipoDocumentoAbrevLike.toString());
    if (params.empresaTipoDocumentoAbrev != null) __params = __params.set('empresa-tipoDocumento-abrev', params.empresaTipoDocumentoAbrev.toString());
    if (params.empresaTextoHash != null) __params = __params.set('empresa-textoHash', params.empresaTextoHash.toString());
    if (params.empresaRazonSocialNe != null) __params = __params.set('empresa-razonSocial[ne]', params.empresaRazonSocialNe.toString());
    if (params.empresaRazonSocialLike != null) __params = __params.set('empresa-razonSocial[like]', params.empresaRazonSocialLike.toString());
    if (params.empresaRazonSocial != null) __params = __params.set('empresa-razonSocial', params.empresaRazonSocial.toString());
    if (params.empresaPautNe != null) __params = __params.set('empresa-paut[ne]', params.empresaPautNe.toString());
    if (params.empresaPautLike != null) __params = __params.set('empresa-paut[like]', params.empresaPautLike.toString());
    if (params.empresaPaut != null) __params = __params.set('empresa-paut', params.empresaPaut.toString());
    if (params.empresaObservacionNe != null) __params = __params.set('empresa-observacion[ne]', params.empresaObservacionNe.toString());
    if (params.empresaObservacionLike != null) __params = __params.set('empresa-observacion[like]', params.empresaObservacionLike.toString());
    if (params.empresaObservacion != null) __params = __params.set('empresa-observacion', params.empresaObservacion.toString());
    if (params.empresaNroDocumentoNe != null) __params = __params.set('empresa-nroDocumento[ne]', params.empresaNroDocumentoNe.toString());
    if (params.empresaNroDocumentoLike != null) __params = __params.set('empresa-nroDocumento[like]', params.empresaNroDocumentoLike.toString());
    if (params.empresaNroDocumento != null) __params = __params.set('empresa-nroDocumento', params.empresaNroDocumento.toString());
    if (params.empresaNombreFantasiaNe != null) __params = __params.set('empresa-nombreFantasia[ne]', params.empresaNombreFantasiaNe.toString());
    if (params.empresaNombreFantasiaLike != null) __params = __params.set('empresa-nombreFantasia[like]', params.empresaNombreFantasiaLike.toString());
    if (params.empresaNombreFantasia != null) __params = __params.set('empresa-nombreFantasia', params.empresaNombreFantasia.toString());
    if (params.empresaIdNe != null) __params = __params.set('empresa-id[ne]', params.empresaIdNe.toString());
    if (params.empresaIdLt != null) __params = __params.set('empresa-id[lt]', params.empresaIdLt.toString());
    if (params.empresaIdLe != null) __params = __params.set('empresa-id[le]', params.empresaIdLe.toString());
    if (params.empresaIdGt != null) __params = __params.set('empresa-id[gt]', params.empresaIdGt.toString());
    if (params.empresaIdGe != null) __params = __params.set('empresa-id[ge]', params.empresaIdGe.toString());
    if (params.empresaId != null) __params = __params.set('empresa-id', params.empresaId.toString());
    if (params.empresaEstadoEmpresaNe != null) __params = __params.set('empresa-estadoEmpresa[ne]', params.empresaEstadoEmpresaNe.toString());
    if (params.empresaEstadoEmpresaLt != null) __params = __params.set('empresa-estadoEmpresa[lt]', params.empresaEstadoEmpresaLt.toString());
    if (params.empresaEstadoEmpresaLe != null) __params = __params.set('empresa-estadoEmpresa[le]', params.empresaEstadoEmpresaLe.toString());
    if (params.empresaEstadoEmpresaGt != null) __params = __params.set('empresa-estadoEmpresa[gt]', params.empresaEstadoEmpresaGt.toString());
    if (params.empresaEstadoEmpresaGe != null) __params = __params.set('empresa-estadoEmpresa[ge]', params.empresaEstadoEmpresaGe.toString());
    if (params.empresaEstadoEmpresa != null) __params = __params.set('empresa-estadoEmpresa', params.empresaEstadoEmpresa.toString());
    if (params.empresaEsPersonaFisica != null) __params = __params.set('empresa-esPersonaFisica', params.empresaEsPersonaFisica.toString());
    if (params.empresaEmailNe != null) __params = __params.set('empresa-email[ne]', params.empresaEmailNe.toString());
    if (params.empresaEmailLike != null) __params = __params.set('empresa-email[like]', params.empresaEmailLike.toString());
    if (params.empresaEmail != null) __params = __params.set('empresa-email', params.empresaEmail.toString());
    if (params.empresaCuitEmpresaAnteriorNe != null) __params = __params.set('empresa-cuitEmpresaAnterior[ne]', params.empresaCuitEmpresaAnteriorNe.toString());
    if (params.empresaCuitEmpresaAnteriorLike != null) __params = __params.set('empresa-cuitEmpresaAnterior[like]', params.empresaCuitEmpresaAnteriorLike.toString());
    if (params.empresaCuitEmpresaAnterior != null) __params = __params.set('empresa-cuitEmpresaAnterior', params.empresaCuitEmpresaAnterior.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/tramites`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<TramiteAPIResponsePaginacion>;
      })
    );
  }
  /**
   * Obtiene una lista de tramites paginados
   * @param params The `TramiteService.FindAllTramitesParams` containing the following parameters:
   *
   * - `usuario[ne]`:
   *
   * - `usuario[like]`:
   *
   * - `usuario`:
   *
   * - `tipoTramite-id[ne]`:
   *
   * - `tipoTramite-id[lt]`:
   *
   * - `tipoTramite-id[le]`:
   *
   * - `tipoTramite-id[gt]`:
   *
   * - `tipoTramite-id[ge]`:
   *
   * - `tipoTramite-id`:
   *
   * - `tipoTramite-descripcion[ne]`:
   *
   * - `tipoTramite-descripcion[like]`:
   *
   * - `tipoTramite-descripcion`:
   *
   * - `tipoTramite-activo`:
   *
   * - `subtipoTramite-id[ne]`:
   *
   * - `subtipoTramite-id[lt]`:
   *
   * - `subtipoTramite-id[le]`:
   *
   * - `subtipoTramite-id[gt]`:
   *
   * - `subtipoTramite-id[ge]`:
   *
   * - `subtipoTramite-id`:
   *
   * - `subtipoTramite-descripcion[ne]`:
   *
   * - `subtipoTramite-descripcion[like]`:
   *
   * - `subtipoTramite-descripcion`:
   *
   * - `subtipoTramite-activo`:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `nroBoletaPago[ne]`:
   *
   * - `nroBoletaPago[lt]`:
   *
   * - `nroBoletaPago[le]`:
   *
   * - `nroBoletaPago[gt]`:
   *
   * - `nroBoletaPago[ge]`:
   *
   * - `nroBoletaPago`:
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `estadoTramite-id[ne]`:
   *
   * - `estadoTramite-id[lt]`:
   *
   * - `estadoTramite-id[le]`:
   *
   * - `estadoTramite-id[gt]`:
   *
   * - `estadoTramite-id[ge]`:
   *
   * - `estadoTramite-id`:
   *
   * - `estadoTramite-descripcion[ne]`:
   *
   * - `estadoTramite-descripcion[like]`:
   *
   * - `estadoTramite-descripcion`:
   *
   * - `estadoTramite-activo`:
   *
   * - `estadoTramite-abrev[ne]`:
   *
   * - `estadoTramite-abrev[like]`:
   *
   * - `estadoTramite-abrev`:
   *
   * - `empresa-vigenciaHasta[ne]`:
   *
   * - `empresa-vigenciaHasta[lt]`:
   *
   * - `empresa-vigenciaHasta[le]`:
   *
   * - `empresa-vigenciaHasta[gt]`:
   *
   * - `empresa-vigenciaHasta[ge]`:
   *
   * - `empresa-vigenciaHasta`:
   *
   * - `empresa-vigenciaDesde[ne]`:
   *
   * - `empresa-vigenciaDesde[lt]`:
   *
   * - `empresa-vigenciaDesde[le]`:
   *
   * - `empresa-vigenciaDesde[gt]`:
   *
   * - `empresa-vigenciaDesde[ge]`:
   *
   * - `empresa-vigenciaDesde`:
   *
   * - `empresa-tipoSociedad[ne]`:
   *
   * - `empresa-tipoSociedad[lt]`:
   *
   * - `empresa-tipoSociedad[le]`:
   *
   * - `empresa-tipoSociedad[gt]`:
   *
   * - `empresa-tipoSociedad[ge]`:
   *
   * - `empresa-tipoSociedad`:
   *
   * - `empresa-tipoDocumento-id[ne]`:
   *
   * - `empresa-tipoDocumento-id[lt]`:
   *
   * - `empresa-tipoDocumento-id[le]`:
   *
   * - `empresa-tipoDocumento-id[gt]`:
   *
   * - `empresa-tipoDocumento-id[ge]`:
   *
   * - `empresa-tipoDocumento-id`:
   *
   * - `empresa-tipoDocumento-descripcion[ne]`:
   *
   * - `empresa-tipoDocumento-descripcion[like]`:
   *
   * - `empresa-tipoDocumento-descripcion`:
   *
   * - `empresa-tipoDocumento-abrev[ne]`:
   *
   * - `empresa-tipoDocumento-abrev[like]`:
   *
   * - `empresa-tipoDocumento-abrev`:
   *
   * - `empresa-textoHash`:
   *
   * - `empresa-razonSocial[ne]`:
   *
   * - `empresa-razonSocial[like]`:
   *
   * - `empresa-razonSocial`:
   *
   * - `empresa-paut[ne]`:
   *
   * - `empresa-paut[like]`:
   *
   * - `empresa-paut`:
   *
   * - `empresa-observacion[ne]`:
   *
   * - `empresa-observacion[like]`:
   *
   * - `empresa-observacion`:
   *
   * - `empresa-nroDocumento[ne]`:
   *
   * - `empresa-nroDocumento[like]`:
   *
   * - `empresa-nroDocumento`:
   *
   * - `empresa-nombreFantasia[ne]`:
   *
   * - `empresa-nombreFantasia[like]`:
   *
   * - `empresa-nombreFantasia`:
   *
   * - `empresa-id[ne]`:
   *
   * - `empresa-id[lt]`:
   *
   * - `empresa-id[le]`:
   *
   * - `empresa-id[gt]`:
   *
   * - `empresa-id[ge]`:
   *
   * - `empresa-id`:
   *
   * - `empresa-estadoEmpresa[ne]`:
   *
   * - `empresa-estadoEmpresa[lt]`:
   *
   * - `empresa-estadoEmpresa[le]`:
   *
   * - `empresa-estadoEmpresa[gt]`:
   *
   * - `empresa-estadoEmpresa[ge]`:
   *
   * - `empresa-estadoEmpresa`:
   *
   * - `empresa-esPersonaFisica`:
   *
   * - `empresa-email[ne]`:
   *
   * - `empresa-email[like]`:
   *
   * - `empresa-email`:
   *
   * - `empresa-cuitEmpresaAnterior[ne]`:
   *
   * - `empresa-cuitEmpresaAnterior[like]`:
   *
   * - `empresa-cuitEmpresaAnterior`:
   *
   * @return Respuesta exitosa.
   */
  findAllTramites(params: TramiteService.FindAllTramitesParams): __Observable<TramiteAPIResponsePaginacion> {
    return this.findAllTramitesResponse(params).pipe(
      __map(_r => _r.body as TramiteAPIResponsePaginacion)
    );
  }

  /**
   * Crea un trámite
   * @param data Datos del trámite a registrar
   * @return Trámite creado correctamente.
   */
  saveTramiteResponse(data: TramiteAPISaveRequest): __Observable<__StrictHttpResponse<TramiteAPIResponseData>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = data;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v2/tramites`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<TramiteAPIResponseData>;
      })
    );
  }
  /**
   * Crea un trámite
   * @param data Datos del trámite a registrar
   * @return Trámite creado correctamente.
   */
  saveTramite(data: TramiteAPISaveRequest): __Observable<TramiteAPIResponseData> {
    return this.saveTramiteResponse(data).pipe(
      __map(_r => _r.body as TramiteAPIResponseData)
    );
  }

  /**
   * Obtiene un tramite por ID
   * @param id Id de Trámite a buscar
   * @return Respuesta exitosa.
   */
  findTramiteResponse(id: number): __Observable<__StrictHttpResponse<TramiteAPIResponseData>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/tramites/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<TramiteAPIResponseData>;
      })
    );
  }
  /**
   * Obtiene un tramite por ID
   * @param id Id de Trámite a buscar
   * @return Respuesta exitosa.
   */
  findTramite(id: number): __Observable<TramiteAPIResponseData> {
    return this.findTramiteResponse(id).pipe(
      __map(_r => _r.body as TramiteAPIResponseData)
    );
  }

  /**
   * Retorna la diferencia de datos entre dos movimientos de trámite por id
   * @param params The `TramiteService.CompareMovimientosByTramiteParams` containing the following parameters:
   *
   * - `idOrigen`: ID de movimiento origen
   *
   * - `idDestino`: ID de movimiento destino
   *
   * - `id`: ID de trámite a buscar
   *
   * @return Respuesta exitosa.
   */
  compareMovimientosByTramiteResponse(params: TramiteService.CompareMovimientosByTramiteParams): __Observable<__StrictHttpResponse<{data?: any, result?: string, status?: number, userMessage?: string, actions?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/tramites/${encodeURIComponent(params.id)}/movimientos/compare/${encodeURIComponent(params.idOrigen)}...${encodeURIComponent(params.idDestino)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{data?: any, result?: string, status?: number, userMessage?: string, actions?: string}>;
      })
    );
  }
  /**
   * Retorna la diferencia de datos entre dos movimientos de trámite por id
   * @param params The `TramiteService.CompareMovimientosByTramiteParams` containing the following parameters:
   *
   * - `idOrigen`: ID de movimiento origen
   *
   * - `idDestino`: ID de movimiento destino
   *
   * - `id`: ID de trámite a buscar
   *
   * @return Respuesta exitosa.
   */
  compareMovimientosByTramite(params: TramiteService.CompareMovimientosByTramiteParams): __Observable<{data?: any, result?: string, status?: number, userMessage?: string, actions?: string}> {
    return this.compareMovimientosByTramiteResponse(params).pipe(
      __map(_r => _r.body as {data?: any, result?: string, status?: number, userMessage?: string, actions?: string})
    );
  }

  /**
   * Permite cambiar el estado de un trámite
   * @param params The `TramiteService.CambiarEstadoTramiteParams` containing the following parameters:
   *
   * - `id`: ID de trámite a actualizar
   *
   * - `codigoEstado`: Código de Estado
   *
   * - `data`: Datos de Trámite
   *
   * @return Trámite actualizado correctamente.
   */
  cambiarEstadoTramiteResponse(params: TramiteService.CambiarEstadoTramiteParams): __Observable<__StrictHttpResponse<TramiteAPIResponseData>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    __body = params.data;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v2/tramites/${encodeURIComponent(params.id)}/estados/${encodeURIComponent(params.codigoEstado)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<TramiteAPIResponseData>;
      })
    );
  }
  /**
   * Permite cambiar el estado de un trámite
   * @param params The `TramiteService.CambiarEstadoTramiteParams` containing the following parameters:
   *
   * - `id`: ID de trámite a actualizar
   *
   * - `codigoEstado`: Código de Estado
   *
   * - `data`: Datos de Trámite
   *
   * @return Trámite actualizado correctamente.
   */
  cambiarEstadoTramite(params: TramiteService.CambiarEstadoTramiteParams): __Observable<TramiteAPIResponseData> {
    return this.cambiarEstadoTramiteResponse(params).pipe(
      __map(_r => _r.body as TramiteAPIResponseData)
    );
  }

  /**
   * Obtiene una lista de subtipos de tramite paginados
   * @param params The `TramiteService.GetSubtipoTramitesParams` containing the following parameters:
   *
   * - `tipoTramite-id[ne]`:
   *
   * - `tipoTramite-id[lt]`:
   *
   * - `tipoTramite-id[le]`:
   *
   * - `tipoTramite-id[gt]`:
   *
   * - `tipoTramite-id[ge]`:
   *
   * - `tipoTramite-id`:
   *
   * - `tipoTramite-descripcion[ne]`:
   *
   * - `tipoTramite-descripcion[like]`:
   *
   * - `tipoTramite-descripcion`:
   *
   * - `tipoTramite-activo`:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `descripcion[ne]`:
   *
   * - `descripcion[like]`:
   *
   * - `descripcion`:
   *
   * - `activo`:
   *
   * - `abrev[ne]`:
   *
   * - `abrev[like]`:
   *
   * - `abrev`:
   *
   * - `<atributo>[<criteria>]`: Lista de atributos a filtrar: ...&abrev[like]=CS%&tipoTramite[eq]=65
   *
   * @return Respuesta exitosa.
   */
  getSubtipoTramitesResponse(params: TramiteService.GetSubtipoTramitesParams): __Observable<__StrictHttpResponse<{result?: string, status?: number, data?: any, userMessage?: string, actions?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.tipoTramiteIdNe != null) __params = __params.set('tipoTramite-id[ne]', params.tipoTramiteIdNe.toString());
    if (params.tipoTramiteIdLt != null) __params = __params.set('tipoTramite-id[lt]', params.tipoTramiteIdLt.toString());
    if (params.tipoTramiteIdLe != null) __params = __params.set('tipoTramite-id[le]', params.tipoTramiteIdLe.toString());
    if (params.tipoTramiteIdGt != null) __params = __params.set('tipoTramite-id[gt]', params.tipoTramiteIdGt.toString());
    if (params.tipoTramiteIdGe != null) __params = __params.set('tipoTramite-id[ge]', params.tipoTramiteIdGe.toString());
    if (params.tipoTramiteId != null) __params = __params.set('tipoTramite-id', params.tipoTramiteId.toString());
    if (params.tipoTramiteDescripcionNe != null) __params = __params.set('tipoTramite-descripcion[ne]', params.tipoTramiteDescripcionNe.toString());
    if (params.tipoTramiteDescripcionLike != null) __params = __params.set('tipoTramite-descripcion[like]', params.tipoTramiteDescripcionLike.toString());
    if (params.tipoTramiteDescripcion != null) __params = __params.set('tipoTramite-descripcion', params.tipoTramiteDescripcion.toString());
    if (params.tipoTramiteActivo != null) __params = __params.set('tipoTramite-activo', params.tipoTramiteActivo.toString());
    if (params.sort != null) __params = __params.set('sort', params.sort.toString());
    if (params.order != null) __params = __params.set('order', params.order.toString());
    if (params.offset != null) __params = __params.set('offset', params.offset.toString());
    if (params.limit != null) __params = __params.set('limit', params.limit.toString());
    if (params.idNe != null) __params = __params.set('id[ne]', params.idNe.toString());
    if (params.idLt != null) __params = __params.set('id[lt]', params.idLt.toString());
    if (params.idLe != null) __params = __params.set('id[le]', params.idLe.toString());
    if (params.idGt != null) __params = __params.set('id[gt]', params.idGt.toString());
    if (params.idGe != null) __params = __params.set('id[ge]', params.idGe.toString());
    if (params.id != null) __params = __params.set('id', params.id.toString());
    if (params.descripcionNe != null) __params = __params.set('descripcion[ne]', params.descripcionNe.toString());
    if (params.descripcionLike != null) __params = __params.set('descripcion[like]', params.descripcionLike.toString());
    if (params.descripcion != null) __params = __params.set('descripcion', params.descripcion.toString());
    if (params.activo != null) __params = __params.set('activo', params.activo.toString());
    if (params.abrevNe != null) __params = __params.set('abrev[ne]', params.abrevNe.toString());
    if (params.abrevLike != null) __params = __params.set('abrev[like]', params.abrevLike.toString());
    if (params.abrev != null) __params = __params.set('abrev', params.abrev.toString());
    if (params.AtributoCriteria != null) __params = __params.set('&lt;atributo&gt;[&lt;criteria&gt;]', params.AtributoCriteria.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/tramites/subtipoTramite`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{result?: string, status?: number, data?: any, userMessage?: string, actions?: string}>;
      })
    );
  }
  /**
   * Obtiene una lista de subtipos de tramite paginados
   * @param params The `TramiteService.GetSubtipoTramitesParams` containing the following parameters:
   *
   * - `tipoTramite-id[ne]`:
   *
   * - `tipoTramite-id[lt]`:
   *
   * - `tipoTramite-id[le]`:
   *
   * - `tipoTramite-id[gt]`:
   *
   * - `tipoTramite-id[ge]`:
   *
   * - `tipoTramite-id`:
   *
   * - `tipoTramite-descripcion[ne]`:
   *
   * - `tipoTramite-descripcion[like]`:
   *
   * - `tipoTramite-descripcion`:
   *
   * - `tipoTramite-activo`:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `descripcion[ne]`:
   *
   * - `descripcion[like]`:
   *
   * - `descripcion`:
   *
   * - `activo`:
   *
   * - `abrev[ne]`:
   *
   * - `abrev[like]`:
   *
   * - `abrev`:
   *
   * - `<atributo>[<criteria>]`: Lista de atributos a filtrar: ...&abrev[like]=CS%&tipoTramite[eq]=65
   *
   * @return Respuesta exitosa.
   */
  getSubtipoTramites(params: TramiteService.GetSubtipoTramitesParams): __Observable<{result?: string, status?: number, data?: any, userMessage?: string, actions?: string}> {
    return this.getSubtipoTramitesResponse(params).pipe(
      __map(_r => _r.body as {result?: string, status?: number, data?: any, userMessage?: string, actions?: string})
    );
  }

  /**
   * Obtiene una lista de estados de tramite paginados
   * @param params The `TramiteService.GetEstadosTramiteParams` containing the following parameters:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `descripcion[ne]`:
   *
   * - `descripcion[like]`:
   *
   * - `descripcion`:
   *
   * - `activo`:
   *
   * - `abrev[ne]`:
   *
   * - `abrev[like]`:
   *
   * - `abrev`:
   *
   * - `<atributo>[<criteria>]`: Lista de atributos a filtrar: ...&abrev[like]=CS%&tipoTramite[eq]=65
   *
   * @return Respuesta exitosa.
   */
  getEstadosTramiteResponse(params: TramiteService.GetEstadosTramiteParams): __Observable<__StrictHttpResponse<{result?: string, status?: number, data?: any, userMessage?: string, actions?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.sort != null) __params = __params.set('sort', params.sort.toString());
    if (params.order != null) __params = __params.set('order', params.order.toString());
    if (params.offset != null) __params = __params.set('offset', params.offset.toString());
    if (params.limit != null) __params = __params.set('limit', params.limit.toString());
    if (params.idNe != null) __params = __params.set('id[ne]', params.idNe.toString());
    if (params.idLt != null) __params = __params.set('id[lt]', params.idLt.toString());
    if (params.idLe != null) __params = __params.set('id[le]', params.idLe.toString());
    if (params.idGt != null) __params = __params.set('id[gt]', params.idGt.toString());
    if (params.idGe != null) __params = __params.set('id[ge]', params.idGe.toString());
    if (params.id != null) __params = __params.set('id', params.id.toString());
    if (params.descripcionNe != null) __params = __params.set('descripcion[ne]', params.descripcionNe.toString());
    if (params.descripcionLike != null) __params = __params.set('descripcion[like]', params.descripcionLike.toString());
    if (params.descripcion != null) __params = __params.set('descripcion', params.descripcion.toString());
    if (params.activo != null) __params = __params.set('activo', params.activo.toString());
    if (params.abrevNe != null) __params = __params.set('abrev[ne]', params.abrevNe.toString());
    if (params.abrevLike != null) __params = __params.set('abrev[like]', params.abrevLike.toString());
    if (params.abrev != null) __params = __params.set('abrev', params.abrev.toString());
    if (params.AtributoCriteria != null) __params = __params.set('&lt;atributo&gt;[&lt;criteria&gt;]', params.AtributoCriteria.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v2/tramites/estadosTramite`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{result?: string, status?: number, data?: any, userMessage?: string, actions?: string}>;
      })
    );
  }
  /**
   * Obtiene una lista de estados de tramite paginados
   * @param params The `TramiteService.GetEstadosTramiteParams` containing the following parameters:
   *
   * - `sort`: Campo por el cual se ordena.
   *
   * - `order`: Método de ordenación, ascendente (ASC) o descendente (DESC).
   *
   * - `offset`: Cantidad de registros que deben omitirse en la consulta.
   *
   * - `limit`: Cantidad máxima de registros que debe regresar la consulta.
   *
   * - `id[ne]`:
   *
   * - `id[lt]`:
   *
   * - `id[le]`:
   *
   * - `id[gt]`:
   *
   * - `id[ge]`:
   *
   * - `id`:
   *
   * - `descripcion[ne]`:
   *
   * - `descripcion[like]`:
   *
   * - `descripcion`:
   *
   * - `activo`:
   *
   * - `abrev[ne]`:
   *
   * - `abrev[like]`:
   *
   * - `abrev`:
   *
   * - `<atributo>[<criteria>]`: Lista de atributos a filtrar: ...&abrev[like]=CS%&tipoTramite[eq]=65
   *
   * @return Respuesta exitosa.
   */
  getEstadosTramite(params: TramiteService.GetEstadosTramiteParams): __Observable<{result?: string, status?: number, data?: any, userMessage?: string, actions?: string}> {
    return this.getEstadosTramiteResponse(params).pipe(
      __map(_r => _r.body as {result?: string, status?: number, data?: any, userMessage?: string, actions?: string})
    );
  }
}

module TramiteService {

  /**
   * Parameters for findAllTramites
   */
  export interface FindAllTramitesParams {
    usuarioNe?: string;
    usuarioLike?: string;
    usuario?: string;
    tipoTramiteIdNe?: number;
    tipoTramiteIdLt?: number;
    tipoTramiteIdLe?: number;
    tipoTramiteIdGt?: number;
    tipoTramiteIdGe?: number;
    tipoTramiteId?: number;
    tipoTramiteDescripcionNe?: string;
    tipoTramiteDescripcionLike?: string;
    tipoTramiteDescripcion?: string;
    tipoTramiteActivo?: boolean;
    subtipoTramiteIdNe?: number;
    subtipoTramiteIdLt?: number;
    subtipoTramiteIdLe?: number;
    subtipoTramiteIdGt?: number;
    subtipoTramiteIdGe?: number;
    subtipoTramiteId?: number;
    subtipoTramiteDescripcionNe?: string;
    subtipoTramiteDescripcionLike?: string;
    subtipoTramiteDescripcion?: string;
    subtipoTramiteActivo?: boolean;

    /**
     * Campo por el cual se ordena.
     */
    sort?: string;

    /**
     * Método de ordenación, ascendente (ASC) o descendente (DESC).
     */
    order?: 'ASC' | 'DESC';

    /**
     * Cantidad de registros que deben omitirse en la consulta.
     */
    offset?: number;
    nroBoletaPagoNe?: number;
    nroBoletaPagoLt?: number;
    nroBoletaPagoLe?: number;
    nroBoletaPagoGt?: number;
    nroBoletaPagoGe?: number;
    nroBoletaPago?: number;

    /**
     * Cantidad máxima de registros que debe regresar la consulta.
     */
    limit?: number;
    idNe?: number;
    idLt?: number;
    idLe?: number;
    idGt?: number;
    idGe?: number;
    id?: number;
    estadoTramiteIdNe?: any;
    estadoTramiteIdLt?: any;
    estadoTramiteIdLe?: any;
    estadoTramiteIdGt?: any;
    estadoTramiteIdGe?: any;
    estadoTramiteId?: any;
    estadoTramiteDescripcionNe?: string;
    estadoTramiteDescripcionLike?: string;
    estadoTramiteDescripcion?: string;
    estadoTramiteActivo?: boolean;
    estadoTramiteAbrevNe?: string;
    estadoTramiteAbrevLike?: string;
    estadoTramiteAbrev?: string;
    empresaVigenciaHastaNe?: any;
    empresaVigenciaHastaLt?: any;
    empresaVigenciaHastaLe?: any;
    empresaVigenciaHastaGt?: any;
    empresaVigenciaHastaGe?: any;
    empresaVigenciaHasta?: any;
    empresaVigenciaDesdeNe?: any;
    empresaVigenciaDesdeLt?: any;
    empresaVigenciaDesdeLe?: any;
    empresaVigenciaDesdeGt?: any;
    empresaVigenciaDesdeGe?: any;
    empresaVigenciaDesde?: any;
    empresaTipoSociedadNe?: number;
    empresaTipoSociedadLt?: number;
    empresaTipoSociedadLe?: number;
    empresaTipoSociedadGt?: number;
    empresaTipoSociedadGe?: number;
    empresaTipoSociedad?: number;
    empresaTipoDocumentoIdNe?: number;
    empresaTipoDocumentoIdLt?: number;
    empresaTipoDocumentoIdLe?: number;
    empresaTipoDocumentoIdGt?: number;
    empresaTipoDocumentoIdGe?: number;
    empresaTipoDocumentoId?: number;
    empresaTipoDocumentoDescripcionNe?: string;
    empresaTipoDocumentoDescripcionLike?: string;
    empresaTipoDocumentoDescripcion?: string;
    empresaTipoDocumentoAbrevNe?: any;
    empresaTipoDocumentoAbrevLike?: any;
    empresaTipoDocumentoAbrev?: any;
    empresaTextoHash?: string;
    empresaRazonSocialNe?: string;
    empresaRazonSocialLike?: string;
    empresaRazonSocial?: string;
    empresaPautNe?: string;
    empresaPautLike?: string;
    empresaPaut?: string;
    empresaObservacionNe?: string;
    empresaObservacionLike?: string;
    empresaObservacion?: string;
    empresaNroDocumentoNe?: string;
    empresaNroDocumentoLike?: string;
    empresaNroDocumento?: string;
    empresaNombreFantasiaNe?: string;
    empresaNombreFantasiaLike?: string;
    empresaNombreFantasia?: string;
    empresaIdNe?: number;
    empresaIdLt?: number;
    empresaIdLe?: number;
    empresaIdGt?: number;
    empresaIdGe?: number;
    empresaId?: number;
    empresaEstadoEmpresaNe?: number;
    empresaEstadoEmpresaLt?: number;
    empresaEstadoEmpresaLe?: number;
    empresaEstadoEmpresaGt?: number;
    empresaEstadoEmpresaGe?: number;
    empresaEstadoEmpresa?: number;
    empresaEsPersonaFisica?: boolean;
    empresaEmailNe?: string;
    empresaEmailLike?: string;
    empresaEmail?: string;
    empresaCuitEmpresaAnteriorNe?: string;
    empresaCuitEmpresaAnteriorLike?: string;
    empresaCuitEmpresaAnterior?: string;
  }

  /**
   * Parameters for compareMovimientosByTramite
   */
  export interface CompareMovimientosByTramiteParams {

    /**
     * ID de movimiento origen
     */
    idOrigen: number;

    /**
     * ID de movimiento destino
     */
    idDestino: number;

    /**
     * ID de trámite a buscar
     */
    id: number;
  }

  /**
   * Parameters for cambiarEstadoTramite
   */
  export interface CambiarEstadoTramiteParams {

    /**
     * ID de trámite a actualizar
     */
    id: number;

    /**
     * Código de Estado
     */
    codigoEstado: string;

    /**
     * Datos de Trámite
     */
    data?: {observaciones?: string};
  }

  /**
   * Parameters for getSubtipoTramites
   */
  export interface GetSubtipoTramitesParams {
    tipoTramiteIdNe?: number;
    tipoTramiteIdLt?: number;
    tipoTramiteIdLe?: number;
    tipoTramiteIdGt?: number;
    tipoTramiteIdGe?: number;
    tipoTramiteId?: number;
    tipoTramiteDescripcionNe?: string;
    tipoTramiteDescripcionLike?: string;
    tipoTramiteDescripcion?: string;
    tipoTramiteActivo?: boolean;

    /**
     * Campo por el cual se ordena.
     */
    sort?: string;

    /**
     * Método de ordenación, ascendente (ASC) o descendente (DESC).
     */
    order?: 'ASC' | 'DESC';

    /**
     * Cantidad de registros que deben omitirse en la consulta.
     */
    offset?: number;

    /**
     * Cantidad máxima de registros que debe regresar la consulta.
     */
    limit?: number;
    idNe?: number;
    idLt?: number;
    idLe?: number;
    idGt?: number;
    idGe?: number;
    id?: number;
    descripcionNe?: string;
    descripcionLike?: string;
    descripcion?: string;
    activo?: boolean;
    abrevNe?: string;
    abrevLike?: string;
    abrev?: string;

    /**
     * Lista de atributos a filtrar: ...&abrev[like]=CS%&tipoTramite[eq]=65
     */
    AtributoCriteria?: string;
  }

  /**
   * Parameters for getEstadosTramite
   */
  export interface GetEstadosTramiteParams {

    /**
     * Campo por el cual se ordena.
     */
    sort?: string;

    /**
     * Método de ordenación, ascendente (ASC) o descendente (DESC).
     */
    order?: 'ASC' | 'DESC';

    /**
     * Cantidad de registros que deben omitirse en la consulta.
     */
    offset?: number;

    /**
     * Cantidad máxima de registros que debe regresar la consulta.
     */
    limit?: number;
    idNe?: any;
    idLt?: any;
    idLe?: any;
    idGt?: any;
    idGe?: any;
    id?: any;
    descripcionNe?: string;
    descripcionLike?: string;
    descripcion?: string;
    activo?: boolean;
    abrevNe?: string;
    abrevLike?: string;
    abrev?: string;

    /**
     * Lista de atributos a filtrar: ...&abrev[like]=CS%&tipoTramite[eq]=65
     */
    AtributoCriteria?: string;
  }
}

export { TramiteService }
