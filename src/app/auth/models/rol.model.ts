export class Rol {
	nombre: string;
	descripcion: string;

	constructor(data) {
		if (data) {
			this.nombre = data.nombre;
			this.descripcion = data.descripcion;
		}
	}
}
